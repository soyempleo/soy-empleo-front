angular
    .module('spa-SE2', ['ngRoute', 'ngResource', 'ui.bootstrap', 'LocalStorageModule', 'ngMaterial' , 'material.svgAssetsCache', 'angular-intro','angucomplete']);

/*----------------------------------------
 Validacion fuera de angularjs
 ------------------------------------------*/
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";
    tecla_especial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}
function valTlf(e) {
    var keynum = e.which;
    if ((keynum == 8) || (keynum == 46) || (keynum == 43) || (keynum == 45) || (keynum == 32)) {
        return true;
    }
    return /\d/.test(String.fromCharCode(keynum));
}

function validarNumero(e) {
    var keynum = e.which;
    if ((keynum == 8) || (keynum == 46) || (keynum == 43) || (keynum == 45) || (keynum == 32)) {
        return true;
    }
    return /\d/.test(String.fromCharCode(keynum));
}


function validarFecha(e) {
    var keynum = e.which;
    if ((keynum == 47)) {
        return true;
    }
    return /\d/.test(String.fromCharCode(keynum));
}

function stringClean(str) {
    for (var i = 0; i < str.length; i++) {
        if (str.charAt(i) == "á") str = str.replace(/á/, "a");
        if (str.charAt(i) == "é") str = str.replace(/é/, "e");
        if (str.charAt(i) == "í") str = str.replace(/í/, "i");
        if (str.charAt(i) == "ó") str = str.replace(/ó/, "o");
        if (str.charAt(i) == "ú") str = str.replace(/ú/, "u");
    }
    return str;
}

try{
    $(document).ready(function(){
        $('#carousel').carousel({ interval: 2000, cycle:true });
    });
}catch(e){
}
