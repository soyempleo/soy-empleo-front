/**
 * Created by WilderAlejandro on 23/11/2015.
 */
/* Esta es una lista de servicios put que se usan a lo largo del sistema */

/**
 *Este es un factory que retorna el $resourse necesario para actualizar persona
 **/
(function (){
    'use strict'

	function ServicesPut ($resource) {
	 	return $resource(ServerDestino+"persons/:id", {id: "@id"}, {update: {method: "PUT"}});
	}

	angular
	  .module('spa-SE2')
	  .factory('putPersons', ServicesPut);
})();

/*app.factory("putPersons", ['$resource', function($resource){
    return $resource(ServerDestino+"persons/:id", {id: "@id"}, {update: {method: "PUT"}});
}]);*/