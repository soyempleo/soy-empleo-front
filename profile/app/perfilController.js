/**
 * Created by WilderAlejandro on 25/11/2015.
 */
app.controller("perfilController", ["$scope", "$http", "$location", "localStorageService", "$rootScope",
    function ($scope, $http, $location, storage, $rootScope) {
        $rootScope.userId = storage.get("dataUser")._id;
        $rootScope.DatosPersonales = {};
        $rootScope.laboral = {};
        $scope.consultar = function () {
            if (storage.get("tokenUser")) {
                $http.get("http://52.23.162.248:8081/persons/"+$rootScope.userId).then(function (response) {
                    $rootScope.DatosPersonales = response.data.data;
                    $rootScope.laboral =  response.data.data.experiences[(response.data.data.experiences.length-1)];
                });
            }
            else {
                $location.path("/login");
            }
        };
        $scope.consultar();
    }]);