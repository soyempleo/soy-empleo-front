/**
 * Created by WilderAlejandro on 23/11/2015.
 */

/**
 * Directiva que se encarga de consumir un json listado y lo cambia por un input que tiene una funcion de autocompletado
 * en la vista se usa de la siguiente manera:
 *
 * <np-autocomplete name="nombreParaValidacion" model="nombreModelo" list="ListaQueConsume" idx="idDelElemento"
 placeholder="Texto de ayuda placeholder" tovalidate="ParaValidarElinputQueSeGenera">
 </np-autocomplete>
 *
 */
angular
    .module('spa-SE2')
    .directive("npAutocomplete", function () {
    return {
        restrict: 'E',
        scope: {
            list: '=',
            ngModel: '=',
            tovalidate: '=',
            validateext: '=',
            disabled: '=',
            placeholder: '@',
            class: '@',
            idx: '@',
            name: '@'
        },
        template: '<input type="text" name="{{name}}" ng-model="ngModel" id="{{idx}}"  class="{{class}}" ng-disabled="disabled" ' +
        'ng-class="{error: tovalidate.$invalid && tovalidate.$dirty,' +
        'success: tovalidate.$valid && tovalidate.$dirty && validateext}" placeholder="{{placeholder}}" required>',
        link: function (scope, element, attrs) {
            $(document).ready(function () {
                var loop = setInterval(function () {
                    var lista = [];
                    if (scope.list) {
                        jQuery.each(scope.list, function (i, val) {
                            lista.push(stringClean(val));
                        });
                        scope.$apply(function () {
                            $("#" + attrs.idx).autocomplete({
                                source: lista,
                                select: function (ev, ui) {
                                    scope.ngModel = ui.item.label;
                                }
                            });
                        });
                        clearInterval(loop);
                    }
                }, 200);
            });
        }
    }
});


angular
    .module('spa-SE2')
    .directive('npRedirect', function ($location) {
    return {
        link: function (scope, element, attrs) {
            element.on('click', function () {
                scope.$apply(function () {
                    $location.path(attrs.npRedirect);
                })
            });
        }
    }
});

/* *--------------------Directiva para subir Imagenes al servidor--------------------* */
/**
 * Created by AnthonyTorres on 12/01/2016.
 */

angular
    .module('spa-SE2')
    .directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            iElement.on("change", function (e) {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}]);


angular
    .module('spa-SE2')
    .directive("mAppLoading", function( $animate ) {
    return({
        link: link,
        restrict: "C"
    });
    function link( scope, element, attributes ) {
            // Due to the way AngularJS prevents animation during the bootstrap
            // of the application, we can't animate the top-level container; but,
            // since we added "ngAnimateChildren", we can animated the inner
            // container during this phase.
            // --
            // NOTE: Am using .eq(1) so that we don't animate the Style block.
            $animate.leave( element.children().eq( 1 ) ).then(
                function cleanupAfterAnimation() {
                    // Remove the root directive element.
                    element.remove();
                    // Clear the closed-over variable references.
                    scope = element = attributes = null;
                }
            );
        }
    }
);