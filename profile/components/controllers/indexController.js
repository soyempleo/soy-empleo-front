(function () {
    'use strict'
    function indexController($http, $location, storage, $rootScope, $mdDialog, $scope) {
        /**
         * Declaracion de variables de utilidad
         */
        var ind = this;
        ind.slideIsOpen = true;
        ind.urlSlideNav = $location.path();
        ind.userId = "";
        $rootScope.idUser = "";
        $rootScope.mensajeBienvenida = 0;
        $rootScope.userAvatar = "../assets/images/user_male.png";
        $rootScope.puntosColores = [];
	$rootScope.idPerson = storage.get("dataUser")._id;

        /**
         * Funcion que se encarga de obtener el porcetaje que se muestra en el perfil
         */
        $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
            if (!isNaN(response.data.percentage)) {
                ind.percentage = Math.round(response.data.percentage);
                $rootScope.porcentaje = ind.percentage;
            }
        });
        /**
         * Funcion que se dedica a cambiar el url pasada por parametro y tambien ayuda
         * a colocar la clase active en la barra lateral
         */
        ind.cambiarDir = function (url) {
            ind.urlSlideNav = "/" + url;
            $location.path("/" + url);
        }
        /**
         * Función que cambia el switch si esta abierto o no el slidenavar y eso
         * genera un cambio de clase
         */
        ind.changeSlideNav = function () {
            if (ind.slideIsOpen == true) {
                ind.slideIsOpen = false;
            } else {
                ind.slideIsOpen = true;
            }
        }
        /**
         * Función que se dedicar a cerrar la sesión y llevar al login
         */
        ind.salir = function () {
            ind.nombreUser = "";
            window.location.href = urlActual + "indin"
        }
        /**
         * En esta funcion se consulta si el usuario a iniciado sesión en caso de no ser así lo enviá al
         * home
         * @param  {[type]} !storage.get("tokenUser") [El token que indica el inicio de session de usuario]
         */
        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };
        /**
         * Cierre de session y manda al home
         */
        ind.cerrarSession = function () {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        }
        /**
         * Enviá al Deshboard
         * @return {[type]} [description]
         */
        ind.goDashboard = function () {
            window.location = "../#/dashboard";
        }
        /**
         * Función usada para quitar segundos nombres y apellidos y poder generar unicamente
         * primer nombre
         * @param  {[String]} cadena [Nombre completo]
         * @return {[String]}        [Nombre corto]
         */
        ind.cortarString = function (cadena) {
            ind.nombreSaludo = "";
            try {
                for (var i = 0; i < cadena.length; i++) {
                    if (cadena[i] == " ") {
                        break;
                    } else {
                        ind.nombreSaludo = ind.nombreSaludo + cadena[i];
                    }
                }
            } catch (e) {
            }
            return ind.nombreSaludo;
        };

        ind.desHojaVida = function () {
            var win = window.open(ServerDestino + "pdf/profile/" + $rootScope.idPerson, '_blank');
            win.focus();
        }

        ind.consultar = function () {
            if (storage.get("tokenUser")) {
                $rootScope.goAutho = true;
                $rootScope.tokens = storage.get("tokenUser");
                $rootScope.datosPersonales = storage.get("dataUser");
                $rootScope.idPerson = $rootScope.datosPersonales._id;
                ind.nombreSaludo = ind.cortarString($rootScope.datosPersonales.first_name) + " " + ind.cortarString($rootScope.datosPersonales.last_name);
                setTimeout(function () {
                    $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/profiledata").then(function (response) {
                        ind.userId = response.data.user._id;
                        $rootScope.idUser = response.data.user._id;
                        do {
                            $http.get(ServerDestino + "users/" + ind.userId).then(function (responses) {
                                if (response.data.user.visits_counter == 0) {
                                    $rootScope.mensajeBienvenida = 1;
                                    $location.path('/actualiza');
                                    storage.set("necesita", 2);
                                } else if (response.data.user.visits_counter == 1) {
                                    storage.remove("necesita");
                                    $rootScope.mensajeBienvenida = 2;
                                    ind.objEnvio = {
                                        visits_counter: 2
                                    };
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", ind.objEnvio)
                                        .then(function (response) {
                                        });
                                } else if (response.data.user.visits_counter == 2) {
                                    $rootScope.mensajeBienvenida = 0;
                                    $mdDialog.show({
                                        controller: dialogController,
                                        templateUrl: 'sociales.tmpl.html',
                                        parent: angular.element(document.body),
                                        escapeToClose: true,
                                        targetEvent: false
                                    });
                                    ind.objEnvio = {
                                        visits_counter: 3
                                    };
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", ind.objEnvio).then(function (response) {
                                    });
                                }
                                $rootScope.userAvatar = (responses.data.user.avatar == null) ? "../assets/images/user_male.png" : ServerDestino + "profile/" + responses.data.user.avatar;
                            });
                        } while (ind.userId == "");
                    });
                }, 200);
            } else {
                $rootScope.loginx = false;
                storage.clearAll();
                window.location = "../";
            }
            ;
        };

        ind.consultar();
        /**
         * Función que se ejecuta después de realizar la consulta inicial
         */
        $rootScope.postInicio = function () {
            $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/checkData")
                .then(function (response) {
                    $rootScope.puntosColores = response.data;
                });
        };
        $rootScope.postInicio();
        /**
         * Funcion que se dispara cuando una persona presiona el btn para descargar la hojda de vida
         */
        function dialogController($scope) {
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }

        ind.showDialogNewPasw = function (ev) {
            $mdDialog.show({
                controller: controllerDialog,
                controllerAs: 'ctrl',
                templateUrl: 'newpasw.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function (answer) {
                /*hb.cargaInicial();*/
            }, function () {
                /*hb.cargaInicial();*/
            });
        };
        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $q, $rootScope) {
            $scope.mostrar = function (pasnew, paswAct) {
                if (pasnew == "" || paswAct == "") {
                    swal("¡Atención!", "Ingresaste una contraseña vaciá", "error");
                }
                $scope.objEnvio = {
                    old_password: paswAct,
                    new_password: pasnew
                }
                $http.post(ServerDestino + "users/" + $rootScope.idUser + "/changePassword", $scope.objEnvio)
                    .then(function (response) {
                        if (response.data.message == "ERROR") {
                            swal("¡Atención!", "La contraseña actual no coincide con la ingresada", "error");
                        } else {
                            $mdDialog.hide();
                            swal("¡Felicitaciones!", "Tu contraseña a sido cambiada correctamente", "success");
                        }
                    });
            }
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }
    }

    indexController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$mdDialog", "$scope"];
    angular
        .module("spa-SE2")
        .controller("indexController", indexController);

})();
