/**
 * Created by WilderAlejandro on 29/12/2015.
 */


(function () {
    'use strict'

    function lateralController($http, $location, storage, $rootScope, $mdDialog, $scope) {

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };
        
        var std = this;
        std.userId = "";
        $rootScope.idUser = "";
        $rootScope.mensajeBienvenida = 0;
        $rootScope.userAvatar = "../assets/images/user_male.png";
        std.cortarString = function (cadena) {
            std.nombreSaludo = "";
            try {
                for (var i = 0; i < cadena.length; i++) {
                    if (cadena[i] == " ") {
                        break;
                    }
                    else {
                        std.nombreSaludo = std.nombreSaludo + cadena[i];
                    }
                }
            } catch (e) {

            }
            return std.nombreSaludo;
        };
        std.consultar = function () {

            if (storage.get("tokenUser")) {  
                $rootScope.accountType = storage.get("tipoUser");              
                $rootScope.goAutho=true;
                $rootScope.tokens = storage.get("tokenUser");
                $rootScope.datosPersonales = storage.get("dataUser");
                $rootScope.idPerson = $rootScope.datosPersonales._id;
                std.nombreSaludo = std.cortarString($rootScope.datosPersonales.first_name) + " "
                    + std.cortarString($rootScope.datosPersonales.last_name);

                setTimeout(function(){
                    $http.get(ServerDestino+ "persons/"  + $rootScope.idPerson + "/profiledata").then(function(response){
                        std.userId = response.data.user._id;
                        $rootScope.idUser = response.data.user._id;
                        do{
                            $http.get(ServerDestino + "users/" + std.userId).then(function(responses){
                                if(response.data.user.visits_counter == 0){
                                    $rootScope.mensajeBienvenida = 1;
                                    $location.path('/actualiza');
                                    storage.set("necesita", 2);

                                }
                                else if(response.data.user.visits_counter == 1){
                                    storage.remove("necesita");
                                    $rootScope.mensajeBienvenida = 2;
                                    std.objEnvio = {visits_counter: 2};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", std.objEnvio).then(function (response) {

                                    });

                                }
                                else if(response.data.user.visits_counter == 2){
                                    $rootScope.mensajeBienvenida = 0;
                                    $mdDialog.show({
                                        controller: dialogController,
                                        templateUrl: 'sociales.tmpl.html',
                                        parent: angular.element(document.body),
                                        escapeToClose: true,
                                        targetEvent: false
                                    });
                                    std.objEnvio = {visits_counter: 3};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", std.objEnvio).then(function (response) {

                                    });
                                }
                                $rootScope.userAvatar = (responses.data.user.avatar == null) ? "../assets/images/user_male.png":ServerDestino+"profile/"+responses.data.user.avatar;
                            });
                        }while(std.userId=="");
                    });
                }, 200);
            }else{
                $rootScope.loginx = false;
                storage.clearAll();
                window.location = "../";
            };
        };
        std.consultar();



        function dialogController($scope){

            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }
    }

    lateralController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$mdDialog", "$scope"];
    angular
        .module("spa-SE2")
        .controller("lateralController", lateralController);

})();