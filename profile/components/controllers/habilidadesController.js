/**
 * Created by WilderAlejandro on 13/1/2016.
 */

(function () {
    'use strict'
    function habilidadesController($http, $location, $rootScope, $scope, putPersons, storage, $mdDialog, $mdMedia, $q) {

        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        }
        ;

        var hb = this;

        hb.mostrarInfoAyuda = function () {
            swal({
                title: "HABILIDADES",
                text: "Queremos conocerte mejor, por eso cuéntanos tus habilidades técnicas, puede ser desde diseño gráfico " +
                "hasta manejo de camiones el limite lo pones tú, escribe tus mejores 8 habilidades y cuanto la dominas, una " +
                "habilidad puede ser una oferta de trabajo.",
                type: "info",
                showLoaderOnConfirm: true
            });
        };


        hb.skills = {};
        $rootScope.skillsLimit = "";
        $rootScope.listaHabilidades = [];
        $rootScope.listPalabrasClave = [];


        /*---------------------------- Comienzo de dialogo de habilidades ----------------------------------- */

        hb.cargaInicial = function () {

            $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/skills").then(function (response) {
                hb.skills = response.data.person_skills;
                $rootScope.skillsLimit = response.data.person_skills.length;
                if (hb.skills == "NO_DATA") {
                    $rootScope.listaHabilidades = [];
                } else {
                    $rootScope.skillsLimit = response.data.person_skills.length;
                    for (var i = 0; i < hb.skills.length; i++) {
                        var aux = [];
                        for (var k = 1; k < hb.skills[i].level + 1; k++) {
                            aux.push({value: k, disp: true});
                        }
                        for (var l = hb.skills[i].level + 1; l < 11; l++) {
                            aux.push({value: l, disp: false});
                        }
                        var data = {};
                        data = {
                            _id: hb.skills[i]._id,
                            skill: hb.skills[i].skill,
                            domain: hb.skills[i].level,
                            arrayDomain: aux
                        }
                        $rootScope.listaHabilidades.push(data);
                        /*$rootScope.listaHabilidades.splice($rootScope.listaHabilidades.length + 1, $rootScope.listaHabilidades.length + 1, data);*/
                    }
                    ;
                }
                ;
            });

            $http.get(ServerDestino + "/skills/all").then(function (response) {
                $rootScope.listPalabrasClave = response.data;
            });
        };

        hb.cargaInicial();

        hb.showDialogAddSkill = function (ev) {
            $mdDialog.show({
                controller: controllerDialog,
                controllerAs: 'ctrl',
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function (answer) {
                /*hb.cargaInicial();*/
            }, function () {
                /*hb.cargaInicial();*/
            });
        };

        hb.elimiarSkill = function (pos) {
            swal({
                title: "Eliminar Habilidad",
                text: "¿Esta seguro que desea eliminar esta habilidad?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, Eliminar!",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $http.delete(ServerDestino + "persons/" + $rootScope.idPerson + "/skills/" + $rootScope.listaHabilidades[pos]._id + "/delete").then(function (response) {
                        $rootScope.postInicio();
                        if (response.data.message == "OK") {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Habilidades actualizadas con éxito </div>');
                            $rootScope.listaHabilidades.splice(pos,1)
                        } else {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar las habilidades </div>');
                        }
                        ;
                    });

                }
            })
        };

        /**
         * Controlador del dialogo, Uso obligatorio del $scope el manejo del alcance que se encuntran dentro de los dialogos
         */
        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $q, $rootScope) {


            $scope.domain = 0;
            $scope.guardar = function () {
                if ($rootScope.skillsLimit < 8) {
                    if ($scope.addHabilidad == "" || $scope.addHabilidad === undefined || $scope.addHabilidad.length == 0) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Debes colocar nombre de la habilidad </div>');
                    } else if ($scope.domain == 0) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Debes elegir tu nivel de dominio </div>');
                    } else {
                        var aux = [];
                        for (var i = 1; i < $scope.domain + 1; i++) {
                            aux.push({value: i, disp: true});
                        }
                        for (var i = $scope.domain + 1; i < 11; i++) {
                            aux.push({value: i, disp: false});
                        }
                        $rootScope.listaHabilidades.splice($rootScope.listaHabilidades.length + 1, $rootScope.listaHabilidades.length + 1, {
                            skill: $scope.addHabilidad,
                            domain: $scope.domain,
                            arrayDomain: aux
                        });
                        alertify.success(' <div class="text-center font-size-16 text-white"> Habilidad Registrada </div>');
                        $rootScope.postInicio();
                        var data = {
                            skill: $scope.addHabilidad,
                            level: $scope.domain
                        };

                        $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/skills", data)
                            .then(function (response) {
                                $rootScope.postInicio();
                            });

                        $mdDialog.hide();
                    }
                } else {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Límite de habilidades alcanzado </div>');
                    $mdDialog.hide();
                }
                ;
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.cambiarColor = function (valor) {
                $scope.domain = valor;
                for (var i = 1; i < valor + 1; i++) {
                    var btn = $("#v" + i);
                    btn.removeClass("btn-escala");
                    btn.addClass("btn-escala-green");
                }
                for (var i = valor + 1; i < 11; i++) {
                    var btn = $("#v" + i);
                    btn.addClass("btn-escala");
                    btn.removeClass("btn-escala-green");
                }
            }


            /*-------------- Comienzo autoCompletado -------------*/
            setTimeout(function () {
                $scope.habilidades = cargarHabilidades();
                $scope.querySearch = querySearch;
                function cargarHabilidades() {
                    return $rootScope.listPalabrasClave.map(function (state) {
                        state = {name: state.description.toLowerCase(), display: state.description};
                        return state;
                    });
                }
            }, 500);
            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(state) {
                    return (state.name.indexOf(lowercaseQuery) > -1);
                };
            }


            function querySearch(query) {
                var results = query ? $scope.habilidades.filter(createFilterFor(query)) : $scope.habilidades;
                return results;
            }
        }
    }

    habilidadesController.$inject = ["$http", "$location", "$rootScope", "$scope", "putPersons", "localStorageService", "$mdDialog", "$mdMedia", "$q"];
    angular
        .module('spa-SE2')
        .controller('habilidadesController', habilidadesController);

})
();