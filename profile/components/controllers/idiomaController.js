/*--------------------------------------------------------
 Funciones para idiomas o software
 ---------------------------------------------------------- */
 (function () {
    'use strict'

    function IdiomaController($http, $location, $rootScope, putPersons, storage) {

        if (!storage.get("tokenUser")) { 
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };

        /*--------------------------------------------------------
         Declaracion de variables
         ---------------------------------------------------------- */
         var im = this;

        /*--------------------------------
         * Mensaje de ayuda
         * --------------------------------*/
         im.mostrarInfoAyuda = function () {
            swal({
                title: "IDIOMAS Y SOFTWARE ",
                text: "Registra el dominio que tengas sobre los idiomas que conoces, puede ser tu lengua nativa o" +
                " también una extrajera, también es importante que indiques que software manejas para una mejor oferta laboral.",
                type: "info",
                showLoaderOnConfirm: true
            });
        };


        im.pasoIdiomas = false;
        im.nombreSaludo = "";
        im.languagesSoftwaresEdit = false;
        im.languagesSoftwaresCreateSoft = false;
        im.languagesSoftwaresCreateLang = false;
        im.langEdit = [];
        im.softEdit = [];
        im.isLangEdit = false;
        im.isSoftEdit = false;
        im.isLangAdd = false; 
        im.isSoftAdd = false;
        im.noLang = false;
        im.noSoft = false;
        im.languages = {};
        im.softwares = {};
        im.newLang = {};
        im.newSoft = {};
        im.nivelesLanguages = [];
        im.nivelesSoftwares = [];
        im.listIdiomas = [];
        im.listProgramas = [];
        im.listSoftwares = {};
        im.idiomaLevel = "";
        im.idiomasAgregados = [];
        im.softwareAgregados = [];
        im.isRefresing = false;

        im.cargaInicial = function () {
            setTimeout(function () {

                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/languages").then(function (response) {
                    im.languages = response.data;
                    for (var i = 0; i < im.languages.person_languages.length; i++) {
                        im.nivelesLanguages.push({
                            nivelLanguage: im.nivelIdioma(im.languages.person_languages[i].level)
                        });
                        im.langEdit.push(false);
                    }
                    im.noLang = (im.languages.person_languages.length == 0) ? true : false;
                });

                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/softwares").then(function (response) {
                    im.softwares = response.data;
                    for (var i = 0; i < im.softwares.person_softwares.length; i++) {
                        im.nivelesSoftwares.push({
                            nivelSoftware: im.nivelIdioma(im.softwares.person_softwares[i].level)
                        });
                        im.softEdit.push(false);
                    }
                    im.noSoft = (im.softwares.person_softwares.length == 0) ? true : false;
                });
            }, 200)
};

im.cargaInicial();

im.nivelIdiomaMuestra = function (pos) {
    im.nivelesLanguages[pos].nivelLanguage = im.nivelIdioma(im.languages.person_languages[pos].level);
};

im.nivelSoftwareMuestra = function (pos) {
    im.nivelesSoftwares[pos].nivelSoftware = im.nivelIdioma(im.softwares.person_softwares[pos].level);
}

im.nivelIdioma = function (value) {
    if (value == 1) {
        return "Básico";
    } else if (value == 2) {
        return "Intermedio";
    } else {
        return "Avanzado";
    }
}

im.editar = function (value) {
    return !value;
};

im.editarObjeto = function (pos, tipo) {
    var url = "";
    var data = {};
    var go = false;

    if (tipo == 'lang' || tipo == 'langG') {
        if (tipo == 'langG') {
            url = ServerDestino + "persons/" + $rootScope.idPerson + "/languages/" + im.languages.person_languages[pos]._id + "/update"
            data = {
                language: im.languages.person_languages[pos].language,
                level: im.languages.person_languages[pos].level
            };
            go = true;
        }
        ;
        im.isLangEdit = !im.isLangEdit;
        im.langEdit[pos] = !im.langEdit[pos];
    } else if (tipo == 'soft' || tipo == 'softG') {
        if (tipo == 'softG') {
            url = ServerDestino + "persons/" + $rootScope.idPerson + "/softwares/" + im.softwares.person_softwares[pos]._id + "/update"
            data = {
                software: im.softwares.person_softwares[pos].software,
                level: im.softwares.person_softwares[pos].level
            };
            go = true;
        }
        ;
        im.isSoftEdit = !im.isSoftEdit;
        im.softEdit[pos] = !im.softEdit[pos];
    }
    ;

    if (go) {
        setTimeout(function () {
            $http.put(url, data)
            .success(function (response, status, headers, config) {
                alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                $rootScope.postInicio();
                im.cargaInicial();
            })
            .error(function (response, status, headers, config) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
            });
        }, 400);
    }
};

im.eliminarObjeto = function (pos, tipo) {
    var operacion = (tipo == "lang") ? "/languages/" : "/softwares/";
    var id = (tipo == "lang") ? im.languages.person_languages[pos]._id : im.softwares.person_softwares[pos]._id;
    swal({
        title: "Eliminar Idioma",
        text: "¿Esta seguro que desea eliminar este idioma?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Si, Eliminar!",
        closeOnConfirm: true
    }, function (isConfirm) {
        if (isConfirm) {

            var url = "";
            url = ServerDestino + "persons/" + $rootScope.idPerson + operacion + id + "/delete"
            $http.delete(url)
            .success(function (data, status, headers, config) {
                alertify.success(' <div class="text-center font-size-16 text-white"> Idiomas actualizados con éxito </div>');
                $rootScope.postInicio();
                im.cargaInicial();
            })
            .error(function (data, status, headers, config) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar los Idiomas </div>');
            });
        }
    })
};

im.cancelar = function(){

    im.isLangEdit = false;
    im.isSoftEdit = false;
    im.isLangAdd = false;
    im.isSoftAdd = false;

}

im.guardarCrear = function (tipo) {
    var tipoOperacion = (tipo == 'lang') ? im.languages : im.softwares;
    var go = false;
    var url = "";
    var cont = 0;

    im.cargaInicial();

    if (tipo == 'lang') {
        if (im.languages.person_languages.length > 0) {
            for (var i = 0; i < im.languages.person_languages.length; i++) {
                if (im.newLang.language == im.languages.person_languages[i].language) {
                    cont += 1;
                }
            }
            go = (cont > 0) ? false : true;
            cont = 0;
        } else {
            go = true;
        }
    } else {
        if (im.softwares.person_softwares.length > 0) {
            for (var i = 0; i < im.softwares.person_softwares.length; i++) {
                if (im.newSoft.software == im.softwares.person_softwares[i].software) {
                    cont += 1;
                }
            }
            go = (cont > 0) ? false : true;
            cont = 0;
        } else {
            go = true;
        }
    }
    ;

    if (go) {
        if (tipo == 'lang') {
            im.isRefresing = true;
            url = ServerDestino + "persons/" + $rootScope.idPerson + "/languages";
            setTimeout(function () {
                $http.post(url, im.newLang).then(function (response) {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Idiomas actualizados con éxito </div>');
                    $rootScope.postInicio();
                    im.cargaInicial();
                    im.isLangAdd = false;
                }, function (error) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar idiomas </div>');
                }).finally(function(){
                    im.isRefresing = false;
                });
            }, 400)
        } else {
            im.isRefresing = true;
            url = ServerDestino + "persons/" + $rootScope.idPerson + "/softwares";
            setTimeout(function () {
                $http.post(url, im.newSoft).then(function (response) {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Softwares actualizados con éxito </div>');
                    $rootScope.postInicio();
                    im.cargaInicial();
                    im.isSoftAdd = false;
                }, function (error) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar softwares </div>');
                }).finally(function(){
                    im.isRefresing = false;
                });
            }, 400)
        }
    } else {
        var aux = "";
        aux = (tipo == 'lang') ? 'Idioma' : 'Software';
        alertify.error(' <div class="text-center font-size-16 text-white"> Ese ' + aux + ' ya lo has agregado anteriormente, intenta con otro diferente. </div>');
    }
};

im.agregarIdioma = function () {
    im.isLangAdd = !im.isLangAdd;
    if (im.isLangAdd) {
        im.newLang = {
            language: "",
            level: ""
        };
        setTimeout(function () {
            $http.get(ServerDestino + "languages/all").then(function (response) {
                im.listIdiomas = response.data;
            });
        }, 400)
    }
    ;
};

im.camposValidos = function (lvl, tipo) {
    if (lvl != "" && lvl != null && tipo != "" && tipo != null) {
        return false;
    } else {
        return true;
    }
};

im.agregarSoftware = function () {
    im.isSoftAdd = !im.isSoftAdd;
    if (im.isSoftAdd) {
        im.newSoft = {
            software: "",
            level: ""
        };
        setTimeout(function () {
            $http.get(ServerDestino + "softwares/all").then(function (response) {
                im.listSoftwares = response.data;
            });
        }, 400)
    }
    ;
};

var yaAgregadosIdiomas = [];
im.registraridiomas = function (idioma, nivel) {

    if (yaAgregadosIdiomas.length < 7) {
        var exist = false;
        for (var i = 0; i < yaAgregadosIdiomas.length; i++) {
            exist = (yaAgregadosIdiomas[i] == idioma) ? (true) : (false);
        }
        if (!exist) {
            if (nivel == 1) {
                im.idiomasAgregados.push({
                    person_id: $rootScope.idPerson,
                    language: idioma,
                    level: nivel,
                    nivelTexto: "Básico"
                });
            }
            else if (nivel == 2) {
                im.idiomasAgregados.push({
                    person_id: $rootScope.idPerson,
                    language: idioma,
                    level: nivel,
                    nivelTexto: "Intermedio"
                });
            } else {
                im.idiomasAgregados.push({
                    person_id: $rootScope.idPerson,
                    language: idioma,
                    level: nivel,
                    nivelTexto: "Avanzado"
                });
            }
            yaAgregadosIdiomas.push(idioma);
        }
    } else {
        swal({
            title: "¡Limite de Idiomas alcanzado!",
            text: "Ya registro el limite de idiomas que puedes almacenar (6)",
            type: "error"
        });
    }
};

im.eliminarIdioma = function (pos) {
    im.idiomasAgregados.splice(pos);
    yaAgregadosIdiomas.splice(pos);
};

var yaSoftware = [];
im.registrarProgramas = function (software, nivel) {
    var exist = false;
    for (var i = 0; i < yaSoftware.length; i++) {
        exist = (yaSoftware[i] == software) ? (true) : (false);
    }
    if (!exist) {
        if (nivel == 1) {
            im.softwareAgregados.push({
                person_id: $rootScope.idPerson,
                software: software,
                level: nivel,
                nivelTexto: "Básico"
            });
        }
        else if (nivel == 2) {
            im.softwareAgregados.push({
                person_id: $rootScope.idPerson,
                software: software,
                level: nivel,
                nivelTexto: "Intermedio"
            });
        } else {
            im.softwareAgregados.push({
                person_id: $rootScope.idPerson,
                software: software,
                level: nivel,
                nivelTexto: "Avanzado"
            });
        }
        yaSoftware.push(software);
    }
};

im.eliminarSoftware = function (pos) {
    im.softwareAgregados.splice(pos);
    yaSoftware.splice(pos);
};

im.guardarCambiosIdiomas = function () {

    for (var i = 0; i < $scope.idiomasAgregados.length; i++) {
        $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/languages", im.idiomasAgregados[i]).then(function (response) {

        });
    }
    for (var i = 0; i < $scope.softwareAgregados.length; i++) {
        $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/softwares", im.softwareAgregados[i]).then(function (response) {
        });
    }

    im.pasoIdiomas = true;
    alertify.success(' <div class="text-center font-size-16 text-white"> Idiomas y Software registrados con éxito </div> ');
};
}

IdiomaController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
angular
.module('spa-SE2')
.controller('idiomaController', IdiomaController);
})();