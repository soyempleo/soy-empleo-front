/*--------------------------------------------------------
 Funciones para Hoja de Vida
 ---------------------------------------------------------- */
var token = "";
var userId = "";

(function () {
    'use strict'

    function HojadevidaController($http, $location, $rootScope,
                                  putPersons, storage, upload, $mdDialog, $mdMedia, $scope, $window, $q, $filter) {

        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        }
        ;

        var vm = this;


        /*--------------------------------
         * Mensaje de ayuda
         * --------------------------------*/
        vm.mostrarInfoAyuda = function () {

            swal({
                title: "Hoja de vida",
                text: "Aquí podrás ver un resumen de tus datos registrados en SoyEmpleo, completa todas las secciones" +
                " que se encuentran en la barra lateral para ver tu hoja de vida completa",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        vm.mostrarError = function (err) {
        }

        /*-------------------------------------
         Ejecucion de dialogo de ayudas
         -----------------------------------------*/
        vm.consultar = function () {
            if (storage.get("tokenUser")) {
                token = storage.get("tokenUser");
            }
        };

        vm.consultar();


        setTimeout(function () {
            if ($rootScope.mensajeBienvenida == 1) {
                swal({
                    title: "¡Bienvenido a SoyEmpleo.com!",
                    text: "Por ser tu primer inicio de sesión te invitamos a que registres tus datos personales",
                    type: "info",
                    showLoaderOnConfirm: true
                });
                $scope.$apply(function () {
                    vm.profileDataEdit = true;
                });
            }
        }, 700);


        /*--------------------------------------------------------
         Declaracion de variables
         ---------------------------------------------------------- */

        vm.profileDataPage = true;
        vm.profileDataEdit = false;
        vm.studiesPage = true;
        vm.studiesEdit = false;
        vm.experiencesPage = true;
        vm.experiencesEdit = false;
        vm.languagesSoftwaresPage = true;
        vm.languagesSoftwaresEdit = false;
        vm.skillsPage = true;
        vm.skillsEdit = false;
        vm.preferencesPage = true;
        vm.preferencesEdit = false;
        vm.referencesPage = true;
        vm.referencesEdit = false;
        vm.noSkills = true;
        vm.noStudies = true;
        vm.noAbout = true;
        vm.isModal = false;
        vm.noLang = true;
        vm.cambioDeTitulo = false;
        vm.hover = false;
        vm.pasoHojaVida = false;
        vm.veriFicado = true;
        vm.noExpe = true;
        vm.departamentoAjax = false;
        vm.skills = [];
        vm.aboutMe = [];
        vm.profileData = {};
        vm.languages = {};
        vm.softwares = {};
        vm.studies = {};
        vm.experiences = {};
        vm.preferences = {};
        vm.references = {};
        vm.edad = "";
        vm.birthYear = "";
        vm.birthMonth = "";
        vm.birthDay = "";
        vm.selectYear = [];
        vm.selectDay = [];
        vm.primerNombre = "";
        vm.segundoNombre = "";
        vm.primerApellido = "";
        vm.segundoApellido = "";
        vm.nombreCompleto = "";
        vm.listDepartamentos = {};
        vm.listaCiudades = {};
        vm.nivelesIdiomas = [];
        vm.licencia = "";
        vm.percentage = 0;
        vm.annioEstudies = [];
        vm.fechaNac = new Date();
        vm.tituloProfesional = "Defínete en pocas palabras";
        vm.antiguoTitle = "";
        vm.imagen = {};
        vm.file = '';
        vm.testing = "../assets/images/user_male.png";
        $rootScope._imageToken = "";
        $rootScope._imageUserId = "";
        vm.code = 3;
        $rootScope.nivelesCounter = {};
        vm.anioActual = new Date().getFullYear();
        vm.isResfresing = false;

        vm.editar = function (value) {
            return !value;
        };

        vm.cargaInicial = function () {
            setTimeout(function () {

                /**
                 * Datos personales
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/profiledata").then(function (response) {
                    vm.profileData = response.data;
                    vm.fechaNac = new Date(vm.profileData.person.birthdate);
                    vm.nombreCompleto = vm.profileData.person.first_name + " " + vm.profileData.person.last_name;
                    if (vm.profileData.person.degree_license == "N") {
                        vm.profileData.person.degree_license = "";
                    }

                    if (vm.profileData.person.birthdate != "") {
                        vm.profileData.person.birthdate = new Date(vm.profileData.person.birthdate);
                        vm.birthDay = vm.profileData.person.birthdate.getDate().toString();
                        vm.birthMonth = vm.profileData.person.birthdate.getMonth() + 1;
                        vm.birthMonth = vm.birthMonth.toString();
                        vm.birthYear = vm.profileData.person.birthdate.getFullYear().toString();
                    }

                    if (vm.profileData.person.birthdate != null && vm.profileData.person.birthdate != "") {
                        vm.edad = vm.calculaEdad(vm.profileData.person.birthdate) + " AÑOS";
                    }
                    ;


                    vm.separarApellidos(vm.profileData.person.last_name);
                    vm.separarPrimerNombre(vm.profileData.person.first_name);
                    userId = vm.profileData.user._id;
                    $rootScope.userId = vm.profileData.user._id;

                    if(vm.profileData.user){   
                        userId = ('_id' in vm.profileData.user) ? vm.profileData.user._id : '';

                        if ($rootScope.userAvatar == "../assets/images/user_male.png" || $rootScope.userAvatar == null || $rootScope.userAvatar == "assets/images/user_male.png" || $rootScope.userAvatar == "https://api.soyempleo.com/profile/") {
                            
                                $rootScope.userAvatar = (vm.profileData.user.avatar != null && vm.profileData.user.avatar != "") ? ServerDestino + "profile/" + vm.profileData.user.avatar : "..assets/images/user_male.png";
                                vm.urlValidator($rootScope.userAvatar);
                        }
                    }

                    $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
                        if (response.data.person.title != null && response.data.person.title.length != 0) {
                            vm.antiguoTitle = response.data.person.title;
                            vm.tituloProfesional = response.data.person.title;
                        }
                        if (!isNaN(response.data.percentage)) {

                            vm.percentage = Math.round(response.data.percentage);
                            $rootScope.porcentaje = vm.percentage;
                        }
                    });
                    if (vm.veriFicado) {
                        if (!vm.calculadorProfiledata() ) {
                            vm.veriFicado = false;
                            window.location = '#/actualiza';
                        }
                    }
                });

                /**
                 * Lenguajes registrados
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/languages").then(function (response) {
                    vm.languages = response.data;
                    if (response.data.person_languages.length == 0 || response.data.person_languages == "NO_DATA") {
                        vm.noLang = true;
                    } else {
                        vm.noLang = false;
                        var i = 0;
                        for (var i = 0; i < vm.languages.person_languages.length; i++) {
                            var h = 0;
                            var k = 0;
                            var aux = [];
                            var aux2 = parseInt(vm.languages.person_languages[i].level);
                            for (var h = 1; h < aux2 + 1; h++) {
                                aux.push({
                                    value: h,
                                    disp: true
                                });
                            }
                            ;

                            for (var k = aux2 + 1; k < 4; k++) {
                                aux.push({
                                    value: k,
                                    disp: false
                                });
                            }
                            ;

                            $rootScope.nivelesCounter[i] = {
                                name: vm.languages.person_languages[i].language,
                                level: vm.level(vm.languages.person_languages[i].level),
                                arrayLevel: aux
                            };
                        }
                        ;
                    }
                });

                /**
                 * Softwares registrados
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/softwares").then(function (response) {
                    vm.softwares = response.data;
                });

                /**
                 * Estudios registrados
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/studies").then(function (response) {
                    vm.studies = response.data;
                    if (response.data.person_studies == "NO_DATA" || response.data.person_studies.length == 0) {
                        vm.noStudies = true;
                    } else {
                        for (var i = 0; i < vm.studies.person_studies.length; i++) {
                            if (vm.studies.person_studies[i].end_year == "" || vm.studies.person_studies[i].end_year == null) {
                                vm.annioEstudies.push("");
                            } else {
                                if (vm.studies.person_studies[i].actual != null && vm.studies.person_studies[i].actual != false) {
                                    vm.annioEstudies.push("ACTUALMENTE");
                                } else {
                                    vm.annioEstudies.push("AÑO " + vm.studies.person_studies[i].end_year);
                                }
                            }
                        }

                        vm.noStudies = false;

                    }
                    ;
                });

                /**
                 * Experiencias registradas
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences").then(function (response) {
                    vm.experiences = response.data;
                    if (response.data.person_experiences == "NO_DATA" || response.data.person_experiences.length == 0) {
                        vm.noExpe = true;
                    } else {
                        vm.noExpe = false;
                    }
                });

                /**
                 * Preferencias registradas
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/preferences").then(function (response) {
                    vm.preferences = response.data;
                });

                /**
                 * Referencias registradas
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/references").then(function (response) {
                    vm.references = response.data;
                });

                /**
                 * Habilidades registradas
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/skills").then(function (response) {
                    if (response.data.person_skills == "NO_DATA" || response.data.person_skills.length == 0) {
                        vm.noSkills = true;
                    } else {
                        vm.noSkills = false;
                        vm.skills = response.data.person_skills;
                    }
                });

                /**
                 * Keywords registradas
                 */
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/keywords").then(function (response) {
                    if (response.data.person_keywords == "NO_DATA") {
                        vm.noAbout = true;
                    } else {
                        vm.noAbout = false;
                        for (var i = 0; i < response.data.person_keywords.length; i++) {
                            if (i < 8) {
                                vm.aboutMe.push(response.data.person_keywords[i]);
                            }
                        }
                        ;
                    }
                    ;
                });

                /**
                 * Data de departamentos
                 */
                $http.get(ServerDestino + "departments/all").then(function (response) {
                    vm.listDepartamentos = response.data;
                });
            }, 400)
        };

        vm.calculadorProfiledata = function(){
            if (vm.nombreCompleto && vm.profileData.person.numb_doc &&
                vm.profileData.person.birthdate && vm.profileData.person.city_birth &&
                vm.profileData.person.act_department && vm.profileData.person.act_city &&
                vm.profileData.person.act_address && vm.profileData.person.local_phone &&
                vm.profileData.person.mobile_phone && vm.profileData.person.gender &&
                vm.profileData.person.vehicle && vm.profileData.person.driver_license) {
                return true;
            }
            return false;
        }

        vm.urlValidator = function (url) {
            return $http.get(url).then(function (response) {
                return null;
            }).catch(function (err) {
                if (err.status === 404) {
                    $rootScope.userAvatar = "../assets/images/user_male.png";
                    return null
                }
                ;

                return $q.reject(err);
            })
        };

        vm.cargarAnyo = function () {
            vm.selectYear = [];
            vm.selectDay = [];
            vm.birthMonth = "";
            vm.birthDay = "";
            var aux = new Date;
            aux = aux.getFullYear();

            for (var i = aux; i >= aux - 100; i--) {
                vm.selectYear.push(i);
            }
        };
        vm.cargarAnyo();
        vm.cargaInicial();

        vm.depSelected = function (department) {
            if (department != "" && department != null) {
                vm.departamentoAjax = true;
                vm.profileData.person.act_city = "";
                vm.listaCiudades = [];
                var id = '';
                var found = $filter('filter')(vm.listDepartamentos,{name:department},true)[0];
                if (found) {
                    id = found._id;
                }
                setTimeout(function () {
                    $http.get(ServerDestino + "cities/byDepartment/" + id).then(function (response) {
                        vm.listaCiudades = response.data;
                    }).finally(function(){
                        vm.departamentoAjax = false;
                    });
                }, 400)
            }else{
                vm.profileData.person.act_city = "";
                vm.listaCiudades = [];
            }
        };

        vm.separarNombre = function (cadena) {
            if (cadena != null) {
                cadena = cadena.trim().split(" ");
                vm.primerNombre = "";
                vm.segundoNombre = "";
                try {
                    for (var i = 0; i < cadena.length; i++) {
                        if (i == 0) {
                            vm.primerNombre = cadena[i];
                            vm.profileData.person.first_name = vm.primerNombre.trim();
                        } else {
                            vm.segundoNombre = vm.segundoNombre + " " + cadena[i];
                            vm.profileData.person.last_name = vm.segundoNombre.trim();
                        }
                    }
                } catch (e) {
                }
            }
        };

        vm.separarApellidos = function (cadena) {
            if (cadena != null) {
                cadena = cadena.trim().split(" ");
                vm.primerApellido = "";
                vm.segundoApellido = "";
                try {
                    for (var i = 0; i < cadena.length; i++) {
                        if (i == 0) {
                            vm.primerApellido = cadena[i].trim();
                        } else {
                            vm.segundoApellido = vm.segundoApellido + " " + cadena[i];
                            vm.segundoApellido = vm.segundoApellido.trim();
                        }
                    }
                } catch (e) {
                }
            }
        };

        vm.separarPrimerNombre = function (cadena) {
            if (cadena != null) {
                cadena = cadena.trim().split(" ");
                try {
                    for (var i = 0; i < cadena.length; i++) {
                        if (i == 0) {
                            vm.primerNombre = cadena[i].trim();
                        } else {
                            vm.segundoNombre = vm.segundoNombre + " " + cadena[i];
                            vm.segundoNombre = vm.segundoNombre.trim();
                        }
                    }
                } catch (e) {
                }
            }
        };

        vm.opcionSelecta = function () {
            if (vm.profileData.person) {
               if (vm.profileData.person.driver_license == "N") {
                    vm.profileData.person.degree_license = "";
                } 
            }
            
        }

        vm.camposVacios = function (value) {
            if (value == null || value.length == 0) {
                return " ";
            }
        }

        vm.level = function (value) {
            if (value == 1) {
                return "Básico";
            } else if (value == 2) {
                return "Intermedio";
            } else {
                return "Avanzado";
            }
        };

        vm.gradoLicencia = function () {
            if (vm.licencia != "" && vm.licencia != null) {
                vm.profileData.person.degree_license = vm.licencia;
            }
        };

        vm.calculaEdad = function calculateAge(fechaNac) {
            var difEdadMs = Date.now() - Date.parse(fechaNac);
            var edad = new Date(difEdadMs);
            return Math.abs(edad.getUTCFullYear() - 1970);
        };

        vm.seleccionarCiudadSaludo = function (valor) {
            var aux = "";
            for (var i = 0; i < valor.length; i++) {
                if (i == 0) {
                    aux = valor.charAt(0).toUpperCase();
                } else {
                    aux = aux + valor.charAt(i).toLowerCase();
                }
            }
            $rootScope.ciudadSelect = aux;
        };

        vm.actualizaDatos = function () {
            vm.isResfresing = true;
            vm.profileData.person.birthdate = new Date(vm.birthYear + "/" + vm.birthMonth + "/" + vm.birthDay);
            putPersons.update({
                id: $rootScope.idPerson
            }, vm.profileData.person, function (response) {
                if (response.$resolved) {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Datos personales actualizados con éxito </div>');
                    $rootScope.postInicio();
                    if ($rootScope.mensajeBienvenida == 1) {
                        $rootScope.mensajeBienvenida = 0;
                        vm.objEnvio = {
                            visits_counter: 1
                        };
                        $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", vm.objEnvio).then(function (response) {
                        }).finally(function(){
                            vm.isResfresing = false;
                        });
                    }

                }

                $location.path("/");
            });
        };

        vm.formatDate = function (date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        };

        vm.cambiarTitulo = function () {
            if (vm.tituloProfesional == "Defínete en pocas palabras") {
                vm.tituloProfesional = "";
            }
            ;
            vm.cambioDeTitulo = true;
        };

        vm.enviarTitulo = function () {
            vm.cambioDeTitulo = false;
            if (vm.tituloProfesional == "" || vm.tituloProfesional == null) {
                vm.tituloProfesional = "Defínete en pocas palabras";
            } else {
                if (vm.tituloProfesional != vm.antiguoTitle) {
                    vm.antiguoTitle = vm.tituloProfesional;
                    $http.put(ServerDestino + "persons/" + $rootScope.idPerson, {
                        "title": vm.tituloProfesional
                    })
                        .then(function (response) {
                            if (response.data.message == "OK") {
                                alertify.success(' <div class="text-center font-size-16 text-white"> Titulo actualizado con éxito </div>');
                            } else {
                                alertify.success(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar el título. Por favor Intenta más tarde. </div>');
                            }
                        });
                }
            }
        };

        vm.registrarHojaVida = function (type_doc, numb_doc, birthdate, city_birth, act_department, act_city, act_address,
                                         local_phone, mobile_phone, alt_phone, gender, vehicle, driver_license, degree_license) {
            degree_license = (degree_license) ? (degree_license) : ("N");
            vm.objEnvio = {
                type_doc: type_doc,
                numb_doc: numb_doc,
                birthdate: birthdate,
                city_birth: city_birth,
                act_department: act_department,
                act_city: act_city,
                act_address: act_address,
                local_phone: local_phone,
                mobile_phone: mobile_phone,
                alt_phone: alt_phone,
                gender: gender,
                vehicle: vehicle,
                driver_license: driver_license,
                degree_license: degree_license
            };

            putPersons.update({
                id: $rootScope.idPerson
            }, vm.objEnvio, function (response) {
                if (response.$resolved) {
                    vm.pasoHojaVida = true;
                    alertify.success(' <div class="text-center font-size-16 text-white"> Hoja de vida registrada con éxito </div>');
                }
            });
        };

        vm.openImgModal = function () {
            vm.isModal = !vm.isModal;
        };


        vm.previewFileClick = function () {
            var img = document.getElementById("aimg");
            img.width = 200;
            img.height = 200;

            var str = img.src;
            str = str.substring(str.indexOf("image/"), str.indexOf(";base64"));
            vm.testing = img.src;

            vm.imagen = {
                encodedImg: document.getElementById("aimg").src,
                imgType: str,
                person_id: $rootScope.idPerson
            }

            setTimeout(function () {
                $http.post(ServerDestino + "users/" + vm.profileData.user._id + "/avatar").then(function (response) {
                    vm.isModal = !vm.isModal;
                });
            }, 400)
        };

        vm.uploadFile = function () {
            var envio = {
                file: vm.file,
                ip: ServerDestino,
                _id: vm.profileData.user._id,
                _token: vm.token
            };

            setTimeout(function () {
                upload.uploadFile(envio).then(function (res) {
                    if (res.data.message == "ERROR") {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto. </div>');
                    } else if (res.data.message == "IMAGE_ERROR") {
                        alertify.error(' <div class="text-center font-size-16 text-white"> El archivo que intentas subir, no es una imagen. </div>');
                    } else if (res.data.message == "SIZE_ERROR") {
                        alertify.error(' <div class="text-center font-size-16 text-white"> El archivo que intentas subir, supera el tamaño permitido (1mb). </div>');
                    } else {
                        alertify.success(' <div class="text-center font-size-16 text-white"> Foto de perfíl actualizada con éxito </div>');
                        vm.cancelImg();
                    }
                })
            }, 500)
        };

        vm.cancelImg = function () {
            $("#divButton").attr("style", "visibility: hidden;");
            vm.file = '';
            vm.isModal = false;

            var input = $("#file");
            input.replaceWith(input.val('').clone(true));
        };

        vm.openModal = function (ev) {
            $mdDialog.show({
                controller: controllerDialog,
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function (answer) {
            }, function () {
            });
        };

        vm.calculateDays = function (mes) {
            vm.selectDay = [];
            if (mes == "1" || mes == "3" || mes == "5" || mes == "7" || mes == "8" || mes == "10" || mes == "12") {
                for (var i = 1; i <= 31; i++) {
                    vm.selectDay.push(i);
                }
            } else if (mes == "4" || mes == "6" || mes == "9" || mes == "11") {
                for (var i = 1; i <= 30; i++) {
                    vm.selectDay.push(i);
                }
            } else {
                if (vm.calculateBisiesto()) {
                    for (var i = 1; i <= 29; i++) {
                        vm.selectDay.push(i);
                    }
                } else {
                    for (var i = 1; i <= 28; i++) {
                        vm.selectDay.push(i);
                    }
                }
            }
        };

        vm.calculateBisiesto = function () {
            var anyo = vm.birthYear;
            anyo = parseInt(anyo)

            if ((((anyo % 100) != 0) && ((anyo % 4) == 0)) || ((anyo % 400) == 0)) {
                return true;
            } else {
                return false;
            }
        };

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window) {
            controllerDialog.prototype.$scope = $scope;

            $scope.domain = 0;
            $scope.guardar = function () {
                if (($scope.monthExp === "" || $scope.monthExp === undefined || $scope.monthExp === null) || ($scope.addHabilidad == "" || $scope.addHabilidad === undefined || $scope.addHabilidad === null)) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Faltan datos </div>');
                } else {
                    var aux = [];
                    for (var i = 1; i < $scope.domain + 1; i++) {
                        aux.push({
                            value: i,
                            disp: true
                        });
                    }
                    for (var i = $scope.domain + 1; i < 11; i++) {
                        aux.push({
                            value: i,
                            disp: false
                        });
                    }
                    $rootScope.listaHabilidades.splice($rootScope.listaHabilidades.length + 1, $rootScope.listaHabilidades.length + 1, {
                        name: $scope.addHabilidad,
                        month: $scope.monthExp,
                        domain: $scope.domain,
                        arrayDomain: aux
                    });
                    alertify.success(' <div class="text-center font-size-16 text-white"> Habilidad Registrada </div>');
                    $mdDialog.hide();
                }
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
            };
            $scope.cambiarColor = function (valor) {
                $scope.domain = valor;
                for (var i = 1; i < valor + 1; i++) {
                    var btn = $("#v" + i);
                    btn.removeClass("btn-escala");
                    btn.addClass("btn-escala-green");
                }
                for (var i = valor + 1; i < 11; i++) {
                    var btn = $("#v" + i);
                    btn.addClass("btn-escala");
                    btn.removeClass("btn-escala-green");
                }
            };

            setTimeout(function () {
                $scope.$apply(function () {
                    $("#avatar").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="../assets/images/user_male.png" alt="Tu Foto" style="width:160px">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);
        };

        controllerDialog.prototype.setFile = function (element) {
            var $scope = this.$scope;
            $scope.$apply(function () {
                $scope.theFile = element.files[0];
            });
        };
    };

    HojadevidaController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService", "upload", "$mdDialog", "$mdMedia", "$scope", "$window", "$q", "$filter"];
    angular
        .module('spa-SE2')
        .controller('hojadevidaController', HojadevidaController);
})();


function previewFile() {
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    if (file) {
        $("#divButton").attr("style", "display: inline-block;");
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
};

function subirImagen() {
    $("#divLoader").attr("style", "display: inline-block;");
    $("#divButton").attr("style", "display: none;");
    var formData = new FormData($("form#formAvatarProfile")[0]);
    setTimeout(function() {
        $.ajax({
            url: ServerDestino + "users/" + userId + "/avatar",
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + token);
            },
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                if (respuesta.message == "IMAGE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> El archivo no es una imagen </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "SIZE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Imagen demasiado pesada </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Foto actualizada con éxito </div>');
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                    location.reload();
                }
            }
        });
    }, 100);
}