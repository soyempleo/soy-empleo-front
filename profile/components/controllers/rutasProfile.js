(function() {
	'use strict'

	function config($routeProvider) {
		$routeProvider

		.when('/', {
			templateUrl: 'app/sectionHoja/hojaGuardar2.html'
		})
			.when('/hoja', {
				templateUrl: 'app/sectionHoja/hojaGuardar2.html'
			})
			.when('/config', {
				templateUrl: 'app/configProfile.html'
			})
			.when('/actualiza', {
				templateUrl: 'app/sectionActualiza/profileData.html'
			})
			.when('/perfil', {
				templateUrl: 'app/sectionActualiza/profileData.html'
			})
			.when('/estudios', {
				templateUrl: 'app/sectionActualiza/studies.html'
			})
			.when('/experiencia', {
				templateUrl: 'app/sectionActualiza/experiences.html'
			})
			.when('/idiomas', {
				templateUrl: 'app/sectionActualiza/languagesSoftwares.html'
			})
			.when('/profesional', {
				templateUrl: 'app/sectionActualiza/skills.html'
			})
			/*.when('/logros', {
				templateUrl: 'app/sectionActualiza/logros.html'
			})*/
			.when('/habilidades', {
				templateUrl: 'app/sectionActualiza/habilidades.html'
			})
			.when('/preferencias', {
				templateUrl: 'app/sectionActualiza/preferences.html'
			})
			.when('/preguntas', {
				templateUrl: 'app/sectionActualiza/answers.html'
			})
			.otherwise({
				templateUrl: '404.html'
			});
	}
	angular
		.module('spa-SE2')
		.config(config);
})();