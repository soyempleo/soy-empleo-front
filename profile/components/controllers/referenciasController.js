/*--------------------------------------------------------
 referencias personales
 ---------------------------------------------------------- */

(function (){
    'use strict'

    function ReferenciasController($http, $location, $rootScope, putPersons, storage){

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };

        var rm = this;

        rm.mostrarInfoAyuda = function () {
            swal({
                title: "REFERENCIAS PERSONALES",
                text: "Muchas empresas a veces necesitan confirmar como eres o dices ser, por eso para agilizar tu proceso" +
                " de selección coloca un referido quien en caso de ser necesario puedan llamar y garantizar quien eres y así" +
                " completar una contratación. ",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        rm.references = {};
        rm.newReference = {};
        rm.isRegistrered = false;
        rm.isEdit=false;
        rm.isCrear = false;
        rm.isVista = false;
        rm.newReference = {};
        rm.tipo = "";
        rm.pos = ""; 


        rm.cargaInicial = function(){
            setTimeout(function(){
                $http.get(ServerDestino + "persons/"+ $rootScope.idPerson + "/references").then(function(response){
                    rm.references = response.data;
                    rm.isRegistrered = (rm.references.person_references.length==0 || rm.references.person_references=='NO_DATA') ? true:false;
                });
            }, 200)
            rm.pos = "";
            rm.newReference = {
                name : "",
                position: "",
                company: "",
                phone: ""
            }
        };

        rm.cargaInicial();

        rm.referencia = function(tipo,pos){
            rm.tipo = tipo;
            if(tipo=="crear"){
                rm.newReference = {
                    name : "",
                    position: "",
                    company: "",
                    phone: ""
                }
                rm.isCrear = true;
                rm.isVista = true;
            }else{
                rm.newReference = rm.references.person_references[pos];
                rm.isEdit = true;
                rm.isVista = true;
                rm.pos = pos;
            }
        };

        rm.guardarReferencia = function(){
            var url = ServerDestino + "persons/" + $rootScope.idPerson + "/references";
            if(rm.isRegistrered || rm.tipo=="crear"){
                $http.post(url, rm.newReference).then(function (response) {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Referencias actualizadas con éxito </div>');
                    rm.cargaInicial();
                    rm.isCrear = false;
                    rm.isVista = false;
                }, function (error) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar referencias </div>');
                });
            }else{
               url = ServerDestino + "persons/" + $rootScope.idPerson + "/references/" + rm.references.person_references[rm.pos]._id + "/update"
               $http.put(url, rm.newReference)
                .success(function (response, status, headers, config) {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Referencias actualizadas con éxito </div>');
                    rm.cargaInicial();
                    rm.isEdit = false;
                    rm.isVista = false;
                })
                .error(function (response, status, headers, config) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar referencias </div>');
                }); 
            }
        };

        rm.camposValidos = function(){
            if(rm.newReference.name!=null && rm.newReference.name!=""
                && rm.newReference.position != null && rm.newReference.position != ""
                && rm.newReference.company != null && rm.newReference.company != ""
                && rm.newReference.phone != null && rm.newReference != ""){
                return false;
            }else{
                return true;
            }
        }
        rm.eliminarReferencia = function (pos) {
            swal({
                title: "Eliminar Referencia",
                text: "¿Esta seguro que desea eliminar esta referencia?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, Eliminar!",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var url = "";
                    url = ServerDestino + "persons/" + $rootScope.idPerson + "/references/" + rm.references.person_references[pos]._id + "/delete"
                    rm.references.person_references.splice(pos, 1);

                    $http.delete(url)
                        .success(function (data, status, headers, config) {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Referencias actualizadas con éxito </div>');
                            rm.cargaInicial();
                        })
                        .error(function (data, status, headers, config) {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar las referencias </div>');
                        });
                }
            })
        };

        rm.registrarReferencia = function (nombreReferido, empresaLaboraReferido, cargoPersonaReferido, tlfPersonaReferido) {
            rm.objEnvio = {
                person_id: $rootScope.idPerson, name: nombreReferido, company: empresaLaboraReferido,
                position: cargoPersonaReferido, phone: tlfPersonaReferido
            };
            $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/references", rm.objEnvio).then(function (response) {
                if (response.data.message == "OK") {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Referencia registrada con éxito </div> ');
                }
            });
        };
    }

    ReferenciasController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
    angular
        .module('spa-SE2')
            .controller('referenciasController', ReferenciasController);
})();

