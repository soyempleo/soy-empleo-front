/**
 * Created by WilderAlejandro on 13/1/2016.
 */

(function () {
    'use strict'
    function logrosController($http, $location, $rootScope, putPersons, storage) {

        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        }
        ;

        var lg = this;

        /*--------------------------------
         * Mensaje de ayuda
         * --------------------------------*/
        lg.mostrarInfoAyuda = function () {
            swal({
                title: "Logros",
                text: "Estudios revelan que los mayores logros son muy importante para las empresas a la hora de tomar un" +
                " candidato, registra Un gran logro, un momento agradable y Algo que te enorgullezca, esto ayudara a identificar" +
                " más fácilmente que tipo de persona eres.",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        lg.data = false;
        lg.dataLogros = [];
        lg.l1 = "", lg.l2 = "", lg.l3 = "";
        lg.editMode = false;
        lg.isRefresing = false;

        lg.consultar = function () {
            setTimeout(function () {
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/achievements").then(function (response) {
                    if (response.data.person_achievements != 'NO_DATA') {
                        lg.data = true;
                        lg.l1 = (response.data.person_achievements[0] == "NO_DATA") ? ("") : (response.data.person_achievements[0]);
                        lg.l2 = (response.data.person_achievements[1] == "NO_DATA") ? ("") : (response.data.person_achievements[1]);
                        lg.l3 = (response.data.person_achievements[2] == "NO_DATA") ? ("") : (response.data.person_achievements[2]);

                    } else {
                        lg.data = false;
                    }
                });
            }, 200);
        };

        lg.consultar();

        lg.editarLogros = function () {
            lg.data = false;
            lg.editMode = true;
        };

        lg.guardarLogros = function (l1, l2, l3) {
            lg.isRefresing = true;
            l1 = (l1 === "" || l1 === undefined || l1 === null) ? ("NO_DATA") : (l1);
            l2 = (l2 === "" || l2 == undefined || l2 == null) ? ("NO_DATA") : (l2);
            l3 = (l3 === "" || l3 == undefined || l3 == null) ? ("NO_DATA") : (l3);


            if (lg.editMode != true) {

                if (l1 != "NO_DATA" || l2 != "NO_DATA" || l3 != "NO_DATA") {
                    lg.objEnvio = {
                        achievement1: l1,
                        achievement2: l2,
                        achievement3: l3
                    };
                    $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/achievements", lg.objEnvio).then(function (response) {
                        if (response.data.message == "OK") {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Logros Registrados </div>');
                            $rootScope.postInicio();
                        }
                        lg.data = true;
                    }).finally(function(){
                        lg.isRefresing = false;
                    });
                }
                else {
                    lg.isRefresing = false;
                }
            } else {
                lg.objEnvio = {
                    achievement1: l1,
                    achievement2: l2,
                    achievement3: l3
                };
                $http.put(ServerDestino + "persons/" + $rootScope.idPerson + "/achievements/update", lg.objEnvio)
                    .then(function (response) {
                    if (response.data.message == "OK") {
                        alertify.success(' <div class="text-center font-size-16 text-white"> Logros Actualizados </div>');
                        lg.data = true;
                        lg.editMode = false;
                    }


                }).finally(function(){
                    lg.isRefresing = false;
                });

            }


        }

    }

    logrosController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
    angular
        .module('spa-SE2')
        .controller('logrosController', logrosController);

})
();