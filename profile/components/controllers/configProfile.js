(function() {

	'use strict';

	configProfileController.$inject = ["$http", "$location", "localStorageService",
	"$rootScope", "$mdDialog", "$scope"
	];
	angular
	.module("spa-SE2")
	.controller("configProfileController", configProfileController);

	function configProfileController($http, $location, storage, $rootScope, $mdDialog, $scope) {
		var cgf = this;
		cgf.tab = 1;
		cgf.editar = false;
		cgf.email = storage.get("Us").user.email;

		cgf.changeTab = function(tab) {
			cgf.tab = tab;
		}
		cgf.changeMode = function(mode) {
			cgf.editar = mode;
		}
		cgf.changePasw = function(mode, emailx) {
			swal({
				title: "Contraseña",
				text: "Ingrese su contraseña para confirmar el cambio de correo",
				type: "input",
				inputType: "password",
				showCancelButton: true,
				closeOnConfirm: false,
				animation: "slide-from-top",
				inputPlaceholder: "Ingrese contraseña"
			}, function(inputValue) {
				if (inputValue === false) return false;
				if (inputValue === "") {
					swal.showInputError("Ingrese una contraseña");
					return false
				}
				cgf.objEnvio2 = {
					password: inputValue,
                         new_email: cgf.email
                    }
                    $http.post(ServerDestino + "users/" + storage.get("Us").user._id + "/changeEmail", 
                         cgf.objEnvio2)
                    .then(function(response) {
                         if (response.data.message == "OK") {
                          swal("Listo", "Operación completada, su correo a sido cambiado exitosamente", "success");
                          cgf.editar = mode;
                          var aux = storage.get("Us");
                          aux.user.email = cgf.email;
                          storage.set("Us", aux);
                     } else {
                          swal("Error", "Ingresaste la contraseña incorrectamente", "error");
                     }
                });
               });
		};

    /**
     * Función necesaria para deshabilitar los usuarios
     */
     cgf.deshabilitarCuenta = function() {
     	$http.post(ServerDestino + "users/" + storage.get("Us").user._id + "/enableDisableAccount ", {})
     	.then(function(response) {
     	});
     }
    /**
     * Funcion utilizada para eliminar la cuenta de usuario
     */
     cgf.elimnarAcount = function() {
     	swal({
     		title: "Eliminar cuenta",
     		text: "¿Estas seguro que seas eliminar tu cuenta?",
     		type: "warning",
     		showCancelButton: true,
     		confirmButtonColor: "#DD6B55",
     		confirmButtonText: "Si, borralo",
     		closeOnConfirm: false
     	}, function() {
     		swal({
     			title: "Contraseña",
     			text: "Ingrese su contraseña para confirmar la eliminación",
     			type: "input",
     			inputType: "password",
     			showCancelButton: true,
     			closeOnConfirm: false,
     			animation: "slide-from-top",
     			inputPlaceholder: "Ingrese contraseña"
     		}, function(inputValue) {
     			if (inputValue === false) return false;
     			if (inputValue === "") {
     				swal.showInputError("Ingrese una contraseña");
     				return false
     			}
     			$scope.$apply(function(){
                         cgf.objEnvio = {
                              password: inputValue
                         }
                    });
                    $http.delete(ServerDestino + "users/" + storage.get("Us").user._id,
                         {params: cgf.objEnvio})
                    .then(function(response) {
                         if (response.data.message == "OK") {
                              swal("Listo", "Operación completada, gracias por pertenecer a SoyEmpleo.com", "success");
                              setTimeout(function() {
                                  $rootScope.loginx = false;
                                  storage.clearAll();
                                  window.location = "../";
                             }, 5000);
                         } else {
                              swal("Error", "Ingresaste la contraseña incorrectamente", "error");

                         }
                    });
               });
     	});
     }

} 
})();