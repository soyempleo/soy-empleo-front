/*--------------------------------------------------------
 referencias personales
 ---------------------------------------------------------- */

(function (){
    'use strict'

    function PreguntasController($http, $location, $rootScope, putPersons, storage, $filter){

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };

        var rm = this;

        rm.mostrarInfoAyuda = function () {
            swal({
                title: "REFERENCIAS PERSONALES",
                text: "Muchas empresas a veces necesitan confirmar como eres o dices ser, por eso para agilizar tu proceso" +
                " de selección coloca un referido quien en caso de ser necesario puedan llamar y garantizar quien eres y así" +
                " completar una contratación. ",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        rm.references = {};
        rm.newReference = {};
        rm.isRegistrered = false;
        rm.isEdit=false;
        rm.isCrear = false;
        rm.isVista = false;
        rm.questionEdit = false;
        rm.newReference = {};
        rm.tipo = "";
        rm.pos = ""; 
        rm.preguntasRest = "";
        rm.preguntasList = [];
        rm.preguntasCount = 0;
        rm.currentPage = 1;
        rm.totalItems = 0;
        rm.entryLimit = 5; // items per page 
        rm.noOfPages = 0;
        rm.items = [];
        rm.preguntasEdit = [];
        rm.isRefreshingQuest = false;

        rm.cargaInicial = function(){
            setTimeout(function(){
                $http.get(ServerDestino + "persons/"+ $rootScope.idPerson + "/answersquestions").then(function(response){
                    rm.preguntasList = response.data.person_answers_questions;
                    rm.isRegistrered = true;

                    if (rm.preguntasList.questions_answered) {
                        rm.items = rm.preguntasList.questions_answered;
                        rm.totalItems = rm.preguntasList.questions_answered.length;
                        rm.entryLimit = 5; // items per page
                        rm.noOfPages = Math.ceil(rm.totalItems / rm.entryLimit);
                    }
                });
            }, 200);
        };


        rm.saltarPreguntas = function(){
            if (rm.preguntasCount == rm.preguntasList.questions_not_answered.length-1) {
                rm.preguntasCount = 0;
            } else {
                rm.preguntasCount++; 
            }
        }

        rm.responderPregunta = function(id,value){
            if (value) {
                rm.isRefreshingQuest = true;
                rm.objEnvio ={
                    question_id:id,
                    answer: value
                }

                $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/answersquestions",rm.objEnvio)
                .then(function(response){
                    var found = $filter('filter')(rm.preguntasList.questions_not_answered,{question_id:id})[0];
                    if(found){
                        var indx = rm.preguntasList.questions_not_answered.indexOf(found);
                        rm.preguntasList.questions_not_answered.splice(indx,1);
                        if(rm.preguntasCount == rm.preguntasList.questions_not_answered.length){
                            rm.preguntasCount = 0;
                        }
                    }
                    rm.cargaInicial();
                    rm.preguntasRest = "";
                }).finally(
                    function(){
                        rm.isRefreshingQuest = false;
                    }
                );
            }
        }

        rm.enviarEditada = function(id,respuesta){
            var found = $filter('filter')(rm.preguntasList.questions_answered,{question_id: id},true)[0];
            if(found){
                if (found.answer != respuesta) {
                    var objEnvio = {
                        question_id:id,
                        answer: respuesta
                    }

                    $http.put(ServerDestino + "persons/"+ $rootScope.idPerson + "/answersquestions/" + id + "/update",objEnvio)
                    .then(function(response){
                        /*rm.cargaInicial();*/
                        rm.questionEdit = false;
                    });
                }
            }
        }

        rm.editarPregunta = function(id){
            rm.preguntasEdit = [];
            var found = $filter('filter')(rm.preguntasList.questions_answered,{question_id: id},true)[0];
            if (found) {
                rm.preguntasEdit.pregunta = found.question;
                rm.preguntasEdit.respuesta = found.answer;
                rm.preguntasEdit.id = found.question_id;
                rm.questionEdit = true;
            }
            $(window).scrollTop(0);
        }

        rm.eliminarPregunta = function(id){
            swal({
              title: "¿Está seguro?",
              text: "Al eliminar perderá su respuesta",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Si, borrar!",
              cancelButtonText: "No!",
              closeOnConfirm: false,
              showLoaderOnConfirm: true,
            },
            function(){
              setTimeout(function(){
                $http.delete(ServerDestino + "persons/"+ $rootScope.idPerson + "/answersquestions/" + id + "/delete")
                .then(function(response){
                    if (response.data.message=='OK') {
                        swal("Respuesta borrada!");
                    }
                    rm.cargaInicial();
                    rm.questionEdit = false;
                });
                
              }, 2000);
            });
        }

        rm.cargaInicial();

    }

    PreguntasController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService","$filter"];
    angular
        .module('spa-SE2')
            .controller('preguntasController', PreguntasController);
})();

