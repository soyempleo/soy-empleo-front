(function () {
    'use strict';

    function ExperienciaController($http, $location, $rootScope, putPersons, storage, $mdDialog, $mdMedia, $scope, $window,$filter) {

        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };
        
        /*--------------------------------------------------------
         Declaracion de variables
         ---------------------------------------------------------- */
         var exp = this;

         exp.mostrarInfoAyuda = function () {
            swal({
                title: "EXPERIENCIA LABORAL",
                text: "En esta sección puedes registrar tu experiencia laboral, todas las experiencias que registres serán" +
                " procesadas por nuestro algoritmo y sumaremos el total de experiencia que tengas por sectores trabajados," +
                " ubicándote con mejores posibilidades de contratación, no dejes ninguna experiencia laboral por fuera todas cuentan. ",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        exp.mostrarInfoSalario = function () {
            swal({
                title: "EXPERIENCIA LABORAL",
                text: "Tu salario no será publico para las empresas, pero nuestro algoritmo lo tendrá en cuenta"+
                " para recomendarte solo a ofertas que mejoren tu salario actual",
                type: "info",
                showLoaderOnConfirm: true
            });
        };


        exp.mensajeDatos = false;
        exp.listProfesiones = {};
        exp.listCargos = {};
        exp.listAreas = {};
        exp.listSector = {};
        exp.listAnios = [];
        exp.listExperiencias = {};
        exp.hideEdit = false;
        exp.showNewExp = false;
        exp.expEdit = {};
        exp.isRefresing = false;

        exp.consultar = function () {
            $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences").then(function (response) {
                setTimeout(function () { 
                    $scope.$apply(function () {
                        var anoActual = new Date().getYear() + 1900;
                        if (exp.listAnios.length <= 0) {
                            for (var i = anoActual; i > 1940; i--) {
                                exp.listAnios.push(i);
                            }
                        }
                        exp.mensajeDatos = (response.data.person_experiences == "NO_DATA" || response.data.person_experiences.length < 1) ? (true) : (false);

                        if (response.data.person_experiences != "NO_DATA" || response.data.person_experiences.length < 1) {
                            exp.listExperiencias = response.data.person_experiences;
                            if (exp.listExperiencias.length > 0) {
                                for (var i = 0; i < exp.listExperiencias.length; i++) {
                                    if (exp.listExperiencias[i].actual != null && exp.listExperiencias[i].actual == true) {
                                        exp.listExperiencias[i].end_month = "";
                                        exp.listExperiencias[i].end_year = "";
                                    }
                                }
                            }
                        }
                        $http.get(ServerDestino + "professions/all").then(function (response) {
                            exp.listProfesiones = response.data;
                        });
                        $http.get(ServerDestino + "positions/all").then(function (response) {
                            exp.listCargos = response.data;
                        });
                        $http.get(ServerDestino + "areapositions/all").then(function (response) {
                            exp.listAreas = response.data;
                        });
                        $http.get(ServerDestino + "sectorscompany/all").then(function (response) {
                            exp.listSector = response.data;
                        });
                    });
                }, 500);
            });
        };


        exp.pasoExpTrabajo = false;

        exp.listAnios = [];
        exp.listCargos = {};
        exp.listCiudades = {};
        exp.listDepartamentos = {};
        exp.listAreas = {};
        exp.listSector = {};
        exp.modeloPrub = "Valor Inicial";
        exp.expLaboralAgregados = [];
        exp.annoInicioEmpresa = "";
        exp.mesInicioEmpresa = "";
        exp.annioFinExpe = "";
        exp.mesFinExpe = "";

        exp.listaPositions = [];
        exp.titulo = {}; 
        exp.tituloEdit = {};
        exp.posicion = "";
        exp.cargoEmpresa = "";


        exp.editar = function (value) {
            return !value;
        };


        exp.cambioEdit = function (objeto,indx) {
            if (!objeto.actual) {
                exp.listExperiencias[indx].end_month = parseInt(exp.listExperiencias[indx].end_month);
                exp.listExperiencias[indx].end_month = ""+exp.listExperiencias[indx].end_month;
                exp.minYear(objeto.start_year);
                exp.minMonth(objeto.start_month,objeto.start_year,objeto.end_year,true);
            }
            exp.hideEdit = true;
            $rootScope.expeIndex = indx;
            exp.expEdit = objeto;
            setTimeout(function(){
                $scope.$apply(function(){
                    $('#ex3_value').val(exp.expEdit.position);
                    exp.tituloEdit = {originalObject:{display:exp.expEdit.position}};
                });
            },100);
        };

        exp.sinExpe = true;
        var yaExpLaboral = [];
        exp.registrarExpLaboral = function (company, sector_company, area_position, start_month, start_year,
                                            end_month, end_year, position, salary, description, actual) {
            var exist = false;
            for (var i = 0; i < yaExpLaboral.length; i++) {
                exist = (yaExpLaboral[i] == company) ? (true) : (false);
            }

            if (!exist) {
                exp.expLaboralAgregados.push({
                    person_id: $rootScope.idPerson,
                    company: company,
                    sector_company: sector_company,
                    area_position: area_position,
                    start_month: start_month,
                    start_year: start_year,
                    end_month: end_month,
                    end_year: end_year,
                    position: position,
                    salary: salary,
                    description: description,
                    actual: actual
                });
                yaExpLaboral.push(company);
                exp.sinExpe = false;
            }
        };

        exp.minAnoEstudio = [];
        exp.minYear = function (min) {
            exp.minAnoEstudio = [];
            exp.mesInicioEmpresa = "";
            exp.annioFinExpe = "";
            exp.mesFinExpe = "";
            exp.expEdit.start_month = "";
            exp.expEdit.end_year = "";
            var añoActual = new Date().getYear() + 1900;
            for (var i = añoActual; i > (min - 1); i--) {
                exp.minAnoEstudio.push(i);
            }
        };

        exp.minMonthEstudio = {};
        exp.minMonth = function (mesActual, annioIni, annioFin,edit) { 
            mesActual = parseInt(mesActual);
            if (edit) {
                exp.mesFinExpe = "";
                exp.expEdit.end_month = "";
            }
            
            exp.minMonthEstudio = [];
            if (annioIni != null && annioIni != "" && annioFin != null && annioFin != "") {
                if (annioIni == annioFin) {
                    for (var i = 0; i < 12; i++) {
                        switch (mesActual) {
                            case 1:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Enero"};
                            break;
                            case 2:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Febrero"};
                            break;
                            case 3:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Marzo"};
                            break;
                            case 4:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Abril"};
                            break;
                            case 5:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Mayo"};
                            break;
                            case 6:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Junio"};
                            break;
                            case 7:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Julio"};
                            break;
                            case 8:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Agosto"};
                            break;
                            case 9:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Septiembre"};
                            break;
                            case 10:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Octubre"};
                            break;
                            case 11:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Noviembre"};
                            break;
                            case 12:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Diciembre"};
                            break;
                            default:
                            break;
                        };
                        mesActual += 1;
                    };
                } else {
                    mesActual = 1;
                    for (var i = 0; i < 12; i++) {
                        switch (mesActual) {
                            case 1:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Enero"};
                            break;
                            case 2:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Febrero"};
                            break;
                            case 3:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Marzo"};
                            break;
                            case 4:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Abril"};
                            break;
                            case 5:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Mayo"};
                            break;
                            case 6:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Junio"};
                            break;
                            case 7:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Julio"};
                            break;
                            case 8:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Agosto"};
                            break;
                            case 9:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Septiembre"};
                            break;
                            case 10:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Octubre"};
                            break;
                            case 11:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Noviembre"};
                            break;
                            case 12:
                            exp.minMonthEstudio[i] = {id: mesActual, nombre: "Diciembre"};
                            break;
                            default:
                            break;
                        }
                        ;
                        mesActual += 1;
                    };
                };
            };
        };

        exp.eliminarExpLaboral = function (pos) {
            exp.expLaboralAgregados.splice(pos);
            yaExpLaboral.splice(pos);
            if (yaExpLaboral.length < 0) {
                exp.sinExpe = true;
            }
        };

        exp.guardarCambiosExpLab = function () {
            /*for (var i = 0; i < yaExpLaboral.length; i++) {
             $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences", exp.expLaboralAgregados[i]).then(function (response) {
             });
         }*/

         exp.pasoExpTrabajo = true;
         alertify.success(' <div class="text-center font-size-16 text-white"> Experiencia laboral registrada con éxito </div> ');
     };

     exp.guardarPrimeraExp = function (company, sector_company, area_position, start_month, start_year,
                                       end_month, end_year, position, salary, description, actual) {
        var aux = parseInt(end_month);
        if (aux < 10 && actual != true) {
            end_month = "0" + aux;
        };
        exp.objEnvio = {
            person_id: $rootScope.idPerson,
            company: company,
            sector_company: sector_company,
            area_position: area_position,
            start_month: start_month,
            start_year: start_year,
            end_month: end_month,
            end_year: end_year,
            position: position,
            salary: salary,
            description: description,
            actual: actual
        };
        $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences", exp.objEnvio).then(function (response) {
            alertify.success(' <div class="text-center font-size-16 text-white"> Experiencia laboral registrada con éxito </div> ');
            $rootScope.postInicio();
            exp.mensajeDatos = false;
            exp.consultar();
        });
    };

    exp.guardarExp = function () {
        exp.isRefresing = true;
        exp.expEdit.position = exp.tituloEdit ? exp.tituloEdit.originalObject.display:exp.expEdit.position;
        var url = ServerDestino + "persons/" + $rootScope.idPerson + "/experiences/" + exp.expEdit._id + "/update";
        var aux = parseInt(exp.expEdit.end_month);
        if (aux < 10 && exp.expEdit.actual != true) {
            exp.expEdit.end_month = "0" + aux;
        }
        ;
        $http.put(url, exp.expEdit)
        .then(function (response) {
            alertify.success(' <div class="text-center font-size-16 text-white"> Experiencia actualizada con éxito </div>');
            exp.consultar();
            exp.hideEdit = false;
            $rootScope.postInicio();
        }, function (error) {
            swal("Error al actualizar registro", "por favor recargue la pagina", "error");
        }).finally(function(){
            exp.isRefresing = false;
        });

    };


    exp.posicionSelected = function(value){
        var ex3Temp = $('#ex3_value').val();
        if (value || ex3Temp) {
            var aux = value || ex3Temp;
            exp.titulo = {originalObject:{display:exp.expEdit.position}};
            exp.cargoEmpresa = aux;
            var foundPosicion = $filter('filter')(exp.posiciones,{display:aux});
            if (!foundPosicion.length) {
                $('#ex3_dropdown').css("display", "none");
            }
        }else if (!value) {
            exp.titulo = {originalObject:{display:""}};
        }
    }

    exp.posicionSelectedEdit = function(value){
        var ex3Temp = $('#ex3_value').val();
        if (value || ex3Temp) {
            var aux = value || ex3Temp;
            exp.tituloEdit = {originalObject:{display:aux}};            
            exp.cargoEmpresa = aux;
            var foundPosicion = $filter('filter')(exp.posiciones,{display:aux});
            if (!foundPosicion.length) {
                $('#ex3_dropdown').css("display", "none");
            }
        }else if (!value) {
            exp.tituloEdit = {originalObject:{display:""}};
        }
    }


    $scope.$watch('exp.titulo',function(){
        if (exp.titulo) {
            if ('originalObject' in exp.titulo) {
                if (exp.titulo.originalObject.name != "" && typeof exp.titulo.originalObject.name !== undefined && exp.titulo.originalObject.name != null) {
                    exp.posicionSelected(exp.titulo.originalObject.name);
                };
            }
        }
    });

    $scope.$watch('exp.tituloEdit',function(){
        if (exp.tituloEdit) {
            if ('originalObject' in exp.tituloEdit) {
                if (exp.tituloEdit.originalObject.name != "" && typeof exp.tituloEdit.originalObject.name !== undefined && exp.tituloEdit.originalObject.name != null) {
                    exp.posicionSelected(exp.tituloEdit.originalObject.name);
                };
            }
        }
    });

    exp.getPosicion = function(valor,tipo){
        if (tipo=='crear') {
            exp.cargoEmpresa = $('#ex3_value').val() || "";
        }else{
            exp.expEdit.position = $('#ex3_value').val() || "";
            exp.tituloEdit = {originalObject:{display: exp.expEdit.position}};
        }                
    }

    /**
     * Build `states` list of key/value pairs
     */
    function remapObjPositions(obj) {
        var value = [];
        return obj.map(function(state) {
            value = {
                value: state.name.toLowerCase(),
                display: state.name,
                name: state.name.toLowerCase()
            };
            return value;
        });
    }

    $http.get(ServerDestino + "positions/all").then(function(response){
        exp.listaPositions = response.data;
        if (exp.listaPositions.length) {
            exp.posiciones = remapObjPositions(exp.listaPositions);
        }
    });

    exp.saltarSinExp = function () {
        exp.pasoExpTrabajo = true;
        $location.path("/resumen");
    };

    exp.sinExperiencia = function () {
        exp.pasoExpTrabajo = true;
        $location.path("/resumen");
    };

    exp.consultar();

    exp.eliminarExp = function (pos) {

        swal({
            title: "Eliminar Experiencia laboral",
            text: "¿Esta seguro que desea eliminar esta experiencia laboral?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Si, Eliminar!",
            closeOnConfirm: true
        }, function (isConfirm) {
            if (isConfirm) {

                var url = ServerDestino + "persons/" + $rootScope.idPerson + "/experiences/" + exp.listExperiencias[pos]._id + "/delete";
                exp.listExperiencias.splice(pos, 1);
                $http.delete(url)
                .then(function (response) {
                    $rootScope.postInicio();
                    alertify.success(' <div class="text-center font-size-16 text-white"> Experiencia eliminada con éxito </div>');
                    exp.mensajeDatos = true;
                }, function (error) {
                    swal("Error al eliminar registro", "por favor recargue la pagina", "error");
                })


            }
        });
    };

    exp.agregarMasEmpleos = function () {
        exp.showNewExp = true;
        exp.hideEdit = true;
    };

    exp.guardarMasExp = function (company, sector_company, area_position, start_month, start_year,
                                  end_month, end_year, position, salary, description, actual) {
        exp.isRefresing = true; 
        var aux = parseInt(end_month);
        if (aux < 10 && actual != true) {
            end_month = "0" + aux;
        };
        exp.objEnvio = {
            person_id: $rootScope.idPerson,
            company: company,
            sector_company: sector_company,
            area_position: area_position,
            start_month: start_month,
            start_year: start_year,
            end_month: end_month,
            end_year: end_year,
            position: position,
            salary: salary,
            description: description,
            actual: actual
        };

        exp.listExperiencias.push({
            person_id: $rootScope.idPerson,
            company: company,
            sector_company: sector_company,
            area_position: area_position,
            start_month: start_month,
            start_year: start_year,
            end_month: end_month,
            end_year: end_year,
            position: position,
            salary: salary,
            description: description,
            actual: actual
        });

        $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences", exp.objEnvio).then(function (response) {
            alertify.success(' <div class="text-center font-size-16 text-white"> Experiencia laboral registrada con éxito </div> ');
            exp.hideEdit = false;
            exp.showNewExp = false;
        }).finally(function(){
            exp.isRefresing = false;
        });
    }

    exp.cancerlarExp = function(){
        exp.hideEdit = false;
        exp.showNewExp = false;
    }

    exp.mostrar = function(h){
    }
}

ExperienciaController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService", "$mdDialog", "$mdMedia", "$scope", "$window","$filter"];
angular
.module('spa-SE2')
.controller('experienciaController', ExperienciaController);
})();