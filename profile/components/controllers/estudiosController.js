/*---------------------------------------------------------
 Funciones para Estudios
 ---------------------------------------------------------- */
 (function () {
    'use strict'

    function EstudiosController($http, $location, $rootScope, putPersons, storage, $timeout, $q, $log) {
        if (!storage.get("tokenUser")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };

        var em = this;
        /*--------------------------------
         * Mensaje de ayuda
         * --------------------------------*/
         em.mostrarInfoAyuda = function () {
            swal({
                title: "Estudios",
                text: "Ingresa tu información académica, puedes registrar tus estudios y tener más posibilidades de" +
                " contratación, al completar esta sección tendrás más posibilidades de recibir ofertas laborales.",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        /*--------------------------------------------------------
         Declaracion de variables
         ---------------------------------------------------------- */

         em.pasoEstudios = false;

         $rootScope.ciudadSelect = "";
         em.studies = {};
         em.nombreSaludo = "";
         em.studioUni = false;
         em.studiesEdit = false;
         em.studioEditable = {};
         em.studiesEditarClick = false;
         em.studActualmente = false;
         em.studiesRegistrered = false;
         em.newStudy = false;
         em.newStudy2 = false;
         em.oldStudy = false;
         em.newStudyCreate = {};
         em.listAnios = [];
         em.listPalabrasClave = [];
         em.listIdiomas = [];
         em.listProgramas = [];
         em.separador = {};
         em.listInstituciones = {};
         em.listProfesiones = {};
         em.listSoftware = {};
         em.listCargos = {};
         em.listCiudades = {};
         em.listDepartamentos = {};
         em.modeloPrub = "Valor Inicial";
         em.estudiosAgragados = [];
         em.listaInstitutos = [];
         em.listEstudios = [];
         em.pos = "";
         em.bloqueador = true;
         em.bloqueador2 = false;
         em.editor = true;
         em.editor2 = false;
         em.adder = false;

        /*--------------------------------------------------------
         Funciones para Estudios
         ---------------------------------------------------------- */

         em.cargaInicial = function () {
            setTimeout(function () {

                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/studies").then(function (response) {
                    em.studies = response.data;

                    for (var i = 0; i < em.studies.person_studies.length; i++) {
                        if (em.studies.person_studies[i].study_level == "Colegio") {
                            em.separador[i] = {
                                pos: ""
                            };
                        } else {
                            em.separador[i] = {
                                pos: "-"
                            };
                        }
                    }
                    ;

                    if (em.studies.person_studies.length == 0 || em.studies.person_studies == "NO_DATA") {
                        em.studiesRegistrered = false;
                        em.adder = false;
                    } else {
                        em.studiesRegistrered = true;
                        em.adder = true;
                    }

                    if (!em.studiesRegistrered) {
                        em.studies.person_studies = {
                            study_level: "",
                            college: "",
                            career: "",
                            study_name: "",
                            start_month: "",
                            end_month: "",
                            start_year: "",
                            end_year: "",
                            average: "",
                            actual: false
                        };
                    }


                });
            }, 200)

            em.newStudyCreate = {
                study_level: "",
                college: "",
                career: "",
                study_name: "",
                start_month: "",
                end_month: "",
                start_year: "",
                end_year: "",
                average: "",
                actual: false
            };
            em.listAnios = [];
            var añoActual = new Date().getYear() + 1900;
            for (var i = añoActual; i > 1940; i--) {
                em.listAnios.push(i);
            }
            ;
        };

        em.cargaInicial();

        em.cambio = function (valor) {
            if (valor < 4) {
                em.studioUni = false;
            }
            else {
                em.studioUni = true;
            }
        };

        em.editarEstudios = function (pos) {
            if (!em.studies.person_studies[pos].actual) {
                em.studies.person_studies[pos].start_month = "" + parseInt(em.studies.person_studies[pos].start_month);
                em.studies.person_studies[pos].start_year = "" + parseInt(em.studies.person_studies[pos].start_year);
                em.studies.person_studies[pos].end_month = "" + parseInt(em.studies.person_studies[pos].end_month);
                em.studies.person_studies[pos].end_year = "" + parseInt(em.studies.person_studies[pos].end_year);
                em.minYear(em.studies.person_studies[pos].start_year);
                em.minMonth(em.studies.person_studies[pos].start_month,
                            em.studies.person_studies[pos].start_year,
                            em.studies.person_studies[pos].end_year);
            }            
            em.pos = pos;
            em.studioEditable = {};
            em.studioEditable = em.studies.person_studies[pos];
            em.studiesEditarClick = true;
        };

        em.guardarCrear = function () {
            em.bloqueador = false;
            em.bloqueador2 = true;

            var url = ServerDestino + "persons/" + $rootScope.idPerson + "/studies";
            var data = {
                study_level: em.newStudyCreate.study_level,
                college: em.newStudyCreate.college,
                career: em.newStudyCreate.career,
                start_month: em.newStudyCreate.start_month,
                end_month: em.newStudyCreate.end_month,
                start_year: em.newStudyCreate.start_year,
                end_year: em.newStudyCreate.end_year,
                average: em.newStudyCreate.average,
                actual: em.newStudyCreate.actual
            };
            if (data.actual) {
                data.end_year = new Date().getFullYear();
            };


            $http.post(url, data).then(function (response) {
                alertify.success(' <div class="text-center font-size-16 text-white"> Estudios actualizados con éxito </div>');
                $rootScope.postInicio();
                em.cargaInicial();
                em.viejoEstudioMostrar('f');
                em.bloqueador = true;
                em.bloqueador2 = false;
            }, function (error) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar estudios </div>');
                em.bloqueador = true;
                em.bloqueador2 = false;
            });
        };

        em.guardarEdit = function (pos) {
            em.editor = false;
            em.editor2 = true;
            em.studies.person_studies[pos] = em.studioEditable;

            var url = "";
            url = ServerDestino + "persons/" + em.studies.person_id + "/studies/" + em.studies.person_studies[pos]._id + "/update"
            
            if (em.studioEditable.actual) {
                em.studioEditable.end_year = new Date().getFullYear();
            }
            ;


            $http.put(url, em.studioEditable)
            .success(function (response, status, headers, config) {
                alertify.success(' <div class="text-center font-size-16 text-white"> Estudios actualizados con éxito </div>');
                $rootScope.postInicio();
                em.cargaInicial();
                em.studiesEditarClick = false;
                em.editor = true;
                em.editor2 = false;
            })
            .error(function (response, status, headers, config) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar estudios </div>');
                em.editor = true;
                em.editor2 = false;
                em.cargaInicial();
                em.studiesEditarClick = false;

            });
        };

        em.eliminarEstudios = function (pos) {
            swal({
                title: "Eliminar Estudio",
                text: "¿Esta seguro que desea eliminar este estudio?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, Eliminar!",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    em.adder = false;
                    var url = "";
                    url = ServerDestino + "persons/" + em.studies.person_id + "/studies/" + em.studies.person_studies[pos]._id + "/delete"
                    em.studies.person_studies.splice(pos, 1);

                    $http.delete(url)
                    .success(function (data, status, headers, config) {
                        $rootScope.postInicio();
                        alertify.success(' <div class="text-center font-size-16 text-white"> Estudios actualizados con éxito </div>');
                        em.cargaInicial();
                    })
                    .error(function (data, status, headers, config) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar los Estudios </div>');
                    });


                }
            })
        };

        em.nuevoEstudioMostrar = function () {
            em.newStudy2 = !em.newStudy2;
            em.studiesEditarClick = !em.studiesEditarClick;
            em.cargaInicial();
        };

        em.viejoEstudioMostrar = function (value) {
            em.adder=false;
            if(value=='f'){
                em.oldStudy = false;
            }else{
                em.oldStudy = true;
            }
            em.cargaInicial();
        };

        em.camposValidos = function (actual, study_level, college, career, start_month, start_year, end_month, end_year, average) {
            if (actual
                && study_level != "Colegio"
                && study_level != ""
                && college != ""
                && career != ""
                && start_month != ""
                && start_month != null
                && start_year != ""
                && start_year != null
                && average != "") {
                return false;
        } else if (!actual
            && study_level != "Colegio"
            && study_level != ""
            && college != ""
            && career != ""
            && start_month != ""
            && start_month != null
            && start_year != ""
            && start_year != null
            && end_month != ""
            && end_month != null
            && end_year != ""
            && end_year != null
            && average != "") {
            return false;
        } else if (study_level == "Colegio"
            && study_level != ""
            && college != "") {
            return false;
        } else {
            return true;
        }
    };

    em.minAnoEstudio = [];
    em.minYear = function (min) {
        em.minAnoEstudio = [];
        em.newStudyCreate.end_year = "";
        em.newStudyCreate.start_month = "";
        em.studioEditable.end_year = "";
        em.studioEditable.start_month = "";
        var añoActual = new Date().getYear() + 1900;
        for (var i = añoActual; i > (min - 1); i--) {
            em.minAnoEstudio.push(i);
        }
    };

    em.minMonthEstudio = {};
    em.minMonth = function (mesActual, annioIni, annioFin) {
        mesActual = parseInt(mesActual);
        em.newStudyCreate.end_month = "";
        em.studioEditable.end_month = "";
        em.minMonthEstudio = [];
        if (annioIni != null && annioIni != "" && annioFin != null && annioFin != "") {
            if (annioIni == annioFin) {
                for (var i = 0; i < 12; i++) {
                    switch (mesActual) {
                        case 1:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Enero"};
                        break;
                        case 2:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Febrero"};
                        break;
                        case 3:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Marzo"};
                        break;
                        case 4:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Abril"};
                        break;
                        case 5:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Mayo"};
                        break;
                        case 6:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Junio"};
                        break;
                        case 7:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Julio"};
                        break;
                        case 8:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Agosto"};
                        break;
                        case 9:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Septiembre"};
                        break;
                        case 10:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Octubre"};
                        break;
                        case 11:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Noviembre"};
                        break;
                        case 12:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Diciembre"};
                        break;
                        default:
                        break;
                    }
                    ;
                    mesActual += 1;
                }
                ;
            } else {
                mesActual = 1;
                for (var i = 0; i < 12; i++) {
                    switch (mesActual) {
                        case 1:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Enero"};
                        break;
                        case 2:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Febrero"};
                        break;
                        case 3:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Marzo"};
                        break;
                        case 4:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Abril"};
                        break;
                        case 5:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Mayo"};
                        break;
                        case 6:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Junio"};
                        break;
                        case 7:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Julio"};
                        break;
                        case 8:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Agosto"};
                        break;
                        case 9:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Septiembre"};
                        break;
                        case 10:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Octubre"};
                        break;
                        case 11:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Noviembre"};
                        break;
                        case 12:
                        em.minMonthEstudio[i] = {id: mesActual, nombre: "Diciembre"};
                        break;
                        default:
                        break;
                    }
                    ;
                    mesActual += 1;
                }
                ;
            }
            ;
        }
        ;
    };

    em.editar = function (value) {
        em.cargaInicial();
        em.newStudy2 = false;
        return !value;
    };

    var yaEstudios = [];
    em.registrarEstudios = function (study_level, college, career, college_degree, start_month, start_year, end_month, end_year, actual, average) {
        /*Limpiar*/

        if (yaEstudios.length < 8) {

            $("#institucion").val("");
            $("#carrera").val("");
            $("#titulo").val("");
            $("#mesfecini").val("");
            $("#annofecini").val("");
            $("#mesFecFin").val("");
            $("#annofecFin").val("");
            $("#studActualmente").val("");
            $("#promedio").val("");
            $("#studActividades").val("");
            em.nivelEstudio = "";
            em.formEstudios.$setPristine();


            var exist = false;
            for (var i = 0; i < yaEstudios.length; i++) {
                exist = (yaEstudios[i] == study_level) ? (true) : (false);
            }
            if (!exist) {
                em.estudiosAgragados.push({
                    person_id: $rootScope.idPerson,
                    study_level: study_level,
                    college: college,
                    career: career,
                    college_degree: college_degree,
                    start_month: start_month,
                    start_year: start_year,
                    end_month: end_month,
                    end_year: end_year,
                    actual: actual,
                    average: average
                });
                yaEstudios.push(study_level);
                em.nivelEstudio = "";
            }
        } else {
            swal({
                title: "¡Limite de Estudios alcanzado!",
                text: "Ya registro el limite de estudios que puedes almacenar (7)",
                type: "error"
            });
        }
    };

    /*---------------------------Directiva autocompletado angularMaterial-------------------------------------------*/

    /*$http.get(ServerDestino + "institutions/all").then(function (response) {
        em.listaInstitutos = response.data;
    }, function (error) {
    });

    $http.get(ServerDestino + "studies/all").then(function (response) {
        em.listEstudios = response.data;
    });*/


    setTimeout(function () {
        $http.get(ServerDestino + "institutions/all").then(function (response) {
            em.listaInstitutos = response.data;
            em.states = cargarInstiuciones();
            em.querySearch = querySearch;
        }, function (error) {
        });

        $http.get(ServerDestino + "studies/all").then(function (response) {
            em.listEstudios = response.data;
            em.estudios = cargarEstudios();
            em.querySearchStudies = querySearchStudies;
        }); 
        
        function cargarEstudios() {
            return em.listEstudios.map(function (state) {
                state = {name: state.description.toLowerCase(), display: state.description};
                return state;
            });
        }

        function cargarInstiuciones() {
            return em.listaInstitutos.map(function (state) {
                state = {name: state.name.toLowerCase(), display: state.name};
                return state;
            });
        }
    }, 500);
    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(state) {
            return (state.name.indexOf(lowercaseQuery)>-1);
        };

    }

    function querySearch(query) {
        var results = query ? em.states.filter(createFilterFor(query)) : em.states;
        return results;
    }

    function querySearchStudies(query) {
        var results = query ? em.estudios.filter(createFilterFor(query)) : em.estudios;
        return results;
    }


    /*-------------------------------Fin de directiva---------------------------------------*/

}

EstudiosController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService", "$timeout", "$q", "$log"];
angular
.module('spa-SE2')
.controller('estudiosController', EstudiosController);
})
();
