/*--------------------------------------------------------
 Perfil profesional (Quien eres)
 ---------------------------------------------------------- */
(function () {
    'use strict'


    function QuieneresController($http, $location, $rootScope, putPersons, storage, $scope) {

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll(); 
            window.location = "../";
        };

        /*--------------------------------------------------------
         Declaracion de variables
         ---------------------------------------------------------- */
        var qt = this;
        /*--------------------------------
         * Mensaje de ayuda
         * --------------------------------*/
        qt.mostrarInfoAyuda = function () {
            swal({
                title: "PERFIL PROFESIONAL",
                text: "En esta sección puedes describir quien eres en 300 caracteres, es una sección para que te vendas" +
                " a las empresas y no te puedan dejar pasar, si te cuesta describirte usa las palabras que están debajo de" +
                " la caja de texto para poder ayudarte en esta redacción, describe tu perfil profesional y prepárate para" +
                " recibir más ofertas laborales.",
                type: "info",
                showLoaderOnConfirm: true
            });
        };


        qt.pasoPerfil = false;
        qt.listPalabrasClave = [];
        qt.masPalabrasClave = [];
        qt.palabras = [];
        qt.apuntador = 0;
        qt.textProfile = "";
        qt.modoConsult = true;
        qt.keyword = []; 

        qt.cargaInicial = function () {

            $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
                if (response.data.person.profile != "" && response.data.person.profile!=null) {
                    qt.textProfile = response.data.person.profile;
                    qt.modoConsult = false;
                } else {
                    qt.textProfile="";
                    qt.modoConsult = true;
                }

            });
            setTimeout(function () {
                var apuntador = 10;
                var auxWord = [];
                $http.get(ServerDestino + "/keywords/all").then(function (response) {
                    var listapalabrasAux = response.data;
                    jQuery.each(listapalabrasAux, function (i, val) {
                        if (qt.palabras.length < 12) {
                            qt.palabras.push(stringClean(val));
                            qt.listPalabrasClave.push(val);
                        }
                        auxWord.push(val.description);
                        qt.masPalabrasClave.push(val)
                    });
                    qt.apuntador = 12;
                    if (qt.palabras.length > 0) {
                        $('#skills').highlightTextarea({
                            words: auxWord,
                            caseSensitive: false
                        });
                    }
                });
            }, 200)
        };

        qt.cargaInicial();

        qt.resaltarPalabra = function () {
            try {
                qt.keyword = [];
                var stringPalabras = stringClean($("#skills").val().toUpperCase());
                for (var x = 0; x < qt.palabras.length; x++) {
                    var aux = qt.palabras[x].description.toUpperCase();
                    var cadena = stringPalabras.search(stringClean(aux));
                    if (cadena >= 0) {
                        $("#p" + x).removeClass("activo");
                        qt.keyword.push(aux);
                    }
                    else {
                        $("#p" + x).addClass("activo");
                    }
                }
            } catch (e) {

            }
        };

        qt.cargarOtrasPalabras = function () {
            if ((qt.apuntador + 12) > qt.masPalabrasClave.length) {
                qt.palabras = [];
                qt.listPalabrasClave = [];
                for (var i = 0; i < 12; i++) {
                    qt.palabras.push(qt.masPalabrasClave[i]);
                    qt.listPalabrasClave.push(qt.masPalabrasClave[i]);
                }
                qt.apuntador = 12;
            } else {
                qt.palabras = [];
                qt.listPalabrasClave = [];
                for (var i = qt.apuntador; i < qt.apuntador + 12; i++) {
                    qt.palabras.push(qt.masPalabrasClave[i]);
                    qt.listPalabrasClave.push(qt.masPalabrasClave[i]);
                }
                qt.apuntador = qt.apuntador + 12;
            }
        };

        qt.registrarPerfilProfesional = function (professional_profile) {
            qt.isResfresing = true;
            qt.objEnvio = {
                person_id: $rootScope.idPerson,
                profile: professional_profile
            };
            $http.put(ServerDestino + "persons/" + $rootScope.idPerson, qt.objEnvio).then(function (response) {
                if (response.data) {
                    $rootScope.postInicio();
                    alertify.success(' <div class="text-center font-size-16 text-white"> Perfil profesional actualizado </div> ');
                    qt.modoConsult = false;
                }
            }).finally(function(){
                qt.isResfresing = false;
            });
            if (qt.keyword.length > 0) {
                qt.otherEnvio = {};
                for (var i = 0; i < qt.keyword.length; i++) {
                    qt.otherEnvio['keyword' + (i + 1)] = qt.keyword[i];
                }

                $http.put(ServerDestino + "persons/" + $rootScope.idPerson + "/keywords/update", qt.otherEnvio).then(function (response) {
                    $rootScope.postInicio();
                    if (response.data.message == "OK") {
                    }
                }).finally(function(){
                    qt.isResfresing = false;
                });
            }
        };

        qt.addPalabra = function (cadena, posicion) {
            $("#skills").val($("#skills").val() + " " + cadena.toLowerCase());
            $("#p" + posicion).removeClass("activo");
            qt.resaltarPalabra();
        };


        qt.modoEdit = function () {
            qt.resaltarPalabra();
            qt.modoConsult = true;
        }

    }

    QuieneresController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService", "$scope"];
    angular
        .module('spa-SE2')
        .controller('quieneresController', QuieneresController);
})();