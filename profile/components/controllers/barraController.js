(function (){

    function BarraController($http, $location, $rootScope, putPersons, storage){
        
        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };
        /*--------------------------------------------------------
         Declaracion de variables
        ---------------------------------------------------------- */
        var vm = this;

        vm.basico = {};
        vm.idiomas = {};
        vm.programas = {};
        vm.estudios = {};
        vm.experiencias = {};
        vm.preferencias = {};
        vm.referencias = {};
        vm.progress = 0;


        vm.consultar = function () {
            if (storage.get("tokenUser")) {
                $location.path("/session");
            }
        };

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/profiledata").then(function (response) {
            vm.basico = response.data;
            if(vm.basico.message == "OK"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences").then(function (response) {
            vm.experiencias = response.data;
            if(vm.experiencias.person_experiences != "NO_DATA"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/studies").then(function (response) {
            vm.estudios = response.data;
            if(vm.estudios.person_studies != "NO_DATA"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/languages").then(function (response) {
            vm.idiomas = response.data;
            if(vm.idiomas.person_languages != "NO_DATA"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/softwares").then(function (response) {
            vm.programas = response.data;
            if(vm.programas.person_softwares != "NO_DATA"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/preferences").then(function (response) {
            vm.preferencias = response.data;
            if(vm.preferencias.person_preferences != "NO_DATA"){
                vm.progress += 10;
            }
        });

        $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/references").then(function (response) {
            vm.referencias = response.data;
            if(vm.referencias.person_references != "NO_DATA"){
                vm.progress += 10;
            }
        });
    }
    
    BarraController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
    angular
        .module('spa-SE2')
            .controller('barraController', BarraController);
})();