/*--------------------------------------------------------
 Preferencias
 ---------------------------------------------------------- */
(function (){
    'use strict'

    function PreferenciasController($http, $location, $rootScope, putPersons, storage){

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };

        var pref = this;

        pref.mostrarInfoAyuda = function () {
            swal({
                title: "PREFERENCIAS",
                text: "Describe en que sección deseas trabajar y el algoritmo lo tomara en cuenta para ofrecerte el trabajo que tu realmente quieres..",
                type: "info",
                showLoaderOnConfirm: true
            });
        };

        pref.pasoPreferencis = false;
        pref.listAreas = {};
        pref.listSector = {};
        pref.mensajeDatos = false;
        pref.preferencesObj = {};

        pref.cargaInicial = function () {
            setTimeout(function () {
                $http.get(ServerDestino + "areapositions/all").then(function (response) {
                    pref.listAreas = response.data;
                });
                $http.get(ServerDestino + "sectorscompany/all").then(function (response) {
                    pref.listSector = response.data;
                });
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/preferences").then(function (response) {
                    pref.mensajeDatos = (response.data.person_preferences == "NO_DATA" || response.data.person_preferences.length<1) ? (true) : (false);
                    if (response.data.person_preferences != "NO_DATA" || response.data.person_preferences.length<1) {
                        pref.preferencesObj = response.data.person_preferences[0];
                    }
                });
            },200)
        };



        pref.registrarPreferencias = function (area_position, sector_company) {
            pref.objEnvio = {
                person_id: $rootScope.idPerson,
                area_position: area_position,
                sector_company: sector_company
            };
            if(pref.mensajeDatos){
                $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/preferences", pref.objEnvio).then(function (response) {
                    if (response.data.message == "OK") {
                        pref.pasoPreferencis = true;
                        alertify.success(' <div class="text-center font-size-16 text-white"> Preferencias registradas con éxito </div> ');
                    }
                });
                pref.mensajeDatos = false;
            }else{
                var url = ServerDestino + "persons/" + $rootScope.idPerson + "/preferences/"+pref.preferencesObj._id+"/update";
                $http.put(url, pref.objEnvio).then(function (response) {
                    if (response.data.message == "OK") {
                        alertify.success(' <div class="text-center font-size-16 text-white"> Preferencias actualizadas con éxito </div> ');
                    }
                });
            }

        }

        pref.cargaInicial();
    }

    PreferenciasController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
    angular
        .module('spa-SE2')
        .controller('preferenciasController', PreferenciasController);
})();