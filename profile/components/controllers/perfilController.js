 (function (){
    'use strict'

    function PerfilController($http, $location, $rootScope, putPersons, storage){

        if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };
        
        /*--------------------------------------------------------
         Declaracion de variables
        ---------------------------------------------------------- */
        var pc = this;

        pc.profileData = {};
        pc.languages = {};
        pc.softwares = {};
        pc.studies = {};
        pc.experiences = {};
        pc.preferences = {};
        pc.references = {};
        pc.nivel = "Básico";

        pc.consultar = function () {
            if (storage.get("tokenUser")) {
                $location.path("/session");
            }
        };

        pc.dominio = function(valor){
            if(valor==1){pc.nivel = "Básico"};
            if(valor==2){pc.nivel ="Intermedio"};
            if(valor==3){pc.nivel = "Avanzado"};
            return pc.nivel;
        }

        pc.cargaInicial = function(){
            setTimeout(function () {
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
                    pc.profileData = response.data;
                    console.leg(pc.profileData);
                });
            }, 200)
        };
    }
    
    PerfilController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"];
    angular
        .module('spa-SE2')
            .controller('perfilController', PerfilController);
})();