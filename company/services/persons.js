/**
 * Created by WilderAlejandro on 23/11/2015.
 */


/**
 *Este factory es usado para consultar, guardar y eliminar una persona
 **/
angular
  	.module('spa-SE3')
  	.factory("persons", ['$resource', function($resource){
	    return $resource(ServerDestino+"persons/:id", {id: "@id"});
  	}]);

/**
 *Este factory es usado para consultar, guardar y eliminar idiomas de personas
 **/
angular
  	.module('spa-SE3')
  	.factory("personsIdioma", ['$resource', function($resource){
	    return $resource(ServerDestino+"languagesperson/:id", {id: "@id"});
	}]);

/**
 *Este factory es usado para consultar, guardar y eliminar Software de personas
 **/
angular
  	.module('spa-SE3')
  	.factory("personsSoftware", ['$resource', function($resource){
	    return $resource(ServerDestino+"softwareperson/:id", {id: "@id"});
	}]);
/**
 *Este es un factory que retorna el $resourse necesario para actualizar persona
 **/

angular
  	.module('spa-SE3')
  	.factory("putPersons", ['$resource', function($resource){
	    return $resource(ServerDestino+"persons/:id", {id: "@id"}, {update: {method: "PUT"}});
	}]);

/* *--------------------Servicio subir archivo al servidor--------------------* */
angular
  .module('spa-SE3')
  .service('upload', ["$http", "$q", function ($http, $q) {
      this.uploadFile = function (envio) {
          var deferred = $q.defer();
          /*var formData = new FormData($(envio.formId)[0]);*/
          var formData = new FormData();
          formData.append(envio.nombreCampo, envio.file);
          /*if(envio.formId == "form#empreAditional"){
            formData.append("position", envio.position);
          }*/          
          return $http.post(envio.url, formData, {
              headers: {
                  'Content-type': undefined,
                  'Authorization': 'Bearer ' + envio._token                
              },
              transformRequest: angular.identity
          })
              .success(function (res) {

                  deferred.resolve(res);
              })
              .error(function (msg, code) {
                  deferred.reject(msg);
              })
          return deferred.promise;
      }
  }]);
  


