/**
 * Created by WilderAlejandro on 29/12/2015.
 */

var logo = "";
(function () {
    'use strict'

    function lateralController($http, $location, storage, $rootScope, $mdDialog, $scope) {

       /* if (!storage.get("tokenCompany")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };*/
        
        var std = this;
        std.userId = "";
        $rootScope.idUser = "";
        $rootScope.mensajeBienvenida = 0;
        $rootScope.companyName = "";
        $rootScope.userAvatar = '../assets/images/company_pic.png';
        std.cortarString = function (cadena) {
            std.nombreSaludo = "";
            try {
                for (var i = 0; i < cadena.length; i++) {
                    if (cadena[i] == " ") {
                        break;
                    }
                    else {
                        std.nombreSaludo = std.nombreSaludo + cadena[i];
                    }
                }
            } catch (e) {

            }
            return std.nombreSaludo;
        };
        std.consultar = function () {

            if (storage.get("tokenCompany")) {  
                $rootScope.datosCompany = storage.get("dataUser");
                $rootScope.idCompany = $rootScope.datosCompany._id;

                setTimeout(function(){
                    $http.get(ServerDestino + "companies/" + $rootScope.idCompany).then(function(response){
                        std.userId = response.data.data.user_id;
                        $rootScope.idUser = response.data.data.user_id;
                        $rootScope.companyName = response.data.data.name;
                        if(response.data.data.logo != "" && response.data.data.logo != null){
                            $rootScope.userAvatar = ServerDestino + "profile/companies/logos/" + response.data.data.logo;
                            std.urlValidator($rootScope.userAvatar);
                            logo = $rootScope.userAvatar;
                        }
                        /*do{
                            $http.get(ServerDestino + "users/" + std.userId).then(function(responses){
                                if(response.data.user.visits_counter == 0){
                                    $rootScope.mensajeBienvenida = 1;
                                    $location.path('/actualiza');
                                    storage.set("necesita", 2);

                                }
                                else if(response.data.user.visits_counter == 1){
                                    storage.remove("necesita");
                                    $rootScope.mensajeBienvenida = 2;
                                    std.objEnvio = {visits_counter: 2};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", std.objEnvio).then(function (response) {

                                    });

                                }
                                else if(response.data.user.visits_counter == 2){
                                    $rootScope.mensajeBienvenida = 0;
                                    $mdDialog.show({
                                        controller: dialogController,
                                        templateUrl: 'sociales.tmpl.html',
                                        parent: angular.element(document.body),
                                        escapeToClose: true,
                                        targetEvent: false
                                    });
                                    std.objEnvio = {visits_counter: 3};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", std.objEnvio).then(function (response) {

                                    });
                                }
                                
                            });
                            
                        }while(std.userId=="");*/

                    });
                }, 200);
            }else{
                $rootScope.loginx = false;
                storage.clearAll();
                window.location = "../";
            };
        };

        std.urlValidator = function(url){
            return $http.get(url).then(function (response) {
                return null;
            }).catch(function (err){
                if(err.status === 404) {
                    $rootScope.userAvatar = "../assets/images/company_pic.png";
                    return null};

                return $q.reject(err);
            })
        };

        std.consultar();



        function dialogController($scope){

            $scope.cancel = function () {
                $mdDialog.cancel();
            };
        }
    }

    lateralController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$mdDialog", "$scope"];
    angular
        .module("spa-SE3")
        .controller("lateralController", lateralController);

})();