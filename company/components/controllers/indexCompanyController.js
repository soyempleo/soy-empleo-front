/**
 * Created by WilderAlejandro on 28/1/2016.
 */

(function() {
	'use strict'

	function indexController($http, $location, storage, $rootScope, $mdDialog, $scope) {

		var ind = this;

		ind.slideIsOpen = true;
		ind.urlSlideNav = $location.path();

		$http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function(response) {
			if (!isNaN(response.data.percentage)) {
				vm.percentage = Math.round(response.data.percentage);
				$rootScope.porcentaje = vm.percentage;
			}
		});

		/**
		 * Funcion que se dedica a cambiar el url pasada por parametro y tambien ayuda
		 * a colocar la clase active en la barra lateral
		 */
		ind.cambiarDir = function(url) {
			ind.urlSlideNav = "/" + url;
			$location.path("/" + url);
		}
		/**
		 * Funcion que cambia el switch si esta abierto o no el slidenava y eso
		 * genera un cambio de clase
		 */
		ind.changeSlideNav = function() {
			if (ind.slideIsOpen == true) {
				ind.slideIsOpen = false;
			} else {
				ind.slideIsOpen = true;
			}
		}


		ind.userId = "";
		$rootScope.idUser = "";
		$rootScope.mensajeBienvenida = 0;
		$rootScope.companyName = "";
		$rootScope.userAvatar = '../assets/images/company_pic.png';
		ind.cortarString = function(cadena) {
			ind.nombreSaludo = "";
			try {
				for (var i = 0; i < cadena.length; i++) {
					if (cadena[i] == " ") {
						break;
					} else {
						ind.nombreSaludo = ind.nombreSaludo + cadena[i];
					}
				}
			} catch (e) {

			}
			return ind.nombreSaludo;
		};
		ind.consultar = function() {

			if (storage.get("tokenCompany")) {
				$rootScope.datosCompany = storage.get("dataUser");
				$rootScope.idCompany = $rootScope.datosCompany._id;

				setTimeout(function() {
					$http.get(ServerDestino + "companies/" + $rootScope.idCompany).then(function(response) {
						ind.userId = response.data.data.user_id;
						$rootScope.idUser = response.data.data.user_id;
						$rootScope.companyName = response.data.data.name;

						  $zopim(function() {
	                        $zopim.livechat.set({
	                          language: 'es',
	                          name: response.data.data.name,
	                          email: response.data.data.users.email
	                        });
	                    
	                        $zopim.livechat.button.show();
	                        
	                      });


						if (response.data.data.logo != "" && response.data.data.logo != null) {
							$rootScope.userAvatar = ServerDestino + "profile/companies/logos/" + response.data.data.logo;
							ind.urlValidator($rootScope.userAvatar);
							logo = $rootScope.userAvatar;
						}
						/*do{
                            $http.get(ServerDestino + "users/" + ind.userId).then(function(responses){
                                if(response.data.user.visits_counter == 0){
                                    $rootScope.mensajeBienvenida = 1;
                                    $location.path('/actualiza');
                                    storage.set("necesita", 2);

                                }
                                else if(response.data.user.visits_counter == 1){
                                    storage.remove("necesita");
                                    $rootScope.mensajeBienvenida = 2;
                                    ind.objEnvio = {visits_counter: 2};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", ind.objEnvio).then(function (response) {

                                    });

                                }
                                else if(response.data.user.visits_counter == 2){
                                    $rootScope.mensajeBienvenida = 0;
                                    $mdDialog.show({
                                        controller: dialogController,
                                        templateUrl: 'sociales.tmpl.html',
                                        parent: angular.element(document.body),
                                        escapeToClose: true,
                                        targetEvent: false
                                    });
                                    ind.objEnvio = {visits_counter: 3};
                                    $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", ind.objEnvio).then(function (response) {

                                    });
                                }
                                
                            });
                            
                        }while(ind.userId=="");*/

					});
				}, 200);
			} else {
				$rootScope.loginx = false;
				storage.clearAll();
				window.location = "../";
			};
		};

		ind.urlValidator = function(url) {
			return $http.get(url).then(function(response) {
				return null;
			}).catch(function(err) {
				if (err.status === 404) {
					$rootScope.userAvatar = "../assets/images/company_pic.png";
					return null
				};

				return $q.reject(err);
			})
		};

		ind.consultar();



		function dialogController($scope) {

			$scope.cancel = function() {
				$mdDialog.cancel();
			};
		}


		ind.cerrarSession = function() {
			$rootScope.loginx = false;
			storage.clearAll();
			window.location = "../";
		}

		ind.goDashboard = function() {
			window.location = "../#/dashboard";
		}

	}

	indexController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$mdDialog", "$scope"];
	angular
		.module("spa-SE3")
		.controller("indexController", indexController);

})();