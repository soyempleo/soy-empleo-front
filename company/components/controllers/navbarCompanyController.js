/**
 * Created by WilderAlejandro on 29/12/2015.
 */
(function () {
    'use strict'

    function navbarController($http, $location, storage, $rootScope) {

        /*setTimeout(
            function asyncBootstrap() {
                angular.bootstrap( document, [ "spa-SE3" ] );
            },
            ( 2 * 1000 )
        );*/

        /*if (!storage.get("tokenUser")) {
           $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        };*/
        
        var std = this;
        std.cerrarSession = function () {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "../";
        }

        std.goDashboard = function(){
            window.location = "../#/dashboard";
        }
    }

    navbarController.$inject = ["$http", "$location", "localStorageService", "$rootScope"];
    angular
        .module('spa-SE3')
        .controller('navbarController', navbarController);


})();