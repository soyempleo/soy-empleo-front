/**
 * Created by AnthonyTorres on 19/2/2016.
 */

var token = "";
var userId = "";
(function () { 
    'use strict'

    function hojaCompanyController($http, $location, storage, $rootScope, $scope, $mdDialog, $mdMedia, $window, $q) {

        var com = this;

        com.isEdit = [];
        com.isFocus = [];
        com.basicData = {};
        com.contacts = {};
        com.contactsOriginal = {};
        com.basicDataOriginal = {};
        com.listDepartamentos = {};
        com.aditionalData = {};
        com.listaCiudades = {};
        com.listaSectores = {};
        com.invalidNIT = false;
        $rootScope.dataCompany = "";
        $rootScope.idContact = "";
        $rootScope.percentBasic = 0;
        $rootScope.percentAditional = 0;
        $rootScope.companyPercent = 0;

        com.editarCampo = function(pos){
            com.isEdit[pos] = true;
        }

        com.guardarCambios = function(pos,text,value){
            if(text!="" && text!=null && com.basicDataOriginal[value] != "" && com.basicDataOriginal[value] != null && !com.invalidNIT){
               com.isEdit[pos] = false; 
            }else if(com.invalidNIT){
                com.isEdit[pos] = true;
            }            
        }

        com.cargaInicial = function(){
            $rootScope.dataCompany = storage.get("dataUser");
            setTimeout(function(){

                $http.get(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata")
                .then(function(response){
                    com.basicData = response.data.company_basicdata;
                    userId = com.basicData._id;
                    token = storage.get("tokenCompany");
                    com.basicDataOriginal = {
                        _id: com.basicData._id,
                        address: com.basicData.address,
                        city: com.basicData.city,
                        full_name: com.basicData.contact[0].full_name,
                        local_phone: com.basicData.contact[0].local_phone,
                        position: com.basicData.contact[0].position,
                        mobile_phone: com.basicData.contact[0].mobile_phone,
                        department: com.basicData.department,
                        description: com.basicData.description,
                        employees_count: com.basicData.employees_count,
                        logo: com.basicData.logo,
                        name: com.basicData.name,
                        nro_doc: com.basicData.nro_doc,
                        sector: com.basicData.sector,
                        social_name: com.basicData.social_name,
                        user_id: com.basicData.user_id
                    };

                    com.contactsOriginal = {
                        id : com.basicData.contact[0]._id,
                        full_name: com.basicData.contact[0].full_name,
                        local_phone: com.basicData.contact[0].local_phone,
                        position: com.basicData.contact[0].position,
                        mobile_phone: com.basicData.contact[0].mobile_phone
                    };

                    com.campoEditChanger();

                    if(com.basicData.logo!="" && com.basicData.logo!=null && 
                        ($rootScope.userAvatar == "../assets/images/company_pic.png" 
                            || $rootScope.userAvatar == "assets/images/company_pic.png" 
                            || $rootScope.userAvatar == null) ){
                        $rootScope.userAvatar = ServerDestino + "profile/companies/logos/" + com.basicData.logo;
                        com.urlValidator($rootScope.userAvatar);
                    };
                });

                $http.get(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/additionaldata")
                .then(function(response){
                    com.aditionalData=response.data.company_additionaldata;
                    com.porcentajeLlenado();
                });
                
                $http.get(ServerDestino + "departments/all").then(function(response) {
                    com.listDepartamentos = response.data;
                });

                $http.get(ServerDestino + "sectorscompany/all").then(function(response){
                    com.listaSectores = response.data;
                });

                
            },600);
        };

        com.campoEditChanger = function (){
            com.isEdit[0] = (com.basicData.social_name!="" && com.basicData.social_name != null) ? false:true;
            com.isEdit[1] = (com.basicData.nro_doc!="" && com.basicData.nro_doc != null) ? false:true;
            com.isEdit[2] = (com.basicData.employees_count!="" && com.basicData.employees_count != null) ? false:true;
            com.isEdit[3] = (com.basicData.department!="" && com.basicData.department != null) ? false:true;
            com.isEdit[4] = (com.basicData.city!="" && com.basicData.city != null) ? false:true;
            com.isEdit[5] = (com.basicData.address!="" && com.basicData.address != null) ? false:true;
            com.isEdit[6] = (com.basicData.sector!="" && com.basicData.sector != null) ? false:true;
            com.isEdit[7] = (com.basicData.description!="" && com.basicData.description != null) ? false:true;
            com.isEdit[8] = (com.basicData.contact[0].full_name!="" && com.basicData.contact[0].full_name != null) ? false:true;
            com.isEdit[9] = (com.basicData.contact[0].local_phone!="" && com.basicData.contact[0].local_phone != null) ? false:true;
            com.isEdit[10] = (com.basicData.contact[0].position!="" && com.basicData.contact[0].position != null) ? false:true;
            com.isEdit[11] = (com.basicData.contact[0].mobile_phone!="" && com.basicData.contact[0].mobile_phone != null) ? false:true;

            for (var i = 0; i < 11; i++) {
                com.isFocus.push(false);
            };
        }
        /*13 campos basic*/
        com.porcentajeLlenado = function(){
            $rootScope.percentBasic = 0;
            $rootScope.percentAditional = 0;
            $rootScope.percentBasic = com.basicData.address!="" && com.basicData.address != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.city!="" && com.basicData.city != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.contact[0].full_name!="" && com.basicData.contact[0].full_name != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.contact[0].local_phone!="" && com.basicData.contact[0].local_phone != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.contact[0].position!="" && com.basicData.contact[0].position != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.contact[0].mobile_phone!="" && com.basicData.contact[0].mobile_phone != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.department!="" && com.basicData.department != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.description!="" && com.basicData.description != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.employees_count!="" && com.basicData.employees_count != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.logo!="" && com.basicData.logo != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.nro_doc!="" && com.basicData.nro_doc != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.sector!="" && com.basicData.sector != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            $rootScope.percentBasic = com.basicData.social_name!="" && com.basicData.social_name != null ? $rootScope.percentBasic+1:$rootScope.percentBasic;
            
            $rootScope.percentAditional = com.aditionalData.employees_benefits.length!=0 ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            $rootScope.percentAditional = com.aditionalData.fb_url!="" && com.aditionalData.fb_url != null ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            $rootScope.percentAditional = com.aditionalData.lk_url!="" && com.aditionalData.lk_url != null ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            $rootScope.percentAditional = com.aditionalData.office_pictures[0]!="" && com.aditionalData.office_pictures[0] != null ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            $rootScope.percentAditional = com.aditionalData.tw_url!="" && com.aditionalData.tw_url != null ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            $rootScope.percentAditional = com.aditionalData.website!="" && com.aditionalData.website != null ? $rootScope.percentAditional+1:$rootScope.percentAditional;
            
            $rootScope.companyPercent = Math.round((($rootScope.percentBasic+$rootScope.percentAditional) * 100) / 19);
            var objEnvio = {_id: com.basicData._id,
                            address: com.basicData.address,
                            city: com.basicData.city,
                            full_name: com.basicData.contact[0].full_name,
                            local_phone: com.basicData.contact[0].local_phone,
                            position: com.basicData.contact[0].position,
                            mobile_phone: com.basicData.contact[0].mobile_phone,
                            department: com.basicData.department,
                            description: com.basicData.description,
                            employees_count: com.basicData.employees_count,
                            logo: com.basicData.logo,
                            name: com.basicData.name,
                            nro_doc: com.basicData.nro_doc,
                            sector: com.basicData.sector,
                            social_name: com.basicData.social_name,
                            user_id: com.basicData.user_id,
                            percentage: ""+$rootScope.companyPercent
                    };
            $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata/" + com.basicData.contact[0]._id + "/update", objEnvio)
            .success(function (response, status, headers, config) {
                    console.log("success");
                })
                .error(function (response, status, headers, config) {
                    console.log("error");
                });
        }; 

        com.urlValidator = function(url){
            return $http.get(url).then(function (response) {
                return null;
            }).catch(function (err){
                if(err.status === 404) {
                    $rootScope.userAvatar = "../assets/images/company_pic.png";
                    return null};

                return $q.reject(err);
            })
        };

        com.depSelected = function(department) {
            com.basicData.city = "";
            for (var i = 0; i < com.listDepartamentos.length; i++) {
                if (department == com.listDepartamentos[i]._id) {
                    com.basicData.department = com.listDepartamentos[i].name;
                }
            };
            setTimeout(function() {
                $http.get(ServerDestino + "cities/byDepartment/" + department).then(function(response) {
                    com.listaCiudades = response.data;
                });
            }, 400)
        };

        com.cargaInicial();

        com.focusEdit = function (pos){
            com.isFocus[pos] = true;
        };

        com.blurEdit = function (pos){
            com.isFocus[pos] = false;
        };


        com.validarNIT = function(valor) {
            var foundError = false;
            var showError = false;
                if(!com.isCheckOK(valor) && showError == false){
                    foundError = true; showError = true;
                    return false;
                }
                else
                    return true;
            }
             
        com.isCheckOK = function(valor) {
            var ceros = "000000";
            var nit = 0;
            var dvnum = 0;
            var str = valor;
            var nit = str.substring(0, str.indexOf("-"));
            var res2 = str.substring(str.indexOf("-")+1,str.length);
            var li_peso= new Array();
            li_peso[0] = 71;
            li_peso[1] = 67;
            li_peso[2] = 59;
            li_peso[3] = 53;
            li_peso[4] = 47;
            li_peso[5] = 43;
            li_peso[6] = 41;
            li_peso[7] = 37;
            li_peso[8] = 29;
            li_peso[9] = 23;
            li_peso[10] = 19;
            li_peso[11] = 17;
            li_peso[12] = 13;
            li_peso[13] = 7;
            li_peso[14] = 3;
         
            var ls_str_nit = ceros + nit;
            var li_suma = 0;
            for(var i = 0; i < 15; i++){
                        li_suma += ls_str_nit.substring(i,i+1) * li_peso[i];
            }
            var digito_chequeo = li_suma%11;
            if (digito_chequeo >= 2)
                digito_chequeo = 11 - digito_chequeo;
            if(res2 != digito_chequeo){
                return false;
            }
            else
                return true;
        }

        com.dataSaver = function(data,pos,num){
            var ext = data;
            var objEnvio = {
                            _id: com.basicData._id,
                            address: com.basicData.address,
                            city: com.basicData.city,
                            full_name: com.basicData.contact[0].full_name,
                            local_phone: com.basicData.contact[0].local_phone,
                            position: com.basicData.contact[0].position,
                            mobile_phone: com.basicData.contact[0].mobile_phone,
                            department: com.basicData.department,
                            description: com.basicData.description,
                            employees_count: com.basicData.employees_count,
                            logo: com.basicData.logo,
                            name: com.basicData.name,
                            nro_doc: com.basicData.nro_doc,
                            sector: com.basicData.sector,
                            social_name: com.basicData.social_name,
                            user_id: com.basicData.user_id,
                            percentage: ""+$rootScope.companyPercent
                    };
                if (data =! "" && data != null && data != com.basicDataOriginal[pos]) {
                    $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata/" + com.basicData.contact[0]._id + "/update", objEnvio)
                    .success(function (response, status, headers, config) {
                        com.basicDataOriginal[pos] = ext;
                        com.guardarCambios(num,ext,pos);
                        alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                        com.porcentajeLlenado();
                    })
                    .error(function (response, status, headers, config) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                    });
                }else if ((data == "" || data == null) && com.basicDataOriginal[pos] != "" && com.basicDataOriginal[pos] != null) {
                    com.guardarCambios(num,ext,pos); 
                    com.basicData[pos] = com.basicDataOriginal[pos];
                };
        }

        com.dataEditor = function (data,pos,num){
            var ext = data;
            var posi = pos;
            var nume = num
            /*if(pos!='nro_doc'){*/
                com.dataSaver(ext,posi,nume);
            /*}else{
                if(com.validarNIT(ext)){
                    com.invalidNIT = false;
                    com.dataSaver(ext,posi,nume);
                }else{
                    com.basicData.nro_doc = "";
                    com.invalidNIT = true;
                    alertify.error(' <div class="text-center font-size-16 text-white"> Al parecer el NIT que ingresaste es incorrecto, verificalo antes de continuar </div>');
                    com.guardarCambios(nume,ext,posi);
                }
            }*/
        };

        com.contactEditor = function(data,pos,num){
            var ext = data;
            var objEnvio = {
                        _id: com.basicData._id,
                        address: com.basicData.address,
                        city: com.basicData.city,
                        full_name: com.basicData.contact[0].full_name,
                        local_phone: com.basicData.contact[0].local_phone,
                        position: com.basicData.contact[0].position,
                        mobile_phone: com.basicData.contact[0].mobile_phone,
                        department: com.basicData.department,
                        description: com.basicData.description,
                        employees_count: com.basicData.employees_count,
                        logo: com.basicData.logo,
                        name: com.basicData.name,
                        nro_doc: com.basicData.nro_doc,
                        sector: com.basicData.sector,
                        social_name: com.basicData.social_name,
                        user_id: com.basicData.user_id,
                        percentage: ""+$rootScope.companyPercent
            };
            if(data != com.contactsOriginal[pos] && data!="" && data!=null){
                $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata/" + com.basicData.contact[0]._id + "/update", objEnvio)
                .success(function (response, status, headers, config) {
                    com.basicDataOriginal[pos] = ext;
                    com.guardarCambios(num,ext,pos);
                    alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                    com.porcentajeLlenado();
                })
                .error(function (response, status, headers, config) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                });
            }else if ((data == "" || data == null) && com.contactsOriginal[pos] != "" && com.contactsOriginal[pos] != null) {
                com.basicData.contact[0][pos] = com.contactsOriginal[pos];
            };
        };

        com.openModalLogo = function(ev) {
            $mdDialog.show({
                controller: controllerDialogLogo,
                templateUrl: 'dialog3.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function(answer) {}, function() {}); 
        };

        function controllerDialogLogo($scope, $mdDialog, $mdMedia, $http, $window, upload) {
            controllerDialogLogo.prototype.$scope = $scope;

            $scope.photopos = 0;
            $scope.uploadGo = false;
            $scope.uploadChange = false;
            $scope.officePhoto = "";
            $scope.isRefreshing = false;
            
            $scope.cancel = function() {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
            };

            $scope.file_changed = function(element) {
                if(element.value!=''){
                    $scope.$apply(function(){
                        $scope.uploadGo = true;
                    });                
                }
            };

            $scope.fileUploading = function(){
                        $scope.uploadGo = false;
                        $scope.uploadChange = true;
            }

            setTimeout(function () {
                $scope.$apply(function () {
                    $("#logo").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="../assets/images/company_pic.png" alt="Tu Foto" style="width:160px">',
                        layoutTemplates: {main2: '{preview} {remove} {browse}'},
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);
        };

        controllerDialogLogo.prototype.setFile = function(element) {
            var $scope = this.$scope;
            $scope.$apply(function() {
                $scope.theFile = element.files[0];
            });
        };
    }

    hojaCompanyController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$scope","$mdDialog", "$mdMedia", "$window", "$q"];
    angular
        .module("spa-SE3")
        .controller("hojaCompanyController", hojaCompanyController);

})();

function previewFileLogoCompany() {
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    if (file) {
        $("#divButton").attr("style", "display: inline-block;");
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
};

function subirImagenLogoCompany() {
    $("#divButton").attr("style", "display: none;");
    $("#divLoader").attr("style", "display: inline-block;"); 
    $("#logoBtn").prop('disabled', true);    
    var formData = new FormData($("form#reg-empre")[0]);
    setTimeout(function() {
        $.ajax({
            url: ServerDestino + "companies/" + userId + "/logo",
            beforeSend: function(request) {
                request.setRequestHeader('Authorization', 'Bearer ' + token);
            },
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                if (respuesta.message == "IMAGE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> El archivo no es una imagen </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else if (respuesta.message == "SIZE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Imagen demasiado pesada </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else if (respuesta.message == "ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Foto actualizada con éxito </div>');
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                    location.reload();
                }
            }
        });
    }, 100);
}