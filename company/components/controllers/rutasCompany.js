(function (){
    'use strict'
    
    function config ($routeProvider,$httpProvider) {
        $routeProvider
        .when('/', {
            templateUrl: 'app/company.html'
        })
        .when('/company', {
            templateUrl: 'app/company.html'
        })
        .when('/basicData', {
            templateUrl: 'app/company.html'
        })
        .when('/aditionalData', {
            templateUrl: 'app/aditionalDataCompany.html'
        })
        .otherwise({
            templateUrl: '404.html'
        });



    }
    angular
    .module('spa-SE3')
    .config(config);
})();