/*
 * Created by AnthonyTorres on 19/2/2016.
 */

var token = "";
var userId = "";
(function () {
    'use strict'

    function aditionalDataController($http, $location, storage, $rootScope, $scope, upload, $mdDialog, $mdMedia, $window, $q) {

        var adt = this;

        adt.imagenes = [];
        adt.imagenes[0] = "office0.png";
        adt.imagenes[1] = "office1.png";
        adt.imagenes[2] = "office2.png";
        adt.imagenes[3] = "office3.png";

        adt.aditionalData = {};
        adt.basicData = {};
        adt.aditionalDataOriginal = {};
        adt.values = {};
        adt.isEdit = [];
        adt.isFocus = [];
        adt.photos = {};
        adt.imagePos = 0;
        adt.uploadGo = false;
        adt.officePhoto = '';
        adt.repetidorImagen = [];
        adt.officePictureLength = 0;
        $rootScope.office_picturesLength = 0;
        $rootScope.dataCompany = "";
        $rootScope.idContact = "";
        $rootScope.arrayPhotos = {};
        $rootScope.percentBasic = 0;
        $rootScope.percentAditional = 0;
        $rootScope.companyPercent = 0;

        adt.editarCampo = function (pos) {
            adt.isEdit[pos] = true;
        }

        adt.guardarCambios = function (pos, text, value) {
            if (text != "" && text != null && adt.aditionalDataOriginal[value] != "" && adt.aditionalDataOriginal[value] != null) {
                adt.isEdit[pos] = false;
            }
        }

        setTimeout(function () {
            $scope.$apply(function () {
                $("#picture").fileinput({
                    overwriteInitial: true,
                    maxFileSize: 1000,
                    showClose: false,
                    showCaption: false,
                    browseLabel: '',
                    removeLabel: '',
                    browseIcon: '<label class="btn-agregarEstudio efectoMouse font-size-18 text-blue-soy" style="cursor: pointer;"><img src="../assets/images/plus.png" alt="Tu Foto" style="width:20px;height:20px;"> Fotos</label>',
                    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                    removeTitle: 'Cancelar o restaurar cambios',
                    elErrorContainer: '#kv-avatar-errors',
                    msgErrorClass: 'alert alert-block alert-danger',
                    defaultPreviewContent: '<img src="../assets/images/plus.png" alt="Tu Foto" style="width:20px;height:20px;">',
                    layoutTemplates: {main2: '{preview} {remove} {browse}'},
                    allowedFileExtensions: ["jpg", "png", "gif"]
                });
            })
        }, 600);

        adt.cargaInicial = function () {
            var auxPhotos = 0;
            $rootScope.dataCompany = storage.get("dataUser");
            setTimeout(function () {
                adt.valuesCreator();
                $http.get(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/additionaldata").then(function (response) {
                    adt.aditionalData = response.data.company_additionaldata;
                    userId = adt.aditionalData._id;
                    token = storage.get("tokenCompany");
                    adt.aditionalDataOriginal = {
                        website: adt.aditionalData.website,
                        tw_url: adt.aditionalData.tw_url,
                        fb_url: adt.aditionalData.fb_url,
                        lk_url: adt.aditionalData.lk_url
                    }

                    if (adt.aditionalData.office_pictures.length > 0) {
                        for (var i = 0; i < adt.aditionalData.office_pictures.length; i++) {
                            if (adt.aditionalData.office_pictures[i] != "" && adt.aditionalData.office_pictures[i] != null) {
                                adt.photos[i] = {
                                    position: i,
                                    url: ServerDestino + "profile/companies/officepictures/" + adt.aditionalData.office_pictures[i]
                                };

                                auxPhotos += 1;
                            }
                        }
                        $rootScope.arrayPhotos = adt.photos;
                        $rootScope.office_picturesLength = auxPhotos;
                        adt.officePictureLength = $rootScope.office_picturesLength;
                        adt.getNumber();
                    } else {
                        $rootScope.office_picturesLength = adt.aditionalData.office_pictures.length;
                        adt.officePictureLength = $rootScope.office_picturesLength;
                    }

                    adt.campoEditChanger();
                    adt.valuesLoader();


                });
                $http.get(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata")
                    .then(function (response) {
                        adt.basicData = response.data.company_basicdata;
                        adt.porcentajeLlenado();
                    });
            }, 600);
        }

        adt.campoEditChanger = function () {
            adt.isEdit[0] = (adt.aditionalData.website != "" && adt.aditionalData.website != null) ? false : true;
            adt.isEdit[1] = (adt.aditionalData.tw_url != "" && adt.aditionalData.tw_url != null) ? false : true;
            adt.isEdit[2] = (adt.aditionalData.fb_url != "" && adt.aditionalData.fb_url != null) ? false : true;
            adt.isEdit[3] = (adt.aditionalData.lk_url != "" && adt.aditionalData.lk_url != null) ? false : true;

            for (var i = 0; i < 11; i++) {
                adt.isFocus.push(false);
            }
            ;
        };

        adt.porcentajeLlenado = function () {
            $rootScope.percentBasic = 0;
            $rootScope.percentAditional = 0;
            $rootScope.percentBasic = adt.basicData.address != "" && adt.basicData.address != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.city != "" && adt.basicData.city != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.contact[0].full_name != "" && adt.basicData.contact[0].full_name != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.contact[0].local_phone != "" && adt.basicData.contact[0].local_phone != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.contact[0].position != "" && adt.basicData.contact[0].position != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.contact[0].mobile_phone != "" && adt.basicData.contact[0].mobile_phone != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.department != "" && adt.basicData.department != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.description != "" && adt.basicData.description != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.employees_count != "" && adt.basicData.employees_count != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.logo != "" && adt.basicData.logo != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.nro_doc != "" && adt.basicData.nro_doc != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.sector != "" && adt.basicData.sector != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = adt.basicData.social_name != "" && adt.basicData.social_name != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;

            $rootScope.percentAditional = adt.aditionalData.employees_benefits.length != 0 ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = adt.aditionalData.fb_url != "" && adt.aditionalData.fb_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = adt.aditionalData.lk_url != "" && adt.aditionalData.lk_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = adt.aditionalData.office_pictures[0] != "" && adt.aditionalData.office_pictures[0] != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = adt.aditionalData.tw_url != "" && adt.aditionalData.tw_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = adt.aditionalData.website != "" && adt.aditionalData.website != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;

            $rootScope.companyPercent = Math.round((($rootScope.percentBasic + $rootScope.percentAditional) * 100) / 19);
            var objEnvio = {
                _id: adt.basicData._id,
                address: adt.basicData.address,
                city: adt.basicData.city,
                full_name: adt.basicData.contact[0].full_name,
                local_phone: adt.basicData.contact[0].local_phone,
                position: adt.basicData.contact[0].position,
                mobile_phone: adt.basicData.contact[0].mobile_phone,
                department: adt.basicData.department,
                description: adt.basicData.description,
                employees_count: adt.basicData.employees_count,
                logo: adt.basicData.logo,
                name: adt.basicData.name,
                nro_doc: adt.basicData.nro_doc,
                sector: adt.basicData.sector,
                social_name: adt.basicData.social_name,
                user_id: adt.basicData.user_id,
                percentage: "" + $rootScope.companyPercent
            };
            $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/basicdata/" + adt.basicData.contact[0]._id + "/update", objEnvio)
                .success(function (response, status, headers, config) {
                    console.log("success");
                })
                .error(function (response, status, headers, config) {
                    console.log("error");
                });
        };

        adt.getNumber = function () {
            adt.repetidorImagen = [];
            var number = 4 - adt.officePictureLength;
            for (var i = 0; i < number; i++) {
                adt.repetidorImagen.push(i);
            }
        }

        adt.focusEdit = function (pos) {
            adt.isFocus[pos] = true;
        };

        adt.blurEdit = function (pos) {
            adt.isFocus[pos] = false;
        };

        adt.valuesCreator = function () {
            adt.values[0] = {text: "Estacionamiento para empleados", value: false, nombre: "value0", id: "l0"};
            adt.values[1] = {text: "Horario flexible", value: false, nombre: "value1", id: "l1"};
            adt.values[2] = {text: "Salud Prepagada", value: false, nombre: "value2", id: "l2"};
            adt.values[3] = {text: "Servicio de alimentación (casino)", value: false, nombre: "value3", id: "l3"};
            adt.values[4] = {text: "Capacitaciones y cursos", value: false, nombre: "value4", id: "l4"};
            adt.values[5] = {text: "Seguro de vida", value: false, nombre: "value5", id: "l5"};
            adt.values[6] = {text: "Días libres", value: false, nombre: "value6", id: "l6"};
            adt.values[7] = {text: "Becas o ayudas financieras", value: false, nombre: "value7", id: "l7"};
        };

        adt.valuesLoader = function () {
            for (var i = 0; i < adt.aditionalData.employees_benefits.length; i++) {
                if (adt.aditionalData.employees_benefits[i] == "Estacionamiento para empleados") {
                    adt.values[0].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Horario flexible") {
                    adt.values[1].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Salud Prepagada") {
                    adt.values[2].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Servicio de alimentación (casino)") {
                    adt.values[3].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Capacitaciones y cursos") {
                    adt.values[4].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Seguro de vida") {
                    adt.values[5].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Días libres") {
                    adt.values[6].value = true;
                } else if (adt.aditionalData.employees_benefits[i] == "Becas o ayudas financieras") {
                    adt.values[7].value = true;
                }
            }
            ;
        };

        adt.dataEditor = function (data, pos, num) {
            var objEnvio = {};
            var ext = data;

            if (data = !"" && data != null && data != adt.aditionalDataOriginal[pos]) {
                adt.aditionalDataOriginal[pos] = ext;

                if (adt.aditionalData.employees_benefits.length > 0) {
                    for (var i = 0; i < adt.aditionalData.employees_benefits.length; i++) {
                        var aux = i + 1;
                        adt.aditionalDataOriginal["benefit" + aux] = adt.aditionalData.employees_benefits[i];
                    }
                    adt.aditionalDataOriginal["number_benefits"] = adt.aditionalData.employees_benefits.length;
                }

                $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/additionaldata/update", adt.aditionalDataOriginal)
                    .success(function (response, status, headers, config) {
                        adt.guardarCambios(num, data, pos);
                        alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                        adt.cargaInicial();
                    })
                    .error(function (response, status, headers, config) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                    });
            } else if ((data == "" || data == null) && adt.aditionalDataOriginal[pos] != "" && adt.aditionalDataOriginal[pos] != null) {
                adt.aditionalData[pos] = adt.aditionalDataOriginal[pos];
                adt.isEdit[pos] = false;
            }
            ;
        };

        adt.benefitsEditor = function (pos, value, text) {
            adt.numberBenefits = 0;
            var objEnvio = {};
            for (var i = 0; i < 8; i++) {
                if (adt.values[i].value) {
                    adt.numberBenefits += 1;
                    objEnvio["benefit" + adt.numberBenefits] = adt.values[i].text;

                }
            }
            ;

            objEnvio["number_benefits"] = adt.numberBenefits;
            objEnvio["website"] = adt.aditionalData.website;
            objEnvio["fb_url"] = adt.aditionalData.fb_url;
            objEnvio["tw_url"] = adt.aditionalData.tw_url;
            objEnvio["lk_url"] = adt.aditionalData.lk_url;

            if (adt.numberBenefits > 0) {
                setTimeout(function () {

                    $http.put(ServerDestino + "companies/" + $rootScope.dataCompany._id + "/additionaldata/update", objEnvio)
                        .success(function (response, status, headers, config) {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                            adt.cargaInicial();
                        })
                        .error(function (response, status, headers, config) {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                        });
                }, 200);
            }
        };

        adt.deletePicture = function (pos) {
            swal({
                title: "Eliminar Imagen",
                text: "¿Esta seguro que desea eliminar esta imagen?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, Eliminar!",
                closeOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $http.put(ServerDestino + "companies/" + userId + "/officepicture", {position: pos}).then(function (response) {
                        if (response.data.message = "OK") {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                            adt.cargaInicial();
                            window.location = "#/basicData";
                            window.location = "#/aditionalData";
                        } else {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                        }
                    });
                }
            })
        }

        $scope.file_changed = function (element, scope) {
            if (element.value != '') {
                $scope.$apply(function () {
                    adt.uploadGo = true;
                });
            }
        };

        adt.openModal = function (ev) {
            $mdDialog.show({
                controller: controllerDialog,
                templateUrl: 'dialog2.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function (answer) {
            }, function () {
            });
        };

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window, upload) {
            controllerDialog.prototype.$scope = $scope;

            $scope.photopos = 0;
            $scope.uploadGo = false;
            $scope.uploadChange = false;
            $scope.officePhoto = "";

            $scope.cancel = function () {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
            };

            $scope.uploadFile2 = function () {

                var aux2 = 0;
                if ($rootScope.office_picturesLength == 4) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Lo sentimos, has alcanzado el límite de fotos permitidas. Intenta borrando una de las antiguas. </div>');
                } else {
                    $scope.fileUploading();
                    for (var i = 0; i < $rootScope.office_picturesLength; i++) {
                        if ($rootScope.arrayPhotos[i] == null) {
                            aux2 = i;
                            break;
                        } else if ($rootScope.arrayPhotos[i].position != i) {
                            aux2 = i;
                            break;
                        } else {
                            aux2 = i + 1;
                        }
                    }
                    $scope.photopos = aux2;
                    var envio = {
                        file: $scope.officePhoto,
                        url: ServerDestino + "companies/" + userId + "/officepicture" + aux2,
                        _token: token,
                        nombreCampo: "picture"
                    };

                    upload.uploadFile(envio).then(function (response) {
                        if (response.data.message == "OK") {
                            window.location = "#/basicData";
                            window.location = "#/aditionalData";
                            $scope.cancel();
                        } else {
                            $scope.cancel();
                        }
                    });
                }
                ;
            };

            $scope.file_changed = function (element) {
                if (element.value != '') {
                    $scope.$apply(function () {
                        $scope.uploadGo = true;
                    });
                }
            };

            $scope.fileUploading = function () {
                $scope.uploadGo = false;
                $scope.uploadChange = true;
            }

            setTimeout(function () {
                $scope.$apply(function () {
                    $("#avatar").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="../assets/images/company_pic.png" alt="Tu Foto" class="pic-260">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);
        };

        controllerDialog.prototype.setFile = function (element) {
            var $scope = this.$scope;
            $scope.$apply(function () {
                $scope.theFile = element.files[0];
            });
        };

        adt.cargaInicial();
    }

    aditionalDataController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$scope", "upload", "$mdDialog", "$mdMedia", "$window", "$q"];
    angular
        .module("spa-SE3")
        .controller("aditionalDataController", aditionalDataController);

})();