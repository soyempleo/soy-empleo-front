/**
 * Created by Anthony Torres 11/04/2016
 */

 angular
 .module('spa-SE')
 .directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

angular
     .module('spa-SE')
     .directive('ngRedirect', function($location) {
         return function(scope, element, attrs) {
             element.on('click', function() {
                 scope.$apply(function() {
                     $location.path(attrs.ngRedirect);
                 });
             })

         }
});


angular
.module('spa-SE')
.directive('disableOnPromise', function ($parse) {
    return {
        restrict: 'A',
        compile: function($element, attr) {
            var fn = $parse(attr.disableOnPromise);
            return function clickHandler(scope, element, attrs) {
                element.on('click', function(event) {
                    attrs.$set('disabled', true);
                    scope.$apply(function() {
                        fn(scope, {$event:event}).finally(function() {
                            attrs.$set('disabled', false);
                        });
                    });
                });
            };
        }
    };
});

angular
    .module('spa-SE')
    .directive('uploaderModel', ["$parse", function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, iElement, iAttrs) {
            iElement.on("change", function (e) {
                $parse(iAttrs.uploaderModel).assign(scope, iElement[0].files[0]);
            });
        }
    };
}]);

angular
    .module('spa-SE')
    .directive("onSrcError", function($filter) {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                scope.$parent.$parent[attrs.onSrcError].is404 = true;
                scope.$apply();
            });
        }
    };
});

