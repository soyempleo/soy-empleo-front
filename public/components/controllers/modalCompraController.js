'use-strict';

noc.openModal = function(ev, titulo) 
        {
            noc.prepararOferta();
            noc.idUser = storage.get("dataUser")._id;
            $rootScope.reference = noc.createReferemce(titulo, noc.idUser);
            $rootScope.signature = noc.createHash($rootScope.reference);
            $rootScope.idUserp = storage.get("dataUser")._id;
            $rootScope.nombreVacante = titulo;


            noc.validarCampos = function() {
                noc.divScroll = "";
                noc.camposValidos[0] = noc.nivelRequerido == '' || noc.nivelRequerido == null ? true : false;
                noc.camposValidos[1] = noc.jobDescription == "" || noc.jobDescription == null ? true : false;
                noc.camposValidos[2] = noc.nivelRequerido != '' && noc.nivelRequerido != 'Ninguno' && noc.nivelRequerido != 'Bachiller' &&
                noc.nivelRequerido != null && noc.preferenciasSelected.length == 0 ? true : false;
                noc.camposValidos[3] = noc.funcionesSelected.length == 0 ? true : false;
                noc.camposValidos[4] = noc.departamento == "" || noc.departamento == null || noc.ciudad == "" || noc.ciudad == null ? true : false;
                noc.camposValidos[5] = noc.contrato == "" || noc.contrato == null ? true : false;
                noc.camposValidos[6] = noc.salario == "" || noc.salario == null || noc.total < 343500 ? true : false;
                noc.camposValidos[7] = noc.mostrarSalario == '' || noc.mostrarSalario == null ? true : false;
                var aux = 0;
                for (var i = 0; i < noc.camposValidos.length; i++) {
                    if (noc.camposValidos[i]) {
                        aux += 1;
                        if (noc.divScroll == "") {
                            noc.divScroll = '#div_' + i;
                        }
                    }
                }

                noc.popup = aux > 0 ? true : false;
            }

            noc.openModalInto = function(ev, titulo) {
                noc.validarCampos();
                var scrolling = noc.divScroll;
                if (noc.popup) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Parece que aún hay campos importantes que debes llenar para continuar </div>');
                    $('html, body').animate({
                        scrollTop: $(scrolling.toString()).offset().top
                    }, 900);
                    noc.divScroll = "";
                } else {
                    noc.prepararOferta();
                    noc.idUser = storage.get("dataUser")._id;
                    $rootScope.reference = noc.createReferemce(titulo, noc.idUser);
                    $rootScope.signature = noc.createHash($rootScope.reference);

                    $mdDialog.show({
                        controller: controllerDialog,
                        templateUrl: 'dialog1.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        escapeToClose: true
                    }).then(function(answer) {}, function() {});
                }

            };

            noc.createReferemce = function(titulo, idUser) {
                var today = moment().format('DD MM YYYY');
                return titulo + idUser + today;
            }

            noc.createHash = function(reference) {
                return md5("4Vj8eK4rloUd272L48hsrarnUA~508029~" + reference + "~99000~COP");
            }

            function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window, $timeout, $rootScope) {
                controllerDialog.prototype.$scope = $scope;

                $scope.urlFront = UrlActual + "public/app/payView.html";
                $scope.urlBack = ServerDestino + "payments/confirm";
                $scope.idUserp = $rootScope.idUserp
                $scope.nombreVacante = $rootScope.nombreVacante;

                $scope.reference = $rootScope.reference;
                $scope.signature = $rootScope.signature;
                $scope.domain = 0;
                $scope.isRefreshing = false;

                $scope.mostrar = function(valx) {
                }

                $scope.procesarOferta = function(val, reference) {
                    $scope.isRefreshing = true;
                    $rootScope.ofertaEnviar.offer_type = val;
                    var req = {
                        method: 'POST',
                        url: ServerDestino + 'offers',
                        headers: {
                            'Accept': 'application/json, text/javascript',
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        data: $rootScope.ofertaEnviar
                    }
                    setTimeout(function() {
                        $http(req).then(function(response) {
                            if (response.data.message == "OK") {
                                window.location = UrlActual + "public/app/payView.html";
                            }

                        }, function(response) {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar publicar oferta </div>');
                        }).finally(
                        function() {
                            $scope.isRefreshing = false;
                        });
                    }, 500);
                }

                $scope.guardar = function() {
                    if (($scope.monthExp === "" || $scope.monthExp === undefined || $scope.monthExp === null) || ($scope.addHabilidad == "" || $scope.addHabilidad === undefined || $scope.addHabilidad === null)) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Faltan datos </div>');
                    } else {
                        var aux = [];
                        for (var i = 1; i < $scope.domain + 1; i++) {
                            aux.push({
                                value: i,
                                disp: true
                            });
                        }
                        for (var i = $scope.domain + 1; i < 11; i++) {
                            aux.push({
                                value: i,
                                disp: false
                            });
                        }
                        $rootScope.listaHabilidades.splice($rootScope.listaHabilidades.length + 1, $rootScope.listaHabilidades.length + 1, {
                            name: $scope.addHabilidad,
                            month: $scope.monthExp,
                            domain: $scope.domain,
                            arrayDomain: aux
                        });
                        alertify.success(' <div class="text-center font-size-16 text-white"> Habilidad Registrada </div>');
                        $mdDialog.hide();
                    }
                };
                $scope.cancel = function() {
                    $mdDialog.cancel();
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                };
                $scope.cambiarColor = function(valor) {
                    $scope.domain = valor;
                    for (var i = 1; i < valor + 1; i++) {
                        var btn = $("#v" + i);
                        btn.removeClass("btn-escala");
                        btn.addClass("btn-escala-green");
                    }
                    for (var i = valor + 1; i < 11; i++) {
                        var btn = $("#v" + i);
                        btn.addClass("btn-escala");
                        btn.removeClass("btn-escala-green");
                    }
                };
            };

            controllerDialog.prototype.setFile = function(element) {
                var $scope = this.$scope;
                $scope.$apply(function() {
                    $scope.theFile = element.files[0];
                });
            };

            /*---------------------------Directiva autocompletado angularMaterial-------------------------------------------*/

            setTimeout(function() {
                $http.get(ServerDestino + "studies/all").then(function(response) {
                    noc.listaInstitutos = response.data;
                    noc.states = cargarInstiuciones();
                    noc.querySearch = querySearch;

                    $scope.estudios = noc.states
                }, function(error) {});

                function cargarInstiuciones() {
                    var value = [];
                    return noc.listaInstitutos.map(function(state) {
                        value = {
                            name: state.description.toLowerCase()
                        };
                        return value;
                    });
                }
            }, 500);

            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);

                return function filterFn(state) {
                    return (state.name.indexOf(lowercaseQuery) > -1);
                };
            }

            function querySearch(query) {
                var results = query ? noc.states.filter(createFilterFor(query)) : noc.states;
                return results;
            }

            /*-------------------------------Fin de directiva---------------------------------------*/
        }