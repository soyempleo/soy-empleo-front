/**
 * Created by WilderAlejandro on 25/11/2015.
 */
var token = "";
var userId = "";
var dashBoard = false;

(function() {
    'use strict'

    function navbarCompanyController(storage, 
        $rootScope, 
        $http, 
        $location, 
        $mdDialog, 
        $mdMedia, 
        $scope, 
        $window, 
        $q, 
        $routeParams,
        $filter,
        upload,
        $sce,
        $uibModal,
        $document,
        $payments) {

        var nbc = this;
        $rootScope.isLoged = false;
        nbc.loadedDiv2 = false;
        nbc.isRefreshingPostu = false;
        $rootScope.userAvatar = "assets/images/company_pic.png";
        $rootScope.provieneDashboard = false;
        $rootScope.profileGo2 = "";
        nbc.profileData = {};
        nbc.basicData = {};
        nbc.listaSectores = {};
        nbc.listaCiudades = {};
        nbc.listDepartamentos = {};
        $rootScope.creditCard = {};
        $rootScope.ofertasDisponibles = {};
        $rootScope.membershipDatas = {};
        nbc.values = {};
        nbc.aditionalData = {};
        nbc.photos = {};
        nbc.imagePos = 0;
        nbc.fechaNac = "";
        nbc.nombreCompleto = "";
        nbc.birthDay = "";
        nbc.birthMonth = ""; 
        nbc.birthMonth = "";
        nbc.birthYear = "";
        nbc.edad = "";
        nbc.antiguoTitle = "";
        nbc.tituloProfesional = "";
        nbc.percentage = "";
        nbc.cityTest = "";
        nbc.nombre = "";
        nbc.passed = false;
        nbc.loged = false;
        nbc.percentage = 0;
        nbc.urlActual = window.location.href;
        nbc.offerts = {};
        nbc.currentPage = 1;
        nbc.totalItems = 0;
        nbc.entryLimit = 5; // items per page 
        nbc.noOfPages = 0;
        nbc.items = [];
        nbc.numOffers = 0;
        nbc.companyBasicData = {};
        nbc.aditionalData = {};
        nbc.percent = 0;
        nbc.cantOffert = 0;
        /*nbc.freeOffers = 0;*/
        $rootScope.freeOferta = 0;
        nbc.officePictureLength = 0;
        nbc.repetidorImagen = [];
        nbc.postuladosCant = [];
        nbc.postuladosTotal = 0;
        $rootScope.office_picturesLength = 0;
        $rootScope.arrayPhotos = {};
        $rootScope.percentBasic = 0;
        $rootScope.percentAditional = 0;
        $rootScope.companyPercent = 0;
        $rootScope.porcentajeL = 0;
        $rootScope.numeroOffertas = 0;
        $rootScope.tOffert = "";
        $rootScope.isEdit = false;
        $rootScope.isPageloading = true;
        $rootScope.blockOffer = true;
        nbc.isCheckedTab = true;
        $rootScope.textoNota = '';
        
        $rootScope.postulates = [];
        $rootScope.ofertaSelect = [];

        $rootScope.currentPagePostu = 1;
        $rootScope.totalItemsPostu = 0;
        $rootScope.entryLimitPostu = 5; // items per page 
        $rootScope.noOfPagesPostu = 0;
        $rootScope.itemsPostu = [];
        $rootScope.profileDataPostu = [];
        $rootScope.userDataPostu = [];
        $rootScope.listaAdmin = [];
        $rootScope.listaPerson = [];
        $rootScope.preguntasList = [];
        $rootScope.selected = 0;
        $rootScope.selectedAvatar = "assets/images/user_male.png";
        $rootScope.lastItem = false;
        $rootScope.IsContratado = false;
        $rootScope.respondidasLength = false;
        $rootScope.listaInicial = [];
        $rootScope.listaPreseleccionados = [];
        $rootScope.listaEntrevistados = [];
        $rootScope.listaContratados = [];
        $rootScope.styles = {};
        $rootScope.estado1 = "";
        $rootScope.estado2 = "";

        /**
            objetos de paginadores
        **/
            /*lista admin*/
            $rootScope.currentPage1 = 1;
            $rootScope.totalItems1 = 0;
            $rootScope.entryLimit1 = 5; // items per page 
            $rootScope.noOfPages1 = 0;
            $rootScope.items1 = [];

            /*lista person*/
            $rootScope.currentPage2 = 1;
            $rootScope.totalItems2 = 0;
            $rootScope.entryLimit2 = 5; // items per page 
            $rootScope.noOfPages2 = 0;
            $rootScope.items2 = [];

            /*lista preseleccionados*/
            $rootScope.currentPagePre = 1;
            $rootScope.totalItemsPre = 0;
            $rootScope.entryLimitPre = 5;
            $rootScope.noOfPagesPre = 0;

            /*lista entrevistados*/
            $rootScope.currentPageEnt = 1;
            $rootScope.totalItemsEnt = 0;
            $rootScope.entryLimitEnt = 5;
            $rootScope.noOfPagesEnt = 0;

            /*lista contratados*/
            $rootScope.currentPageCon = 1;
            $rootScope.totalItemsCon = 0;
            $rootScope.entryLimitCon = 5;
            $rootScope.noOfPagesCon = 0;
        /**
            objetos de paginadores
        **/

        $(document).ready(function () {
            $.scrollTo('html, body',900);
        });

        $(window).load(function () {
            $('.userAvatar').attr('src',$rootScope.userAvatar);
            $scope.$apply(function(){
                $rootScope.isPageloading = false;
            });
        });

        
        nbc.descEnvio = "";
        nbc.dirEnvio = "";
        /*nbc.guardarDescripcion = function(id){
            nbc.descripcion = nbc.descEnvio;
            document.getElementById(id.toString()).classList.remove('show');
            nbc.descEnvio = ""; 
        }*/

        nbc.guardarDireccion = function(id){ 
            nbc.direccion = nbc.dirEnvio;
            document.getElementById(id.toString()).classList.remove('show');
            nbc.dirEnvio = ""; 
        }

        nbc.openDropdown = function(id) {
            document.getElementById("myDropdown"+id.toString()).classList.toggle("show");
            /*myFunction(id);*/
           /* setTimeout(function () {
                $scope.$apply(function () {
                    document.getElementById("myDropdown"+id.toString()).classList.toggle("show");
                });
            }, 20);*/
            
        }

        nbc.modalPopover = function (){
            $('#dialogPopover').modalPopover('show');
            $('#dialogPopover').modalPopover({
                target: '#target22',
                placement: 'bottom'
            });
        }
        
        nbc.goToBlog = function(){
            $('#tab-1').prop('checked', true);
            var win = window.open('https://blog.soyempleo.com/#/', '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Porfavor permitir popups para este website');
            }
        }

        nbc.closeDropdown = function(id) {
            document.getElementById("myDropdown"+id.toString()).classList.remove('show');            
        }
        
        nbc.depSelected = function(department) {
            nbc.cityTest = "";
            var foundItem = $filter('filter')(nbc.listDepartamentos, { name: department  }, true)[0];
            setTimeout(function() {
                $http.get(ServerDestino + "cities/byDepartment/" + foundItem._id).then(function(response) {
                    nbc.listaCiudades = response.data;
                });
            }, 400)
        };

        nbc.consultarEstado = function() {
            if ($rootScope.isOfferted != null && $rootScope.isOfferted == true) {
                $rootScope.isOfferted = false;
                swal({
                    title: "Tu vacante ha sido recibida, ¡choca esas cuatro!",
                    text: "Ahora pasará por revisión y en momentos la podrás ver activa en el portal",
                    imageUrl: "assets/images/soyempleo-chocala-simple.png",
                    confirmButtonColor: "#00BF52",
                    confirmButtonText: "Aceptar"
                });

            }else if($routeParams.status == "offer" && $rootScope.tOffert == "own"){
                swal({
                    title: "Tu vacante ha sido recibida, ¡choca esas cuatro!",
                    text: "Ahora pasará por revisión y en momentos la podrás ver activa en el portal",
                    imageUrl: "assets/images/soyempleo-chocala-simple.png",
                    confirmButtonColor: "#00BF52",
                    confirmButtonText: "Aceptar"
                }).then(
                    function () {
                        window.location.href = "#/dashboard-company"; 
                    },function(){}
                );
            }else if($rootScope.tOffert != "" && $rootScope.tOffert == 'enterprise'){
                swal({
                    title: "Tu solicitud de compra ha sido procesada, ¡choca esas cuatro!",
                    text: "Ahora pasará por revisión y en momentos hacer uso de tus vacantes",
                    imageUrl: "assets/images/soyempleo-chocala-simple.png",
                    confirmButtonColor: "#00BF52",
                    confirmButtonText: "Aceptar"
                }).then(
                    function () {
                        window.location.href = "#/dashboard-company"; 
                    },function(){}
                );
            }else if($rootScope.isEditted){
                $rootScope.isEditted = false;
                swal({
                    title: "Tu vacante ha sido editada, ¡choca esas cuatro!",
                    text: "Ahora pasará por revisión y en momentos la podrás ver activa en el portal",
                    imageUrl: "assets/images/soyempleo-chocala-simple.png",
                    confirmButtonColor: "#00BF52",
                    confirmButtonText: "Aceptar"
                });
            }
        };

        nbc.consultarEstado();

        if (!storage.get("tokenCompany")) {
            $rootScope.loginx = false;
            storage.clearAll();
            window.location = "#/";
        };

        nbc.consultar = function() {
            if (storage.get("tokenCompany")) {
                $rootScope.loginx = true;
                $rootScope.isLoged = true;
                nbc.loged = true;
                nbc.passed = true;
                token = storage.get("tokenCompany");
                return false;
            } else {
                $rootScope.isLoged = false;
                nbc.loged = false;
                nbc.passed = true; 
                return true;
            }
        };

        nbc.getNumber = function () {
            nbc.repetidorImagen = [];
            var number = 4 - nbc.officePictureLength;
            for (var i = 0; i < number; i++) {
                nbc.repetidorImagen.push(i);
            }
        }

        nbc.deletePicture = function (pos) {
            swal({
                title: "Eliminar Imagen",
                text: "¿Esta seguro que desea eliminar esta imagen?",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Si, Eliminar!"
            }).then(
                function (isConfirm) {
                    if (isConfirm) {
                        $http.put(ServerDestino + "companies/" + userId + "/officepicture", {position: pos}).then(function (response) {
                            if (response.data.message = "OK") {
                                alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                                nbc.cargaInicial();
                            } else {
                                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                            }
                        });
                    }
                }, function(){

                }
            );
        }

        nbc.cargaInicial = function() {
            var auxPhotos = 0;
            $rootScope.idPerson = storage.get("dataUser")._id;
            userId = $rootScope.idPerson;
            nbc.valuesCreator();
            $http.get(ServerDestino + "companies/" + $rootScope.idPerson + "/basicdata").then(function(response) {
                nbc.companyBasicData = response.data;
                $rootScope.isMembership = nbc.companyBasicData.company_basicdata.membership;
                var tempoUser = storage.get("dataUser");
                tempoUser.isMembership = $rootScope.isMembership;
                storage.set("dataUser", tempoUser);
                $zopim(function() {
                    $zopim.livechat.set({
                      language: 'es',
                      name: response.data.company_basicdata.name,
                      email: response.data.user.email
                    });
                
                    $zopim.livechat.button.show();
                    
                });

                nbc.basicData = response.data.company_basicdata;
                nbc.cityTest = nbc.basicData.city;
                nbc.nombre = nbc.basicData.social_name;
                nbc.percent = nbc.companyBasicData.company_basicdata.percentage != "" && nbc.companyBasicData.company_basicdata.percentage != null ? parseInt(nbc.companyBasicData.company_basicdata.percentage) : 0;
                $rootScope.porcentajeL = nbc.percent;
                if (nbc.companyBasicData.company_basicdata.logo != "" && nbc.companyBasicData.company_basicdata.logo != null &&
                    ($rootScope.userAvatar == "../assets/images/company_pic.png" || $rootScope.userAvatar == "assets/images/company_pic.png" || $rootScope.userAvatar == null)) {
                    $rootScope.userAvatar = ServerDestino + "profile/companies/logos/" + nbc.companyBasicData.company_basicdata.logo;
                    nbc.urlValidator($rootScope.userAvatar);
                };
            }).finally(function(){
                $http.get(ServerDestino + "departments/all").then(function(response) {
                    nbc.listDepartamentos = response.data;
                    var foundItem = $filter('filter')(response.data, { name: nbc.basicData.department  }, true)[0];
                    if(foundItem){
                        $http.get(ServerDestino + "cities/byDepartment/" + foundItem._id).then(function(response) {
                            nbc.listaCiudades = response.data;
                        });
                    }
                });

                $http.get(ServerDestino + "companies/" + $rootScope.idPerson + "/noffers")
                .then(function(response){
                    $rootScope.numeroOffertas = response.data.company_data.offers_availables.basic;
                    nbc.cantOffert = response.data.company_data.nco !== "" &&
                            response.data.company_data.nco != null ? parseInt(response.data.company_data.nco):0;
                    $rootScope.ofertasDisponibles = response.data.company_data.offers_availables;
                    $rootScope.ofertasDisponibles.basic = parseInt($rootScope.ofertasDisponibles.basic);
                    $rootScope.ofertasDisponibles.premium = parseInt($rootScope.ofertasDisponibles.premium);
                    $rootScope.ofertasDisponibles.membership = parseInt($rootScope.ofertasDisponibles.membership);
                }).finally(function(){
                    $rootScope.blockOffer = false;
                });

                if ($rootScope.isMembership) {
                    $http.get(ServerDestino+'membership/subscription/show/'+$rootScope.idPerson)
                    .then(function(response){
                        $rootScope.membershipDatas = response.data.subscription;
                        $rootScope.membershipDatas.period_end = new Date($rootScope.membershipDatas.period_end);
                    });
                }
            });

            $http.get(ServerDestino + "companies/" + $rootScope.idPerson + "/additionaldata").then(function(response) {
                nbc.aditionalData = response.data.company_additionaldata;
                if (nbc.aditionalData.office_pictures.length > 0) {
                        for (var i = 0; i < nbc.aditionalData.office_pictures.length; i++) {
                            if (nbc.aditionalData.office_pictures[i] != "" && nbc.aditionalData.office_pictures[i] != null) {
                                nbc.photos[i] = {
                                    position: i,
                                    url: ServerDestino + "profile/companies/officepictures/" + nbc.aditionalData.office_pictures[i]
                                };

                                auxPhotos += 1;
                            }
                        }
                        $rootScope.arrayPhotos = nbc.photos;
                        $rootScope.office_picturesLength = auxPhotos;
                        nbc.officePictureLength = $rootScope.office_picturesLength;
                        nbc.getNumber();
                    } else {
                        $rootScope.office_picturesLength = nbc.aditionalData.office_pictures.length;
                        nbc.officePictureLength = $rootScope.office_picturesLength;
                    }

                nbc.valuesLoader();
                nbc.porcentajeLlenado();
            });            

            $http.get(ServerDestino + "offers/bycompany/" + $rootScope.idPerson).then(function(response) {
                nbc.offerts = response.data; 
                nbc.numOffers = response.data.num_offers;
                storage.set("companyOffers",nbc.offerts);
                if(nbc.numOffers){
                    nbc.postuladosCant = [];
                    for (var i = 0;i<nbc.offerts.offers.length;i++){
                        if (nbc.offerts.offers[i].num_candidates) {
                            for (var k = 0; k < nbc.offerts.offers[i].candidates.length; k++) {
                                if (!nbc.offerts.offers[i].candidates[k]._id) {
                                    nbc.offerts.offers[i].candidates[k]._id = nbc.offerts.offers[i].candidates[k].person._id;
                                }
                            }
                        }
                        nbc.offerts.offers[i].limit_date.date =  nbc.dateEditor(nbc.offerts.offers[i].limit_date.date);
                    }

                    nbc.items = nbc.offerts.offers;
                    nbc.totalItems = nbc.offerts.num_offers;
                    nbc.entryLimit = 5; // items per page
                    nbc.noOfPages = Math.ceil(nbc.totalItems / nbc.entryLimit);
                    for (var i = 0; i < nbc.items.length; i++) {
                        var base = nbc.items[i].base_salary.length > 0 ? parseInt(nbc.items[i].base_salary) : 0;
                        var extra = nbc.items[i].extra_salary.length > 0 ? parseInt(nbc.items[i].extra_salary) : 0;
                        nbc.items[i].total = base + extra;
                    }
                }
            }).finally(function(){
                $rootScope.isPageloading = false;
            });
                

            $http.get(ServerDestino + "job/"+$rootScope.idPerson+"/countAllPostulates").then(function(response){
                nbc.postuladosTotal = response.data.cantidad;
            });

            

            /*$http.get(ServerDestino + "/companies/" + $rootScope.idPerson + "/freeOffers")
                .then(function(response){
                nbc.freeOffers = response.data.message == "OK" &&
                    response.data.company_data.freeoffers !== "" ?
                    response.data.company_data.freeoffers:0;
                $rootScope.freeOferta = nbc.freeOffers;
            });*/

            $http.get(ServerDestino + "sectorscompany/all").then(function(response){
                nbc.listaSectores = response.data;
            });

	        $http.get(ServerDestino + "blog/post/3").then(function (response) {
                nbc.lastPosts = response.data;
            });  
        };

        nbc.urlValidator = function(url) {
            return $http.get(url).then(function(response) {
                return null;
            }).catch(function(err) {
                if (err.status === 404) {
                    $rootScope.userAvatar = "assets/images/company_pic.png";
                    return null
                };

                return $q.reject(err);
            })
        };

        nbc.dateEditor = function(date){
            var res = date.split(" ");
            return res[0];
        }

        nbc.recargarOfertas = function(){
            $http.get(ServerDestino + "offers/bycompany/" + $rootScope.idPerson).then(function(response) {
                nbc.offerts = response.data; 
                nbc.numOffers = response.data.num_offers;
                storage.set("companyOffers",nbc.offerts);
                if(nbc.numOffers){
                    nbc.postuladosCant = [];
                    for (var i = 0;i<nbc.offerts.offers.length;i++){
                        if (nbc.offerts.offers[i].num_candidates) {
                            for (var k = 0; k < nbc.offerts.offers[i].candidates.length; k++) {
                                if (!nbc.offerts.offers[i].candidates[k]._id) {
                                    nbc.offerts.offers[i].candidates[k]._id = nbc.offerts.offers[i].candidates[k].person._id;
                                }
                            }
                        }
                        nbc.offerts.offers[i].limit_date.date =  nbc.dateEditor(nbc.offerts.offers[i].limit_date.date);
                    }

                    nbc.items = nbc.offerts.offers;
                    nbc.totalItems = nbc.offerts.num_offers;
                    nbc.entryLimit = 5; // items per page
                    nbc.noOfPages = Math.ceil(nbc.totalItems / nbc.entryLimit);
                    for (var i = 0; i < nbc.items.length; i++) {
                        var base = nbc.items[i].base_salary.length > 0 ? parseInt(nbc.items[i].base_salary) : 0;
                        var extra = nbc.items[i].extra_salary.length > 0 ? parseInt(nbc.items[i].extra_salary) : 0;
                        nbc.items[i].total = base + extra;
                    }
                }
            }).finally(function(){
                $rootScope.isPageloading = false;
            });
        }

        nbc.pausarOferta = function (ofert,value) {
            /*offers/{id}/pauseOffer*/
            var ing = value=='Pausar' ? 'pausada':'reanudada';
            var ing2 = value=='Pausar' ? 'Pausada':'Reanudada';
            var msj = value=='Pausar' ? "Al pausar esta oferta, dejará de ser pública":"Al reanudar esta oferta, volverá a ser pública"
            
            swal({
                title: "¿Está seguro?",
                text: msj,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, "+value.toLowerCase()+"!",
                showLoaderOnConfirm: true
            }).then(
                function () {
                    setTimeout(function(){
                        $http.put(ServerDestino + "offers/" + ofert._id+ "/pauseOffer").then(function(response){
                            if(response.data.message=='ERROR'){
                                swal("Error", "Tu solicitud falló, por favor intenta luego", "error");
                            }else{
                                swal(ing2+"!", "Tu oferta ha sido "+ing, "success");
                                ofert.stop = value=='Pausar' ? true : false;
                            }                        
                        });
                    }, 10);
                }, function(){}
            );
        }

        nbc.detenerOferta = function(ofert){
            swal({
                title: "¿Está seguro?",
                text: "Al finalizar esta oferta, dejará de ser pública, y desaparecera de tus vacantes",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, finalizar!",
                showLoaderOnConfirm: true
            }).then(
                function () {
                    setTimeout(function(){
                       $http.put(ServerDestino + "offers/" + ofert._id+ "/stopOffer").then(function(response){
                            if(response.data.message=='ERROR'){
                                swal("Error", "Tu solicitud falló, por favor intenta luego", "error");
                            }else{
                                swal("Finalizada!", "Tu oferta ha sido finalizada.", "success");
                                ofert.finish = true;
                            }
                        },10); 
                    });
                },function(){}
            );
        }

        nbc.guardarDescripcion = function(id){
            nbc.descripcion = nbc.descEnvio;
            document.getElementById(id.toString()).classList.remove('show');
            nbc.descEnvio = ""; 
        }

        nbc.goToUrl = function(url){
            window.location = url;
        }

        nbc.dataSaver = function(idDiv1,idDiv2){
            var objEnvio = {
                _id: nbc.basicData._id,
                address: nbc.basicData.address,
                city: nbc.cityTest,
                full_name: nbc.basicData.contact[0].full_name,
                local_phone: nbc.basicData.contact[0].local_phone,
                position: nbc.basicData.contact[0].position,
                mobile_phone: nbc.basicData.contact[0].mobile_phone,
                department: nbc.basicData.department,
                description: nbc.basicData.description,
                employees_count: nbc.basicData.employees_count,
                logo: nbc.basicData.logo,
                name: nbc.basicData.name,
                nro_doc: nbc.basicData.nro_doc,
                sector: nbc.basicData.sector,
                social_name: nbc.nombre,
                user_id: nbc.basicData.user_id,
                percentage: ""+$rootScope.companyPercent
            };

            $http.put(ServerDestino + "companies/" + $rootScope.idPerson + "/basicdata/" + nbc.basicData.contact[0]._id + "/update", objEnvio).then(function(response){
                nbc.cargaInicial();
                document.getElementById(idDiv1.toString()).classList.remove('show');
                document.getElementById(idDiv2.toString()).classList.toggle("show");
            });
        }

        nbc.validarGuardarBasic = function(){
            if(nbc.basicData.address != "" || nbc.cityTest!="" ||
               (nbc.basicData.department!="" && nbc.basicData.department!=null) ||
               nbc.basicData.description !=null || nbc.basicData.employees_count!=null ||
               nbc.basicData.nro_doc!=null || nbc.basicData.sector!=null ||
               nbc.nombre!=""){
                nbc.dataSaver("myDropdown1","myDropdown2");
            }else{
                document.getElementById("myDropdown1").classList.remove('show');
                document.getElementById("myDropdown2").classList.toggle("show");
            }
        }

        nbc.validarGuardarContact = function(){
            if(nbc.basicData.contact[0].full_name != "" || nbc.basicData.contact[0].position !="" ||
               nbc.basicData.contact[0].local_phone != "" || nbc.basicData.contact[0].mobile_phone !=""){
                nbc.dataSaver("myDropdown3","myDropdown4");
            }else{
                document.getElementById("myDropdown3").classList.remove('show');
                document.getElementById("myDropdown4").classList.toggle("show");
            }
        }

        nbc.valuesCreator = function () {
            nbc.values[0] = {text: "Estacionamiento para empleados", value: false, nombre: "value0", id: "l0"};
            nbc.values[1] = {text: "Horario flexible", value: false, nombre: "value1", id: "l1"};
            nbc.values[2] = {text: "Salud Prepagada", value: false, nombre: "value2", id: "l2"};
            nbc.values[3] = {text: "Servicio de alimentación (casino)", value: false, nombre: "value3", id: "l3"};
            nbc.values[4] = {text: "Capacitaciones y cursos", value: false, nombre: "value4", id: "l4"};
            nbc.values[5] = {text: "Seguro de vida", value: false, nombre: "value5", id: "l5"};
            nbc.values[6] = {text: "Días libres", value: false, nombre: "value6", id: "l6"};
            nbc.values[7] = {text: "Becas o ayudas financieras", value: false, nombre: "value7", id: "l7"};
        };

        nbc.valuesLoader = function () {
            for (var i = 0; i < nbc.aditionalData.employees_benefits.length; i++) {
                if (nbc.aditionalData.employees_benefits[i] == "Estacionamiento para empleados") {
                    nbc.values[0].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Horario flexible") {
                    nbc.values[1].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Salud Prepagada") {
                    nbc.values[2].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Servicio de alimentación (casino)") {
                    nbc.values[3].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Capacitaciones y cursos") {
                    nbc.values[4].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Seguro de vida") {
                    nbc.values[5].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Días libres") {
                    nbc.values[6].value = true;
                } else if (nbc.aditionalData.employees_benefits[i] == "Becas o ayudas financieras") {
                    nbc.values[7].value = true;
                }
            }
            ;
        };

        nbc.benefitsEditor = function () {
            nbc.numberBenefits = 0;
            var objEnvio = {};
            for (var i = 0; i < 8; i++) {
                if (nbc.values[i].value) {
                    nbc.numberBenefits += 1;
                    objEnvio["benefit" + nbc.numberBenefits] = nbc.values[i].text;
                }
            };
            objEnvio["number_benefits"] = nbc.numberBenefits;
            objEnvio["website"] = nbc.aditionalData.website;
            objEnvio["fb_url"] = nbc.aditionalData.fb_url;
            objEnvio["tw_url"] = nbc.aditionalData.tw_url;
            objEnvio["lk_url"] = nbc.aditionalData.lk_url;
            if (nbc.numberBenefits > 0) {
                setTimeout(function () {
                    $http.put(ServerDestino + "companies/" + $rootScope.idPerson + "/additionaldata/update", objEnvio)
                        .success(function (response, status, headers, config) {
                           nbc.cargaInicial();
                           alertify.success(' <div class="text-center font-size-16 text-white"> Información actualizada con éxito </div>');
                            nbc.closeDropdown(4);
                            nbc.openDropdown(5);
                        })
                        .error(function (response, status, headers, config) {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar información </div>');
                        });
                }, 200);
            }
        };

        nbc.eliminarOferta = function(idOferta) {}

        nbc.calculaEdad = function(fechaNac) {
            var difEdadMs = Date.now() - Date.parse(fechaNac);
            var edad = new Date(difEdadMs);
            return Math.abs(edad.getUTCFullYear() - 1970);
        };

        nbc.porcentajeLlenado = function() {
            $rootScope.percentBasic = 0;
            $rootScope.percentAditional = 0;
            $rootScope.percentBasic = nbc.basicData.address != "" && nbc.basicData.address != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.city != "" && nbc.basicData.city != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            
            $rootScope.percentBasic = nbc.basicData.contact && nbc.basicData.contact[0].full_name != "" && nbc.basicData.contact[0].full_name != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.contact && nbc.basicData.contact[0].local_phone != "" && nbc.basicData.contact[0].local_phone != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.contact && nbc.basicData.contact[0].position != "" && nbc.basicData.contact[0].position != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.contact && nbc.basicData.contact[0].mobile_phone != "" && nbc.basicData.contact[0].mobile_phone != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.department != "" && nbc.basicData.department != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.description != "" && nbc.basicData.description != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.employees_count != "" && nbc.basicData.employees_count != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.logo != "" && nbc.basicData.logo != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.nro_doc != "" && nbc.basicData.nro_doc != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.sector != "" && nbc.basicData.sector != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;
            $rootScope.percentBasic = nbc.basicData.social_name != "" && nbc.basicData.social_name != null ? $rootScope.percentBasic + 1 : $rootScope.percentBasic;

            $rootScope.percentAditional = nbc.aditionalData.employees_benefits.length != 0 ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = nbc.aditionalData.fb_url != "" && nbc.aditionalData.fb_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = nbc.aditionalData.lk_url != "" && nbc.aditionalData.lk_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = nbc.aditionalData.office_pictures[0] != "" && nbc.aditionalData.office_pictures[0] != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = nbc.aditionalData.tw_url != "" && nbc.aditionalData.tw_url != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;
            $rootScope.percentAditional = nbc.aditionalData.website != "" && nbc.aditionalData.website != null ? $rootScope.percentAditional + 1 : $rootScope.percentAditional;

            $rootScope.companyPercent = Math.round((($rootScope.percentBasic + $rootScope.percentAditional) * 100) / 19);
            nbc.percent = $rootScope.companyPercent;
            $rootScope.porcentajeL = nbc.percent;
            nbc.isCheckedTab =$rootScope.companyPercent<100 ? true:false;
            var objEnvio = {
                _id: nbc.basicData._id,
                address: nbc.basicData.address,
                city: nbc.basicData.city,
                full_name: nbc.basicData.contact ? nbc.basicData.contact[0].full_name:'',
                local_phone: nbc.basicData.contact ? nbc.basicData.contact[0].local_phone:'',
                position: nbc.basicData.contact ? nbc.basicData.contact[0].position:'',
                mobile_phone: nbc.basicData.contact ? nbc.basicData.contact[0].mobile_phone:'',
                department: nbc.basicData.department,
                description: nbc.basicData.description,
                employees_count: nbc.basicData.employees_count,
                logo: nbc.basicData.logo,
                name: nbc.basicData.name,
                nro_doc: nbc.basicData.nro_doc,
                sector: nbc.basicData.sector,
                social_name: nbc.basicData.social_name,
                user_id: nbc.basicData.user_id,
                percentage: "" + $rootScope.companyPercent
            };
            $http.put(ServerDestino + "companies/" + $rootScope.idPerson + "/basicdata/" + nbc.basicData.contact[0]._id + "/update", objEnvio)
                .success(function(response, status, headers, config) {
                })
                .error(function(response, status, headers, config) {
                });
        };

        nbc.irCompras = function() {
            $location.path('/prebuy');
        }

        nbc.cargaNumeroVacantes = function() {
            if (storage.get("tokenCompany")) {
                $http.get(ServerDestino + '/offers/numbycompany/' + storage.get("tokenCompany"))
                    .then(function(response) {
                        if (response.status == 200) {
                            nbc.resp = response.data.xtivs;
                        } else {
                        }
                    });
            }
        }

        nbc.cargaNumeroVacantes();

        nbc.vacantCreateValidate = function() {

            if (nbc.percent < 50) {
                swal({
                    title: "Para crear una oferta necesitas que tu perfíl este lleno como mínimo un 50%",
                    text: "¿Quieres ir a llenar tu perfíl ahora?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "##00CB5A",
                    confirmButtonText: "¡Sí, Llevame allí!",
                    cancelButtonText: "No, ¡gracias!"
                }).then(
                    function () {
                        window.location = "company";
                    },function(){}
                );
            }else if(nbc.cantOffert > 0){
                $rootScope.tOffert="a";
                nbc.redirigirOffer("#/nuevaOferta");
            }else{
                nbc.gotoId('#pricing-box-sect');
            } 
        }

        /**
         * función que redirige la oferta, validada
         * según parámetros recibidos
         * @return {[type]} [description]
         */
        nbc.redirigirOffer = function(url){
            window.location = url;
        }

        /**
         * funcion para hacer scroll hacia donde 
         * se le indica con el parámetro val 
         * @param  {[type]} val [description]
         * @return {[type]}     [description]
         */
        nbc.gotoId = function(val){
            var id = val;
            $('html, body').animate({
                scrollTop: $(id.toString()).offset().top
            }, 1000);

        }

        nbc.changeUrl = function() {
            if (storage.get("tokenUser")) {
                nbc.urlActual = nbc.urlActual.substring(0, nbc.urlActual.indexOf("/#/")) + "/profile/#/";
                $rootScope.profileGo = nbc.urlActual;
                return true;
            } else {
                return false;
            }
        };

        nbc.consultar();
        nbc.cargaInicial();


        nbc.goProfile = function() {
            window.location = "company";
        }

        nbc.profileDataGo = function() {
            if (nbc.changeUrl()) {
                var aux = $rootScope.profileGo + "actualiza"
                window.location = aux;
            }
        }

        nbc.profileGo = function() {
            if (nbc.changeUrl()) {
                var aux = $rootScope.profileGo + ""
                window.location = aux;
            }
        }

        nbc.goDashboard = function() {
            window.location = "#/dashboard";
        }

        nbc.cerrarSesion = function() {
            $rootScope.loginx = false;
            $rootScope.isLoged = false;
            nbc.loged = false;
            storage.remove("tokenUser");
            window.location = "#/";
        }

        nbc.offers = function(oferta) {

            if ($rootScope.isLoged) {
                window.location = "#" + oferta;
            } else {
                window.location = "#/login";
            }
        };
        /*
            id "583601b482e63c65249b44e9"
            first_name "anthony"
            last_name "torres"
            email "josejose2@josejose2"
            company "empresa5"
            position Oferta 1
        */
        nbc.applyForJob = function(posicion, empresa) {
            var respuesta = {};
            $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function(response) {
                respuesta = response.data;
                if (!isNaN(response.data.percentage)) {
                    nbc.percentage = Math.round(response.data.percentage);
                };

                if (nbc.percentage > 79) {
                    var objEnvio = {
                        person_id: respuesta.person._id,
                        first_name: respuesta.person.first_name,
                        last_name: respuesta.person.last_name,
                        email: respuesta.user.email,
                        position: posicion,
                        company: empresa
                    };

                    $http.post(ServerDestino + "job/applyforjob", objEnvio).then(function(response) {
                        if (response.data.message == "OK") {
                            swal({title:"¡Felicitaciones!", html: 'tu postulación ha sido todo un éxito. <br /> <img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/353439455420850/?script=0">', type: "success"});
                        } else {
                            alertify.error(' <div class="text-center font-size-16 text-white"> Estamos presentando inconvenientes para realizar tu postulación, por favor intentalo más tarde. </div>');
                        }
                    });
                } else {
                    swal({
                        title: "Para postularte necesitas que tu perfíl este lleno como mínimo un 80%",
                        text: "¿Quieres ir a llenar tu pefíl ahora?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "##00CB5A",
                        confirmButtonText: "¡Sí, Llevame allí!",
                        cancelButtonText: "No, ¡gracias!"
                    }).then(
                        function () {
                            window.location = "profile";
                        },function(){}
                    );
                }
            });
        }; 

        $(window).resize(function(){
            if($("#divBasic").height()>=$("#divPremium").height() && $("#divBasic").height()>=$("#divPackage").height()){
                $("#divPremium").height($("#divBasic").height());
                $("#divPackage").height($("#divBasic").height())
            }else if($("#divPremium").height()>=$("#divBasic").height() && $("#divPremium").height()>=$("#divPackage").height()){
                $("#divPackage").height($("#divPremium").height());
                $("#divBasic").height($("#divPremium").height());
            }else{
                $("#divBasic").height($("#divPackage").height());
                $("#divPremium").height($("#divPackage").height());
            }
            if($("#divBasic2").height()>=$("#divPremium2").height() && $("#divBasic2").height()>=$("#divPackage2").height()){
                $("#divPremium2").height($("#divBasic2").height());
                $("#divPackage2").height($("#divBasic2").height())
            }else if($("#divPremium2").height()>=$("#divBasic2").height() && $("#divPremium2").height()>=$("#divPackage2").height()){
                $("#divPackage2").height($("#divPremium2").height());
                $("#divBasic2").height($("#divPremium2").height());
            }else{
                $("#divBasic2").height($("#divPackage2").height());
                $("#divPremium2").height($("#divPackage2").height());
            }
        });

        nbc.resizeDivs = function(){
            nbc.loadedDiv2 = true; 
             if($("#divBasic").height()>=$("#divPremium").height() && $("#divBasic").height()>=$("#divPackage").height()){
                $("#divPremium").height($("#divBasic").height());
                $("#divPackage").height($("#divBasic").height())
            }else if($("#divPremium").height()>=$("#divBasic").height() && $("#divPremium").height()>=$("#divPackage").height()){
                $("#divPackage").height($("#divPremium").height());
                $("#divBasic").height($("#divPremium").height());
            }else{
                $("#divBasic").height($("#divPackage").height());
                $("#divPremium").height($("#divPackage").height());
            }
        }

        nbc.openModal = function(ev,template,packages) {
            $.scrollTo('html, body',90);
            if (packages) {
                $rootScope.packages = true;
            }
            setTimeout(function(){
                $scope.$apply(function(){
                    $.scrollTo('html, body',90);
                    $rootScope.idUser = storage.get("dataUser")._id;
                    if(template == 'dialog2' || template == 'dialog56' || template == 'dialog55'){
                        nbc.idUser = storage.get("dataUser")._id;
                        $rootScope.reference = nbc.createReferemce(nbc.idUser);
                        $rootScope.signature = nbc.createHash($rootScope.reference);
                    }

                    $mdDialog.show({
                        controller: controllerDialog,
                        templateUrl: template+'.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        escapeToClose: true
                    }).then(function(answer) {}, function() {});
                });
            },200);            
        };

        nbc.createReferemce = function(idUser) {
            var today = moment().format('DD/MM/YYYY');
            return "Plan paquetes " + today + " " + idUser;
        }

        nbc.createHash = function(reference) {
            return md5("qWS65yqX4MDsNa1hFE87sWA8Ah~562401~" + reference + "~1000000~COP");
        }

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window, localStorageService,upload) {
            controllerDialog.prototype.$scope = $scope;
            $scope.isConfirm = false;
            $scope.isPackage = $rootScope.packages ? true:false;
            $scope.isRefreshing2 = true;
            $scope.domain = 0;
            $scope.urlFront = UrlActual + "public/app/payView.html";
            $scope.urlBack = ServerDestino + "payments/confirm";
            $scope.nombreVacante = "Plan paquetes";
            $scope.reference1 = $rootScope.reference;
            $scope.signature1 = $rootScope.signature;
            $scope.precio = 189000;
            $scope.totalPrecio = 0;
            $scope.msgPrecio = "Total a pagar"; 
            $scope.cantElegida = "";
            $scope.cantidadList = [];
            $scope.descriptionCompra = "";
            $scope.tarjetas = [];
            $scope.nuevaTarjeta = false;
            $scope.isSelectaTarjeta = false;
            $scope.listaAniosCC = [];
            $scope.listaMesCC = [];
            $scope.activeTab = 0;
            $scope.tabInfo = {};
            $scope.tarjetaSelecta = {};
            $scope.procesado = false;
            $scope.planDetails = [];
            $scope.isAjax = false;
            $scope.membership = $rootScope.isMembership;
            $scope.membershipData = {};
            $scope.ofertasDipo = $rootScope.ofertasDisponibles;
            $scope.precioBasic = 0;
            $scope.serverActual = ServerDestino;
            $scope.numeOffertas = $rootScope.numeroOffertas;

            $scope.cancel = function() {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
                $rootScope.packages = false;
            };

            $scope.primerCarga = function() {
                $scope.precioBasic = $scope.calculador(24900);
                if (localStorageService.get("tokenCompany")) {
                    $http.get(ServerDestino + "companies/" + $rootScope.idUser)
                        .then(function(response){
                        $scope.companyEmail = response.data.data.users.email;
                        $scope.companyEditName = response.data.data.social_name;
                        $scope.companyEditContact = response.data.data.contact[0].full_name;
                        $scope.companyEditTel = response.data.data.contact[0].mobile_phone;
                        $scope.companyEditEmail = response.data.data.users.email;
                    });
                }
                $scope.cargarAniosNuevaTDC();
                $scope.cargarMesesNuevaTDC();

                $http.get(ServerDestino+'membership/plans/all').then(function(response){
                    $scope.planDetails = response.data.plans[0];
                });
                if ($scope.membership) {
                    $scope.membershipData = $rootScope.membershipDatas;
                    if ($scope.membershipData.client && $scope.membershipData.client.creditCards.length) {
                        $scope.tarjetas = $scope.membershipData.client.creditCards;
                        for (var i = 0; i < $scope.tarjetas.length; i++) {
                            $scope.tarjetas[i].name = $scope.nombreTarjetaMostrarFormater($scope.tarjetas[i].name);
                            $scope.tarjetas[i].number = $scope.tarjetaMostrarFormater($scope.tarjetas[i].number);
                        }
                    }
                }
            }

            $scope.goPackage = function (val){
                $scope.isPackage = true;
                $scope.tipoCompra = val;
                $scope.tipoPackage = val=='BASIC' ? 'Básicas':'Premium';
                $scope.isRefreshing2 = true;
                $scope.precio = val=='BASIC' ? $scope.calculador(24900):189000;
                $scope.totalPrecio = 0;
                $scope.msgPrecio = "Total a pagar"; 
                if ($scope.cantElegidaData) {
                    $scope.cambiarPaquete($scope.cantElegidaData,val);
                }
            }

            if ($rootScope.packages) {
                $scope.goPackage('PREMIUM');
            }

            /**
             * función que permite definir la opción elegida
             * del cajón de precios y redirigir al usuario
             * según su elección
             * @param  {[type]} val [description]
             * @return {[type]}     [description]
             */
            $scope.tOfferta = function(val,gratis){
                if ($rootScope.porcentajeL<50) {
                    $scope.cancel();
                    swal({
                        title: "Para crear una oferta necesitas que tu perfíl este lleno como mínimo un 50%",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "##00CB5A",
                        confirmButtonText: "¡Entendido!"
                    }).then(
                        function () {
                        },function(){}
                    );
                }else{
                    var tempos = storage.get("dataUser");
                    tempos.typeOff = val;
                    storage.set("dataUser", tempos);
                    $rootScope.tOffert = val;
                    $scope.redirigirOffer("#/nuevaOferta/");
                    $scope.cancel();
                }
            }

            $scope.redirigirOffer = function(url){
                window.location = url;
            }

            $scope.cargaPreciosCant = function(){
                for (var i = 1; i <= 50; i++) {
                    $scope.cantidadList.push(i);
                }
            };

            $scope.cargaPreciosCant();
            $scope.calculador = function (precio) {
                precio = parseFloat(precio);
                return precio + (precio * 0.19);
            }

            $scope.cambiarPaquete = function(data,tipo){
                if(data){
                    $scope.cantElegidaData = parseInt(data);
                    $scope.isRefreshing2 = false;
                }
                if($scope.cantElegidaData==1){
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:189000;
                    $scope.msgPrecio = "Total a pagar"; 
                }else if ($scope.cantElegidaData==2 || $scope.cantElegidaData==3) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:175000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 7% por oferta";
                }else if ($scope.cantElegidaData==4 || $scope.cantElegidaData==5) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:160000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 15% por oferta";
                }else if ($scope.cantElegidaData>=6 && $scope.cantElegidaData<=11) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:149000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 21% por oferta";
                }else if ($scope.cantElegidaData>=12 && $scope.cantElegidaData<=17) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:139000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 26% por oferta";
                }else if ($scope.cantElegidaData>=18 && $scope.cantElegidaData<=24) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:125000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 34% por oferta";
                }
                // else if ($scope.cantElegidaData>=25 && $scope.cantElegidaData<=50) {
                //     $scope.precio = 89000;
                //     $scope.msgPrecio = "| Estas ahorrando 58% por oferta";
                // }

                $scope.descriptionCompra = "Compra paquete, cantidad ofertas: "+$scope.cantElegidaData+tipo;
                $scope.totalPrecio = $scope.precio * $scope.cantElegidaData;
                setTimeout(function(){
                    $scope.$apply(function(){
                        $scope.reference1 = $scope.createReferemce($rootScope.idUser,$scope.cantElegidaData+tipo);
                        $scope.signature1 = $scope.createrHash($scope.reference1,$scope.totalPrecio);
                    });
                },200);
            }

            $scope.createrHash = function(reference,total) {
                return md5("qWS65yqX4MDsNa1hFE87sWA8Ah~562401~" + reference + "~"+total+"~COP");
            }

            $scope.createReferemce = function(idUser,tipo) {
                var entre = tipo ? tipo:"";
                var today = moment().format('DD/MM/YYYY');
                return "Plan paquetes " + today + " " + entre + " " + idUser;
            }

            $scope.confirmarDatos = function(){
                $scope.isConfirm = true;
            }

            $scope.prepararEnvio = function(description){
                var objEnvio = {
                    company_id: $rootScope.idUser,
                    job_title: $scope.nombreVacante,
                    job_description: description,
                    contact_offer: {
                        empresa: $scope.companyEditName,
                        nombre: $scope.companyEditContact,
                        phone: $scope.companyEditTel,
                        email: $scope.companyEditEmail
                    },
                    area: "",
                    required_level: "",
                    carrers: "",                    
                    functions: "",
                    min_experience: "",
                    max_experience: "",
                    city: "",
                    department: "",
                    observation: "",
                    type_contract: "",
                    num_offers: "",
                    limit_date: "0000-00-00",
                    base_salary: "",
                    extra_salary: "",
                    show_salary: "",
                    language: "",
                    relevancia: "",
                    sector: "",
                    preferencias: "",
                    softwares: "",
                    areas: ""
                };

                $rootScope.ofertaEnviar = objEnvio;
            }

            $scope.procesarOferta = function(val) {
                if($scope.cantElegidaData){
                    $scope.tipoSolicitud = val == 'BASIC' || 'PREMIUM' ? "Compra paquete, cantidad ofertas: " +$scope.cantElegidaData + val :'Solicitud de contacto'; 
                    $scope.prepararEnvio($scope.tipoSolicitud);
                    $scope.isRefreshing = true;
                    $scope.isRefreshing2 = true;
                    alertify
                    .success(' <div class="text-center font-size-16 text-white">' 
                        + ' Procesando solicitud </div>');
                    $rootScope.ofertaEnviar.offer_type = val;
                    var req = {
                        method: 'POST',
                        url: ServerDestino + 'offers',
                        headers: {
                            'Accept': 'application/json, text/javascript',
                            'Content-Type': 'application/json; charset=utf-8'
                        },
                        data: $rootScope.ofertaEnviar
                    }
                    $http(req).then(function(response) {
                        if(response.data.message == "OK"){
                            $scope.cancel();
                            alertify
                            .success(' <div class="text-center font-size-16 text-white">' 
                                + ' Confirmación de datos enviada </div>');
                        }
                    }, function(response) {
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al enviar datos de confirmación </div>');
                    }).finally(
                    function() {
                        $scope.isRefreshing = false;
                        $scope.isRefreshing2 = false;
                    });
                }else{
                    alertify.error(' <div class="text-center font-size-16 text-white"> Debes elegir al menos una 1 oferta para continuar </div>');
                    $scope.isRefreshing = false;
                    $scope.isRefreshing2 = true;
                }
            }

            $scope.uploadFile2 = function () {

                var aux2 = 0;
                if ($rootScope.office_picturesLength == 4) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Lo sentimos, has alcanzado el límite de fotos permitidas. Intenta borrando una de las antiguas. </div>');
                } else {
                    $scope.fileUploading();
                    for (var i = 0; i < $rootScope.office_picturesLength; i++) {
                        if ($rootScope.arrayPhotos[i] == null) {
                            aux2 = i;
                            break;
                        } else if ($rootScope.arrayPhotos[i].position != i) {
                            aux2 = i;
                            break;
                        } else {
                            aux2 = i + 1;
                        }
                    }
                    $scope.photopos = aux2;
                    var envio = {
                        file: $scope.officePhoto,
                        url: ServerDestino + "companies/" + userId + "/officepicture" + aux2,
                        _token: token,
                        nombreCampo: "picture"
                    };

                    upload.uploadFile(envio).then(function (response) {
                        if (response.data.message == "OK") {
                            /*window.location = "#/basicData";
                            window.location = "#/aditionalData";*/

                            $scope.cancel();
                        } else {
                            $scope.cancel();
                        }
                    });
                }
                ;
            };

            $scope.file_changed = function (element) {
                if (element.value != '') {
                    $scope.$apply(function () {
                        $scope.uploadGo = true;
                    });
                }
            };

            $scope.fileUploading = function () {
                $scope.uploadGo = false;
                $scope.uploadChange = true;
            }

            setTimeout(function() {
                $scope.$apply(function() {
                    $("#logo").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="assets/images/company_pic.png" alt="Tu Foto" style="width:160px">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });

                    $("#avatar").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="assets/images/company_pic.png" alt="Tu Foto" class="pic-260">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);

            /**
             * Funcion que envia la data de la nueva tdc al back
             */
            $scope.agregarTarjeta = function(
                payer_name,
                creditCard,
                payer_street,
                credit_card_document,
                payer_phone,
                payer_city,
                payer_state,
                credit_card_expiration_month,
                credit_card_expiration_year,
                credit_card_ccv,
                payer_country,
                payer_postal_code){
                /*$scope.nuevaTarjeta = false;*/
                $scope.procesado = true;
                
                $scope.tarjetaSelecta = {
                    name:$scope.nombreTarjetaMostrarFormater(payer_name),
                    type:creditCard.type,
                    number:$scope.tarjetaMostrarFormater(creditCard.number),
                    picked:true
                }
                $scope.tarjetas.push($scope.tarjetaSelecta);
                
                $scope.subscription = {
                    plan_code:$scope.planDetails.code,
                    company_id: $rootScope.idUser,
                    payer_name:payer_name,
                    credit_card_number:creditCard.number,
                    credit_card_ccv:credit_card_ccv,
                    credit_card_expiration_year:credit_card_expiration_year,
                    credit_card_expiration_month:credit_card_expiration_month,
                    payment_method:creditCard.type.toUpperCase(),
                    credit_card_document:credit_card_document,
                    payer_dni:credit_card_document,
                    payer_street:payer_street,
                    payer_city:payer_city,
                    payer_state:payer_state,
                    payer_country:payer_country,
                    payer_postal_code:payer_postal_code,
                    payer_phone:payer_phone
                }
                /*$scope.cambioIndividual(1);*/
            }

            $scope.applyDiscount = function(){

                $scope.planDetails.tax_return_base = ($scope.subscription.promo_code == 'YoSoyEmpleo') ? ($scope.planDetails.tax_return_base / 2) : 200000;

            }

            $scope.comprarMembresia = function(){
                
                $scope.isAjax = true;
                $scope.subscription;
                $http.post(ServerDestino + "membership/subscription",$scope.subscription).then(function successCallback(response){
                    if (response.data.message='OK') {
                        $mdDialog.cancel();
                        swal({
                            title: "Tu membresia ha sido activada, ¡choca esas cuatro!",
                            text: "Ya puedes disfrutar de los beneficios de tu membresia mensual SoyEmpleo",
                            imageUrl: "assets/images/soyempleo-chocala-simple.png",
                            confirmButtonColor: "#00BF52",
                            confirmButtonText: "Aceptar"
                        });
                    }else{
                        alertify.error("Hubo un error al procesar tu solicitud, por favor intenta más tarde.");
                    }
                },function errorCallback(response) {
                    alertify.error("Hubo un error al procesar tu solicitud, por favor intenta más tarde.");
                }).finally(function(){
                    $scope.isAjax = false;
                });
            }

            $scope.validarEnvioBoton = function(valor) {
                if (!valor) {
                    $scope.blockeaTab = valor;
                    return valor;
                }
                $scope.blockeaTab = valor;
                return valor;
            }

            $scope.tarjetaMostrarFormater = function(num){
                return "**** **** **** " + num.substring(num.length-4,num.length);
            }

            $scope.nombreTarjetaMostrarFormater = function(name){
                var tokens = name.split(" ");
                var nombre = '';
                if (tokens.length > 2) {
                    var length = tokens.length>4 ? 4:tokens.length;
                    for (var i = 0; i < length; i++) {
                        if (i%2==1 && i>0) {
                            nombre = nombre + " " + tokens[i].charAt(0).toUpperCase();
                        }else{
                            if (!i) {
                                nombre = nombre + tokens[i].toUpperCase();
                            }else{
                                nombre = nombre + " " + tokens[i].toUpperCase();
                            }
                            
                        }
                    }
                    return nombre; 
                }else if (tokens.length==2){
                    return tokens[0].toUpperCase() + " " + tokens[1].toUpperCase();
                }
                return name.toUpperCase();                
            }

            /*$scope.cambiarTabById = function(indx,to){
                $scope.tabInfo = indx.$parent.tabset;
                if (to=='mtp') {
                    $scope.procesado = false;
                }
            }*/

            $scope.cambioIndividual = function(valor){
                if ($scope.tabInfo.tabs) {
                    setTimeout(function(){
                        $scope.$apply(function(){
                            $scope.tabInfo.active = valor;
                        });
                    },100)
                }else{
                    setTimeout(function(){
                        $scope.$apply(function(){
                             $scope.activeTab = valor;
                        });
                    },100);
                }
            }

            /**
             * Función que carga en una variable 10 años apartir del actual
             */
            $scope.cargarAniosNuevaTDC = function(){
                $scope.listaAniosCC = [];
                var actual = new Date();
                for (var i = 0; i < 11; i++) {
                    $scope.listaAniosCC.push({anio:actual.getFullYear()+i});
                }
            }

            /**
             * Función que carga valores del 1 al 12 (meses)
             */
            $scope.cargarMesesNuevaTDC = function(){
                $scope.listaMesCC = [];
                var actual = new Date();
                for (var i = 0; i < 12; i++) {
                    $scope.listaMesCC.push({mes:i+1});
                }
            }

            /**
             * Funcion que renderiza el formulario para crear nueva tarjeta
             */
            $scope.nuevaTarjetaForm = function(value){
                $scope.nuevaTarjeta = value;
            }

            /**
             * Funcion que selecciona una tarjeta de la lista en el front
             */
            $scope.seleccionarTarjeta = function(indx){
                var found = $filter('filter')($scope.tarjetas,{picked:true},true);
                if (found.length) {
                    for (var i = 0; i < found.length; i++) {
                        found[i].picked = false;
                    }
                }
                $scope.tarjetas[indx].picked = !$scope.tarjetas[indx].picked;
                /*$scope.agregarTarjeta(
                    $scope.tarjetas[indx].name,
                    $scope.tarjetas[indx],
                    $scope.tarjetas[indx].address.line1,
                    $scope.tarjetas[indx].document,
                    $scope.tarjetas[indx].address.phone,
                    $scope.tarjetas[indx].address.city,
                    $scope.tarjetas[indx].address.state,
                    credit_card_expiration_month,
                    credit_card_expiration_year,
                    credit_card_ccv,
                    payer_country,
                    payer_postal_code);*/
            }

            /*TEMPORAL SOLO PARA PRUEBAS BORRARRRR*/
            /*for (var i = 0; i < 5; i++) {
                $scope.tarjetas.push({nombreCC:"Anthony J Torres Y "+i,type:i%2==0 ?"mastercard":"visa",numeroCC:"**** **** **** 8965",picked:false});
            }*/
            /*TEMPORAL SOLO PARA PRUEBAS*/

            $scope.primerCarga();
        };

        controllerDialog.prototype.setFile = function(element) {
            var $scope = this.$scope;
            $scope.$apply(function() {
                $scope.theFile = element.files[0];
            });
        };

        nbc.irEdicion = function(company_id){
            $rootScope.isEdit = true;
            $location.path("/editOferta/"+company_id);
        }

        /**
         * Get a random integer between `min` and `max`.
         * 
         * @param {number} min - min number
         * @param {number} max - max number
         * @return {int} a random integer
         */
        nbc.getRandomInt = function getRandomInt(min, max) {
          return Math.floor(Math.random() * (max - min + 1) + min);
        }

        nbc.goPostulados = function(id){
            $rootScope.companyInfo = nbc.companyBasicData;
            nbc.isRefreshingPostu = true;
            var found = $filter('filter')(nbc.items,{_id:id},true)[0];
            if(found){
                var indx = nbc.items.indexOf(found);
                setTimeout(function(){
                    $http.get(ServerDestino + "job/"+id+"/showbyoffer").then(function(response){
                        $rootScope.postulates = response.data;
                        var foundF = $filter('filter')($rootScope.postulates,{person:null},true);
                        if (foundF.length) {
                            for (var i = foundF.length - 1; i >= 0; i--) {
                                $rootScope.postulates.splice($rootScope.postulates.indexOf(foundF[i]),1);
                            }
                        }
                        for (var i = 0; i < $rootScope.postulates.length; i++) {
                            /*if ($rootScope.postulates[i].person.avatar) {*/
                            if ('avatar' in $rootScope.postulates[i].person) {
                                $rootScope.postulates[i].person.avatar =  ServerDestino + "profile/" + $rootScope.postulates[i].person.avatar;
                            }else{
                                $rootScope.postulates[i].person.sprite = $rootScope.postulates[i].person.gender=='M' ? 'male'+nbc.getRandomInt(1,6):'femme'+nbc.getRandomInt(1,6);
                            }
                            $rootScope.postulates[i].dynamicPopover = {
                                content: 'Hello, World!',
                                templateUrl: 'myPopoverTemplate.html',
                                title: 'Title',
                                picked:false,
                                isAjax:false
                            }                            
                        }
                        $rootScope.ofertaSelect = found;
                        $rootScope.cargaPostulates();
                        $rootScope.separarEstados();
                        nbc.goToUrl('/#/postulados');
                    })
                    .finally(
                        function() {
                            nbc.isRefreshingPostu = false;
                    });
                },200);
            }
        }

        $rootScope.isImage = function(src,indx) {
            var deferred = $q.defer();
            deferred.promise.indx = indx;
            var image = new Image();
            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;
            return deferred;
            /*return deferred.promise;*/
        }

        /**
         * SECCIÓN DE DETALLES DE POSTULADOS
         *
         */

        $rootScope.toggleNotas = function(item){
            item.dynamicPopover.picked = !item.dynamicPopover.picked;
            var indx = $scope.postulates.indexOf(item);
            for (var i = 0; i < $scope.postulates.length; i++) {
                if (indx==i && $scope.postulates[i].dynamicPopover.picked) {
                    continue;
                }
                $scope.postulates[i].dynamicPopover.picked = false;
            }
            if (item.dynamicPopover.picked) {
                item.dynamicPopover.isAjax = true;
                $http.get(ServerDestino + "comments/" + item._id).then(function(response){
                    if (response.data.message=='OK' && response.data.comments.length) {
                        item.notas = [];
                        for (var i = 0; i < response.data.comments.length; i++) {
                            response.data.comments[i].is404 = false;
                            $rootScope.onSrcFail = "assets/images/logo-offert-default.png";
                            if (!response.data.comments[i].authorAvatar ||
                                response.data.comments[i].authorAvatar.length <= 50) {
                                response.data.comments[i].authorAvatar = response.data.comments[i].authorType == 'company' ?
                                    $rootScope.userAvatar:"assets/images/logo-offert-default.png";
                            }else if (!$rootScope.buscadorExt(response.data.comments[i].authorAvatar)) {
                                response.data.comments[i].is404 = true;
                            }
                            response.data.comments[i].updated_at = new Date(response.data.comments[i].updated_at);
                        }
                        item.notas = response.data.comments;
                    }
                    
                }).finally(function(){
                    item.dynamicPopover.isAjax = false;
                    setTimeout(function(){
                        $("#popover-content2").animate({ scrollTop: $("#popover-content2").prop("scrollHeight") }, 900);
                    },200);
                    $('#notaInput').focus();
                });
            }
        }

        $rootScope.buscadorExt = function(obj){
            var types = [".jpeg", ".jpg", ".png", ".gif", ".tiff", ".bmp", ".raw"];
            var indx = false;
            for (var i = 0; i < types.length; i++) {
                indx = obj.substring(obj.length-types[i].length, obj.length)==types[i];
                if (indx) {
                    break;
                }
            }
            return indx;
        }

        $rootScope.cargarChat = function(empresa,imgUrl,item) {
            item.dynamicPopover.isAjax = true;
            if (item.textoNota) {
                item.notas = Array.isArray(item.notas) ? item.notas:[];
                var objNota = {
                    "postulation_id":item._id,
                    "comment":item.textoNota,
                    "type":"public",
                    "author": $rootScope.companyInfo.company_basicdata.user_id
                }
                var notaTempo = item.textoNota;
                item.textoNota = '';
                $http.post(ServerDestino + "comments",objNota).then(function(response){
                    if (response.data.message=='OK') {
                        if (!response.data.comment.authorAvatar ||
                            response.data.comment.authorAvatar.length <= 50) {
                            response.data.comment.authorAvatar = $rootScope.userAvatar;
                        }
                        response.data.comment.updated_at = new Date(response.data.comment.updated_at);
                        item.notas.push(response.data.comment);
                        if (item.notas.errorDelete) {item.notas.errorDelete = false;}
                        if (item.notas.errorSend) {item.notas.errorSend = false;}
                        if (item.notas.errorEdit) {item.notas.errorEdit = false;}
                    }else{
                        item.notas.push({
                            authorAvatar:$rootScope.userAvatar,
                            authorName: $rootScope.companyInfo.company_basicdata.social_name || $rootScope.companyInfo.company_basicdata.name,
                            comment:notaTempo,
                            updated_at: new Date(),
                            errorSend: true
                        })
                    }
                }).finally(function(){
                    item.dynamicPopover.isAjax = false;
                    event.preventDefault();
                    $("#popover-content2").animate({ scrollTop: $("#popover-content2").prop("scrollHeight") }, 900);
                    $('#notaInput').focus();
                });
            }
        }

        $rootScope.reenviarNota = function(nota,postulation_id) {
            var objNota = {
                    "postulation_id":postulation_id,
                    "comment":nota.comment,
                    "type":"public",
                    "author": $rootScope.companyInfo.company_basicdata.user_id
                }
                
            $http.post(ServerDestino + "comments",objNota).then(function(response){
                if (response.data.message=='OK') {
                    if (nota.errorDelete) {nota.errorDelete = false;}
                    if (nota.errorSend) {nota.errorSend = false;}
                    if (nota.errorEdit) {nota.errorEdit = false;}
                }
            }).finally(function(){
                $("#popover-content2").animate({ scrollTop: $("#popover-content2").prop("scrollHeight") }, 900);
                $('#notaInput').focus();
            });
        }

        $rootScope.editarNota = function(item){
            item.dynamicPopover.isAjax = true;
            var foundId = $filter('filter')(item.notas,{_id:item.notaEditId},true)[0];
            if (foundId) {
                var index = item.notas.indexOf(foundId);
            }
            var objEdit = {
                id:item.notaEditId,
                comment: item.textoNota
            }
            $http.post(ServerDestino + "comments/" + item.notaEditId,objEdit).then(function(response){
                if (!response.data.message=='OK') {
                    item.notas[index].errorEdit = true;
                }else{

                    item.textoNota = '';
                    if (foundId) {
                        response.data.comment.updated_at = new Date(response.data.comment.updated_at);
                        item.notas[index].comment = response.data.comment.comment;
                        item.notas[index].updated_at = response.data.comment.updated_at;
                        if (item.notas[index].errorDelete) {
                            item.notas[index].errorDelete = false;
                        }
                        if (item.notas[index].errorSend) {
                            item.notas[index].errorSend = false;
                        }
                        if (item.notas[index].errorEdit) {
                            item.notas[index].errorEdit = false;
                        }
                    }
                    item.isEdit = false;
                    var parentDiv = $("#popover-content2");
                    var innerListItem = $("#nota"+item.notas[index]._id);
                    /*parentDiv.scrollTop(parentDiv.scrollTop() + innerListItem.position().top - (parentDiv.height()/2 + innerListItem.height()/2));*/
                    parentDiv.animate({ scrollTop: parentDiv.scrollTop() + innerListItem.position().top - (parentDiv.height()/2 + innerListItem.height()/2) }, 900);
                    innerListItem.addClass('pulseNotas');

                    setTimeout(function() {
                        innerListItem.removeClass('pulseNotas');
                    }, 5000);
                }
            }).finally(function(){
                item.isEdit = false;
                item.dynamicPopover.isAjax = false;
                $('#notaInput').focus();
            });
        };

        $rootScope.prepararEdit = function(nota,item){
            item.isEdit = true;
            item.textoNota = nota.comment;
            item.notaEditId = nota._id;
            $('#notaInput').focus();           
        }

        $rootScope.eliminarNota = function(nota,item){
            nota.deleting = true;
            if (item.isEdit) {item.isEdit=false; item.textoNota=''}
            var index = item.notas.indexOf(nota);
            $http.delete(ServerDestino + "comments/" + item.notas[index]._id).then(function(response){
                if (response.data.message=='OK') {
                    item.notas.splice(index,1);
                }else{
                    item.notas[index].errorDelete = true;
                }
                $("#popover-content2").animate({ scrollTop: $("#popover-content2").prop("scrollHeight") }, 900);
                $('#notaInput').focus();
            });
        };

        nbc.conversa = [];
        nbc.conversa.push({
            nombre:"SoyEmpleo",
            nota:'Estas notas no son visibles para el candidato, por medio de ellas puedes comunicarte con el equipo de selección de SoyEmpleo.com',
            fecha: new Date(),
            imgUrl: "https://soyempleo.com/assets/images/logo-offert-default.png"
        });


        $rootScope.cargaPostulates = function(){
            $rootScope.styles = {preseleccionado:""},
                        {entrevistado:""},
                        {contratado:""};
            if ($rootScope.postulates && $rootScope.ofertaSelect) {
                $rootScope.ofertaActual = $rootScope.ofertaSelect;

                for (var i = 0; i < $rootScope.postulates.length; i++) {
                    $rootScope.postulates[i].person.edad = nbc.calculaEdad(new Date($rootScope.postulates[i].person.birthdate)) + " Años";
                }

                $rootScope.itemsPostu = $rootScope.postulates;
                $rootScope.totalItemsPostu = $rootScope.postulates.length;
                $rootScope.entryLimitPostu = 5; // items per page
                $rootScope.noOfPagesPostu = Math.ceil($rootScope.totalItemsPostu / $rootScope.entryLimitPostu);
            }else{
                nbc.goToUrl('/#/dashboard-company');
            }
        }

        nbc.desHojaVida = function (id) {
            var win = window.open(ServerDestino + "pdf/profile/" + id, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Porfavor permitir popups para este website');
            } 
        }
        
        $rootScope.getPerson = function(id,estado1,estado2,estado3){ 
            if(id){/* storage.get("personItems") */
                $http.get(ServerDestino + "persons/" + id + "/profiledata").then(function(response){
                    $rootScope.profileData = response.data.person;
                    $rootScope.userData = response.data.user;

                    $rootScope.selectedAvatar = $rootScope.userData.avatar ? ServerDestino + "profile/" + $rootScope.userData.avatar : "assets/images/user_male.png";
                    if($rootScope.selectedAvatar!="assets/images/user_male.png"){
                        $rootScope.urlValidator2($rootScope.selectedAvatar);
                    }
                    

                    $http.get(ServerDestino + "persons/" + id  + "/answersquestions").then(function(response2){
                        var busca = {person:{_id:id}}
                        var found = $filter('filter')($rootScope.postulates,busca,true)[0];
                        if(found){
                            $rootScope.selected = $rootScope.postulates.indexOf(found);
                            $rootScope.postulates[$rootScope.selected].user = $rootScope.userData;
                        }
                        if ($rootScope.postulates[$rootScope.selected].softwares) {
                            for (var j = 0; j < $rootScope.postulates[$rootScope.selected].softwares.length; j++) {
                                $rootScope.postulates[$rootScope.selected].softwares[j] = $rootScope.preparador3Niveles($rootScope.postulates[$rootScope.selected].softwares[j]);
                            }
                        }

                        if($rootScope.postulates[$rootScope.selected].skills){
                            for (var j = 0; j < $rootScope.postulates[$rootScope.selected].skills.length; j++) {
                                $rootScope.postulates[$rootScope.selected].skills[j] = $rootScope.preparador10Niveles($rootScope.postulates[$rootScope.selected].skills[j]);
                            }
                        }

                        $rootScope.postulates[$rootScope.selected].estado1 = estado1;
                        $rootScope.postulates[$rootScope.selected].estado2 = estado2;
                        if (estado3) {
                            $rootScope.postulates[$rootScope.selected].IsContratado = true;
                        }
                        $rootScope.postulates[$rootScope.selected].preguntasList = [];
                        $rootScope.postulates[$rootScope.selected].preguntasList = response2.data.person_answers_questions;
                        $rootScope.postulates[$rootScope.selected].selectedAvatar = $rootScope.selectedAvatar;
                        
                        if (!storage.get("personItems")) {
                            storage.set("personItems",[]);
                        }

                        if (!storage.get("personItems").length) {
                            var personaItms = [];
                            personaItms.push({postulates:$rootScope.postulates[$rootScope.selected]});
                            storage.set("personItems",personaItms);
                            var ess = storage.get("personItems");
                        }else{
                            var personaItms = [];
                            personaItms = storage.get("personItems");
                            var testing = {
                                postulates:{
                                    person:{
                                        _id:id
                                    }
                                }
                            }
                            var foundPerson = $filter('filter')(personaItms,testing,true)[0];
                            if (!foundPerson) {
                                personaItms.push({postulates:$rootScope.postulates[$rootScope.selected]});
                                storage.set("personItems",personaItms);
                            }
                            if (foundPerson) {
                                if (!foundPerson.postulates.selectedAvatar) {
                                    var indxx = personaItms.indexOf(foundPerson);
                                    personaItms[indxx].postulates.selectedAvatar = $rootScope.selectedAvatar;
                                    storage.set("personItems",personaItms);
                                } 
                            }
                            
                        }
                        /*var url = UrlActual + '#/perfil/persona/'+id;*/
                        /*var url = "http://localhost:8001/"+ '#/perfil/persona/'+id;*/
                        $location.path('/perfil/persona/');
                        /*$window.location.reload();*/
                        /*var win = window.open(url, '_blank');
                        if (win) {*/
                            //Browser has allowed it to be opened
                           /* win.focus();
                        } else {*/
                            //Browser has blocked it
                            /*alert('Porfavor permitir popups para este website');
                        }   */                     
                    });
                });
            }
            /*nbc.goToUrl('/#/perfil/persona');*/
        }

        $rootScope.preparador3Niveles = function(obj){
            if (obj.level) {
                var objColores = [];
                for (var i = 0; i <5; i++) {
                    if (obj.level == "1" && i<=1) {
                        objColores.push({act:true});
                    }else if (obj.level == "1" && i>1){
                        objColores.push({act:false});
                    }

                    if (obj.level == "2" && i<=2) {
                        objColores.push({act:true});
                    }else if (obj.level == "2" && i>2){
                        objColores.push({act:false});
                    }

                    if (obj.level == "3" && i<=4) {
                        objColores.push({act:true});
                    }
                }
                obj.nivel = objColores;
            }
            return obj;
        }

        $rootScope.preparador10Niveles = function(obj){
            if (obj.level) {
                var objColores = [];
                for (var i = 0; i <5; i++) {
                    if (obj.level>=1 && obj.level<=2 && i<=0) {
                        objColores.push({act:true});
                    }else if (obj.level>=1 && obj.level<=2 && i>0) {
                        objColores.push({act:false});
                    }

                    if (obj.level>=3 && obj.level<=4 && i<=1) {
                        objColores.push({act:true});
                    }else if (obj.level>=3 && obj.level<=4 && i>1) {
                        objColores.push({act:false});
                    }

                    if (obj.level>=5 && obj.level<=6 && i<=2) {
                        objColores.push({act:true});
                    }else if (obj.level>=5 && obj.level<=6 && i>2) {
                        objColores.push({act:false});
                    }

                    if (obj.level>=7 && obj.level<=8 && i<=3) {
                        objColores.push({act:true});
                    }else if (obj.level>=7 && obj.level<=8 && i>3) {
                        objColores.push({act:false});
                    }

                    if (obj.level>=9 && obj.level<=10) {
                        objColores.push({act:true});
                    }
                }

                obj.nivel = objColores;
            }
            return obj;
        }

        $rootScope.urlValidator2 = function (url) {
            return $http.get(url).then(function (response) {
                return null;
            }).catch(function (err) {
                if (err.status === 404) {
                    $rootScope.selectedAvatar = "assets/images/user_male.png";
                    return null
                }
                ;

                return $q.reject(err);
            })
        };

        $rootScope.tempo = function(obj){
            for (var i = 0; i < obj.length; i++) { 
                if (i % 2) {
                    obj[i].postulated_by = "admin";
                } else {
                    obj[i].postulated_by = "person";
                }
            }
            $rootScope.listaAdmin = $filter('filter')(obj,{postulated_by:'admin'},true);
            $rootScope.listaPerson = $filter('filter')(obj,{postulated_by:'person'},true);

            for (var i = 0; i < 10; i++) {
                $rootScope.listaAdmin.push($rootScope.listaAdmin[i]);
                $rootScope.listaPerson.push($rootScope.listaPerson[i]);
            }
            
            $rootScope.tempo2($rootScope.listaAdmin,$rootScope.listaPerson);

        }

        $rootScope.tempo2 = function(obj1,obj2){
            if (obj1.length) {
                $rootScope.items1 = obj1;
                $rootScope.totalItems1 = obj1.length;
                $rootScope.entryLimit1 = 5; // items per page
                $rootScope.noOfPages1 = Math.ceil($rootScope.totalItems1 / $rootScope.entryLimit1);
            }
            if (obj2.length) {
                $rootScope.items2 = obj2;
                $rootScope.totalItems2 = obj2.length;
                $rootScope.entryLimit2 = 5; // items per page
                $rootScope.noOfPages2 = Math.ceil($rootScope.totalItems2 / $rootScope.entryLimit2);
            }
        }

        $rootScope.separarEstados = function(){
            if($rootScope.postulates){
                for (var i = 0; i < $rootScope.postulates.length; i++) {
                    if($rootScope.postulates[i].status.length>1){
                        if ($rootScope.postulates[i].status[0].label=="descartado") {
                            if ($rootScope.postulates[i].status[1].label=='postulado') {
                                $rootScope.listaInicial.push($rootScope.postulates[i]);
                                $rootScope.listaInicial[$rootScope.listaInicial.length-1].isDescartado=true;
                            } else if ($rootScope.postulates[i].status[1].label=='preseleccionado') {
                                $rootScope.listaPreseleccionados.push($rootScope.postulates[i]);
                                $rootScope.listaPreseleccionados[$rootScope.listaPreseleccionados.length-1].isDescartado=true;
                            } else if ($rootScope.postulates[i].status[1].label=='entrevistado') {
                                $rootScope.listaEntrevistados.push($rootScope.postulates[i]);
                                $rootScope.listaEntrevistados[$rootScope.listaEntrevistados.length-1].isDescartado=true;
                            } else if ($rootScope.postulates[i].status[1].label=='contratado') {
                                $rootScope.listaContratados.push($rootScope.postulates[i]);
                                $rootScope.listaContratados[$rootScope.listaContratados.length-1].isDescartado=true;
                            } 
                        } else {
                             if ($rootScope.postulates[i].status[0].label=='postulado') {
                                $rootScope.listaInicial.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='preseleccionado') {
                                $rootScope.listaPreseleccionados.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='entrevistado') {
                                $rootScope.listaEntrevistados.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='contratado') {
                                $rootScope.listaContratados.push($rootScope.postulates[i]);
                            }
                        }
                    }else{
                        if ($rootScope.postulates[i].status[0].label=='postulado') {
                            $rootScope.listaInicial.push($rootScope.postulates[i]);
                        } else if ($rootScope.postulates[i].status[0].label=='preseleccionado') {
                            $rootScope.listaPreseleccionados.push($rootScope.postulates[i]);
                        } else if ($rootScope.postulates[i].status[0].label=='entrevistado') {
                            $rootScope.listaEntrevistados.push($rootScope.postulates[i]);
                        } else if ($rootScope.postulates[i].status[0].label=='contratado') {
                            $rootScope.listaContratados.push($rootScope.postulates[i]);
                        }
                    }
                }

                $rootScope.prepararSecciones();
            }
        }
        
        $rootScope.prepararSecciones = function(){
            if ($rootScope.listaInicial.length) {
                var postulatedTempo = $filter('filter')($rootScope.listaInicial,{postulated_by:null},true);
                if(postulatedTempo.length){
                    for (var i = 0; i < postulatedTempo.length; i++) {
                        postulatedTempo[i].postulated_by = "person";
                    }
                }
                $rootScope.listaAdmin = $filter('filter')($rootScope.listaInicial,{postulated_by:'admin'},true);
                $rootScope.lastItem = $rootScope.listaAdmin.length ? false:true;
                $rootScope.listaPerson = $filter('filter')($rootScope.listaInicial,{postulated_by:"person"},true);
                $rootScope.tempo2($rootScope.listaAdmin,$rootScope.listaPerson);
            }
            if ($rootScope.listaPreseleccionados.length) {
                $rootScope.totalItemsPre = $rootScope.listaPreseleccionados.length;
                $rootScope.entryLimitPre = 5; // items per page
                $rootScope.noOfPagesPre = Math.ceil($rootScope.totalItemsPre / $rootScope.entryLimitPre);
            }
            if ($rootScope.listaEntrevistados.length) {
                $rootScope.totalItemsEnt = $rootScope.listaEntrevistados.length;
                $rootScope.entryLimitEnt = 5; // items per page
                $rootScope.noOfPagesEnt = Math.ceil($rootScope.totalItemsEnt / $rootScope.entryLimitEnt);
            }
            if ($rootScope.listaContratados.length) {
                $rootScope.totalItemsCon = $rootScope.listaContratados.length;
                $rootScope.entryLimitCon = 5; // items per page
                $rootScope.noOfPagesCon = Math.ceil($rootScope.totalItemsCon / $rootScope.entryLimitCon);
            }
        }

        $rootScope.cambiarEstatus = function(id,estado,id2){
            var objEnvio = { "status":  
                [ 
                    {
                        "label": estado,
                        "date": $rootScope.convertDate(new Date())
                    }
                ]
            }

            $http.post(ServerDestino + 'job/' + id + '/updatePostulation',objEnvio).then(function(response){
                if(response.data.message=='OK'){
                    alertify.success(' <div class="text-center font-size-16 text-white"> El perfil ha sido '+estado+' </div>');
                    $rootScope.filtrarCambio(id,estado);
                }else{
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al enviar datos de postulación </div>');
                }
            });
        }

        $rootScope.filtrarCambio = function(id,estado){
            var found = $filter('filter')($rootScope.postulates,{_id:id},true)[0];
            if(found){
                switch(estado){
                    case "preseleccionado":
                        $rootScope.cambiarSeccion($rootScope.listaInicial,found,$rootScope.listaPreseleccionados,"inicial");
                        $rootScope.styles.preseleccionado = "parpadea";
                    break;

                    case "entrevistado":
                        $rootScope.cambiarSeccion($rootScope.listaPreseleccionados,found,$rootScope.listaEntrevistados);
                        $rootScope.styles.entrevistado = "parpadea";
                    break;

                    case "contratado":
                        $rootScope.cambiarSeccion($rootScope.listaEntrevistados,found,$rootScope.listaContratados);
                        $rootScope.styles.contratado = "parpadea";
                    break;

                    case "descartado":
                        /*$rootScope.descartador(found);*/
                        found.isDescartado = true;
                    break;
                }
            }
        }

        $rootScope.cambiarSeccion = function(objOrigen,objSelect,objDestino,typo){
            var indx = objOrigen.indexOf(objSelect);
            objDestino.push(objSelect);
            if (typo=="inicial") {
                var found1 = $filter('filter')($rootScope.listaAdmin,{_id:objSelect._id},true)[0];
                if(found1){
                    $rootScope.listaAdmin.splice($rootScope.listaAdmin.indexOf(found1),1);
                }else{
                    var found2  = $filter('filter')($rootScope.listaPerson,{_id:objSelect._id},true)[0];
                    if (found2) {
                        $rootScope.listaPerson.splice($rootScope.listaPerson.indexOf(found1),1);
                    }
                }
            }
            objOrigen.splice(indx,1);

        }

        $rootScope.noParpadear = function(pos){
            $rootScope.styles[pos] = "";
        }

        $rootScope.convertDate = function(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
                var d = new Date(inputFormat);
            return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
        }

        $rootScope.getLast = function(id){
            if (id) {
                var found = $filter('filter')($rootScope.listaAdmin,{_id:id},true)[0];
                
                if (found) {
                    var indx = $rootScope.listaAdmin.indexOf(found);
                    if (indx==$rootScope.listaAdmin.length-1) {
                        $rootScope.lastItem = true;
                        return true;
                    } else {
                        $rootScope.lastItem = false;
                        return false;
                    }
                } else {
                    $rootScope.lastItem = false;
                    return false;
                }
            }
            $rootScope.lastItem = false;
            return false;
        }

        $scope.dynamicPopover = {
            content: 'Hello, World!',
            templateUrl: 'myPopoverTemplate.html',
            title: 'Title'
        };

        $scope.placement = {
            options: [
              'top',
              'top-left',
              'top-right',
              'bottom',
              'bottom-left',
              'bottom-right',
              'left',
              'left-top',
              'left-bottom',
              'right',
              'right-top',
              'right-bottom'
            ],
            selected: 'top'
        };
          
        $scope.htmlPopover = $sce.trustAsHtml('<b style="color: red">I can</b> have <div class="label label-success">HTML</div> content');        
    }
    
    navbarCompanyController
        .$inject = ["localStorageService", 
            "$rootScope", 
            "$http", 
            "$location", 
            "$mdDialog", 
            "$mdMedia",
            "$scope", 
            "$window", 
            "$q", 
            "$routeParams", 
            "$filter",
            "upload",
            "$sce",
            "$uibModal",
            "$document",
            "$payments"
    ];
    angular
        .module('spa-SE')
        .controller('navbarCompanyController', navbarCompanyController);
})();

function previewFileCompany() {
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    if (file) {
        $("#divButton").attr("style", "display: inline-block;");
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
};

function subirImagenCompany() {
    $("#divLoader").attr("style", "display: inline-block;");
    $("#divButton").attr("style", "display: none;");
    var formData = new FormData($("form#formCompanyLogo")[0]);
    setTimeout(function() {
        $.ajax({
            url: ServerDestino + "companies/" + userId + "/logo",
            beforeSend: function(request) {
                request.setRequestHeader('Authorization', 'Bearer ' + token);
            },
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                if (respuesta.message == "IMAGE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> El archivo no es una imagen </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "SIZE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Imagen demasiado pesada </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Foto actualizada con éxito </div>');
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                    location.reload();
                }
            }
        });
    }, 100);
}

function previewFileLogoCompany() {
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    if (file) {
        $("#divButton").attr("style", "display: inline-block;");
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
};

function subirImagenLogoCompany() {
    $("#divButton").attr("style", "display: none;");
    $("#divLoader").attr("style", "display: inline-block;"); 
    $("#logoBtn").prop('disabled', true);    
    var formData = new FormData($("form#reg-empre")[0]);
    setTimeout(function() {
        $.ajax({
            url: ServerDestino + "companies/" + userId + "/logo",
            beforeSend: function(request) {
                request.setRequestHeader('Authorization', 'Bearer ' + token);
            },
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                if (respuesta.message == "IMAGE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> El archivo no es una imagen </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else if (respuesta.message == "SIZE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Imagen demasiado pesada </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else if (respuesta.message == "ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                } else {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Foto actualizada con éxito </div>');
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                    $("#logoBtn").prop('disabled', false); 
                    location.reload();
                }
            }
        });
    }, 100);
}
