/**
 * Created by WilderAlejandro on 3/12/2015.
 */

(function (){
    'use strict'

    function RecuperaController($http, $location, $rootScope, $routeParams, $scope){
        /* --------------------------------------------------------
         * Declaracion que guarda el tokens
         * ---------------------------------------------------------*/
        $rootScope.tokens = "";
        $rootScope.correo = "";

        this.cambiarContrasena = function (pasw, paswConfirm) {
            this.objEnvio = {
                email: $rootScope.correo,
                password: pasw,
                password_confirmation: paswConfirm,
                token: $rootScope.tokens
            };

            $http.post(ServerDestino + "password/reset", this.objEnvio).then(function (response) {
                if (response.data.message == "OK") {
                    swal({
                        title: "¡Felicidades!",
                        text: "Su contraseña fue modificada correctamente, lo redireccionaremos al inicio de sesión ",
                        type: "success",
                        confirmButtonColor: "#00B34F"
                    }).then(
                        function () {
                            $scope.$apply(function () {
                                setTimeout(function () {
                                    $location.path("/login");
                                }, 200);
                            });
                        }
                    );
                }
                else {
                    swal({
                        title: "¡Error!",
                        text: "Su contraseña no puede ser cambiada, inténtelo nuevamente, si el error persiste escribanos a somos@soyempleo.com",
                        type: "error",
                        showCancelButton: true,
                        cancelButtonText: "Regresar",
                        cancelButtonColor: "#DD6B55"
                    });
                }
            });
        };

        this.getTokens = function () {
            $rootScope.tokens = $routeParams.id;
            $rootScope.correo = $routeParams.em;
        };

        this.getTokens();
    }
    RecuperaController.$inject = ["$http", "$location", "$rootScope", "$routeParams", "$scope"];
    angular
        .module('spa-SE')
            .controller('recuperaController', RecuperaController);
})();