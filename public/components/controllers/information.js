(function(){
	'use strict';

	informationController.$inject = ["localStorageService", "$rootScope", "$http", "$location",
	"$scope"];
	angular
	.module('spa-SE')
	.controller('informationController', informationController);

	function informationController(storage, $rootScope, $http, $location, $scope){

		var inf = this;
		inf.resp = {};
		inf.urlFront = UrlActual+"pay";
		inf.urlBack = "urlBack";

		inf.primerCarga = function(){
			if(storage.get("tokenCompany")){
				$http.get('https://api.soyempleo.co/offers/numbycompany/'+storage.get("tokenCompany"))
				.then(function(response){
					if(response.status == 200){
						inf.resp = response.data;
					}else{
						console.log("Error en conexión recargue la pagina");
					}
				});
			}else{
				$location.path('/dashboard');
			}	
		}

		inf.primerCarga();

		inf.enviarPago = function($event){
			swal({
				title: "Pago",
				text: "En este momento te redirigiremos a PayU plataforma de pago oficial de SoyEmpleo.com",
				type: "info"
			});
		}

		inf.goOff = function(){
			$location.path("/nuevaOferta")
		}

	}

})()