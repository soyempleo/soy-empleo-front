/**
 * Created by WilderAlejandro on 30/11/2015.
 */

 (function (){
    'use strict'

    function EmpresaController($http, $location, $rootScope, $scope, storage){

        var emc = this;

        $rootScope.profileGo = "";
        emc.urlActual = window.location.href;
        emc.nombreSaludo = "";
        emc.msgVal = {};
        emc.isResfresing = false;
        $rootScope.UserId = "";

        emc.consultar = function () {
            if (storage.get("tokenUser")) {
                $location.path("/session");
            }
        };

        emc.changeUrl = function(value){
            emc.aux = "";
            for (var i = 0;i<(emc.urlActual.length-7); i++){
                emc.aux = emc.aux + emc.urlActual.charAt(i)
            }
            emc.urlActual = emc.aux+value;
            $rootScope.profileGo = emc.urlActual;
            return emc.urlActual;
        };

        emc.cortarString = function (cadena) {
            emc.nombreSaludo = "";
            try {
                for (var i = 0; i < cadena.length; i++) {
                    if (cadena[i] == " ") {
                        break;
                    }
                    else {
                        emc.nombreSaludo = emc.nombreSaludo + cadena[i];
                    }
                }
            } catch (e) {
                console.log(e);
            }
        };

        emc.validarForm = function(){
            if(!emc.legalTerms){
                emc.msgVal.legal = true;
                return false
            }
            emc.msgVal.legal = false;
            return true
        }

        emc.registrar = function (first_name, email, password, repetir, valid) {

            if(emc.validarForm() && valid && password == repetir)
            {   
                emc.isResfresing = true;
                emc.msgVal.revisar = false;
                emc.objEnvio = {
                    "name": first_name,
                    "email": email,
                    "password": password,
                    "type_user": "company"
                };
                $http.post(ServerDestino + "/users", emc.objEnvio).then(function (response) {
                    $rootScope.UserId = response.data.data;
                    if (response.data.message == "OK") {
                        swal({
                            title: "¡Felicidades!",
                            text: "¡Ya su empresa ha sido registrada en Soyempleo.com!",
                            type:"success",
                            confirmButtonColor: "#00B34F"
                        }).then(
                            function () {
                                emc.objApi = {
                                    email: email,
                                    password: password
                                };
                                $http.post(ServerDestino + "api/authenticate", emc.objApi).then(function (response) {
                                    storage.set("tokenCompany", response.data.token);
                                    storage.set("dataUser", response.data.company[0]);
                                    storage.set("tipoUser", "company");
                                    $rootScope.loginx = true;
                                    alertify.success(' <div class="text-center font-size-16 text-white"> Bienvenido a SoyEmpleo.com </div> ');
                                    $rootScope.isLoged = true;
                                    window.location = "#/dashboard-company";
                                }).finally(
                                    function() {
                                        emc.isResfresing = false;
                                    }
                                );
                            }
                        );
                    }else if (response.data.message == "USER_EXIST") {
                        swal({
                            title: "¡Error!",
                            text: "Al parecer ya existe una empresa con estos datos, ¿Se te olvidaron tus datos de acceso?",
                            type: "error",
                            showCancelButton: true,
                            cancelButtonText: "Regresar",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ir a inicio de sesión"
                        }).then(
                            function () {
                                if (isConfirm) {
                                    $scope.$apply(function () {
                                        $location.path("/");
                                    });
                                }
                            }
                        );


                    }

                });
            }else
            {
                emc.msgVal.revisar = true;
                setTimeout(function(){
                    $scope.$apply(function(){
                        emc.msgVal.revisar = false;
                    });
                },6000);
            }
        };
    }
    EmpresaController.$inject = ["$http", "$location", "$rootScope", "$scope","localStorageService"];
    angular
    .module('spa-SE')
    .controller('empresaController', EmpresaController);
})();