(function() {
	'use strict'

	function ofertaInController(
		$http, 
		$location, 
		$scope, 
		$rootScope, 
		$routeParams, 
		storage, 
		$filter,
		$mdDialog,
		$mdMedia,
        $window, 
        $q) {
	                         
		var ofi = this;
		$rootScope.urlOffert = '';
		ofi.listOfertas = [];
		ofi.responseOfertas = [];
		ofi.idOferta = $routeParams.id;
		ofi.empresa = $routeParams.company;
		ofi.city = $routeParams.ciudad;
		ofi.percentage = 0;
		ofi.share = "";

		ofi.share = window.location.href;
 		ofi.colsm8 = $rootScope.isLoged ? "":"col-sm-8";
 		ofi.colsm4 = $rootScope.isLoged ? "":"col-sm-4 top--180";
		if ($routeParams.id == null) {
			$location.path('/listOfertas');
		} else {
			$http.get(ServerDestino + 'offers/'+$routeParams.id)
			.then(function(response){
				ofi.o = response.data.offer;
				ofi.o.limit_date.date = ofi.convertDate(ofi.o.limit_date.date);
				ofi.imgCheck();
				ofi.getOtherOf(); 
				var base = ofi.o.base_salary.length > 0 ? parseInt(ofi.o.base_salary):0;
				var extra = ofi.o.extra_salary.length > 0 ? parseInt(ofi.o.extra_salary):0;
				ofi.o.total = base + extra;
			});
		}

		ofi.convertDate = function(inputFormat) {
		  function pad(s) { return (s < 10) ? '0' + s : s; }
		  var d = new Date(inputFormat);
		  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
		}
		

		ofi.consultar = function () {
            if (storage.get("tokenUser")) {
                $rootScope.loginx = true;
                $rootScope.isLoged = true;
                token = storage.get("tokenUser");
                try {
                    $rootScope.idPerson = storage.get("dataUser")._id;
                } catch (e) {
                }
                return false;
            } else {
                $rootScope.isLoged = false;
                return true;
            }
        };

		if ($rootScope.isLoged) {
			$rootScope.urlOffert = '';
			ofi.consultar();			
		}

		ofi.getOtherOf = function() {

			$http.get(ServerDestino + 'offers/bycompany/' 
			          + ofi.o.company._id)
			.then(function(response) {
				ofi.listOfertas = [];
				ofi.responseOfertas = response.data.offers;
				for(var i=0;i<ofi.responseOfertas.length;i++){
					var base = ofi.responseOfertas[i].base_salary.length>0 ? parseInt(ofi.responseOfertas[i].base_salary):0;
					var extra = ofi.responseOfertas[i].extra_salary.length>0 ? parseInt(ofi.responseOfertas[i].extra_salary):0;
					ofi.responseOfertas[i].total = base + extra;
				};
				var vueltas = 0;
				for(var i = ofi.responseOfertas.length-1; i >0; i--){
					if(vueltas>3){
						break;
					}else{
						ofi.listOfertas.push(ofi.responseOfertas[i]);
						ofi.listOfertas[ofi.listOfertas.length-1].ciudad =
						ofi.cambiarString(ofi.city);
						ofi.listOfertas[ofi.listOfertas.length-1].urlCompany =
						ofi.cambiarString(ofi.empresa);
						ofi.listOfertas[ofi.listOfertas.length-1].job =
						ofi.cambiarString(ofi.listOfertas
						                  [ofi.listOfertas.length-1]
						                  .job_title);
						vueltas++;
					}
				}
			});
		}

		ofi.irOferta = function(i) {
			$location
			.path('/ofertas/empleo/' + ofi.listOfertas[i].ciudad 
			      + '/' + ofi.listOfertas[i].urlCompany 
			      + '/' + ofi.listOfertas[i].job
			      + '/' + ofi.listOfertas[i]._id);
		}

		ofi.cambiarString = function(s) {
			var aux = s.replace(/ /g, "-");
			return aux;
		}

		ofi.imgCheck = function(){
			
			ofi.o.company.logo = (ofi.o.company.logo!="" && ofi.o.company.logo!=null) ? "https://api.soyempleo.com/img/pictures/companies/logos/" + ofi.o.company.logo : "assets/images/logo-offert-default.png";
			
		}
		
		ofi.irPerfil = function(){
			var nombre = ofi.o.company.social_name != "" &&
			 			 ofi.o.company.social_name != null ?
			 			 ofi.urlLimpia(ofi.o.company.social_name): 
			 			 ofi.urlLimpia(ofi.o.company.name);
			$location
			.path('/company/' + nombre + '/' + ofi.o.company._id);
		}

		ofi.urlLimpia = function (value) {
            var tmp_this = value.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            return tmp_this.replace(/[^\\s\w]/g, "-");
        }

        ofi.openModal = function(ev,template) {
            $.scrollTo('html, body',90);
            setTimeout(function(){
                $scope.$apply(function(){
                    $.scrollTo('html, body',90);
                    if(template == 'dialog55'){
                        $rootScope.urlOffert = window.location.href;
                    }

                    $mdDialog.show({
                        controller: controllerDialog,
                        templateUrl: template+'.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        escapeToClose: true
                    }).then(function(answer) {}, function() {});
                });
            },200);            
        };

        ofi.openModal = function(ev,template) {
            $.scrollTo('html, body',90);
            setTimeout(function(){
                $scope.$apply(function(){
                    $.scrollTo('html, body',90);
                    if(template == 'dialog55'){
                        $rootScope.urlOffert = window.location.href;
                    }

                    $mdDialog.show({
                        controller: controllerDialog,
                        templateUrl: template+'.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        escapeToClose: true
                    }).then(function(answer) {}, function() {});
                });
            },200);            
        };

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window) {
            controllerDialog.prototype.$scope = $scope;

            $scope.cancel = function () {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
            };

            $scope.goToUrl = function(url){
	            window.location = url;
	        } 
        };

        controllerDialog.prototype.setFile = function(element) {
            var $scope = this.$scope;
            $scope.$apply(function() {
                $scope.theFile = element.files[0];
            });
        };
	}
	ofertaInController.$inject = [
		"$http", 
		"$location", 
		"$scope",
		"$rootScope", 
		'$routeParams',
		"localStorageService", 
		'$filter',
		"$mdDialog",
		"$mdMedia",
        "$window", 
        "$q"];


	angular
	.module('spa-SE')
	.controller('ofertaInController', ofertaInController);
})();
