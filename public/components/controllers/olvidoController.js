/**
 * Created by WilderAlejandro on 2/12/2015.
 */

(function (){
    'use strict'

    function OlvidoController($http, $location, $scope){

        this.consultarCorreo = function (correo) {
            this.objEnvio = {email: correo, link: "https://soyempleo.com/#/recupera/"+correo+"/"};
            $http.post(ServerDestino + "password/email", this.objEnvio).then(function (response) {
                if (response.data.message == "OK") {
                    swal({
                        title: "¡Siguiente paso!",
                        text: "Te enviamos un correo informando el siguiente paso para recuperar tu contraseña ¡Ya falta poco!",
                        type: "success",
                        confirmButtonColor: "#00B34F"
                    }).then(
                        function () {
                            $scope.$apply(function () {
                                $location.path("/login");
                            });
                        }
                    );
                }
                else {
                    swal({
                        title: "¡Error!",
                        text: "No existe un usuario en nuestra basde de datos con este correo, ¿desea registrarse?",
                        type: "error",
                        showCancelButton: true,
                        cancelButtonText: "Regresar",
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ir a registro"
                    }).then(
                        function () {
                            $scope.$apply(function () {
                                $location.path("/");
                            });
                        }
                    );
                }
            });
        };
    }
    OlvidoController.$inject = ["$http", "$location", "$scope"];
    angular
        .module('spa-SE')
            .controller('olvidoController', OlvidoController);
})();
