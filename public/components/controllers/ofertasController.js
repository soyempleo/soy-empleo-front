(function () {
    'use strict'
    function ofertasController($http, $location, $scope,
                               $rootScope, $routeParams,$filter) {
        var of = this;
        of.listOfertas = [];
        of.response = [];
        of.puntero = 1;
        of.position = 1;
        of.hojas = [];

        // paginator items
        of.currentPage = 1;
        of.totalItems = 0;
        of.entryLimit = 10; // items per page
        of.noOfPages = 0;
        of.items = [];
 
        $rootScope.selectOf = {};
        $rootScope.companyPic = "assets/images/logo-offert-default.png";

        of.redirect = function (url) {
            $location.path(url);
        }

        of.irOferta = function (id) {
            var foundItem = $filter('filter')(of.response,{_id: id}, true)[0];
            $rootScope.selectOf = of.response[of.response.indexOf(foundItem)];
            var ciudad = of.urlLimpia(of.response[of.response.indexOf(foundItem)].ciudad);
            var job = of.urlLimpia(of.response[of.response.indexOf(foundItem)].job);

            $location
                .path('/ofertas/empleo/' + ciudad
                + '/' + of.response[of.response.indexOf(foundItem)].urlCompany
                + '/' + job
                + '/' + of.response[of.response.indexOf(foundItem)]._id);
        }

        /**
         * Funcion que sustituye espacios y caracteres especiales
         * de los strings que recibe
         * @param  string
         * @return string
         */
        of.urlLimpia = function (value) {
            var tmp_this = value.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            return tmp_this.replace(/[^\\s\w]/g, "-");
        }

        /**
         * Funcion que se encarga de lanzar la primera carga para llenar
         * los datos iniciales necesarios para mostrar las ofertas
         * @return {[type]} [description]
         */
        of.firstCharge = function () {
            $http.get(ServerDestino + "offers/all")
                .then(function (response) {
                    of.response = response.data.offers;
                    for (var i = 0; i < of.response.length; i++) {
                        var base = of.response[i].base_salary.length > 0 ? parseInt(of.response[i].base_salary):0;
                        var extra = of.response[i].extra_salary.length > 0 ? parseInt(of.response[i].extra_salary):0;
                        of.response[i].total = base + extra;
                        var fecha = of.cotarFecha(of.response[i].created_at);
                        of.response[i].fecha = fecha;
                        of.response[i].urlCompany = of.cambiarString(of.response[i].companies.name);
                        of.response[i].job = of.cambiarString(of.response[i].job_title);
                        of.response[i].ciudad = of.cambiarString(of.response[i].city);
                        if (of.response[i].companies.logo != "" && of.response[i].companies.logo != null) {
                            of.response[i].companies.logo = ServerDestino + "profile/companies/logos/" + of.response[i].companies.logo;
                            if (of.urlValidator(of.response[i].companies.logo) == null) {
                                of.response[i].companies.logo = "assets/images/logo-offert-default.png";
                            }
                        } else {
                            of.response[i].companies.logo = "assets/images/logo-offert-default.png";
                        }

                    }
                    of.items = of.response;
                    of.totalItems = of.response.length;
                    of.entryLimit = 10; // items per page
                    of.noOfPages = Math.ceil(of.totalItems / of.entryLimit);
                });
        }

        /**
         * Funcion que verifica y valida la existencia de una imagen
         * segun la url recibida
         * @param  {[string]} url [string con url de imagen]
         */
        of.urlValidator = function (url) {
            return $http.get(url).then(function (response) {
                return "OK";
            }).catch(function (err) {
                if (err.status === 404) {
                    return null
                }
                ;

                return $q.reject(err);
            })
        };

        of.firstCharge();

        /**
         * Funcion que se encarga de cortar las fechas en un formato
         * correcto para que pueda visualizarse de la manera correcta
         * @param  {[DATE]} p [Fecha tipo timeStamp]
         * @return {[String]}   [Fecha formato correcto]
         */
        of.cotarFecha = function (p) {
            var aux = "";
            for (var i = 0; i < 10; i++) {
                aux = aux + p[i];
            }
            return aux;
        }
        
        of.cambiarString = function (s) {
            var aux = s.replace(/ /g, "-");
            aux = of.urlLimpia(aux);
            return aux;
        }

        of.urlLimpia = function(value){
            var tmp_this = value.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for(var i=0; i<arr_busca.length; i++ ){
                tmp_this = tmp_this.replace(arr_busca[i],arr_reemplaza[i]);
            }
            return tmp_this.replace(/[^\\s\w]/g,"-");
        }


    }

    ofertasController.$inject = ["$http", "$location", "$scope",
        "$rootScope", '$routeParams','$filter'];
    angular
        .module('spa-SE')
        .controller('ofertasController', ofertasController);
})();
