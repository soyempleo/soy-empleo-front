(function () {
    'use strict'


    function config($routeProvider, $mdDateLocaleProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'public/app/home.html'
            })
            .when('/dashboard', {
                templateUrl: 'public/app/perfil.html'
            })
            .when('/postulados', {
                templateUrl: 'public/app/companyPostulates.html'
            })
            .when('/perfil2', {
                templateUrl: 'public/app/perfil2.html'
            })
            .when('/ofertas/empleo/:ciudad/:company/:job/:id', {
                templateUrl: 'public/app/ofertaIn.html'
            })
            .when('/perfil/persona/:id', {
                templateUrl: 'public/app/perfil-public.html'
            })
            .when('/perfil/persona', {
                templateUrl: 'public/app/perfil-public.html'
            })
            .when('/dashboard-company', {
                templateUrl: 'public/app/perfil-company.html'
            })
            .when('/dashboard-company/:status?', {
                templateUrl: 'public/app/perfil-company.html'
            })
            .when('/prebuy', {
                templateUrl: 'public/app/information.html'
            })
            .when('/pay', {
                templateUrl: 'public/app/payView.html'
            })
            .when('/listOfertas', {
                templateUrl: 'public/app/ofertas.html'
            })
            .when('/nuevaOferta/', {
                templateUrl: 'public/app/nuevaOferta.html'
            })
            .when('/editOferta/:idOff', {
                templateUrl: 'public/app/editarOferta.html'
            })
            .when('/persona', {
                templateUrl: 'public/app/persona.html'
            })
            .when('/empresa', {
                templateUrl: 'public/app/empresa.html'
            })
            .when('/contacto', {
                templateUrl: 'public/app/contacto.html'
            })
            .when('/como-funciona', {
                templateUrl: 'public/app/funciona.html'
            })
            .when('/login', {
                templateUrl: 'public/app/login.html'
            })
            .when('/olvido', {
                templateUrl: 'public/app/olvido.html'
            })
            .when('/recupera/:em/:id', {
                templateUrl: 'public/app/recuperar.html'
            })
            .when('/faq', {
                templateUrl: 'public/app/faq.html'
            })
            .when('/company/:name/:id', {
                templateUrl: 'public/app/companyOffers.html',
            })
            .when('/pionera/1', {
                templateUrl: 'public/app/pionera1.html',
            })
            .when('/pionera/2', {
                templateUrl: 'public/app/pionera2.html',
            })
            .when('/pionera/3', {
                templateUrl: 'public/app/pionera3.html',
            })
            .when('/pionera/4', {
                templateUrl: 'public/app/pionera4.html',
            })
            .when('/pionera/5', {
                templateUrl: 'public/app/pionera5.html',
            })
            .when('/somos', {
                templateUrl: 'public/app/somos.html',
            })
            .when('/terminos', {
                templateUrl: 'public/app/terminos.html',
            })
            .when('/session', {
                templateUrl: 'public/app/perfil.html'
            })
            .when('/hoja', {
                templateUrl: 'public/app/hoja.html'
            })
            .when('/aplicacion', {
                templateUrl: 'public/app/aplicacion.html'
            })
            .when('/actualiza', {
                templateUrl: 'public/app/actualiza.html'
            })
            .when('/buscar', {
                templateUrl: 'public/app/buscar.html'
            })
            .when('/detalle', {
                templateUrl: 'public/app/detalle.html'
            })
            .when('/config', {
                templateUrl: 'public/app/config.html'
            })
            /*.when('/membership', {
                templateUrl: 'public/app/membership.html'
            })*/
            .when('/landing-company', {
                templateUrl: 'public/app/landCompany.html'
            })
            .when('/adminlogin/:status/:typeuser?/:person?/:first?/:last?/:tk?', {
                templateUrl: 'public/app/authuser.html'
            })
            .when('/linkedin/:status/:person?/:first?/:last?/:tk?', {
                templateUrl: 'public/app/linkedin.html'
            })
            .otherwise({
                templateUrl: 'public/app/404.html'
            });

        $mdDateLocaleProvider.months = ['Enero', 'Febrero', 'Marzo', 'Abril',
            'Mayo', 'Junio', 'Julio', 'Agosto',
            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        $mdDateLocaleProvider.shortMonths = ['Ene', 'Feb', 'Mar', 'Abr',
            'May', 'Jun', 'Jul', 'Ago',
            'Sep', 'Oct', 'Nov', 'Dic'];
        $mdDateLocaleProvider.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes'];
        $mdDateLocaleProvider.shortDays = ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'];
        // Can change week display to start on Monday.
        $mdDateLocaleProvider.firstDayOfWeek = 1;
        // Optional.
        $mdDateLocaleProvider.dates = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
            31];
        // Example uses moment.js to parse and format dates.
        $mdDateLocaleProvider.parseDate = function (dateString) {
            var m = moment(dateString, 'L', true);
            return m.isValid() ? m.toDate() : new Date(NaN);
        };
        $mdDateLocaleProvider.formatDate = function (date) {
            return moment(date).format('DD/MM/YYYY');
        };

        /*$mdDateLocaleProvider.monthHeaderFormatter = function(date) {
         return myShortMonths[date.getMonth()] + ' ' + date.getFullYear();
         };*/
        // In addition to date display, date components also need localized messages
        // for aria-labels for screen-reader users.
        $mdDateLocaleProvider.weekNumberFormatter = function (weekNumber) {
            return 'Semana ' + weekNumber;
        };
        $mdDateLocaleProvider.msgCalendar = 'Calendario';
        $mdDateLocaleProvider.msgOpenCalendar = 'Abrir Calendario';
    }

    angular
        .module('spa-SE')
        .config(config);
})();