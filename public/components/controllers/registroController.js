/*--------------------------------------------------------
 Funciones para Datos Basicos
 ---------------------------------------------------------- */
 (function () {
    'use strict'

    function RegistroController($http, $location, $rootScope, putPersons,
       storage, $scope) {

        var rg = this;
        rg.legalTerms = false;
        rg.urlActual = window.location.href;
        rg.msgVal = {};
        rg.msgVal.legal = false;
        rg.isRefresing = false;

        rg.consultar = function () {
            if (storage.get("tokenUser")) {
            }
        };

        rg.changeUrl = function(){
            rg.aux = "";
            for (var i = 0;i<(rg.urlActual.length-9); i++){
                rg.aux = rg.aux + rg.urlActual.charAt(i)
            }
            rg.urlActual = rg.aux+"profile";
        };

        rg.changeUrl();
        rg.consultar();

        rg.pasoDatosBasicos = false;

        rg.nombreSaludo = "";
        $rootScope.UserId = "";



        rg.consultar = function () {
            if (storage.get("tokenUser")) {
                $location.path("/session");
            }
        };

        rg.cortarString = function (cadena) {
            rg.nombreSaludo = "";
            try {
                for (var i = 0; i < cadena.length; i++) {
                    if (cadena[i] == " ") {
                        break;
                    }
                    else {
                        rg.nombreSaludo = rg.nombreSaludo + cadena[i];
                    }
                }
            } catch (e) {
            }
        };

        rg.validarForm = function(){
            if(!rg.legalTerms){
                rg.msgVal.legal = true;
                return false
            }
            rg.msgVal.legal = false;
            return true
        }

        rg.registrarDatosBasicos = function (nombre, apellido, email, password, repetir,
         valid) {

            if(rg.validarForm() && valid && password == repetir){
                rg.isResfresing = true;
                rg.msgVal.revisar = false;
                rg.objEnvio = {
                    "first_name": nombre,
                    "last_name": apellido,
                    "email": email,
                    "password": password,
                    "type_user": "person"
                };
                setTimeout(function() {
                    $http.post(ServerDestino + "/users", rg.objEnvio).then(function (response) {
                        $rootScope.UserId = response.data.data;
                        if (response.data.message == "OK") {
                                //cargar campos
                                rg.pasoDatosBasicos = true;

                                swal({
                                    title: "¡Felicidades!",
                                    text: "¡Ya eres un usuario de Soyempleo.com! Ahora para tener más oportunidades ¿Por qué no nos cuentas un poco de ti?!",
                                    type: "success",
                                    confirmButtonColor: "#00B34F"
                                }).then(
                                    function () {
                                        rg.isResfresing = true;
                                        rg.objEnvio = {
                                            email: email,
                                            password: password
                                        };
                                        $http.post(ServerDestino + "api/authenticate", rg.objEnvio).then(function (response) {
                                            storage.set("tokenUser", response.data.token);
                                            storage.set("dataUser", response.data.person[0]);
                                            storage.set("Us", response.data);
                                            storage.set("tipoUser", "person");
                                            $rootScope.loginx = true;
                                            alertify.success(' <div class="text-center font-size-16 text-white"> Bienvenido a SoyEmpleo.com </div> ');
                                            if ($rootScope.urlOffert) {
                                                $rootScope.isLoged=true;;
                                                window.location = $rootScope.urlOffert;
                                            }else{
                                                window.location = rg.aux+"#/dashboard";
                                            }

                                        }).finally(
                                            function() {
                                                rg.isResfresing = false;
                                            }
                                        );
                                    }
                                );
                            }
                            else if (response.data.message == "USER_EXIST") {
                                rg.pasoDatosBasicos = false;

                                swal({
                                    title: "¡Error!",
                                    text: "Al parecer ya existe un usuario con estos datos, ¿Se te olvidaron tus datos de acceso?",
                                    type: "error",
                                    showCancelButton: true,
                                    cancelButtonText: "Regresar",
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ir a inicio de sesión"
                                }).then(
                                    function () {
                                        $scope.$apply(function () {
                                            $location.path("/login");
                                        });
                                    }, function () {

                                    }
                                );
                            }
                        }).finally(
                            function() {
                                rg.isResfresing = false;
                            }
                        );
                }, 100);
            }else{
                rg.msgVal.revisar = true;
                setTimeout(function(){
                    $scope.$apply(function(){
                        rg.msgVal.revisar = false;
                    });
                },6000);
            }
        };
    }

    RegistroController.$inject = ["$http", "$location", "$rootScope", "putPersons", "localStorageService"
    , "$scope"];
    angular
    .module('spa-SE')
    .controller('registroController', RegistroController);
})
();