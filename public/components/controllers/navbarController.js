/**
 * Created by WilderAlejandro on 25/11/2015.
 */
var token = "";
var userId = "";
var dashBoard = false;

(function () {
    'use strict'

    function NavbarController(storage, $rootScope, $http, $location, $mdDialog, $mdMedia, $scope, $window, $q,$filter,filterFilter) {

        var nbc = this;
        nbc.textIr = "Ir a perfil";
        nbc.isProfile = false;
        $rootScope.selectOf = {};
        nbc.ordened = {
            type:"",
            reversa:false
        }
        nbc.loadedDiv = false;
        $rootScope.isLoged = false;
        $rootScope.userAvatar = "assets/images/user_male.png";
        $rootScope.provieneDashboard = false;
        $rootScope.isLandingCompany = true;
        nbc.listoPrecio = false;
        $rootScope.profileGo2 = "";
        nbc.orden = "";
        nbc.profileData = {};
        nbc.preguntas= [];
        nbc.offersByPreference = [];
        nbc.personExp = [];
        nbc.postulations = [];
        nbc.offersLoaded = [];
        nbc.preferencias = [];
        nbc.ciudadesFiltro = [];
        nbc.sectoresFiltro = [];
        nbc.experienciaFiltro = [];
        nbc.fechas = [];
        nbc.fechas[0] = false;
        nbc.fechas[1] = false;
        nbc.fechas[2] = false;
        nbc.respondidasLength = false;
        nbc.provieneDeAjustador = false;
        nbc.personEstudies = 0;
        nbc.preguntasCount = 0;
        nbc.preguntasList = [];
        nbc.preguntasRest = "";
        nbc.fechaNac = "";
        nbc.nombreCompleto = "";
        nbc.birthDay = "";
        nbc.birthMonth = "";
        nbc.birthMonth = "";
        nbc.birthYear = "";
        nbc.edad = "Agregar edad";
        nbc.antiguoTitle = "";
        nbc.tituloProfesional = "";
        nbc.percentage = "";
        nbc.nuevoTitulo = "";
        // paginator items 
        // buscador ofertas paginador
        nbc.currentPageOffers = 1;
        nbc.itemsOffers = []; 
        nbc.totalItemsOffers = 0; 
        nbc.entryLimitOffers = 5; 
        nbc.noOfPagesOffers = 0;
        // ofertas sugeridas paginador
        nbc.currentPagePrefs = 1;
        nbc.totalItemsPrefs = 0; 
        nbc.entryLimitPrefs = 5; 
        nbc.noOfPagesPrefs = 0;
        nbc.price = {
            range: {
                min: 0,
                max: 0
            },
            minPrice: 0,
            maxPrice: 0
        };
        nbc.passed = false;
        nbc.loged = false;
        nbc.isPrecios = false;
        nbc.cambiandoTitle = false;
        nbc.percentage = 0;
        nbc.urlActual = window.location.href;
        nbc.lastOffert = {};
        nbc.textSwitch = false;
        nbc.btnSwitch = [{btnEmpresa:true,btnClass:"btn-switch-active btn-switch-shadow-left"},
                         {btnPersona:false,btnClass:"btn-switch-default"}];

        nbc.cargarPreguntas = function(value){
            nbc.preguntas = [];
            if(value){
                nbc.preguntas.push({pregunta:"¿Subir mi hoja de vida a SoyEmpleo.com tiene algún costo?",
                    respuesta:["No. SoyEmpleo es un servicio gratuito para todas las personas que quieran encontrar empleo. Queremos que siga siendo así siempre. No tenemos paquetes VIP, ni preferencias con ningún usuario. Nos regimos bajo principios de igualdad y no discriminación"],
                    especial:false
                });
                nbc.preguntas.push({pregunta:"¿Que puedo hacer para que mi perfil sea tenido en cuenta más frecuentemente a las empresas?",
                    respuesta:["Te podemos podemos dar algunostips para rankear mejor en la búsqueda de candidatos:",
                               "1)  Completa tu perfil al 100%, es muy importante. No olvides poner una foto tuya.",
                               "2)  Utiliza las opciones que te sugieren en los autocompletados, especialmente en el campo de estudios y habilidades. Evita utilizar la opción “Otros” en las áreas o sectores de tus experiencias laborales.",
                               "3)  No olvides poner los salarios de tus experiencias laborales, estos NO serán vistos por las empresas, pero es una parte importante en nuestro análisis.",
                               "4)  Cuida la ortografía, resalta tus habilidades y los logros que has alcanzado en tu vida"],
                    especial:false
                });
                nbc.preguntas.push({pregunta:"¿Como registrarme y llenar mi perfil?",
                    respuesta:["Para registrarte por primera vez ingresa AQUÍ (((Dirigir a registro personas))), después podrás dirigirte a tu perfil y llenar la información de tu hoja de vida."],
                    especial:true
                });
                /*nbc.preguntas.push({pregunta:"¿SoyEmpleo me busca empleo automáticamente?",
                    respuesta:["En SoyEmpleo constantemente hacemos búsquedas de candidatos y remitimos los que mejor se ajusten a las necesidades de las empresas. Si encontramos una oferta que se ajusta a tu perfil y aspiración salarial te vamos a remitir. Te llegará un email o llamada telefónica informándote del proceso, tienes la opción de retirarte del mismo si no te llama la atención."],
                    especial:false
                });*/
                nbc.preguntas.push({pregunta:"¿Puedo postularme a las ofertas que me llamen la atención?",
                    respuesta:["Si, puedes postularte a las vacantes que te llamen la atención. El perfil será remitido a la psicóloga responsable del reclutamiento, ella los va a analizar y si cumples con el perfil se comunicará contigo. No te postules a todas las vacantes ya que el sistema puede bloquear tu cuenta temporalmente."],
                    especial:false
                });

                nbc.preguntas.push({pregunta:"La psicóloga de SoyEmpleo me ha contactado para una pre-entrevista telefónica, ¿ahora qué hago?",
                    respuesta:["Felicitaciones! Estás más cerca de obtener el empleo que quieres, es importante que estés pendiente en los próximos días ya que es posible que recibas una llamada para programar una entrevista presencial. Si no pasas al proceso de entrevista recibirás un email notificándote la novedad."],
                    especial:false
                });

                nbc.preguntas.push({pregunta:"Recibí una llamada para un proceso de entrevista, ¿ahora qué hago?",
                    respuesta:["Vamos muy bien, ahora prepárate! Te recomendamos seguir los siguientes consejos:",
                               "1)  Busca en internet información sobre la empresa, a que se dedica? Donde tienen sede?",
                               "2.  Prepara tu “elevator pitch”. No sabes qué es un Elevator Pitch? Mira este video, te va a ayudar a crear el tuyo https://www.youtube.com/watch?v=L9e_PMkgAvQ",
                               "3)  La presentación personal es fundamental, vístete como si ya fuera un día de trabajo. Intenta averiguar si en la empresa visten con trajes formales o si usar un jean está bien.",
                               "4)  Asiste a la entrevista y sé puntual!",
                               "Si no vas a asistir, escríbenos con 24 horas de anticipación a seleccion@soyempleo.com cancelando tu cita, nuestra imagen está en tus manos. Si faltas a una entrevista previamente programa sin justificación, no volverás a ser tenido en cuenta para otros procesos."],
                    especial:false,
                    especialX:true
                });

                nbc.preguntas.push({pregunta:"¿Puedo desactivar mi cuenta para no ser tenido en cuenta en ningún proceso?",
                    respuesta:["Si, en cualquier momento puedes deshabilitar tu cuenta desde la pestaña configuraciones, en la parte superior derecha de tu perfil, puedes volverla a habilitar cuando desees ser tenido en cuenta nuevamente. En este menú también puedes eliminar tu cuenta de forma permanente de la base de datos, este proceso es permanente y no se puede revertir."],
                    especial:false
                });
            }else{
                /*nbc.preguntas.push({pregunta:"¿Qué diferencias hay entre una vacante básica y una Premium?",
                    respuesta:["Con la publicación básica recibes las hojas de vida de las personas que voluntariamente se registren en tu vacante, sin límite de candidatos. No te garantizamos que cumplan el perfil requerido. En la vacante premium aparte de las personas que se postulen, analizamos miles de hojas de vida, preseleccionamos las que mejor cumplan con el perfil y hacemos verificación telefónica de los candidatos, así recibes hojas de vida útiles para llevar a cabo tus procesos de selección. Ahorra aproximadamente el 60% del tiempo y tus contrataciones son más efectivas. Más del 80% de las vacantes Premium terminan en contrataciones exitosas."],
                    especial:false,
                    especial2:false
                });*/
                nbc.preguntas.push({pregunta:"¿Cuánto cuesta una vacante en SoyEmpleo?",
                    respuesta:["Depende del tipo de vacante que elijas. Tenemos vacantes GRATIS ($0), vacante básica ($50.000) y vacante Premium la cual cuesta $149.000 o para perfiles cuyo salario sea superior a un millón de pesos, cobramos el 15% del salario."],
                    especial:false,
                    especial2:false
                });
                nbc.preguntas.push({pregunta:"¿Qué recibo en una vacante gratis?",
                    respuesta:["Cuando creas una vacante gratis, la publicamos en nuestro portal, siempre que cumpla con los términos y condiciones de SoyEmpleo. Los candidatos podrán ver la oferta de empleo y postularse a ella, podrás ver los perfiles y datos de contacto de todos los postulados, la publicación dura 30 días, sin límite de postulados. Publicando en SoyEmpleo cumples con el decreto 2852 de 2013 que obliga a todas las empresas a hacer públicas sus ofertas laborales. En nuestra plataforma puedes administrar tu proceso de selección, moviendo los candidatos a través de las diferentes fases del proceso de selección (pre-seleccionados, entrevistados, finalistas o descartados), los candidatos recibirán un mensaje cada vez que se modifique el estado de su postulación. Nunca más tendrás que realizar esa incomoda llamada para informar a un candidato que ha sido descartado, nosotros lo hacemos por ti."],
                    especial:false,
                    especial2:false
                });
                nbc.preguntas.push({pregunta:"¿En que consiste la vacante Premium de SoyEmpleo?",
                    respuesta:["SoyEmpleo ha construido una comunidad de reclutadores y caza-talentos virtuales, que son psicólogos profesionales en selección de personal. Cuando creas una vacante Premium, uno de nuestros caza-talentos recibe tu solicitud, se comunica contigo para entender tus necesidades y posteriormente realiza una búsqueda intensiva de candidatos en nuestras bases de datos, en redes sociales y en otros portales de empleo. Selecciona los más indicados, los pre-entrevista y escoge 4 finalistas. Así sólo recibes candidatos filtrados que les interesa tu oferta y cumplen el perfil, listos para una entrevista con su jefe inmediato, para que contrates el mejor."],
                    especial:false,
                    especial2:false
                });
                nbc.preguntas.push({pregunta:"¿Cuanto se demora el proceso de selección de una vacante Premium?",
                    respuesta:["Depende de la dificultad del perfil, en la mayoría de los casos podremos entregar los 4 finalistas en menos de 48 horas hábiles. En todo momento puedes ver en tiempo real los avances de tu proceso de selección en nuestra plataforma."],
                    especial:false,
                    especial2:false
                });
                nbc.preguntas.push({pregunta:"¿Como aumentar mis posibilidades de hacer una contratación exitosa?",
                    respuesta:["No garantizamos que termines contratando una persona. Pero nuestras estadísticas nos dicen que el 80% de las vacantes terminan en contrataciones exitosas. Algunos consejos son:"],
                    especial:false,
                    especial2:true
                });
            }
        }

        nbc.cargarPreguntas(nbc.textSwitch);

        nbc.botonSwitch = function(value){
            nbc.btnSwitch[0].btnClass = value == "empresa" ? "btn-switch-active btn-switch-shadow-left":"btn-switch-default";
            nbc.btnSwitch[1].btnClass = value == "persona" ? "btn-switch-active btn-switch-shadow-right":"btn-switch-default";
            nbc.textSwitch = value == "empresa" ? false:true;
            nbc.cargarPreguntas(nbc.textSwitch);
        }

        nbc.script = function(url) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url;
            var x = document.getElementsByTagName('head')[0];
            x.appendChild(s);
        }

        nbc.consultar = function () {
            setInterval(function () {
                if (storage.get("tipoUser") == "person") {
                    if ($location.path() == "/dashboard") {
                        nbc.textIr = "Ir a home";
                    } else {
                        nbc.textIr = "Ir a perfil";
                    }
                } else if (storage.get("tipoUser") == "company") {
                    nbc.script('assets/js/zopim.js');
                    if ($location.path() == "/dashboard-company") {
                        nbc.textIr = "Ir a home";
                    } else {
                        nbc.textIr = "Ir a perfil";
                    }
                }

            }, 500);

            if (storage.get("tokenUser")) {
                $rootScope.loginx = true;
                $rootScope.isLoged = true;
                nbc.loged = true;
                nbc.passed = true;
                token = storage.get("tokenUser");
                try {
                    $rootScope.idPerson = storage.get("dataUser")._id;
                } catch (e) {
                }
                return false;
            } else if (storage.get("tokenCompany")) {
                $rootScope.loginx = true;
                $rootScope.isLoged = true;
                nbc.loged = true;
                nbc.passed = true;
                token = storage.get("tokenCompany");
                return false;
            } else {
                $rootScope.isLoged = false;
                nbc.loged = false;
                nbc.passed = true;
                return true;
            }
        };

        /**
         * funcion para hacer scroll hacia donde se le indica
         * con el parámetro val 
         * @param  {[type]} val [description]
         * @return {[type]}     [description]
         */
        nbc.gotoId = function(val){
            var id = val;
            $('html, body').animate({
                scrollTop: $(id.toString()).offset().top
            }, 1000);
        }

        nbc.goToPrecios = function(){
            nbc.isPrecios = true;
            setTimeout(function () {
                $scope.$apply(function () {
                    nbc.gotoId('#preciosBox');
                });
            }, 20); 
            /*nbc.resizeDiv(); */          
        }

        nbc.redirect = function (url) {
            $location.path(url);
        }

        nbc.goToUrl = function(url){
            window.location = url;
        }

        $rootScope.desHojaVida = function (id) {
            $("#pdf_container").attr("src", ServerDestino + "pdf/profile/" + id);
            $rootScope.pdfRefresh = true;
            setTimeout(function(){
                $scope.$apply(function(){
                    $rootScope.pdfRefresh = false;
                }); 
            },3000);
        }

        nbc.goToBlog2 = function(){
            $('#tab-1').prop('checked', true);
            var win = window.open('https://blog.soyempleo.com/#/', '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Porfavor permitir popups para este website');
            }
        }

        nbc.acortarNombres = function(tipo, value){
            var auxNom = value.split(" ");
            var infor = '';
            if (tipo=='apellido') {
                if (auxNom.length>1) {
                    if (auxNom[0].toLowerCase()=="de" || auxNom[0].toLowerCase()=="del" || auxNom[0].toLowerCase()=="la") {
                        infor = auxNom[0] + " " + auxNom[1];
                    }else{
                        infor = auxNom[0];
                    }
                }else if (auxNom.length) {
                    infor = auxNom[0];
                }
            }else{
                if (auxNom.length>1) {
                    infor = auxNom[0] + " " + auxNom[1].charAt(0);
                }else if (auxNom.length) {
                    infor = auxNom[0];
                }
            }
            return infor;
        }

        nbc.cargaInicial = function () {
            /*if ($rootScope.isLoged) {*/
            /**
             * Datos personales
             */
            try {
                $rootScope.idPerson = storage.get("dataUser")._id;
                userId = storage.get("Us").user._id;
            } catch (e) {
            }

            if($rootScope.idPerson!=null && $rootScope.idPerson!="" && storage.get("tokenUser")){
                if (storage.get("tipoUser")=='person') {
                    $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/profiledata")
                    .then(function (response) {
                        nbc.profileData = response.data;
                        if (nbc.profileData.person) {
                            nbc.fechaNac = new Date(nbc.profileData.person.birthdate);
                            nbc.nombreCompleto = nbc.acortarNombres('nombre',nbc.profileData.person.first_name) + " " + nbc.acortarNombres('apellido',nbc.profileData.person.last_name);
                            
                            if (nbc.profileData.person.degree_license == "N") {
                                nbc.profileData.person.degree_license = "";
                            }
                            nbc.PDFurl = ServerDestino + "pdf/profile/" + nbc.profileData.person._id
                            userId = nbc.profileData.user._id;
                        
                            if (nbc.profileData.person.birthdate) {
                                nbc.profileData.person.birthdate = new Date(nbc.profileData.person.birthdate);
                                nbc.birthDay = nbc.profileData.person.birthdate.getDate().toString();
                                nbc.birthMonth = nbc.profileData.person.birthdate.getMonth() + 1;
                                nbc.birthMonth = nbc.birthMonth.toString();
                                nbc.birthYear = nbc.profileData.person.birthdate.getFullYear().toString();
                            }

                            if (nbc.profileData.person.birthdate) {
                               nbc.edad = nbc.calculaEdad(nbc.profileData.person.birthdate) + " AÑOS";
                            };

                            if ((nbc.profileData.person.birthdate != "" || 
                                nbc.profileData.person.numb_doc != "" || 
                                nbc.profileData.person.city_birth != "") && 
                                $rootScope.mensajeBienvenida == 1) {
                                nbc.objEnvio = {
                                    visits_counter: 1
                                };
                                $rootScope.mensajeBienvenida = 0;
                                $http.put(ServerDestino + "users/" + $rootScope.idUser + "/visitscounter", nbc.objEnvio)
                                .then(function (response) {
                                });
                            }

                        }                    
                        
                        if(nbc.profileData.user){	
                            userId = ('_id' in nbc.profileData.user) ? nbc.profileData.user._id : '';

                            if ($rootScope.userAvatar == "assets/images/user_male.png" || $rootScope.userAvatar == null) {
                                
                                    $rootScope.userAvatar = (nbc.profileData.user.avatar != null && nbc.profileData.user.avatar != "") ? ServerDestino + "profile/" + nbc.profileData.user.avatar : "assets/images/user_male.png";
                                    nbc.urlValidator($rootScope.userAvatar);
                            }
                        }   

                        if (storage.get("tokenUser")) {
                            $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
                                if (response.data.person.title != null && response.data.person.title.length != 0) {
                                    nbc.antiguoTitle = response.data.person.title;
                                    nbc.tituloProfesional = response.data.person.title;
                                }
                                if (!isNaN(response.data.percentage)) {
                                    nbc.percentage = Math.round(response.data.percentage);
                                }
                            });

                            $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/preferences")
                            .then(function(response){
                                if(response.data.person_preferences.length){
                                    nbc.preferencias = response.data.person_preferences;
                                    $http.get(ServerDestino + "job/showByPreferences/"+$rootScope.idPerson).then(function(response){
                                        nbc.offersByPreference = response.data;
                                        nbc.totalItemsPrefs = nbc.offersByPreference.length;
                                        nbc.entryLimitPrefs = 5; // items per page
                                        nbc.noOfPagesPrefs = Math.ceil(nbc.totalItemsPrefs / nbc.entryLimitPrefs);
                                    });
                                }
                            });
                        }
                    }, function (response) {
                        console.log(response);
                      }
                    );
                }
            }

            $http.get(ServerDestino + "offers/mostrecent").then(function (response) {
                nbc.lastOffert = response.data;
                for (var i = 0; i < nbc.lastOffert.offers.length; i++) {
                    var base = nbc.lastOffert.offers[i].base_salary.length > 0 ? parseInt(nbc.lastOffert.offers[i].base_salary) : 0;
                    var extra = nbc.lastOffert.offers[i].extra_salary.length > 0 ? parseInt(nbc.lastOffert.offers[i].extra_salary) : 0;
                    nbc.lastOffert.offers[i].total = base + extra;
                    // if (nbc.lastOffert.offers[i].company.logo != "") {
                    //     nbc.lastOffert.offers[i].company.logo = ServerDestino + "profile/companies/logos/" + nbc.lastOffert.offers[i].company.logo;
                    // }else {
                    //     nbc.lastOffert.offers[i].company.logo = "assets/images/logo-offert-default.png"
                    // }
                }
            });
            
            if (storage.get("tipoUser")=='person') { 
                $http.get(ServerDestino + "job/postulationsByPerson/" + $rootScope.idPerson).then(function(response){
                    nbc.postulations = response.data;
                    if (nbc.postulations) {
                        for (var i = 0; i < nbc.postulations.length; i++) {
                             var base = nbc.postulations[i].offers.base_salary.length ? parseInt(nbc.postulations[i].offers.base_salary) : 0;
                            var extra = nbc.postulations[i].offers.extra_salary.length ? parseInt(nbc.postulations[i].offers.extra_salary) : 0;
                            nbc.postulations[i].offers.total = base + extra;
                            if (nbc.postulations[i].offers.companies.logo) {
                                nbc.postulations[i].offers.companies.logo = ServerDestino + "profile/companies/logos/" + nbc.postulations[i].offers.companies.logo;
                                nbc.postulations[i].offers.companies.logo = nbc.urlValidator(nbc.postulations[i].offers.companies.logo,"assets/images/logo-offert-default.png") == "assets/images/logo-offert-default.png" ? "assets/images/logo-offert-default.png":nbc.postulations[i].offers.companies.logo;
                            } else{
                                nbc.postulations[i].offers.companies.logo = "assets/images/logo-offert-default.png";
                            }
                        }
                    }
                });
            }

            $http.get("https://api-test.soyempleo.com/blog/post/3").then(function (response) {
                nbc.lastPosts = response.data;
            });  

            $http.get(ServerDestino + "companies/list/6").then(function (response) {
                nbc.companies = response.data.companies; 
            }); 

            if (storage.get("tokenUser") && storage.get("tipoUser")=='person') {
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/experiences")
                    .then(function(response){
                    nbc.personExp = response.data;
                }); 

                $http.get(ServerDestino + "persons/" + $rootScope.idPerson + "/studies").then(function(response){
                    nbc.personEstudies = response.data.person_studies.length;
                }); 

                /*http://api-test.soyempleo.com/questions/all*/
                $http.get(ServerDestino + "persons/" + $rootScope.idPerson  + "/answersquestions").then(function(response){
                    nbc.preguntasList = response.data.person_answers_questions;
                    nbc.respondidasLength = nbc.preguntasList.questions_answered.length ? true:false;
                    /*if ($rootScope.urlOffert) {
                        window.location = $rootScope.urlOffert;
                        $rootScope.urlOffert = '';
                    }*/
                }); 
            } 

            nbc.offersLoad();      
        }

        nbc.saltarPreguntas = function(){
            if (nbc.preguntasCount == nbc.preguntasList.questions_not_answered.length-1) {
                nbc.preguntasCount = 0;
            } else {
                nbc.preguntasCount++; 
            }
        }

        nbc.comenzarRespuesta = function(){
            nbc.respondidasLength = true;
        }

        nbc.responderPregunta = function(id,value){
            if (value) {
                nbc.objEnvio ={
                    question_id:id,
                    answer: value
                }

                $http.post(ServerDestino + "persons/" + $rootScope.idPerson + "/answersquestions",nbc.objEnvio)
                .then(function(response){
                    var found = $filter('filter')(nbc.preguntasList.questions_not_answered,{question_id:id})[0];
                    if(found){
                        var indx = nbc.preguntasList.questions_not_answered.indexOf(found);
                        nbc.preguntasList.questions_not_answered.splice(indx,1);
                        if(nbc.preguntasCount == nbc.preguntasList.questions_not_answered.length){
                            nbc.preguntasCount = 0;
                        }
                    }
                    nbc.preguntasRest = "";
                });
            }
        }

        nbc.urlValidator = function (url,dfault) {
            return $http.get(url).then(function (response) {
                return dfault;
            }).catch(function (err) {
                if (err.status === 404) {
                    $rootScope.userAvatar = "assets/images/user_male.png";
                    return dfault;
                };

                return $q.reject(err);
            })
        };

        nbc.orderBy = function(){
            if (nbc.orden=='relevance' || nbc.orden=='' || nbc.orden=='date_actual') {
                nbc.ordened = {
                    type:"created_at",
                    reversa:true
                }
            } else if(nbc.orden=='date_reverse'){
                nbc.ordened = {
                    type:"created_at",
                    reversa:false
                }
            } else if(nbc.orden=='max_salary'){
                nbc.ordened = {
                    type:"base_salary",
                    reversa:true
                }
            } else if(nbc.orden=='min_salary'){
                nbc.ordened = {
                    type:"base_salary",
                    reversa:false
                }
            }

            nbc.itemsOffers = $filter('orderObjectBy2')(nbc.itemsOffers,nbc.ordened.type,nbc.ordened.reversa);
        }

        nbc.offersLoad = function () {
            $http.get(ServerDestino + "offers/all")
                .then(function (response) {
                    nbc.offersLoaded = response.data.offers;
                    for (var i = 0; i < nbc.offersLoaded.length; i++) {
                        var base = nbc.offersLoaded[i].base_salary.length > 0 ? parseInt(nbc.offersLoaded[i].base_salary):0;
                        var extra = nbc.offersLoaded[i].extra_salary.length > 0 ? parseInt(nbc.offersLoaded[i].extra_salary):0;
                        nbc.offersLoaded[i].total = base + extra;
                        nbc.offersLoaded[i].base_salary = parseInt(nbc.offersLoaded[i].base_salary);
                        var fecha = nbc.cotarFecha(nbc.offersLoaded[i].created_at);
                        nbc.offersLoaded[i].fecha = fecha;
                        nbc.offersLoaded[i].created_at = new Date(nbc.offersLoaded[i].created_at);
                        nbc.offersLoaded[i].urlCompany = nbc.cambiarString(nbc.offersLoaded[i].companies.name);
                        nbc.offersLoaded[i].job = nbc.cambiarString(nbc.offersLoaded[i].job_title);
                        nbc.offersLoaded[i].ciudad = nbc.cambiarString(nbc.offersLoaded[i].city);
                        nbc.offersLoaded[i].sectorTempo = nbc.offersLoaded[i].companies.sector;
                        if (nbc.offersLoaded[i].companies.logo != "" && nbc.offersLoaded[i].companies.logo != null) {
                            nbc.offersLoaded[i].companies.logo = ServerDestino + "profile/companies/logos/" + nbc.offersLoaded[i].companies.logo;
                            if (nbc.urlValidator(nbc.offersLoaded[i].companies.logo) == null) {
                                nbc.offersLoaded[i].companies.logo = "assets/images/logo-offert-default.png";
                            }
                        } else {
                            nbc.offersLoaded[i].companies.logo = "assets/images/logo-offert-default.png";
                        }
                    }
                    nbc.itemsOffers = nbc.offersLoaded;
                    nbc.totalItemsOffers = nbc.offersLoaded.length;
                    nbc.entryLimitOffers = 5; // items per page
                    nbc.noOfPagesOffers = Math.ceil(nbc.totalItemsOffers / nbc.entryLimitOffers);
                    nbc.separadorCiudades(nbc.itemsOffers);
                    nbc.separadorSector(nbc.itemsOffers);
                    nbc.separadorExperiencia(nbc.itemsOffers);
                    nbc.ajustadorRangoPrecio();
                    if(nbc.price.cantidadOffers == nbc.itemsOffers.length){
                        nbc.listoPrecio = true;
                    }
                    
                });
        }

        nbc.ajustadorRangoPrecio = function(){
            nbc.provieneDeAjustador = true;
            nbc.price.range.max = 0;
            nbc.price.maxPrice = 0;
            for (var i = 0; i < nbc.itemsOffers.length; i++) {
                if (parseInt(nbc.itemsOffers[i].base_salary) > nbc.price.range.max) {
                    nbc.price.range.max = parseInt(nbc.itemsOffers[i].base_salary);
                }  
                nbc.price.cantidadOffers = i+1;
            }
            nbc.price.maxPrice = nbc.price.range.max;

        }

        Array.prototype.unique=function(a){
            return function(){return this.filter(a)}}(function(a,b,c){return c.indexOf(a,b+1)<0
        });

        $scope.$watch('nbc.filtroTmp', function(term) {
            nbc.itemsOffers = [];
            var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "",
                city = reCity.city || "",
                reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "",
                sector = reSector.sector || "",
                reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "",
                experiencia = reExperiencia.experiencia || "";

            nbc.itemsOffers = filterFilter(nbc.offersLoaded, term);
            if(sector){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{sectorTempo:sector},true);
            }else if(!sector){                   
                nbc.separadorSector(nbc.itemsOffers);
            } 

            if(city){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{city:city},true);
            }else if (!city) {
                nbc.separadorCiudades(nbc.itemsOffers);
            }          

            if(experiencia){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{required_level: experiencia}, true);
            }else if (!experiencia) {
                nbc.separadorExperiencia(nbc.itemsOffers);
            }

            if (nbc.fechas[0]) {
                nbc.fechas[0] = false;
                nbc.clickFecha('0',false);
            } else if (nbc.fechas[1]){
                nbc.fechas[1] = false;
                nbc.clickFecha('1',false);
            } else if (nbc.fechas[2]){
                nbc.fechas[2] = false;
                nbc.clickFecha('2',false);
            }

            nbc.reubicadorOfertFilter();
        });

        $scope.$watch('[nbc.price.minPrice,nbc.price.maxPrice]', function () { 
            if(nbc.listoPrecio){
                if(!nbc.provieneDeAjustador){
                    nbc.filtradorSalario(nbc.offersLoaded);
                }else{
                    nbc.provieneDeAjustador = false;
                }
            }
        }, true);

        nbc.filtradorSalario = function(obj){
            var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "",
                city = reCity.city || "",
                reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "",
                sector = reSector.sector || "",
                reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "",
                experiencia = reExperiencia.experiencia || "";

            nbc.itemsOffers = [];
            nbc.itemsOffers = $filter('priceFilter')(obj,nbc.price);

            if(city){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{city: city}, true);
            }else if(!city){
                nbc.separadorCiudades(nbc.itemsOffers);
            }

            if(sector){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{sectorTempo: sector}, true);
            }else if(!sector){
                nbc.separadorSector(nbc.itemsOffers);
            }

            if(experiencia){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{required_level: experiencia}, true);
            } else if(!experiencia){
                nbc.separadorExperiencia(nbc.itemsOffers);
            }
            var tempo = nbc.filtroTmp || "";
            if(tempo.trim()){
                nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,tempo.trim());
            }

            nbc.reubicadorOfertFilter();
        }

        nbc.separadorCiudades = function(obj){
            var found = $filter('filter')(nbc.ciudadesFiltro, { picked: true  }, true)[0];
            if(found){
                nbc.ciudadesFiltro = [];
                nbc.ciudadesFiltro.push({city:found.city,cantidad:0,picked:true,style:"fondo-default"});
            }else{
               nbc.ciudadesFiltro = []; 
            }
            
            for (var i = 0; i < obj.length; i++) {
                if(obj[i].city){
                    var aux = $filter('filter')(nbc.ciudadesFiltro, { city: obj[i].city  }, true)[0];

                    if(aux){
                        var indx = nbc.ciudadesFiltro.indexOf(aux);
                        nbc.ciudadesFiltro[indx].cantidad+=1;
                    }else{
                        nbc.ciudadesFiltro.push({city:obj[i].city,cantidad:1,picked:false,style:"fondo-default"});
                    }
                }                
            }
        }

        nbc.separadorSector = function(obj){
            var found = $filter('filter')(nbc.sectoresFiltro, { picked: true  }, true)[0];
            if(found){
                nbc.sectoresFiltro = [];
                nbc.sectoresFiltro.push({sector:found.sector,cantidad:0,picked:true,style:"fondo-activo"});
            }else{
                nbc.sectoresFiltro = [];
            }
            nbc.sectoresFiltro = [];
            for (var i = 0; i < obj.length; i++) {nbc.offersLoaded[i].sectorTempo
                if(obj[i].sectorTempo){
                    var aux = $filter('filter')(nbc.sectoresFiltro, { sector: obj[i].sectorTempo  }, true)[0];

                    if(aux){
                        var indx = nbc.sectoresFiltro.indexOf(aux);
                        nbc.sectoresFiltro[indx].cantidad+=1;
                    }else{
                        nbc.sectoresFiltro.push({sector:obj[i].sectorTempo,cantidad:1,picked:false,style:"fondo-default"});
                    }
                }else if (!obj[i].sectorTempo) {
                    obj[i].sectorTempo = "Otro";
                    var aux = $filter('filter')(nbc.sectoresFiltro, { sector: "Otro"  }, true)[0];
                    if(aux){
                        var indx = nbc.sectoresFiltro.indexOf(aux);
                        nbc.sectoresFiltro[indx].cantidad+=1;
                    }else{
                        nbc.sectoresFiltro.push({sector:"Otro",cantidad:1,picked:false,style:"fondo-default"});
                    }
                }                                
            }
        }

        nbc.separadorExperiencia = function(obj){
            var found = $filter('filter')(nbc.experienciaFiltro,{picked:true},true)[0];
            if(found){
                nbc.experienciaFiltro = [];
                nbc.experienciaFiltro.push({experiencia:found.experiencia,cantidad:0,picked:true,style:"fondo-activo"});
            }else{
                nbc.experienciaFiltro = [];
            }
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].required_level) {
                    var aux = $filter('filter')(nbc.experienciaFiltro, { experiencia: obj[i].required_level  }, true)[0];
                    if (aux) {
                        var indx = nbc.experienciaFiltro.indexOf(aux);
                        nbc.experienciaFiltro[indx].cantidad+=1;
                    }else{
                        nbc.experienciaFiltro.push({experiencia:obj[i].required_level,cantidad:1,picked:false,style:"fondo-default"});
                    }
                }
            }
        }

        nbc.clickCiudades = function(name){
            var found = $filter('filter')(nbc.ciudadesFiltro,{city:name})[0];
            var id = nbc.ciudadesFiltro.indexOf(found);
            if(nbc.ciudadesFiltro[id].picked){
                nbc.ciudadesFiltro[id].picked = false;
                nbc.ciudadesFiltro[id].style = "fondo-default";
                if (nbc.fechas[0]) {
                    nbc.fechas[0] = false;
                    nbc.clickFecha('0',true);
                } else if (nbc.fechas[1]){
                    nbc.fechas[1] = false;
                    nbc.clickFecha('1',true);
                } else if (nbc.fechas[2]){
                    nbc.fechas[2] = false;
                    nbc.clickFecha('2',true);
                }else{
                    nbc.rePickerFiltro();
                }
                nbc.ajustadorRangoPrecio();
            }else{
                for (var i = 0; i < nbc.ciudadesFiltro.length; i++) {
                    if(i==id){
                        continue;
                    }
                    nbc.ciudadesFiltro[i].picked = false;
                    nbc.ciudadesFiltro[i].style = "fondo-default";
                }
                nbc.ciudadesFiltro[id].picked = true;
                nbc.ciudadesFiltro[id].style = "fondo-activo";
                nbc.itemsOffers = [];
                nbc.itemsOffers = $filter('filter')(nbc.offersLoaded,{city:nbc.ciudadesFiltro[id].city},true);

                var reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "";
                var sector = reSector.sector || "";
                if(sector){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{sectorTempo: sector}, true);
                }else if(!sector){
                    nbc.separadorSector(nbc.itemsOffers);
                }
                
                var reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "";
                var experiencia = reExperiencia.experiencia || "";
                if(experiencia){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{required_level: experiencia}, true);
                }else if(!experiencia){
                    nbc.separadorExperiencia(nbc.itemsOffers);
                }

                var tempo = nbc.filtroTmp || "";
                if(tempo.trim()){
                    nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,tempo.trim());
                }
                nbc.ajustadorRangoPrecio();
            }
        }

        nbc.clickSectores = function(name){
            var found = $filter('filter')(nbc.sectoresFiltro,{sector:name})[0];
            var id = nbc.sectoresFiltro.indexOf(found);
            if(nbc.sectoresFiltro[id].picked){
                nbc.sectoresFiltro[id].picked = false;
                nbc.sectoresFiltro[id].style = "fondo-default";
                if (nbc.fechas[0]) {
                    nbc.fechas[0] = false;
                    nbc.clickFecha('0',true);
                } else if (nbc.fechas[1]){
                    nbc.fechas[1] = false;
                    nbc.clickFecha('1',true);
                } else if (nbc.fechas[2]){
                    nbc.fechas[2] = false;
                    nbc.clickFecha('2',true);
                }else{
                    nbc.rePickerFiltro();
                }
                nbc.ajustadorRangoPrecio();
            }else{
                for (var i = 0; i < nbc.sectoresFiltro.length; i++) {
                    if(i==id){
                        continue;
                    }
                    nbc.sectoresFiltro[i].picked = false;
                    nbc.sectoresFiltro[i].style = "fondo-default";
                }
                nbc.sectoresFiltro[id].picked = true;
                nbc.sectoresFiltro[id].style = "fondo-activo";
                nbc.itemsOffers = [];
                nbc.itemsOffers = $filter('filter')(nbc.offersLoaded,{sectorTempo:nbc.sectoresFiltro[id].sector},true);
                
                var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "";
                var city = reCity.city || "";var tempo = nbc.filtroTmp || "";
                if(city){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{city:city},true);
                }else if (!city) {
                    nbc.separadorCiudades(nbc.itemsOffers);
                }

                var reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "";
                var experiencia = reExperiencia.experiencia || "";
                if(experiencia){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{required_level: experiencia}, true);
                }else if (!experiencia) {
                    nbc.separadorExperiencia(nbc.itemsOffers);
                }

                var tempo = nbc.filtroTmp || "";
                if(tempo.trim()){
                    nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,tempo.trim());
                }
                nbc.ajustadorRangoPrecio();
            }
        }

        nbc.clickExperiencia = function(name){
            var found = $filter('filter')(nbc.experienciaFiltro,{experiencia:name})[0];
            var id = nbc.experienciaFiltro.indexOf(found);
            if(nbc.experienciaFiltro[id].picked){
                nbc.experienciaFiltro[id].picked = false;
                nbc.experienciaFiltro[id].style = "fondo-default";
                if (nbc.fechas[0]) {
                    nbc.fechas[0] = false;
                    nbc.clickFecha('0',true);
                } else if (nbc.fechas[1]){
                    nbc.fechas[1] = false;
                    nbc.clickFecha('1',true);
                } else if (nbc.fechas[2]){
                    nbc.fechas[2] = false;
                    nbc.clickFecha('2',true);
                }else{
                    nbc.rePickerFiltro();
                }
                nbc.ajustadorRangoPrecio();
            }else{
                for (var i = 0; i < nbc.experienciaFiltro.length; i++) {
                    if(i==id){
                        continue;
                    }
                    nbc.experienciaFiltro[i].picked = false;
                    nbc.experienciaFiltro[i].style = "fondo-default";
                }
                nbc.experienciaFiltro[id].picked = true;
                nbc.experienciaFiltro[id].style = "fondo-activo";
                
                var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "",
                    city = reCity.city || "",
                    reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "",
                    sector = reSector.sector || "";
                nbc.itemsOffers = [];
                nbc.itemsOffers = $filter('filter')(nbc.offersLoaded,{required_level: nbc.experienciaFiltro[id].experiencia}, true);
                
                if(sector){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{sectorTempo:sector},true);
                }else if(!sector){                   
                    nbc.separadorSector(nbc.itemsOffers);
                }

                if(city){
                    nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{city:city},true);
                }else if (!city) {
                    nbc.separadorCiudades(nbc.itemsOffers);
                }
                
                var tempo = nbc.filtroTmp || "";
                if(tempo.trim()){
                    nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,tempo.trim());
                }
                nbc.ajustadorRangoPrecio();
            }
        }

        nbc.generalFilter = function(){
            var reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "";
            var experiencia = reExperiencia.experiencia || "";
            if(experiencia){
                nbc.itemsOffers = $filter('filter')(nbc.offersLoaded,{required_level: experiencia}, true);
            }
            var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "";
            var city = reCity.city || "";
            var reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "";
            var sector = reSector.sector || "";
            var tempo = nbc.filtroTmp || "";
            var textoFiltro = tempo + " " + city + " " + sector + " " + experiencia;
            if(textoFiltro.trim()){
                nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,textoFiltro.trim());
            }
        }

        nbc.clickFecha = function(type,especial){
            nbc.itemsOffers = nbc.offersLoaded;
            if (especial) {
                nbc.rePickerFiltro();
            }
            
            if (type=='0') {
                if(nbc.fechas[0]){
                    nbc.fechas[0] = false;
                }else{
                    nbc.fechas[0] = true;
                    nbc.fechas[1] = false;
                    nbc.fechas[2] = false;
                }
            } else if (type=='1') {
                if(nbc.fechas[1]){
                    nbc.fechas[1] = false;
                }else{
                    nbc.fechas[1] = true;
                    nbc.fechas[0] = false;
                    nbc.fechas[2] = false;
                    var fechaActual = new Date();
                    var fechaAtras = nbc.sumarDias(fechaActual,-7);
                    nbc.itemsOffers = $filter('fechaFilter')(nbc.itemsOffers,fechaAtras,fechaActual);
                }
            } else if (type=='2') {
                if(nbc.fechas[2]){
                    nbc.fechas[2] = false;
                }else{
                    nbc.fechas[2] = true;
                    nbc.fechas[1] = false;
                    nbc.fechas[0] = false;
                    var fechaActual = new Date();
                    var fechaAtras = nbc.sumarDias(fechaActual,-30);
                    nbc.itemsOffers = $filter('fechaFilter')(nbc.itemsOffers,fechaAtras,fechaActual);
                }
            }
        }

        nbc.parseDate = function(date) {
          return new Date(date.getFullYear(), (date.getMonth()+1), date.getDate()); 
        }

        nbc.sumarDias = function(fecha, dias){
            var aux = new Date(fecha.getFullYear(), fecha.getMonth(), fecha.getDate());
            aux.setDate(aux.getDate() + dias);
            return aux;
        }

        nbc.reubicadorOfertFilter = function(){
            nbc.totalItemsOffers = nbc.itemsOffers.length;
            nbc.noOfPagesOffers = Math.ceil(nbc.totalItemsOffers/nbc.entryLimitOffers);
            nbc.ajustadorRangoPrecio();
        }

        nbc.rePickerFiltro = function(){
            nbc.itemsOffers = nbc.offersLoaded;
            var reCity = $filter('filter')(nbc.ciudadesFiltro,{picked: true}, true)[0] || "",
                city = reCity.city || "",
                reSector = $filter('filter')(nbc.sectoresFiltro,{picked: true}, true)[0] || "",
                sector = reSector.sector || "",
                reExperiencia = $filter('filter')(nbc.experienciaFiltro,{picked: true}, true)[0] || "",
                experiencia = reExperiencia.experiencia || "",
                tempo = nbc.filtroTmp || "";
            
            if(sector){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{sectorTempo:sector},true);
            }else if(!sector){                   
                nbc.separadorSector(nbc.itemsOffers);
            }  

            if(city){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{city:city},true);
            }else if (!city) {
                nbc.separadorCiudades(nbc.itemsOffers);
            }         

            if(experiencia){
                nbc.itemsOffers = $filter('filter')(nbc.itemsOffers,{required_level: experiencia}, true);
            }else if (!experiencia) {
                nbc.separadorExperiencia(nbc.itemsOffers);
            }

            if(tempo.trim()){
                nbc.itemsOffers = nbc.filtroMultiple(nbc.itemsOffers,tempo.trim());
            }

            nbc.reubicadorOfertFilter();
        }

        nbc.filtroMultiple = function(item,term,type){
            return $filter('multipleFiltro')(item,term,type);
        }

        nbc.filtroIndividual = function(item,term,key){
            var objFilter = {};
            objFilter[key] = term;
            return $filter('filter')(item,objFilter,true);
        }

        nbc.irOferta = function (id) { 
            var foundItem = $filter('filter')(nbc.offersLoaded,{_id: id}, true)[0];
            $rootScope.selectOf = nbc.offersLoaded[nbc.offersLoaded.indexOf(foundItem)];
            var ciudad = nbc.urlLimpia(nbc.offersLoaded[nbc.offersLoaded.indexOf(foundItem)].ciudad);
            var job = nbc.urlLimpia(nbc.offersLoaded[nbc.offersLoaded.indexOf(foundItem)].job);

            $location
                .path('/ofertas/empleo/' + ciudad
                + '/' + nbc.offersLoaded[nbc.offersLoaded.indexOf(foundItem)].urlCompany
                + '/' + job
                + '/' + nbc.offersLoaded[nbc.offersLoaded.indexOf(foundItem)]._id);
        }

        nbc.goToPath = function (path){
             $location.path(path);
        }

        nbc.cambiarTitle = function () {
            nbc.cambiandoTitle = true;
        };

        nbc.enviarTitulo = function () {
            nbc.cambiandoTitle = false;
            if (nbc.nuevoTitulo) {
                if (nbc.profileData.person.title != nbc.nuevoTitulo) {
                    nbc.profileData.person.title = nbc.nuevoTitulo;
                    $http.put(ServerDestino + "persons/" + $rootScope.idPerson, {
                        "title": nbc.nuevoTitulo
                    }).then(function (response) {
                        if (response.data.message == "OK") {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Titulo actualizado con éxito </div>');
                        } else {
                            alertify.success(' <div class="text-center font-size-16 text-white"> Error al intentar actualizar el título. Por favor Intenta más tarde. </div>');
                        }
                    });
                }
                
            }
        };

        nbc.calculaEdad = function (fechaNac) {
            var difEdadMs = Date.now() - Date.parse(fechaNac);
            var edad = new Date(difEdadMs);
            return Math.abs(edad.getUTCFullYear() - 1970);
        };

        nbc.cotarFecha = function (p) {
            var aux = "";
            for (var i = 0; i < 10; i++) {
                aux = aux + p[i];
            }
            return aux;
        }

        nbc.cambiarString = function (s) {
            var aux = s.replace(/ /g, "-");
            aux = nbc.urlLimpia(aux);
            return aux;
        }

        nbc.changeUrl = function () {
            if (storage.get("tokenUser")) {
                nbc.urlActual = nbc.urlActual.substring(0, nbc.urlActual.indexOf("/#/")) + "/profile/#/";
                $rootScope.profileGo = nbc.urlActual;
                return true;
            } else {
                return false;
            }
        };

        nbc.offertReload = function () {
            nbc.cargaInicial();
            window.location = "#/";
        };

        nbc.consultar();
        nbc.cargaInicial();

        nbc.quetrae = function(value){
            
        }

        nbc.openDropdown = function(id) {
            document.getElementById("myDropdown"+id.toString()).classList.toggle("show");
        }

        nbc.detectGo = function () {
            if (storage.get("tipoUser") == "person") {
                if ($location.path() == "/dashboard") {
                    nbc.textIr = "Ir a Dashboard";
                    $location.path("/")
                } else {
                    nbc.textIr = "Ir a Inicio";
                    $location.path("/dashboard")
                }
            } else if (storage.get("tipoUser") == "company") {
                if ($location.path() == "/dashboard-company") {
                    nbc.textIr = "Ir a Dashboard";
                    $location.path("/")
                } else {
                    nbc.textIr = "Ir a Inicio";
                    $location.path("/dashboard-company")
                }
            }

        } 


        nbc.goProfile = function () {
            if (nbc.changeUrl()) {
                window.location = $rootScope.profileGo;
            }
            ;
        }

        nbc.profileDataGo = function () {
            if (nbc.changeUrl()) {
                var aux = $rootScope.profileGo + "actualiza"
                window.location = aux;
            }
        }

        nbc.profileGo = function () {
            if (nbc.changeUrl()) {
                var aux = $rootScope.profileGo + ""
                window.location = aux;
            }
        }

        nbc.goDashboard = function () {
            nbc.isProfile = true;
            if (storage.get("tipoUser") == "person") {
                window.location = "#/dashboard";
            } else if (storage.get("tipoUser") == "company") {
                window.location = "#/dashboard-company";
            }
        }

        nbc.goHome = function () {
            nbc.isProfile = false;
            if (storage.get("tipoUser") == "person") {
                window.location = "#/";
            } else if (storage.get("tipoUser") == "company") {
                window.location = "#/";
            }
        }

        nbc.cerrarSesion = function () {
            $rootScope.loginx = false;
            $rootScope.isLoged = false;
            nbc.loged = false;
            if (storage.get("tipoUser") == "person") {
                storage.remove("tokenUser");
                storage.remove("dataUser");
                storage.remove("tipoUser");
            } else if (storage.get("tipoUser") == "company") {
                storage.remove("tokenCompany");
                storage.remove("dataUser");
                storage.remove("tipoUser");
                location.reload(true);
                $zopim(function() {
                    $zopim.livechat.hideAll();
                });
            };
            window.location = "#/";
        };

        nbc.offers = function (oferta) {

            if ($rootScope.isLoged) {
                window.location = "#" + oferta;
            } else {
                window.location = "#/login";
            }
        };

        nbc.offersUrl = function (ciudad, company, job, company_id) {
            ciudad = (typeof ciudad != undefined) ? nbc.urlLimpia(ciudad) : '';
            company = (typeof company != undefined) ? nbc.urlLimpia(company) : '';
            job = (typeof job != undefined) ?  nbc.urlLimpia(job) : '';
                window.location = "#/ofertas/empleo/"
                + ciudad
                + "/" + company
                + "/" + job
                + "/" + company_id;
        };

        nbc.companyUrl = function (company, id) {
            company = nbc.urlLimpia(company);
                window.location = "#/company/"
                + company
                + "/" + id;
        };

        nbc.urlLimpia = function (value) {
            var tmp_this = value.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            return tmp_this.replace(/[^\\s\w]/g, "-");
        }

        $scope.window = angular.element($window);
        /*nbc.getWindowDimensions = function(){
            return{
                'h':nbc.window.height(),
                'w':nbc.window.width()
            };
        }*/

        $(window).load(function () {
            $scope.$apply(function(){
               nbc.loadedDiv = true; 
            });
             if($("#divBasic").height()>=$("#divPremium").height() && $("#divBasic").height()>=$("#divPackage").height()){
                $("#divPremium").height($("#divBasic").height());
                $("#divPackage").height($("#divBasic").height())
            }else if($("#divPremium").height()>=$("#divBasic").height() && $("#divPremium").height()>=$("#divPackage").height()){
                $("#divPackage").height($("#divPremium").height());
                $("#divBasic").height($("#divPremium").height());
            }else{
                $("#divBasic").height($("#divPackage").height());
                $("#divPremium").height($("#divPackage").height());
            }
        });

        $(window).resize(function(){
            if($("#divBasic").height()>=$("#divPremium").height() && $("#divBasic").height()>=$("#divPackage").height()){
                $("#divPremium").height($("#divBasic").height());
                $("#divPackage").height($("#divBasic").height())
            }else if($("#divPremium").height()>=$("#divBasic").height() && $("#divPremium").height()>=$("#divPackage").height()){
                $("#divPackage").height($("#divPremium").height());
                $("#divBasic").height($("#divPremium").height());
            }else{
                $("#divBasic").height($("#divPackage").height());
                $("#divPremium").height($("#divPackage").height());
            }
            if($("#divBasic2").height()>=$("#divPremium2").height() && $("#divBasic2").height()>=$("#divPackage2").height()){
                $("#divPremium2").height($("#divBasic2").height());
                $("#divPackage2").height($("#divBasic2").height())
            }else if($("#divPremium2").height()>=$("#divBasic2").height() && $("#divPremium2").height()>=$("#divPackage2").height()){
                $("#divPackage2").height($("#divPremium2").height());
                $("#divBasic2").height($("#divPremium2").height());
            }else{
                $("#divBasic2").height($("#divPackage2").height());
                $("#divPremium2").height($("#divPackage2").height());
            }
        });

        nbc.applyForJob = function (pos, empresa, idOfer) {
            var posicion = pos.toUpperCase(); 
            var respuesta = {};
            $http.get(ServerDestino + "persons/" + $rootScope.idPerson).then(function (response) {
                respuesta = response.data;

                if (!isNaN(response.data.percentage)) {
                    nbc.percentage = Math.round(response.data.percentage);
                }
                ;

                if (nbc.percentage > 79) {
                    var objEnvio = {
                        offer_id: idOfer,
                        first_name: respuesta.person.first_name,
                        last_name: respuesta.person.last_name,
                        email: respuesta.user.email,
                        company: empresa,
                        position: posicion,
                        status: [
                            {
                                label: "postulado",
                                date: moment(new Date()).format('DD/MM/YYYY')
                            }
                        ]
                    };

                    $http.post(ServerDestino + "job/" + $rootScope.idPerson + "/applyforjob", objEnvio).then(function (response) {
                        if (response.data.message == "OK") {
                            swal({title:"¡Felicitaciones!", html: 'tu postulación ha sido todo un éxito. <br /> <img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/353439455420850/?script=0">', type: "success"});
                        } else {
                            swal("¡Error!", "Ya te postulaste para esta oferta", "error");
                        }
                    });
                } else {
                    swal({
                        title: "Para postularte necesitas que tu perfíl este lleno como mínimo un 80%",
                        text: "¿Quieres ir a llenar tu pefíl ahora?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "##00CB5A",
                        confirmButtonText: "¡Sí, Llevame allí!",
                        cancelButtonText: "No, ¡gracias!"
                    }).then(
                        function () {
                            window.location = "profile";
                        }
                    );
                }
            });
            
        };

        nbc.openModal = function (ev) {
            $mdDialog.show({
                controller: controllerDialog,
                templateUrl: 'dialog1.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                escapeToClose: true
            }).then(function (answer) {
            }, function () {
            });
        };

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window) {
            controllerDialog.prototype.$scope = $scope;

            $scope.domain = 0;
            $scope.guardar = function () {
                if (($scope.monthExp === "" || $scope.monthExp === undefined || $scope.monthExp === null) || ($scope.addHabilidad == "" || $scope.addHabilidad === undefined || $scope.addHabilidad === null)) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Faltan datos </div>');
                } else {
                    var aux = [];
                    for (var i = 1; i < $scope.domain + 1; i++) {
                        aux.push({
                            value: i,
                            disp: true
                        });
                    }
                    for (var i = $scope.domain + 1; i < 11; i++) {
                        aux.push({
                            value: i,
                            disp: false
                        });
                    }
                    $rootScope.listaHabilidades.splice($rootScope.listaHabilidades.length + 1, $rootScope.listaHabilidades.length + 1, {
                        name: $scope.addHabilidad,
                        month: $scope.monthExp,
                        domain: $scope.domain,
                        arrayDomain: aux
                    });
                    alertify.success(' <div class="text-center font-size-16 text-white"> Habilidad Registrada </div>');
                    $mdDialog.hide();
                }
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
            };
            $scope.cambiarColor = function (valor) {
                $scope.domain = valor;
                for (var i = 1; i < valor + 1; i++) {
                    var btn = $("#v" + i);
                    btn.removeClass("btn-escala");
                    btn.addClass("btn-escala-green");
                }
                for (var i = valor + 1; i < 11; i++) {
                    var btn = $("#v" + i);
                    btn.addClass("btn-escala");
                    btn.removeClass("btn-escala-green");
                }
            };
            setTimeout(function () {
                $scope.$apply(function () {
                    $("#avatar").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="assets/images/user_male.png" alt="Tu Foto" style="width:160px">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);
        };

        controllerDialog.prototype.setFile = function (element) {
            var $scope = this.$scope;
            $scope.$apply(function () {
                $scope.theFile = element.files[0];
            });
        };
        $rootScope.ofertaActual

        /*if (window.location.hash.substring(0, 17)=="#/perfil/persona/" && window.location.hash.length>18) {*/
            /**
             * NUEVO CAMBIO TEMPORAL 
             */
            /*$rootScope.selected = 0;*/
            /**
             * FIN NUEVO   
             */
            /*if (storage.get("personItems")) {
                if (storage.get("personItems").length) {
                    var personaItms = [];
                    personaItms = storage.get("personItems");
                    var id = window.location.hash.substring(17, window.location.hash.length);
                    var testing = {
                        postulates:{
                            person:{
                                _id:id
                            }
                        }
                    } 
                    var foundPerson = $filter('filter')(personaItms,testing,true)[0];
                    if (foundPerson) {*/
                        /**
                         * NUEVO CAMBIO TEMPORAL 
                         */
                        /* $rootScope.postulates = [];
                         $rootScope.postulates.push(foundPerson.postulates);*/
                        /**
                         * FIN NUEVO   
                         */
                       /* ANTIGUA FORMA $rootScope.postulates = foundPerson.postulates;*/
                        /*$rootScope.selectedAvatar = foundPerson.postulates.selectedAvatar;
                    }else{
                        if (storage.get("tokenCompany")) {
                            nbc.goToUrl('/#/dashboard-company');
                        }else{
                           nbc.goToUrl('/#/'); 
                        }
                    }
                    
                }
            }else if (storage.get("tokenCompany") ) {
                nbc.goToUrl('/#/dashboard-company');
            }else{
               nbc.goToUrl('/#/'); 
            }
        }else*/ 
        /*nuevo*/
        if (storage.get("tokenCompany") && window.location.hash.substring(0, 17)=="#/perfil/persona") {
                nbc.goToUrl('/#/dashboard-company');
        }/*fin nuevo*/
        
        else if (window.location.hash=="#/postulados" && !$rootScope.postulates && !$rootScope.ofertaActual && storage.get("tokenCompany")) {
            nbc.goToUrl('/#/dashboard-company');
        }else if (window.location.hash=="#/postulados" && !storage.get("tokenCompany")) {
            nbc.goToUrl('/#/');
        }

        /* Esto va en master */
        $scope.$watch(function(){
            return $location.path();
        }, function(value){
            if (value==='/landing-company') { 
                $rootScope.isLandingCompany = true;
            }else{
                $rootScope.isLandingCompany = false;
            }
        });

    }

    NavbarController.$inject = ["localStorageService", "$rootScope", "$http", "$location", "$mdDialog", "$mdMedia", "$scope", "$window", "$q", "$filter","filterFilter"];
    angular
        .module('spa-SE')
        .controller('navbarController', NavbarController);
})();

function previewFile() {
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();
    if (file) {
        $("#divButton").attr("style", "display: inline-block;");
        reader.readAsDataURL(file);
    } else {
        preview.src = "";
    }
};
function subirImagen() {
    $("#divLoader").attr("style", "display: inline-block;");
    $("#divButton").attr("style", "display: none;");
    var formData = new FormData($("form#formAvatarProfile")[0]);
    setTimeout(function() {
        $.ajax({
            url: ServerDestino + "users/" + userId + "/avatar",
            beforeSend: function (request) {
                request.setRequestHeader('Authorization', 'Bearer ' + token);
            },
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            error: function(error){
                alertify.error(' <div class="text-center font-size-16 text-white">Se ha producido un error al Subir la Imagen </div> ');
            },
            success: function (respuesta) {
                if (respuesta.message == "IMAGE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> El archivo no es una imagen </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "SIZE_ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Imagen demasiado pesada </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else if (respuesta.message == "ERROR") {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar subir la foto </div> ');
                    $("#divButton").attr("style", "display: inline-block;");
                    $("#divLoader").attr("style", "display: none;");
                } else {
                    alertify.success(' <div class="text-center font-size-16 text-white"> Foto actualizada con éxito </div>');
                    $("#divButton").attr("style", "display: none;");
                    $("#divLoader").attr("style", "display: none;");
                    location.reload();
                }
            }
        });
    }, 100);
}
