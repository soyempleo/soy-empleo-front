/**
 * Created by wilde on 11/8/2016.
 */

(function () {
    'use strict';

    editarOfertaController.$inject = ["$http", "$location", "localStorageService", "$rootScope", "$scope", "$window", "$q", "$routeParams"];
    angular
        .module("spa-SE")
        .controller("editarOfertaController", editarOfertaController);


    function editarOfertaController($http, $location, storage, $rootScope, $scope, $window, $q, $routeParams) {

        var noc = this;
        noc.saludo = "Saludo";
        noc.relevanciaVacante = "Son igual de relevantes";
        noc.idOff = $routeParams.idOff;
        noc.oferta = {};
        noc.isBasicValid= true;

        $http.get(ServerDestino + "offers/" + noc.idOff).then(function (response) {
            noc.oferta = response.data.offer;
        });

 
    } 
})();