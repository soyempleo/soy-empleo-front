(function (){
    'use strict'

    function LoginController($http, $location, storage, $rootScope){
        var std = this;
        $rootScope.profileGo = "";
        std.urlActual = window.location.href;
        std.urlGo = "";
        std.isRefresing = false;

        std.consultar = function () {
            if (storage.get("tokenUser") && $rootScope.profileGo!="" && $rootScope.profileGo!=null) {
                window.location = $rootScope.profileGo;
            }
        };

        std.changeUrl = function(value){
            std.aux = "";
            for (var i = 0;i<(std.urlActual.length-7); i++){
                std.aux = std.aux + std.urlActual.charAt(i)
            }
            std.urlActual = std.aux+value;
            $rootScope.profileGo = std.urlActual;
            return std.urlActual;
        };

        std.script = function(url) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url;
            var x = document.getElementsByTagName('head')[0];
            x.appendChild(s);
        }

        std.iniciarSesion = function (email, password) {
            std.isRefresing = true;
            std.objEnvio = {
                email: email,
                password: password
            };
            $http.post(ServerDestino+"api/authenticate", std.objEnvio).then(function (response) {
                storage.clearAll();
                if(response.data.user.type_user == "company"){
                    storage.set("tokenCompany", response.data.token);
                    storage.set("dataUser", response.data.company[0]);
                    storage.set("tipoUser", "company");
                    $rootScope.loginx = true;
                    alertify.success(' <div class="text-center font-size-16 text-white"> Bienvenido a SoyEmpleo.com </div> ');
                    $location.path("/dashboard-company");
                    $rootScope.isLoged = true;
                    std.script('assets/js/zopim.js');
                }
                else{
                    storage.set("tokenUser", response.data.token);
                    storage.set("Us", response.data);
                    storage.set("dataUser", response.data.person[0]);
                    storage.set("tipoUser", "person");
                    $rootScope.loginx = true;
                    alertify.success(' <div class="text-center font-size-16 text-white"> Bienvenido a SoyEmpleo.com </div> ');
                    $rootScope.isLoged = true;
                    if ($rootScope.urlOffert) {
                        window.location = $rootScope.urlOffert;
                    }else{
                        std.urlGo = $rootScope.fromApply ? $rootScope.urlOffertIn:"/dashboard"; 
                        $location.path(std.urlGo);  
                    }                                     
                }
            }, function (error) {
                swal({
                    title: "¡Error!",
                    text: "Usuario o contraseña invalidos",
                    type: "error"
                });
            }).finally(
                function() {
                    std.isRefresing = false;
            });
        };
    }
    LoginController.$inject = ["$http", "$location", "localStorageService", "$rootScope"];
    angular
        .module('spa-SE')
            .controller('loginController2', LoginController);
})();
