/*
 * Created by Anthony Torres 07/04/2016.
 */

 (function() {
    'use strict'

    function nuevaOfertaController(storage, 
        $rootScope, 
        $http, 
        $location, 
        $scope, 
        $filter,
        $mdDialog, 
        $mdMedia, 
        $window, 
        $q, 
        $anchorScroll, 
        $timeout, 
        $log, 
        $routeParams,
        $sce,
        $uibModal,
        $document,
        $payments) {



        if (!storage.get("tokenCompany")) {
            $rootScope.loginx = false;
            window.location = "#/";
        };

        var noc = this;

        // Arrays
        noc.preferenciasSelected = [];
        noc.listaPositions = [];
        noc.funcionesSelected = [];
        noc.softwareSelected = []; 
        noc.sectoresSelected = []; 
        noc.areasSelected = [];
        noc.listEstudios = [];
        noc.listaInstitutos = [];
        noc.listaIdiomas = [];
        noc.listaSoftwares = [];
        noc.listaSectores = [];
        noc.listaHabilidades = [];
        noc.idiomas = [];
        noc.idiomas2 = [];
        noc.minExpeList = [];
        noc.maxExpeList = [];
        noc.departmentList = [];
        noc.cityList = [];
        noc.limitYear = [];
        noc.limitDay = [];
        noc.camposValidos = [];
        noc.words = [];
        noc.wordAdded = [];
        noc.repetidorFun = [];
        noc.repetidorPre = [];
        noc.repetidorSof = [];
        noc.repetidorSec = [];
        noc.repetidorAre = [];       

        //Object arrays
        noc.nuevaOferta = {};
        noc.listaAreas = {};
        noc.limitMonth = {};
        noc.idiomasBackup = {};
        noc.titulo = {}; 

        //Strings
        noc.career = ""; 
        noc.funcion = ""; 
        noc.posicion = "";
        noc.software = "";
        noc.sectores = "";
        noc.minExpe = ""; 
        noc.maxExpe = ""; 
        noc.ciudad = ""; 
        noc.departamento = ""; 
        noc.contrato = ""; 
        noc.nivelRequerido = ""; 
        noc.salario = ""; 
        noc.comisiones = ""; 
        noc.fechaLimite = ""; 
        noc.monthLimiteSelected = ""; 
        noc.dayLimiteSelected = ""; 
        noc.annioLimiteSelected = ""; 
        noc.cantOfertas = ""; 
        noc.observacion = ""; 
        noc.area = ""; 
        noc.areas = ""; 
        noc.carreraRequerida = ""; 
        noc.jobDescription = ""; 
        noc.limiteOfertar = ""; 
        noc.divScroll = ""; 
        noc.wordManual = "";
        $scope.details = "";
        noc.valordeesto = "";
        noc.offerId = "";
        noc.idiomasID = "";

        //Boolean
        noc.isBasicValid= false;
        noc.isDetailsValid= false;
        noc.isRefreshing = false;
        $rootScope.isOfferted = false;
        noc.popup = true;
        noc.pruebaBool = false;
        $rootScope.isEdit = false;
        $rootScope.editorOn = false;
        $rootScope.departamentoAjax = false;

        //Others
        noc.fechaActual = new Date();
        noc.dateLimite = noc.fechaActual.getFullYear() 
                            + "/" + (noc.fechaActual.getMonth() + 1) 
                            + "/" + noc.fechaActual.getDate();
        noc.total = 0;
        noc.currentPage = 1;
        noc.totalWords = 0;
        noc.wordLimit = 20;
        noc.noOfLoads = 0;
        noc.barraCompletado = 0;
        noc.pasosCompletado = 0;
        noc.relevanciaVacante = "Son igual de relevantes";
        $rootScope.idPerson = storage.get("dataUser")._id;
        $rootScope.isMembership = storage.get("dataUser").isMembership;
        $rootScope.premium_offers_avaiables = 0;
        $rootScope.basic_offers_avaiables = 0;
        $rootScope.ofertasDisponibles = {};
        $rootScope.ofertaConTicket = false;
        $scope.myDate = new Date();

        $scope.minDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth(),
            $scope.myDate.getDate());

        $scope.maxDate = new Date(
            $scope.myDate.getFullYear(),
            $scope.myDate.getMonth() + 1,
            $scope.myDate.getDate());

        noc.cargaInicial = function() {
            $http.get(ServerDestino + "departments/all").then(function(response) {
                noc.departmentList = response.data;
            }, function(error) {});

            if(!$routeParams.idOff){
                if (storage.get("dataUser").typeOff) {
                    $rootScope.tOffert = storage.get("dataUser").typeOff;
                    $rootScope.ofertaConTicket = storage.get("dataUser").typeOff == 'free' ? true:false;
                } else{
                    noc.goDashboard();
                }
            }else if ($routeParams.idOff) {
                noc.idOff = $routeParams.idOff;
                noc.isBasicValid= true;
                $rootScope.isEdit = true;
                $http.get(ServerDestino + "offers/" + noc.idOff).then(function (response) {
                    noc.offerId = response.data.offer._id;
                    $rootScope.editorOn = true;
                    noc.cargarOfertEditor(response.data.offer);
                });
            }            

            for (var i = 0; i < 8; i++) {
                noc.camposValidos.push(false);
            }

            $http.get(ServerDestino + "languages/all").then(function(response) {
                var indice;
                for (indice in response.data) {
                    noc.listaIdiomas.push(response.data[indice]);
                }
            });

            $http.get(ServerDestino + "softwares/all").then(function(response) {
                var indice;
                for (indice in response.data) {
                    noc.listaSoftwares.push(response.data[indice]);
                }
            });

            $http.get(ServerDestino + "areapositions/all").then(function(response) {
                noc.listaAreas = response.data; 
            });

            $http.get(ServerDestino + "sectorscompany/all").then(function(response) {
                noc.listaSectores = response.data;
            });            

            $http.get(ServerDestino + "/keywords/all").then(function (response) {
                noc.totalWords = response.data.length;
                noc.wordLimit = 20;
                noc.words = response.data;
                noc.noOfLoads = Math.ceil(noc.totalWords / noc.wordLimit);
                for (var i = 0; i < noc.words.length; i++) {
                    noc.words[i].added = false;
                    noc.words[i].description = noc.stringCapitalize(noc.words[i].description);
                }
            });

            noc.repetidorPre = noc.setNumber(noc.preferenciasSelected.length,5);
            noc.repetidorFun = noc.setNumber(noc.funcionesSelected.length,5);
            noc.repetidorSof = noc.setNumber(noc.softwareSelected.length,4);
            noc.repetidorSec = noc.setNumber(noc.sectoresSelected.length,4);
            noc.repetidorAre = noc.setNumber(noc.areasSelected.length,4);
        };

        noc.goDashboard = function(){
            window.location.href = "#/dashboard-company";
        }

        noc.cargarOfertEditor = function(obj){
            noc.titulo = obj.job_title;
            noc.area = obj.area;
            var foundItem = $filter('filter')(noc.departmentList, { name: obj.department  }, true)[0];
            noc.departamento = foundItem!=null && foundItem!="" ? foundItem._id: "";
            noc.depaSelected(noc.departamento);
            noc.ciudad = obj.city;
            noc.jobDescription = obj.job_description;
            for(var i = 0; i < obj.functions.length; i++){
                noc.preFuncSelected(obj.functions[i]); 
            }
            noc.contrato = obj.type_contract;
            noc.salario = obj.base_salary;
            noc.comisiones = obj.extra_salary;
            noc.sumatotal();
            noc.mostrarSalario = obj.show_salary;
            noc.nivelRequerido = obj.required_level;
            noc.relevanciaVacante = obj.relevancia;

            if(obj.carrers[0] != "No requerido"){
                for(var i = 0; i < obj.carrers.length; i++){
                    noc.preferenciasSelected.push({
                            name: obj.carrers[i]
                    });
                    noc.repetidorPre = noc.setNumber(noc.preferenciasSelected.length,5);
                }   
            };
            noc.idiomasID = obj.languages[0]._id;
            if(obj.languages[0].language.length>0){
                for(var i = 0; i < obj.languages[0].language.length; i++){

                    noc.idiomas.push({nombre:obj.languages[0].language[i].nombre,
                                        nivel:obj.languages[0].language[i].nivel});
                }
            }
            
            for(var i = 0; i < obj.softwares.length; i++){
                noc.softSelected(obj.softwares[i].name);
            }

            noc.minExpe = obj.min_experience;
            noc.expeSelected(noc.minExpe);
            noc.maxExpe = obj.max_experience;

            for(var i = 0; i < obj.sector.length; i++){
                noc.sectSelected(obj.sector[i].name);
            }

            for (var i = 0; i < obj.preferencias.length; i++){
                noc.wordAddManual(obj.preferencias[i]);
            }
            
        }

        /**
         * Función que reordena el array según el orden 
         * dado por el usuario, usando el drag and drop
         */
        noc.onDropComplete = function (index, obj, evt) { 
            var otherObj = noc.areasSelected[index];
            var otherIndex = noc.areasSelected.indexOf(obj);
            var otherPriority = noc.areasSelected[index].priority;
            var objPriority = obj.priority;
            obj.priority = otherPriority;
            otherObj.priority = objPriority;
            noc.areasSelected[index] = obj;
            noc.areasSelected[otherIndex] = otherObj;
        }

        noc.fechasMinimas = function() {
            for (var i = noc.fechaActual.getFullYear(); i < noc.fechaActual.getFullYear() + 5; i++) {
                noc.limitYear.push(i);
            }
            noc.limitMonth = [{value: "1",display: "Enero"}, 
                              {value: "2",display: "Febrero"}, 
                              {value: "3",display: "Marzo"}, 
                              {value: "4",display: "Abril"}, 
                              {value: "5",display: "Mayo"}, 
                              {value: "6",display: "Junio"}, 
                              {value: "7",display: "Julio"}, 
                              {value: "8",display: "Agosto"}, 
                              {value: "9",display: "Septiembre"}, 
                              {value: "10",display: "Octubre"}, 
                              {value: "11",display: "Noviembre"}, 
                              {value: "12",display: "Diciembre"}];
        }

        noc.fechasMinimas();

        noc.monthSelected = function(valor) {
            var cant = 0;
            var mes = parseInt(valor);
            noc.limitDay = [];
            noc.dayLimiteSelected = "";
            if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12) {
                cant = 31;
            } else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                cant = 30;
            } else if (mes == 2) {
                cant = noc.isBisiesto(noc.fechaActual.getFullYear());
            }

            for (var i = 1; i <= cant; i++) {
                noc.limitDay.push(i);
            }
        }

        noc.isBisiesto = function(anyo) {
            anyo = parseInt(anyo);
            if ((((anyo % 100) != 0) && ((anyo % 4) == 0)) || ((anyo % 400) == 0)) {
                return 29;
            } else {
                return 28;
            }
        };

        noc.careerSelected = function() {
            var go = 0;
            if (noc.preferenciasSelected.length < 5) {
                for (var i = 0; i < noc.preferenciasSelected.length; i++) {
                    if (noc.preferenciasSelected[i].name == noc.career.originalObject.name) {
                        go += 1;
                    }
                };
                if (go == 0) {
                    noc.preferenciasSelected.push({
                        name: noc.career.originalObject.name
                    });
                    noc.repetidorPre = noc.setNumber(noc.preferenciasSelected.length,5);
                    noc.cargarCompletado();
                } else {
                    alertify
                    .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado esa carreras o estudios anteriormente.</div>');
                }
            } else {
                alertify
                .error(' <div class="text-center font-size-16 text-white">' + ' Solo puedes agregar 5 carreras o estudios. Prueba eliminando uno de la lista. </div>');
            }
            noc.career = "";
        }

        noc.preFuncSelected = function(value){
            var value2 = $('#ex2_value').val();
            var go = 0;
            var dale = false;
            if(value){
                dale = true;
            }else if(value2){
                dale = true;
                value = value2;
            }else{
                dale = false;
            }

            if(dale){
                for(var i = 0; i < noc.funcionesSelected.length;i++){
                    if(value.toLowerCase() == noc.funcionesSelected[i].name.toLowerCase()){
                        go +=1;
                    }
                }
                if(go==0){
                    noc.funcSelected(value);
                }else{
                    noc.funcion = "";
                    $('#ex2_value').val("");
                    $('#ex2_dropdown').css("display", "none");
                    $('#ex2_value').focus();
                     alertify
                        .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado esa función anteriormente.</div>');
                }
            }            
        }

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        noc.funcSelected = function(value) {
            var go = 0;
            if (value != "" && value != null) {
                if (noc.funcionesSelected.length < 5) {
                    for (var i = 0; i < noc.funcionesSelected.length; i++) {
                        if (noc.funcionesSelected[i].name == capitalizeFirstLetter(value)) {
                            go += 1;
                        }
                    };
                    if (go == 0) {
                        noc.funcionesSelected.push({
                            name: capitalizeFirstLetter(value)
                        });
                        noc.cargarCompletado();
                    } else {
                        alertify
                        .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado esa función anteriormente.</div>');
                    }
                } else {
                    alertify
                    .error(' <div class="text-center font-size-16 text-white">' + ' Solo puedes agregar 5 funciones a desempeñar. Prueba eliminando uno de la lista. </div>');
                }
                noc.repetidorFun = noc.setNumber(noc.funcionesSelected.length,5);
                $('#ex2_value').val("");
                $('#ex2_dropdown').css("display", "none");
                $('#ex2_value').focus();
            }
        }

        noc.softSelected = function(value) {
            var go = 0;
            if (value != "" && value != null) {
                if (noc.softwareSelected.length < 4) {
                    for (var i = 0; i < noc.softwareSelected.length; i++) {
                        if (noc.softwareSelected[i].name == value.toUpperCase()) {
                            go += 1;
                        }
                    };
                    if (go == 0) {
                        noc.softwareSelected.push({
                            name: value.toUpperCase()
                        });
                        noc.repetidorSof = noc.setNumber(noc.softwareSelected.length,4);
                        noc.cargarCompletado();
                    } else {
                        alertify
                        .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado ese software anteriormente.</div>');
                    }
                } else {
                    alertify
                    .error(' <div class="text-center font-size-16 text-white">' + ' Solo puedes agregar 4 software informáticos. Prueba eliminando uno de la lista. </div>');
                }
                noc.software = "";
            }
        }

        noc.sectSelected = function(value) {
            var go = 0;
            if (value != "" && value != null) {
                if (noc.sectoresSelected.length < 4) {
                    for (var i = 0; i < noc.sectoresSelected.length; i++) {
                        if (noc.sectoresSelected[i].name == value.toUpperCase()) {
                            go += 1;
                        }
                    };
                    if (go == 0) {
                        noc.sectoresSelected.push({
                            name: value.toUpperCase()
                        });
                        noc.repetidorSec = noc.setNumber(noc.sectoresSelected.length,4);
                        noc.cargarCompletado();
                    } else {
                        alertify
                        .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado ese sector anteriormente.</div>');
                    }
                } else {
                    alertify
                    .error(' <div class="text-center font-size-16 text-white">' + ' Solo puedes agregar 4 sectores laborales. Prueba eliminando uno de la lista. </div>');
                }
                noc.sectores = "";
            }
        }

        noc.areaSelected = function(value) {
            var go = 0;
            if(value != "" && value != null){
                if (noc.areasSelected.length < 4) {
                    for (var i = 0; i < noc.areasSelected.length; i++) {
                        if (noc.areasSelected[i].name == value.toUpperCase()) {
                            go += 1;
                        }
                    };
                    if (go == 0) {
                        noc.areasSelected.push({
                            name: value.toUpperCase(),
                            priority: noc.areasSelected.length + 1
                        });
                        noc.repetidorAre = noc.setNumber(noc.areasSelected.length,4);
                        noc.cargarCompletado();
                    } else {
                        alertify
                        .error(' <div class="text-center font-size-16 text-white">' + ' Ya has agregado esa área anteriormente.</div>');
                    }
                } else {
                    alertify
                    .error(' <div class="text-center font-size-16 text-white">' + ' Solo puedes agregar 4 áreas. Prueba eliminando una de la lista. </div>');
                }
                noc.areas = "";
            }
        }

        noc.sumatotal = function() {
            var aux1 = !isNaN(noc.salario) && noc.salario != "" ? parseInt(noc.salario) : 0;
            var aux2 = !isNaN(noc.comisiones) && noc.comisiones != "" ? parseInt(noc.comisiones) : 0;
            noc.total = aux1 + aux2;
        }

        /**
         * Función que canbia el valor de repetidor
         * de filas
         * @return array
         */
        noc.setNumber = function(length, cant){
            var array = [];
            var number = cant-length;
            for(var i = 0; i < number; i++){
                array.push(i);
            }
            return array;
        } 

        noc.minExpeListFiller = function() {
            for (var i = 0; i < 50; i++) {
                if (i == 1) {
                    noc.minExpeList.push({
                        value: i,
                        display: i + " año"
                    });
                } else {
                    noc.minExpeList.push({
                        value: i,
                        display: i + " años"
                    });
                }
            }
        }

        noc.minExpeListFiller();

        noc.expeSelected = function(value) {
            noc.maxExpe = "";
            noc.maxExpeList = [];
            for (var i = value; i < 100; i++) {
                if (i == 1) {
                    noc.maxExpeList.push({
                        value: i,
                        display: i + " año"
                    });
                } else {
                    noc.maxExpeList.push({
                        value: i,
                        display: i + " años"
                    });
                }
            };
        }

        noc.eliminarPreferencia = function(pos) {
            noc.preferenciasSelected.splice(pos, 1);
            noc.repetidorPre = noc.setNumber(noc.preferenciasSelected.length,5);
            noc.cargarCompletado();
        }

        noc.eliminarFuncion = function(pos) {
            noc.funcionesSelected.splice(pos, 1); 
            noc.repetidorFun = noc.setNumber(noc.funcionesSelected.length,5);
            noc.cargarCompletado();
        }

        noc.eliminarSoftware = function(pos) {
            noc.softwareSelected.splice(pos, 1); 
            noc.repetidorSof = noc.setNumber(noc.softwareSelected.length,4);
            noc.cargarCompletado();
        }

        noc.eliminarSector = function(pos) {
            noc.sectoresSelected.splice(pos, 1); 
            noc.repetidorSec = noc.setNumber(noc.sectoresSelected.length,4);
            noc.cargarCompletado();
        }

        noc.eliminarArea = function(pos) {
            noc.areasSelected.splice(pos, 1);
            for(var i = 0; i < noc.areasSelected.length; i++){
                noc.areasSelected[i].priority = i+1;
            }
            noc.repetidorAre = noc.setNumber(noc.areasSelected.length,4);
            noc.cargarCompletado();
        }

        noc.depaSelected = function(value) {
            var idDepa = value;
            if (value != "" && value != null) {
                noc.cityList = [];
                $rootScope.departamentoAjax = true;
                noc.ciudad = "";
                setTimeout(function() {
                    $http.get(ServerDestino + "cities/byDepartment/" + idDepa)
                        .then(function(response) {
                        noc.cityList = response.data;
                    }).finally(function(){
                        $rootScope.departamentoAjax = false;
                    });
                }, 400);
            } else {
                noc.ciudad = "";
                noc.cityList = [];
            }
        };

        noc.basicValido = function() {
            noc.isBasicValid = true;
            noc.gotoId("#div_details");
        }

        /**
         * funcion para hacer scroll hacia donde se le indica
         * con el parámetro val 
         * @param  {[type]} val [description]
         * @return {[type]}     [description]
         */
        noc.gotoId = function(val){
            var id = val;
            $('html, body').animate({
                scrollTop: $(id.toString()).offset().top
            }, 1000);
        }

        /**
         * function que cambia el puntero para mostrar diferente 
         * lote de items en el front
         * @return {[type]} [description]
         */
        noc.wordLoad = function(){
            if(noc.currentPage<noc.noOfLoads){
                noc.currentPage+=1;
            }else if(noc.currentPage == noc.currentPage){
                noc.currentPage=1;
            }
        }

        noc.tieneFunc = function(){
            if ($('#ex2_value').val()) {
                return false;
            }
            return true;
        }

        /**
         * funcion que filtra el item por id y realiza la operacion
         * de agregar 
         * dicho item a otra coleccion de datos
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        noc.wordAdd = function(id){
            if(noc.wordAdded.length < 7){
                var foundItem = $filter('filter')(noc.words, { _id: id  }, true)[0];
                noc.words[noc.words.indexOf(foundItem)].added = true;
                if(noc.wordAdded.length>0){
                    if($filter('filter')(noc.wordAdded, { _id: id  }, true)[0]==null){
                        noc.wordAdded.push(noc.words[noc.words.indexOf(foundItem)]);
                        noc.cargarCompletado();
                    }
                }else{
                    noc.wordAdded.push(noc.words[noc.words.indexOf(foundItem)]);
                    noc.cargarCompletado();
                }
            }else{
                swal({   
                    title: "¡Ups!",   
                    text: "Sabemos que es divertido agregar palabras, pero el máximo son siete",   
                    imageUrl: "assets/images/soyempleo-pet-fun.png",
                    imageWidth: 100,
                    imageHeight: 100 });
            }
        }

        /**
         * función que permite borrar elemento de la coleccion mediante ID 
         * recibido por el parámetro de la función
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        noc.wordDelete = function(id){
            var foundItem = $filter('filter')(noc.wordAdded, { _id: id  }, true)[0];
            noc.wordAdded.splice(noc.wordAdded.indexOf(foundItem),1);
            var auxFound = $filter('filter')(noc.words, { _id: id  }, true)[0];
            if(auxFound!=null){
                noc.words[noc.words.indexOf(auxFound)].added = false;
            }
            noc.cargarCompletado();
        }

        /**
         * Función para agregar elementos a la coleccion words por medio
         * de input
         * @param  {[type]} val [description]
         * @return {[type]}     [description]
         */
        noc.wordAddManual = function(val){
            if(val!=null && val!="" && noc.wordAdded.length < 7){
                var auxVal = [];
                auxVal = noc.stringTokenizer(val,',');
                for(var i = 0; i < auxVal.length; i++){                    
                    var foundItem = $filter('filter')(noc.wordAdded, { description: auxVal[i]  }, true)[0];
                    var foundItem2 = $filter('filter')(noc.words, { description: auxVal[i]  }, true)[0];
                    if(foundItem==null && foundItem2==null){
                        noc.wordAdded.push({description:auxVal[i]});
                        noc.cargarCompletado();
                    }
                    else if(foundItem2!=null){
                        noc.words[noc.words.indexOf(foundItem2)].added = true;
                        noc.wordAdded.push(noc.words[noc.words.indexOf(foundItem2)]);
                        noc.cargarCompletado();
                    }
                };
            }else if(val!=null && val!=""){
                swal({   
                    title: "¡Ups!",   
                    text: "Sabemos que es divertido agregar palabras, pero el máximo son siete",   
                    imageUrl: "assets/images/soyempleo-pet-fun.png" });
            }
            noc.wordManual = "";
        }

        /**
         * función para capitalizar el string recibido por
         * parámetro, retorna un string
         * @param  string [string a separar]
         * @param  separador [parametro separador]
         * @return string
         */
        noc.stringCapitalize = function(string) {
            string = string.toLowerCase();
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        /**
         * Función que separa string enviados en parametro
         * 'string' y el termino separador se recibe en el 
         * parámetro 'separador', retorna un array
         * @param  {[type]} string [description]
         * @return array        [cada valor string previamente capitalizado]
         */
        noc.stringTokenizer = function(string,separador){
            var valueToken,valueTokenEnd = [];
            valueToken = string.split(separador);
            valueToken.forEach(function(item){
                valueTokenEnd.push(noc.stringCapitalize(item));
            });
            return valueTokenEnd;
        }

        /**
         * función que cambia el valor de la variable 
         * completado de la barra flotante
         * @return int
         */
        noc.cargarCompletado = function(){
            var percent = 0;
            percent += noc.titulo != "" && noc.titulo!=null ? 1:0;
            percent += noc.area != "" && noc.area!=null ? 1:0;
            percent += noc.nivelRequerido != "" && noc.nivelRequerido!=null ? 1:0;
            percent += noc.preferenciasSelected.length != 0 && noc.preferenciasSelected!=null ? 1:0;
            percent += noc.funcionesSelected.length != 0 && noc.funcionesSelected!=null ? 1:0;
            percent += noc.minExpe != "" && noc.minExpe!=null ? 1:0;
            percent += noc.maxExpe != "" && noc.maxExpe!=null ? 1:0;
            percent += noc.ciudad != "" && noc.ciudad!=null ? 1:0;
            percent += noc.departamento != "" && noc.departamento!=null ? 1:0;
            percent += noc.jobDescription != "" && noc.jobDescription!=null ? 1:0;
            percent += noc.contrato != "" && noc.contrato!=null ? 1:0;
            percent += noc.salario != "" && noc.salario!=null ? 1:0;
            percent += noc.comisiones != "" && noc.comisiones!=null ? 1:0;
            percent += noc.mostrarSalario != "" && noc.mostrarSalario!=null ? 1:0;
            percent += noc.idiomas.length != 0 && noc.idiomas!=null ? 1:0;
            percent += noc.relevanciaVacante != "" && noc.relevanciaVacante!=null ? 1:0;
            percent += noc.sectoresSelected.length != 0 && noc.sectoresSelected!=null ? 1:0;
            percent += noc.wordAdded.length != 0 && noc.wordAdded!=null ? 1:0;
            percent += noc.softwareSelected.length != 0 && noc.softwareSelected!=null ? 1:0;
            percent += noc.areasSelected.length != 0 && noc.areasSelected!=null ? 1:0;

            noc.barraCompletado = Math.round((percent * 100) / 20);
            noc.pasosCompletado = noc.barraCompletado >=0 && noc.barraCompletado <25 ? 0:noc.pasosCompletado;
            noc.pasosCompletado = noc.barraCompletado >=25 && noc.barraCompletado <50 ? 25:noc.pasosCompletado;
            noc.pasosCompletado = noc.barraCompletado >=50 && noc.barraCompletado <75 ? 50:noc.pasosCompletado;
            noc.pasosCompletado = noc.barraCompletado >=75 && noc.barraCompletado <100 ? 75:noc.pasosCompletado;
            noc.pasosCompletado = noc.barraCompletado ==100 ? 100:noc.pasosCompletado;
        }

        /**
         * Este watcher, vigila algún canbio en las diferentes variables
         * para ejecutar una función en cada cambio detectado
         */
        $scope.$watchGroup(['noc.titulo', 'noc.area',
            'noc.nivelRequerido','noc.preferenciasSelected',
            'noc.funcionesSelected','noc.minExpe',
            'noc.maxExpe','noc.ciudad',
            'noc.departamento','noc.jobDescription',
            'noc.contrato','noc.salario',
            'noc.comisiones','noc.mostrarSalario',
            'noc.idiomas','noc.relevanciaVacante',
            'noc.sectoresSelected','noc.wordAdded',
            'noc.softwareSelected','noc.areasSelected'], 
            function(newValues, oldValues, scope) {
            noc.cargarCompletado();            
        });

        noc.validarDetails = function() {
            noc.isDetailsValid = true;
        }

        noc.basicSectionValid = function(value) {
            if (value == "1") {
                if ($('#ex3_value').val()) {
                    var titu = $('#ex3_value').val();
                    if (titu.length < 4) {
                        noc.isBasicValid = false;
                        noc.isDetailsValid = false;
                    }else if (titu.length >=4) {
                        noc.posicionSelected(titu);
                    }
                }
                /*if (noc.titulo.length < 4 || noc.titulo == "" || noc.titulo == null) {
                    noc.isBasicValid = false;
                    noc.isDetailsValid = false;
                }*/
            } else {
                if (noc.area == null || noc.area == "") {
                    noc.isBasicValid = false;
                    noc.isDetailsValid = false;
                }else if(noc.ciudad == null || noc.ciudad == ""){
                    noc.isBasicValid = false;
                    noc.isDetailsValid = false;
                }else if(noc.departamento == null || noc.departamento == ""){
                    noc.isBasicValid = false;
                    noc.isDetailsValid = false;
                }
            }
        }

        noc.preguntaSalario = function() {
            swal({
                title: "¿Está seguro que quiere ocultar el salario?",
                text: "Las ofertas que no muestran salario son menos atractivas para los postulantes",
                showCancelButton: true,
                confirmButtonColor: "#00BF52",
                confirmButtonText: "No!",
                cancelButtonText: "Si, ocultalo!"
            }).then(
                function () {
                    noc.mostrarSalario = "Si";
                    $scope.$apply(function() {
                        noc.mostrarSalario = "Si";
                    });
                },
                function (dismiss) {
                    if (dismiss === 'cancel') {
                        noc.mostrarSalario = "No";
                        $scope.$apply(function() {
                            noc.mostrarSalario = "No";
                        });
                    }
                }
            );
        }

        noc.setLimit = function(date) {
            noc.limiteOfertar = new Date(date);
        }

        noc.languageSelected = function(value, pos) {
            var go = false;
            if (value == "" && noc.idiomas[pos].nivel != "" && noc.idiomas[pos].nivel != null) {
                noc.idiomas[pos].nivel = "";
            }
            if (noc.idiomas2.length != 0) {
                for (var i = 0; i < noc.idiomas2.length; i++) {
                    if (value == noc.idiomas2[i].name) {
                        for (var y = i + 1; y < noc.idiomas.length; y++) {
                            if (value == noc.idiomas[y].nombre) {
                                noc.idiomas.splice(y, 1);
                                alertify
                                .error(' <div class="text-center font-size-16 text-white">'
                                +' Parece que ya elegiste ese idioma, intenta con otro. </div>');
                                go = true;
                            }
                        }
                    }
                }
                if (!go) {
                    noc.idiomas2 = [];
                    for (var u = 0; u < noc.idiomas.length; u++) {
                        noc.idiomas2.push({
                            name: noc.idiomas[u].nombre,
                            level: noc.idiomas[u].nivel
                        });
                    }
                }
            } else {
                noc.idiomas2 = [];
                for (var u = 0; u < noc.idiomas.length; u++) {
                    noc.idiomas2.push({
                        name: noc.idiomas[u].nombre,
                        level: noc.idiomas[u].nivel
                    });
                }
            }
        };

        $scope.$watch('noc.career', function() {
            if (noc.career.originalObject != null) {
                if (noc.career.originalObject.name != "" && noc.career.originalObject.name != null) {
                    noc.careerSelected();
                };
            }
        });

        $scope.$watch('noc.funcion', function() {
            if (noc.funcion) {
                if (noc.funcion.originalObject) {
                    if (noc.funcion.originalObject.name != "" && noc.funcion.originalObject.name != null) {
                        noc.funcSelected(noc.funcion.originalObject.name);
                    };
                }
            }
        });

        $scope.$watch('noc.posicion',function(){
            if (noc.titulo) {
                if (noc.titulo.originalObject) {
                    if (noc.titulo.originalObject.name != "" && noc.titulo.originalObject.name != null) {
                        noc.posicionSelected(noc.titulo.originalObject.name);
                    };
                }
            }
        });

        /*$scope.$watch('noc.areas', function() {
            if (noc.areas.originalObject != null) {
                if (noc.areas.originalObject.name != "" && noc.areas.originalObject.name != null) {
                    noc.areaSelected();
                };
            }
        });*/

        noc.posicionSelected = function(value){
            var ex3Temp = $('#ex3_value').val();
            if (value || ex3Temp) {
                var aux = value || ex3Temp;
                noc.titulo = {
                    originalObject:{
                        display:aux
                    }
                };
                var foundPosicion = $filter('filter')(noc.posiciones,{display:aux});
                if (!foundPosicion.length) {
                    $('#ex3_dropdown').css("display", "none");
                }
            }else if (!value) {
                noc.titulo.originalObject.display = "";
            }
        }

        noc.validarCampos = function() {
            noc.divScroll = "";
            
            noc.camposValidos[0] = noc.jobDescription == "" || noc.jobDescription == null ? true : false;            
            noc.camposValidos[1] = noc.funcionesSelected.length == 0 ? true : false;            
            noc.camposValidos[2] = noc.contrato == "" || noc.contrato == null ? true : false;
            noc.camposValidos[3] = noc.salario == "" || noc.salario == null || noc.total < 343500 ? true : false;
            noc.camposValidos[4] = noc.mostrarSalario == '' || noc.mostrarSalario == null ? true : false;
            noc.camposValidos[5] = noc.nivelRequerido == '' || noc.nivelRequerido == null ? true : false;
            noc.camposValidos[6] = noc.nivelRequerido != '' && noc.nivelRequerido != 'Ninguno' && noc.nivelRequerido != 'Bachiller' &&
            noc.nivelRequerido != null && noc.preferenciasSelected.length == 0 ? true : false;
            var aux = 0;
            for (var i = 0; i < noc.camposValidos.length; i++) {
                if (noc.camposValidos[i]) {
                    aux += 1;
                    if (noc.divScroll == "") {
                        noc.divScroll = '#div_' + i;
                    }
                }
            }

            noc.popup = aux > 0 ? true : false;
        }

        /**
         * Esta funcion es usada para ofertar cuando la cuenta tienen vacantes disponibles
         */
        noc.ofertarConTickets = function(){
            noc.validarCampos();
            var scrolling = noc.divScroll;
            if(noc.popup) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Parece que aún hay campos importantes que debes llenar para continuar </div>');
                $('html, body').animate({
                    scrollTop: $(scrolling.toString()).offset().top
                }, 900);
                noc.divScroll = "";
            } else{
                noc.prepararOferta();
                if(!$rootScope.isEdit){
                    noc.enviarOferta();
                }else{
                    noc.enviarOfertaEdit();
                }
                
            }
        }

        noc.prepararOferta = function() {
            var auxCareer = [];
            var auxFunction = [];
            var auxIdiomas = [];
            var auxDepa = "";
            var auxMonth = parseInt(noc.monthLimiteSelected);
            var auxday = parseInt(noc.dayLimiteSelected);
            var auxWords = [];
            noc.monthLimiteSelected = auxMonth < 10 ? "0" + noc.monthLimiteSelected : noc.monthLimiteSelected;
            noc.dayLimiteSelected = auxday < 10 ? "0" + noc.dayLimiteSelected : noc.dayLimiteSelected;

            if (noc.preferenciasSelected.length == 0) {
                auxCareer = ["No requerido"];
            } else {
                for (var i = 0; i < noc.preferenciasSelected.length; i++) {
                    auxCareer.push(noc.preferenciasSelected[i].name);
                }
            }

            noc.nivelRequerido = noc.nivelRequerido != "" && noc.nivelRequerido != null ? noc.nivelRequerido : "Ninguno";
            var foundEd = $filter('filter')(noc.departmentList, { _id: noc.departamento  }, true)[0];
            auxDepa = foundEd!=null && foundEd != "" ? foundEd.name:"";

            for (var i = 0; i < noc.funcionesSelected.length; i++) {
                auxFunction.push(noc.funcionesSelected[i].name);
            }

            for (var i = 0; i < noc.idiomas.length; i++) {
                if (noc.idiomas[i].nombre != "" && noc.idiomas[i].nombre != null) {
                    if (noc.idiomas[i].nivel == "" || noc.idiomas[i].nivel == null) {
                        noc.idiomas[i].nivel = "Básico";
                    }
                    auxIdiomas.push(noc.idiomas[i]);
                }
            }
            if($rootScope.isEdit){
                var auxIdiomas2 = [];
                auxIdiomas2[0] = auxIdiomas;
            }
            
            for(var i = 0; i < noc.wordAdded.length; i ++){
                auxWords.push(noc.wordAdded[i].description);
            }
            noc.limiteOfertar = noc.limiteOfertar == "" ? $scope.maxDate : noc.limiteOfertar;
            var dia = noc.limiteOfertar.getDate() < 10 ? "0" + noc.limiteOfertar.getDate() : noc.limiteOfertar.getDate();
            var mes = (noc.limiteOfertar.getMonth() + 1) < 10 ? "0" + (noc.limiteOfertar.getMonth() + 1) : (noc.limiteOfertar.getMonth() + 1);
            var auxExtraSal = noc.comisiones != null && noc.comisiones != "" ? noc.comisiones : "";
            var objEnvio = {
                company_id: $rootScope.idPerson,
                job_title: $rootScope.editorOn ? noc.titulo:noc.titulo.originalObject.display,
                area: noc.area,
                required_level: noc.nivelRequerido,
                carrers: auxCareer,
                functions: auxFunction,
                min_experience: noc.minExpe,
                max_experience: noc.maxExpe,
                city: noc.ciudad,
                department: auxDepa,
                observation: noc.jobDescription,
                type_contract: noc.contrato,
                num_offers: noc.cantOfertas,
                limit_date: noc.limiteOfertar.getFullYear() + "-" + mes + "-" + dia,
                base_salary: noc.salario,
                extra_salary: auxExtraSal,
                show_salary: noc.mostrarSalario,
                job_description: noc.jobDescription,
                relevancia: noc.relevanciaVacante,
                sector: noc.sectoresSelected,
                preferencias: auxWords,
                softwares: noc.softwareSelected,
                areas: noc.areasSelected
            };

            if($rootScope.isEdit){
                objEnvio.languages = [];
                var languagex = [];
                for(var i = 0; i < auxIdiomas2[0].length; i++){
                    languagex.push({nivel: auxIdiomas2[0][i].nivel,nombre:auxIdiomas2[0][i].nombre});
                }
                objEnvio.languages.push({language:languagex,_id:noc.idiomasID});
            }else{
                objEnvio.language = auxIdiomas;
            }

            $rootScope.ofertaEnviar = objEnvio;
        };

        /**
         * función que resta una vacante del json y envía la 
         * información a la bd
         * @return {[type]} [description]
         */
        noc.restarVacante = function(type){
            $http.get(ServerDestino + "companies/" + $rootScope.idPerson + "/useoffer/" + type)
            .then(function(response){
                return response.data.message;
            });
        }
        /**
         * Función que envia un obj json usando el método http, esta
         * función es útilizada cuando no se usa el modal 
         * de precios en el front de creación de nueva oferta.
         * @return {[type]} [description]
         */
        noc.enviarOferta = function(){
            noc.isRefreshing = true;
            alertify.success(' <div class="text-center font-size-16 text-white"> Procesando Oferta </div>');
            $rootScope.ofertaEnviar.offer_type = $rootScope.tOffert;
            $rootScope.ofertaEnviar.num_offers = noc.cantOferta;
            var req = {
                method: 'POST',
                url: ServerDestino + 'offers',
                headers: {
                    'Accept': 'application/json, text/javascript',
                    'Content-Type': 'application/json; charset=utf-8'
                },
                data: $rootScope.ofertaEnviar
            }
            var uRLAUX = window.location.href;
            var n = uRLAUX.indexOf("#/");
            var res = uRLAUX.substring(0,n);
            res = res + "#/dashboard-company";
            $http(req).then(function(response) {
                if(response.data.message == "OK"){
                    if($rootScope.tOffert == 'premium'){
                        noc.restarVacante($rootScope.tOffert); 
                    }else if($rootScope.tOffert == 'membership'){
                        noc.restarVacante($rootScope.tOffert);
                    }else if (!$rootScope.isMembership && $rootScope.tOffert == 'basic') {
                        noc.restarVacante($rootScope.tOffert);
                    }
                    $rootScope.isOfferted = true;
                    window.location.href = res;
                }
            }, function(response) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar publicar oferta </div>');
            }).finally(function() {
                noc.isRefreshing = false;
            });
        }

        noc.enviarOfertaEdit = function(){
            noc.isRefreshing = true;
            /*$rootScope.ofertaEnviar.num_offers = noc.cantOferta;*/
            alertify.success(' <div class="text-center font-size-16 text-white"> Procesando Oferta </div>');
            var req = {
                method: 'PUT',
                url: ServerDestino + 'offers/' + noc.offerId,
                headers: {
                    'Accept': 'application/json, text/javascript',
                    'Content-Type': 'application/json; charset=utf-8'
                },
                data: $rootScope.ofertaEnviar
            }
            var uRLAUX = window.location.href;
            var n = uRLAUX.indexOf("#/");
            var res = uRLAUX.substring(0,n);
            res = res + "#/dashboard-company";
            $http(req).then(function(response) {
                if(response.data.message == "OK"){
                    $rootScope.isEditted = true;
                    window.location.href = res;
                }
            }, function(response) {
                alertify.error(' <div class="text-center font-size-16 text-white"> Error al intentar editar oferta </div>');
            }).finally(
            function() {
                noc.isRefreshing = false;
            });
        }

        noc.openModal = function(ev, titulo) {
            $rootScope.tituloOffer = titulo.originalObject.display;
            if(!$rootScope.isEdit){
                noc.validarCampos();
                var scrolling = noc.divScroll;
                if (noc.popup) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Parece que aún hay campos importantes que debes llenar para continuar </div>');
                    $('html, body').animate({
                        scrollTop: $(scrolling.toString()).offset().top
                    }, 900);
                    noc.divScroll = "";
                }else{
                    $.scrollTo('html, body',90);
                    setTimeout(function(){
                        $scope.$apply(function(){
                            noc.prepararOferta();
                            noc.idUser = storage.get("dataUser")._id;
                            $rootScope.precio = $rootScope.tOffert=='basic' ? '50000':'149000';
                            $rootScope.reference = noc.createReferemce(titulo.originalObject.display, noc.idUser);
                            $rootScope.signature = noc.createHash($rootScope.reference,$rootScope.precio);
                            $rootScope.idUser = storage.get("dataUser")._id;
                            $rootScope.nombreVacante = titulo;

                            $mdDialog.show({
                                controller: controllerDialog,
                                templateUrl: 'dialog65.tmpl.html',
                                parent: angular.element(document.body),
                                targetEvent: ev,
                                escapeToClose: true
                            }).then(function(answer) {}, function() {});
                        });
                    },200);
                    
                }
            }else{
                noc.ofertarConTickets();
            }            
        };

        noc.createReferemce = function(titulo, idUser) {
            titulo = noc.omitirAcentos(titulo);
            var today = moment().format('DD/MM/YYYY');
            return titulo + " " + today + " " + idUser;
        }

        noc.omitirAcentos = function(text) {
            var acentos = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
            var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
            for (var i=0; i<acentos.length; i++) {
                text = text.replace(acentos.charAt(i), original.charAt(i));
            }
            return text;
        }

        noc.createHash = function(reference,precio) {
            return md5("qWS65yqX4MDsNa1hFE87sWA8Ah~562401~" + reference + "~"+precio+"~COP");
        }

        function controllerDialog($scope, $mdDialog, $mdMedia, $http, $window, localStorageService,upload) {
            controllerDialog.prototype.$scope = $scope;
            $scope.isConfirm = false;
            $scope.isPackage = $rootScope.packages ? true:false;
            $scope.isRefreshing2 = true;
            $scope.domain = 0;
            $scope.urlFront = UrlActual + "public/app/payView.html";
            $scope.urlBack = ServerDestino + "payments/confirm";
            $scope.nombreVacante = "Plan paquetes";
            $scope.reference1 = $rootScope.reference;
            $scope.signature1 = $rootScope.signature;
            $scope.precio = $rootScope.precio;
            $scope.tipoOferta = $rootScope.tOffert == 'basic' ? 'Básica':'Premium';
            $scope.totalPrecio = 0;
            $scope.msgPrecio = "Total a pagar"; 
            $scope.cantElegida = "";
            $scope.cantidadList = [];
            $scope.descriptionCompra = "";
            $scope.tarjetas = [];
            $scope.nuevaTarjeta = false;
            $scope.isSelectaTarjeta = false;
            $scope.listaAniosCC = [];
            $scope.listaMesCC = [];
            $scope.activeTab = 0;
            $scope.tabInfo = {};
            $scope.tarjetaSelecta = {};
            $scope.procesado = false;
            $scope.planDetails = [];
            $scope.isAjax = false;
            $scope.membership = $rootScope.isMembership;
            $scope.membershipData = {};
            $scope.ofertasDipo = $rootScope.ofertasDisponibles;
            $scope.precioBasic = 0;
            $scope.serverActual = ServerDestino;
            $scope.deviceSessionId = '';

            $scope.cancel = function() {
                $mdDialog.cancel();
                $("#divButton").attr("style", "display: none;");
                $("#divLoader").attr("style", "display: none;");
                $rootScope.packages = false;
            };

            $scope.primerCarga = function() {
                $scope.precioBasic = $scope.calculador(24900);
                if (localStorageService.get("tokenCompany")) {
                    $http.get(ServerDestino + "companies/" + $rootScope.idUser)
                        .then(function(response){
                        $scope.companyEmail = response.data.data.users.email;
                        $scope.companyEditName = response.data.data.social_name;
                        $scope.companyEditContact = response.data.data.contact[0].full_name;
                        $scope.companyEditTel = response.data.data.contact[0].mobile_phone;
                        $scope.companyEditEmail = response.data.data.users.email;
                    });
                }
                $scope.cargarAniosNuevaTDC();
                $scope.cargarMesesNuevaTDC();

                $http.get(ServerDestino+'membership/plans/all').then(function(response){
                    $scope.planDetails = response.data.plans[0];
                });

                if (!$scope.deviceSessionId) {
                    $http.get(ServerDestino + "payments/deviceid").then(function(response){
                        $scope.deviceSessionId = response.data /*+ storage.get("dataUser")._id*/;
                    });
                }
            }

            $scope.goPackage = function (val){
                $scope.isPackage = true;
                $scope.tipoCompra = val;
                $scope.tipoPackage = val=='BASIC' ? 'Básicas':'Premium';
                $scope.isRefreshing2 = true;
                $scope.precio = val=='BASIC' ? $scope.calculador(50000):149000;
                $scope.totalPrecio = 0;
                $scope.msgPrecio = "Total a pagar"; 
                if ($scope.cantElegidaData) {
                    $scope.cambiarPaquete($scope.cantElegidaData,val);
                }
            }

            if ($rootScope.packages) {
                $scope.goPackage('PREMIUM');
            }

            /**
             * función que permite definir la opción elegida
             * del cajón de precios y redirigir al usuario
             * según su elección
             * @param  {[type]} val [description]
             * @return {[type]}     [description]
             */
            $scope.tOfferta = function(val){
                if ($rootScope.porcentajeL<50) {
                    $scope.cancel();
                    swal({
                        title: "Para crear una oferta necesitas que tu perfíl este lleno como mínimo un 50%",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "##00CB5A",
                        confirmButtonText: "¡Entendido!"
                    }).then(
                        function () {
                        }
                    );
                }else{
                    var tempos = storage.get("dataUser");
                    tempos.typeOff = val;
                    storage.set("dataUser", tempos);
                    $rootScope.tOffert = val;
                    $scope.redirigirOffer("#/nuevaOferta/");
                    $scope.cancel();
                }
            }

            $scope.redirigirOffer = function(url){
                window.location = url;
            }

            $scope.cargaPreciosCant = function(){
                for (var i = 1; i <= 50; i++) {
                    $scope.cantidadList.push(i);
                }
            };

            $scope.cargaPreciosCant();
            $scope.calculador = function (precio) {
                precio = parseFloat(precio);
                return precio + (precio * 0.19);
            }

            $scope.cambiarPaquete = function(data,tipo){
                if(data){
                    $scope.cantElegidaData = parseInt(data);
                    $scope.isRefreshing2 = false;
                }
                if($scope.cantElegidaData==1){
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:189000;
                    $scope.msgPrecio = "Total a pagar"; 
                }else if ($scope.cantElegidaData==2 || $scope.cantElegidaData==3) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:175000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 7% por oferta";
                }else if ($scope.cantElegidaData==4 || $scope.cantElegidaData==5) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:160000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 15% por oferta";
                }else if ($scope.cantElegidaData>=6 && $scope.cantElegidaData<=11) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:149000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 21% por oferta";
                }else if ($scope.cantElegidaData>=12 && $scope.cantElegidaData<=17) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:139000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 26% por oferta";
                }else if ($scope.cantElegidaData>=18 && $scope.cantElegidaData<=24) {
                    $scope.precio = tipo=='BASIC' ? $scope.precioBasic:125000;
                    $scope.msgPrecio = tipo=='BASIC' ? "Total a pagar":"Estas ahorrando 34% por oferta";
                }
                // else if ($scope.cantElegidaData>=25 && $scope.cantElegidaData<=50) {
                //     $scope.precio = 89000;
                //     $scope.msgPrecio = "| Estas ahorrando 58% por oferta";
                // }

                $scope.descriptionCompra = "Compra paquete, cantidad ofertas: "+$scope.cantElegidaData+tipo;
                $scope.totalPrecio = $scope.precio * $scope.cantElegidaData;
                setTimeout(function(){
                    $scope.$apply(function(){
                        $scope.reference1 = $scope.createReferemce($rootScope.idUser,$scope.cantElegidaData+tipo);
                        $scope.signature1 = $scope.createrHash($scope.reference1,$scope.totalPrecio);
                    });
                },200);
            }

            $scope.createrHash = function(reference,total) {
                return md5("qWS65yqX4MDsNa1hFE87sWA8Ah~562401~" + reference + "~"+total+"~COP");
            }

            $scope.createReferemce = function(idUser,tipo) {
                var entre = tipo ? tipo:"";
                var today = moment().format('DD/MM/YYYY');
                return "Plan paquetes " + today + " " + entre + " " + idUser;
            }

            $scope.confirmarDatos = function(){
                $scope.isConfirm = true;
            }

            $scope.prepararEnvio = function(description){
                var objEnvio = {
                    company_id: $rootScope.idUser,
                    job_title: $scope.nombreVacante,
                    job_description: description,
                    contact_offer: {
                        empresa: $scope.companyEditName,
                        nombre: $scope.companyEditContact,
                        phone: $scope.companyEditTel,
                        email: $scope.companyEditEmail
                    },
                    area: "",
                    required_level: "",
                    carrers: "",                    
                    functions: "",
                    min_experience: "",
                    max_experience: "",
                    city: "",
                    department: "",
                    observation: "",
                    type_contract: "",
                    num_offers: "",
                    limit_date: "0000-00-00",
                    base_salary: "",
                    extra_salary: "",
                    show_salary: "",
                    language: "",
                    relevancia: "",
                    sector: "",
                    preferencias: "",
                    softwares: "",
                    areas: ""
                };

                $rootScope.ofertaEnviar = objEnvio;
            }

            $scope.procesarCompra = function(){
                $scope.isAjax = true;
                $http.post(ServerDestino + "membership/subscription",$scope.subscription).then(function successCallback(response){
                    if (response.data.message='OK') {
                        $mdDialog.cancel();
                        swal({
                            title: "Tu membresia ha sido activada, ¡choca esas cuatro!",
                            text: "Ya puedes disfrutar de los beneficios de tu membresia mensual SoyEmpleo",
                            imageUrl: "assets/images/soyempleo-chocala-simple.png",
                            confirmButtonColor: "#00BF52",
                            confirmButtonText: "Aceptar"
                        });
                    }else{
                        alertify.error("Hubo un error al procesar tu solicitud, por favor intenta más tarde.");
                    }
                },function errorCallback(response) {
                    alertify.error("Hubo un error al procesar tu solicitud, por favor intenta más tarde.");
                }).finally(function(){
                    $scope.isAjax = false;
                });
            }


            $scope.procesarOfertaCompra = function(reference){
                $scope.isAjax = true;
                $rootScope.ofertaEnviar.offer_type = $rootScope.tOffert;
                $rootScope.ofertaEnviar.paid_by = 'tdc';
                $http.post(ServerDestino + 'offers',$rootScope.ofertaEnviar).then(function(response){
                    if (response.data.message=='OK') {
                        $scope.subscription.offer_id = response.data.offer._id;
                        $scope.subscription.credit_card_expiration_month = $scope.subscription.credit_card_expiration_month.length < 2 ? "0"+$scope.subscription.credit_card_expiration_month:$scope.subscription.credit_card_expiration_month;
                        $http.post(ServerDestino + "payments/process",$scope.subscription)
                        .then(function(response){
                            if (response.data.state=='APPROVED') {
                                $scope.cancel();
                                setTimeout(function(){
                                    swal({
                                        title: "Tu transacción fué aprobada, en instantes tu ofertas pasará a revisión final para ser publicada",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "##00CB5A",
                                        confirmButtonText: "¡Entendido!"
                                    }).then(function () {
                                        window.location.href = "#/dashboard-company";
                                    });
                                },200);
                            }else if (response.data.state=='PENDING') {
                                $scope.cancel();
                                setTimeout(function(){
                                    swal({
                                      title: "Tu transacción está siendo verificada, cuando el pago se haga efectivo recibirás un correo con los detalles de la operación",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "##00CB5A",
                                        confirmButtonText: "¡Entendido!"
                                    }).then(function () {
                                        window.location.href = "#/dashboard-company";
                                    });
                                },200);
                            }else {
                                $scope.cancel();
                                setTimeout(function(){
                                    swal({
                                        title: "Tu transacción fué rechazada, por favor verifica los datos ingresados",
                                        type: "error",
                                        showCancelButton: false,
                                        confirmButtonColor: "##00CB5A",
                                        confirmButtonText: "¡Entendido!"
                                    }).then(function () {
                                       
                                    });
                                },200);
                            }
                        },function(response){
                            console.log("error: ",response);
                        }).finally(function(){
                            $scope.isAjax = false;
                        });
                    }
                });
            }

            $scope.procesarOfertaCompraTransfer = function(reference){
                $scope.isAjax = true;
                $rootScope.ofertaEnviar.offer_type = $rootScope.tOffert;
                $rootScope.ofertaEnviar.paid_by = 'transfer';
                $http.post(ServerDestino + 'offers',$rootScope.ofertaEnviar).then(function(response){
                    if (response.data.message=='OK') {
                        $scope.cancel();
                        setTimeout(function(){
                            swal({
                                title: "Tu oferta fué procesada!",
                                text: 'En instantes recibirás un correo a '+$scope.companyEmail+' con las instrucciones de pago',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "##00CB5A",
                                confirmButtonText: "¡Entendido!"
                            }).then(function () {
                                window.location.href = "#/dashboard-company";
                            });
                        },200);
                    }else{
                        $scope.cancel();
                        setTimeout(function(){
                            swal({
                                title: "Tu transacción fué rechazada, por favor intentar más tarde",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "##00CB5A",
                                confirmButtonText: "¡Entendido!"
                            }).then(function () {
                               
                            });
                        },200);
                    }
                }).finally(function(){
                    $scope.isAjax = false;
                });
            }

            $scope.uploadFile2 = function () {

                var aux2 = 0;
                if ($rootScope.office_picturesLength == 4) {
                    alertify.error(' <div class="text-center font-size-16 text-white"> Lo sentimos, has alcanzado el límite de fotos permitidas. Intenta borrando una de las antiguas. </div>');
                } else {
                    $scope.fileUploading();
                    for (var i = 0; i < $rootScope.office_picturesLength; i++) {
                        if ($rootScope.arrayPhotos[i] == null) {
                            aux2 = i;
                            break;
                        } else if ($rootScope.arrayPhotos[i].position != i) {
                            aux2 = i;
                            break;
                        } else {
                            aux2 = i + 1;
                        }
                    }
                    $scope.photopos = aux2;
                    var envio = {
                        file: $scope.officePhoto,
                        url: ServerDestino + "companies/" + userId + "/officepicture" + aux2,
                        _token: token,
                        nombreCampo: "picture"
                    };

                    upload.uploadFile(envio).then(function (response) {
                        if (response.data.message == "OK") {
                            /*window.location = "#/basicData";
                            window.location = "#/aditionalData";*/

                            $scope.cancel();
                        } else {
                            $scope.cancel();
                        }
                    });
                }
                ;
            };

            $scope.file_changed = function (element) {
                if (element.value != '') {
                    $scope.$apply(function () {
                        $scope.uploadGo = true;
                    });
                }
            };

            $scope.fileUploading = function () {
                $scope.uploadGo = false;
                $scope.uploadChange = true;
            }

            setTimeout(function() {
                $scope.$apply(function() {
                    $("#logo").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="assets/images/company_pic.png" alt="Tu Foto" style="width:160px">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });

                    $("#avatar").fileinput({
                        overwriteInitial: true,
                        maxFileSize: 1000,
                        showClose: false,
                        showCaption: false,
                        browseLabel: '',
                        removeLabel: '',
                        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                        removeTitle: 'Cancelar o restaurar cambios',
                        elErrorContainer: '#kv-avatar-errors',
                        msgErrorClass: 'alert alert-block alert-danger',
                        defaultPreviewContent: '<img src="assets/images/company_pic.png" alt="Tu Foto" class="pic-260">',
                        layoutTemplates: {
                            main2: '{preview} {remove} {browse}'
                        },
                        allowedFileExtensions: ["jpg", "png", "gif"]
                    });
                })
            }, 600);

            /**
             * Funcion que envia la data de la nueva tdc al back
             */
            $scope.agregarTarjeta = function(
                payer_name,
                creditCard,
                payer_street,
                credit_card_document,
                payer_phone,
                payer_city,
                payer_state,
                credit_card_expiration_month,
                credit_card_expiration_year,
                credit_card_ccv,
                payer_country,
                payer_postal_code){
                /*$scope.nuevaTarjeta = false;*/
                $scope.procesado2 = true;
                
                $scope.tarjetaSelecta = {
                    name:$scope.nombreTarjetaMostrarFormater(payer_name),
                    type:creditCard.type,
                    number:$scope.tarjetaMostrarFormater(creditCard.number),
                    picked:true
                }
                $scope.tarjetas.push($scope.tarjetaSelecta);
                
                $scope.subscription = {
                    company_id: $rootScope.idUser,
                    offer_id:'',
                    job_title:$rootScope.tituloOffer,
                    offer_price: $rootScope.tOffert =='basic' ? 50000:149000,
                    payer_name:payer_name,
                    payer_phone:payer_phone,
                    payer_dni:credit_card_document,
                    payer_street:payer_street,
                    payer_city:payer_city,
                    payer_state:payer_state,
                    payer_country:'co',
                    payer_postal_code:'1234',
                    credit_card_number:creditCard.number,
                    credit_card_expiration_year:credit_card_expiration_year,
                    credit_card_expiration_month:credit_card_expiration_month,
                    credit_card_security_code:credit_card_ccv,
                    payment_method:creditCard.type.toUpperCase(),
                    credit_card_document:credit_card_document,
                    device_id:$scope.deviceSessionId
                }
                /*$scope.cambioIndividual(1);*/
            }

            $scope.applyDiscount = function(){

                $scope.planDetails.tax_return_base = ($scope.subscription.promo_code == 'YoSoyEmpleo') ? ($scope.planDetails.tax_return_base / 2) : 200000;

            }

            $scope.validarEnvioBoton = function(valor) {
                if (!valor) {
                    $scope.blockeaTab = valor;
                    return valor;
                }
                $scope.blockeaTab = valor;
                return valor;
            }

            $scope.tarjetaMostrarFormater = function(num){
                return "**** **** **** " + num.substring(num.length-4,num.length);
            }

            $scope.nombreTarjetaMostrarFormater = function(name){
                var tokens = name.split(" ");
                var nombre = '';
                if (tokens.length > 2) {
                    var length = tokens.length>4 ? 4:tokens.length;
                    for (var i = 0; i < length; i++) {
                        if (i%2==1 && i>0) {
                            nombre = nombre + " " + tokens[i].charAt(0).toUpperCase();
                        }else{
                            if (!i) {
                                nombre = nombre + tokens[i].toUpperCase();
                            }else{
                                nombre = nombre + " " + tokens[i].toUpperCase();
                            }
                            
                        }
                    }
                    return nombre; 
                }else if (tokens.length==2){
                    return tokens[0].toUpperCase() + " " + tokens[1].toUpperCase();
                }
                return name.toUpperCase();                
            }

            $scope.cambiarTabById = function(indx,to){
                $scope.tabInfo = indx.$parent.tabset;
                if (to=='mtp') {
                    $scope.procesado = false;
                }
            }

            $scope.cambioIndividual = function(valor){
                if ($scope.tabInfo.tabs) {
                    setTimeout(function(){
                        $scope.$apply(function(){
                            $scope.tabInfo.active = valor;
                        });
                    },100)
                }else{
                    setTimeout(function(){
                        $scope.$apply(function(){
                             $scope.activeTab = valor;
                        });
                    },100);
                }
            }

            /**
             * Función que carga en una variable 10 años apartir del actual
             */
            $scope.cargarAniosNuevaTDC = function(){
                $scope.listaAniosCC = [];
                var actual = new Date();
                for (var i = 0; i < 11; i++) {
                    $scope.listaAniosCC.push({anio:actual.getFullYear()+i});
                }
            }

            /**
             * Función que carga valores del 1 al 12 (meses)
             */
            $scope.cargarMesesNuevaTDC = function(){
                $scope.listaMesCC = [];
                var actual = new Date();
                for (var i = 0; i < 12; i++) {
                    $scope.listaMesCC.push({mes:i+1});
                }
            }

            /**
             * Funcion que renderiza el formulario para crear nueva tarjeta
             */
            $scope.nuevaTarjetaForm = function(value){
                $scope.nuevaTarjeta = value;
            }

            /**
             * Funcion que selecciona una tarjeta de la lista en el front
             */
            $scope.seleccionarTarjeta = function(indx){
                var found = $filter('filter')($scope.tarjetas,{picked:true},true);
                if (found.length) {
                    for (var i = 0; i < found.length; i++) {
                        found[i].picked = false;
                    }
                }
                $scope.tarjetas[indx].picked = !$scope.tarjetas[indx].picked;
            }

            $scope.primerCarga();
        };

        controllerDialog.prototype.setFile = function(element) {
            var $scope = this.$scope;
            $scope.$apply(function() {
                $scope.theFile = element.files[0];
            });
        };

        setTimeout(function() {
            $http.get(ServerDestino + "studies/all").then(function(response) {
                noc.listaInstitutos = response.data;
                $scope.estudios = remapObj(noc.listaInstitutos);
            }, function(error) {});


            $http.get(ServerDestino + "/skills/all").then(function (response) {
                noc.listaHabilidades = response.data;
                // list of `skills` value/display objects
                if(noc.listaHabilidades.length){
                    noc.habilidades = remapObjHabilidades(noc.listaHabilidades);
                    noc.querySearch = querySearch;
                    noc.selectedItemChange = selectedItemChange;
                }
            });

            

            $http.get(ServerDestino + "positions/all").then(function(response){
                noc.listaPositions = response.data;
                if (noc.listaPositions.length) {
                    noc.posiciones = remapObjPositions(noc.listaPositions);
                    for (var i = 0; i < noc.posiciones.length; i++) {
                        noc.posiciones[i].name = noc.posiciones[i].display;
                        noc.posiciones[i].value = noc.posiciones[i].display;
                    }
                }
            });

            $http.get(ServerDestino + "areapositions/all").then(function(response) {
                noc.autoAreas = response.data;
                $scope.areas = remapObj(noc.autoAreas);
            }, function(error) {});

            function remapObj(obj) {
                var value = [];
                return obj.map(function(state) {
                    value = {
                        name: state.description.toLowerCase()
                    };
                    return value;
                });
            }
        }, 500); 

    /**
     * Angular Material AutoComplete methods
     * @type {[type]}
     */
    noc.searchTextChange   = searchTextChange;

    // ******************************
    // Internal methods
    // ******************************
    
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query,obj) {
      var results = query ? obj.filter( createFilterFor(query) ) : obj,
          deferred;
          return results;
    }

    function searchTextChange(text) {
      /*$log.info('Text changed to ' + text);*/
    }

    function selectedItemChange(item) {
        if(item!=null){
            noc.funcSelected(item.display);
        }
    }

    /**
     * Build `states` list of key/value pairs
     */
    function remapObjHabilidades(obj) {
        var value = [];
        return obj.map(function(state) {
            value = {
                value: state.description.toLowerCase(),
                display: state.description,
                name: state.description.toLowerCase()
            };
            return value;
        });
    } 

    /**
     * Build `states` list of key/value pairs
     */
    function remapObjPositions(obj) {
        var value = [];
        return obj.map(function(state) {
            value = {
                value: state.name.toLowerCase(),
                display: state.name,
                name: state.name.toLowerCase()
            };
            return value;
        });
    }     

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(state) {
        return (state.value.indexOf(lowercaseQuery) === 0);
      };

    } 

    /**
     * Ejecutar carga inicial
     */
    noc.cargaInicial();      
    }

    nuevaOfertaController
        .$inject = ["localStorageService", 
            "$rootScope", 
            "$http", 
            "$location", 
            "$scope",
            "$filter", 
            "$mdDialog", 
            "$mdMedia", 
            "$window", 
            "$q", 
            "$anchorScroll", 
            "$timeout", 
            "$log", 
            "$routeParams",
            "$sce",
            "$uibModal",
            "$document",
            "$payments"
    ];
    angular
    .module('spa-SE')
    .controller('nuevaOfertaController', nuevaOfertaController);
})();
