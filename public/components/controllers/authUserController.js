/**
 * Created by Daniel Sánchez on 02/06/2017
 */

(function () {

    AuthUserController.$inject = ["$http", "$location", "$rootScope", "localStorageService", "$routeParams", "$window"];
    angular
        .module('spa-SE')
        .controller('authUserController', AuthUserController);

    function AuthUserController($http, $location, $rootScope, storage, $routeParams) {

        switch ($routeParams.status) {
            case 'success':

                if ($routeParams.typeuser == 'person') {
                    storage.set("tokenUser", $routeParams.tk);
                    storage.set("dataUser", {_id: $routeParams.person, first_name: $routeParams.first, last_name: $routeParams.last});
                    storage.set("tipoUser", "person");
                    $rootScope.loginx = true;
                    $rootScope.isLoged = true;
                    $location.path("/dashboard");
                }else if($routeParams.typeuser == 'company') {
                    storage.set("tokenCompany", $routeParams.last);
                    storage.set("dataUser", {_id: $routeParams.person, name: $routeParams.first});
                    storage.set("tipoUser", "company");
                    $rootScope.loginx = true;
                    $rootScope.isLoged = true;
                    $location.path("/dashboard-company");
                    this.script('assets/js/zopim.js');
                }
                break;
            case 'error':
                swal({
                    title: "¡Error!",
                    text: "Usuario no autorizado",
                    type: "error"
                });
                $window.close();
                break;
        }

    }

})();
