/**
 * Created by Daniel Sánchez on 07/07/2016
 */

(function () {

    LinkedinController.$inject = ["$http", "$location", "$rootScope", "localStorageService", "$routeParams"];
    angular
        .module('spa-SE')
        .controller('linkedinController', LinkedinController);

    function LinkedinController($http, $location, $rootScope, storage, $routeParams) {

        switch ($routeParams.status) {
            case 'success':
                storage.set("tokenUser", $routeParams.tk);
                storage.set("dataUser", {_id: $routeParams.person, first_name: $routeParams.first, last_name: $routeParams.last});
                storage.set("tipoUser", "person");
                $rootScope.loginx = true;
                alertify.success(' <div class="text-center font-size-16 text-white"> Bienvenido a SoyEmpleo.com </div> ');
                $location.path("/dashboard");
                $rootScope.isLoged = true;
                break;
            case 'error':
                swal({
                    title: "¡Error!",
                    text: "No se han podido Obtener los Datos de Usuario",
                    type: "error"
                });
                $location.path("/login");
                break;
        }

    }

})();
