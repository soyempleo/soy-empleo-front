/**
 * Created by DanielS.
 */

(function() {
	'use strict'

	function companyOffersController($http, $location, $scope, $routeParams, $window) {

		var co = this;

		co.cargaInicial = function () {

			$http.get(ServerDestino + "companies/dataCompany/"+$routeParams.id).then(function(response) {
				
				co.company = response.data.company_data; 
                co.company.social_name = co.company.social_name != "" && 
                                         co.company.social_name != null ?
                                         co.company.social_name: co.company.name;
                if (co.company.logo != "" && co.company.logo != null) {
                    co.company.logo = "https://api.soyempleo.com/img/pictures/companies/logos/" + co.company.logo;
                    co.urlValidator(co.company.logo);
                }else{
                    co.company.logo = "assets/images/company_pic.png";
                }
				
			});

		}		

		co.offersUrl = function (ciudad, company, job, company_id) {
            ciudad = co.urlLimpia(ciudad);
            company = co.urlLimpia(company);
            job = co.urlLimpia(job);
                window.location = "#/ofertas/empleo/"
                + ciudad
                + "/" + company
                + "/" + job
                + "/" + company_id;
        };

        co.urlLimpia = function (value) {
            var tmp_this = value.toLowerCase();
            var arr_busca = "áéíóúñü".split("");
            var arr_reemplaza = "aeiounu".split("");
            for (var i = 0; i < arr_busca.length; i++) {
                tmp_this = tmp_this.replace(arr_busca[i], arr_reemplaza[i]);
            }
            return tmp_this.replace(/[^\\s\w]/g, "-");
        };

        co.urlValidator = function(url) {
            return $http.get(url).then(function(response) {
                return null;
            }).catch(function(err) {
                if (err.status === 404) {
                    co.company.logo = "assets/images/company_pic.png";
                    return null
                };

                return $q.reject(err);
            })
        };

	}

	companyOffersController.$inject = ["$http", "$location", "$scope", "$routeParams", "$window"];
	angular
		.module('spa-SE')
		.controller("companyOffersController", companyOffersController);

})();