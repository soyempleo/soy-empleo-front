/**
 * Created by Daniel Sánchez on 19/10/2016.
 */

(function (){
    'use strict'

    function contactController($http, $scope){

        this.correoContacto = function () {
            
            this.objEnvio = {contact_name: this.name,
                             contact_email: this.email,
                             contact_subject: this.subject,
                             contact_message: this.message};

            $http.post(ServerDestino + "contact/email", this.objEnvio)
                 .then(function (response) {
                if (response.data.message == "OK") {
                    alertify.success('Mensaje enviado Correctamente!!!');
                }else{
                    alertify.error('Ha ocurrido un error al enviar el Mensaje, intenta más tarde!!!');                    
                }
            });
        };
    }
    contactController.$inject = ["$http", "$scope"];
    angular
        .module('spa-SE')
            .controller('contactController', contactController);
})();
