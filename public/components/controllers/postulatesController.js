(function () {
    'use strict'

    function PostulatesController(storage, $rootScope, $http, $location, $mdDialog, $mdMedia, $scope, $window, $q, $routeParams,$filter,upload) {
		
        var po = this;

		po.postulates = [];
		po.ofertaActual = [];
		po.currentPage = 1;
        po.totalItems = 0;
        po.entryLimit = 5; // items per page 
        po.noOfPages = 0;
        po.items = [];
        po.profileData = [];
        po.userData = [];
        po.listaAdmin = [];
        po.listaPerson = [];
        po.preguntasList = [];
        $rootScope.selected = 0;
        $rootScope.selectedAvatar = "assets/images/user_male.png";
        po.lastItem = false;
        po.IsContratado = false;
        po.respondidasLength = false;
        po.listaInicial = [];
        po.listaPreseleccionados = [];
        po.listaEntrevistados = [];
        po.listaContratados = [];
        po.styles = {};
        po.estado1 = "";
        po.estado2 = "";


        /**
            objetos de paginadores
        **/
            /*lista admin*/
            po.currentPage1 = 1;
            po.totalItems1 = 0;
            po.entryLimit1 = 5; // items per page 
            po.noOfPages1 = 0;
            po.items1 = [];

            /*lista person*/
            po.currentPage2 = 1;
            po.totalItems2 = 0;
            po.entryLimit2 = 5; // items per page 
            po.noOfPages2 = 0;
            po.items2 = [];

            /*lista preseleccionados*/
            po.currentPagePre = 1;
            po.totalItemsPre = 0;
            po.entryLimitPre = 5;
            po.noOfPagesPre = 0;

            /*lista entrevistados*/
            po.currentPageEnt = 1;
            po.totalItemsEnt = 0;
            po.entryLimitEnt = 5;
            po.noOfPagesEnt = 0;

            /*lista contratados*/
            po.currentPageCon = 1;
            po.totalItemsCon = 0;
            po.entryLimitCon = 5;
            po.noOfPagesCon = 0;
        /**
            objetos de paginadores
        **/
		

		po.goToUrl = function(url){
            window.location = url;
        }

        if ($rootScope.postulates) {
            po.cargaInicial = function(){
                po.styles = {preseleccionado:""},
                            {entrevistado:""},
                            {contratado:""};
                if ($rootScope.postulates && $rootScope.ofertaSelect) {
                    po.postulates = $rootScope.postulates;
                    po.ofertaActual = $rootScope.ofertaSelect;

                    for (var i = 0; i < po.postulates.length; i++) {
                        po.postulates[i].person.edad = po.calculaEdad(new Date(po.postulates[i].person.birthdate)) + " Años";
                    }

                    po.items = po.postulates;
                    po.totalItems = po.postulates.length;
                    po.entryLimit = 5; // items per page
                    po.noOfPages = Math.ceil(po.totalItems / po.entryLimit);
                }else{
                    po.goToUrl('/#/dashboard-company');
                }
            }

            po.calculaEdad = function(fechaNac) {
                var difEdadMs = Date.now() - Date.parse(fechaNac);
                var edad = new Date(difEdadMs);
                return Math.abs(edad.getUTCFullYear() - 1970);
            };

            po.cargaInicial();

            po.getPerson = function(id,estado1,estado2,estado3){ 
                po.goToUrl('/#/perfil/persona');
                if(id){/*"https://api.soyempleo.com/job/58331fa9b7eb3a1c18e461b9/showbyoffer"*/
                    /*$http.get(ServerDestino + "persons/" + id + "/profiledata").then(function(response){*/
                    $http.get(ServerDestino + "persons/" + id + "/profiledata").then(function(response){
                        po.profileData = response.data.person;
                        po.userData = response.data.user;

                        $rootScope.selectedAvatar = po.userData.avatar ? ServerDestino + "profile/" + po.userData.avatar : "assets/images/user_male.png";
                        if($rootScope.selectedAvatar!="assets/images/user_male.png"){
                            po.urlValidator($rootScope.selectedAvatar);
                        }
                        var busca = {person:{_id:id}}
                        var found = $filter('filter')(po.postulates,busca,true)[0];
                        if(found){
                            $rootScope.selected = po.postulates.indexOf(found);
                            $rootScope.postulates[$rootScope.selected].user = po.userData;
                        }
                        if ($rootScope.postulates[$rootScope.selected].softwares) {
                            for (var j = 0; j < $rootScope.postulates[$rootScope.selected].softwares.length; j++) {
                                $rootScope.postulates[$rootScope.selected].softwares[j] = po.preparador3Niveles($rootScope.postulates[$rootScope.selected].softwares[j]);
                            }
                        }

                        if($rootScope.postulates[$rootScope.selected].skills){
                            for (var j = 0; j < $rootScope.postulates[$rootScope.selected].skills.length; j++) {
                                $rootScope.postulates[$rootScope.selected].skills[j] = po.preparador10Niveles($rootScope.postulates[$rootScope.selected].skills[j]);
                            }
                        }

                        $rootScope.postulates[$rootScope.selected].estado1 = estado1;
                        $rootScope.postulates[$rootScope.selected].estado2 = estado2;
                        if (estado3) {
                            $rootScope.postulates[$rootScope.selected].IsContratado = true;
                        }

                        $http.get(ServerDestino + "persons/" + id  + "/answersquestions").then(function(response){
                            $rootScope.postulates[$rootScope.selected].preguntasList = [];
                            $rootScope.postulates[$rootScope.selected].preguntasList = response.data.person_answers_questions;
                        });
                    });
                }
            }

            po.preparador3Niveles = function(obj){
                if (obj.level) {
                    var objColores = [];
                    for (var i = 0; i <5; i++) {
                        if (obj.level == "1" && i<=1) {
                            objColores.push({act:true});
                        }else if (obj.level == "1" && i>1){
                            objColores.push({act:false});
                        }

                        if (obj.level == "2" && i<=2) {
                            objColores.push({act:true});
                        }else if (obj.level == "2" && i>2){
                            objColores.push({act:false});
                        }

                        if (obj.level == "3" && i<=4) {
                            objColores.push({act:true});
                        }
                    }
                    obj.nivel = objColores;
                }
                return obj;
            }

            po.preparador10Niveles = function(obj){
                if (obj.level) {
                    var objColores = [];
                    for (var i = 0; i <5; i++) {
                        if (obj.level>=1 && obj.level<=2 && i<=0) {
                            objColores.push({act:true});
                        }else if (obj.level>=1 && obj.level<=2 && i>0) {
                            objColores.push({act:false});
                        }

                        if (obj.level>=3 && obj.level<=4 && i<=1) {
                            objColores.push({act:true});
                        }else if (obj.level>=3 && obj.level<=4 && i>1) {
                            objColores.push({act:false});
                        }

                        if (obj.level>=5 && obj.level<=6 && i<=2) {
                            objColores.push({act:true});
                        }else if (obj.level>=5 && obj.level<=6 && i>2) {
                            objColores.push({act:false});
                        }

                        if (obj.level>=7 && obj.level<=8 && i<=3) {
                            objColores.push({act:true});
                        }else if (obj.level>=7 && obj.level<=8 && i>3) {
                            objColores.push({act:false});
                        }

                        if (obj.level>=9 && obj.level<=10) {
                            objColores.push({act:true});
                        }
                    }

                    obj.nivel = objColores;
                }
                return obj;
            }

            po.urlValidator = function (url) {
                return $http.get(url).then(function (response) {
                    return null;
                }).catch(function (err) {
                    if (err.status === 404) {
                        $rootScope.selectedAvatar = "assets/images/user_male.png";
                        return null
                    }
                    ;

                    return $q.reject(err);
                })
            };

            po.tempo = function(obj){
                for (var i = 0; i < obj.length; i++) { 
                    if (i % 2) {
                        obj[i].postulated_by = "admin";
                    } else {
                        obj[i].postulated_by = "person";
                    }
                }
                po.listaAdmin = $filter('filter')(obj,{postulated_by:'admin'},true);
                po.listaPerson = $filter('filter')(obj,{postulated_by:'person'},true);

                po.tempo2(po.listaAdmin,po.listaPerson);

            }

            po.tempo2 = function(obj1,obj2){
                if (obj1) {
                    po.items1 = obj1;
                    po.totalItems1 = obj1.length;
                    po.entryLimit1 = 5; // items per page
                    po.noOfPages1 = Math.ceil(po.totalItems1 / po.entryLimit1);
                }
                if (obj2) {
                    po.items2 = obj2;
                    po.totalItems2 = obj2.length;
                    po.entryLimit2 = 5; // items per page
                    po.noOfPages2 = Math.ceil(po.totalItems2 / po.entryLimit2);
                }
            }

            po.separarEstados = function(){
                if($rootScope.postulates){
                    for (var i = 0; i < $rootScope.postulates.length; i++) {
                        if($rootScope.postulates[i].status.length>1){
                            if ($rootScope.postulates[i].status[0].label=="descartado") {
                                if ($rootScope.postulates[i].status[1].label=='postulado') {
                                    po.listaInicial.push($rootScope.postulates[i]);
                                    po.listaInicial[po.listaInicial.length-1].isDescartado=true;
                                } else if ($rootScope.postulates[i].status[1].label=='preseleccionado') {
                                    po.listaPreseleccionados.push($rootScope.postulates[i]);
                                    po.listaPreseleccionados[po.listaPreseleccionados.length-1].isDescartado=true;
                                } else if ($rootScope.postulates[i].status[1].label=='entrevistado') {
                                    po.listaEntrevistados.push($rootScope.postulates[i]);
                                    po.listaEntrevistados[po.listaEntrevistados.length-1].isDescartado=true;
                                } else if ($rootScope.postulates[i].status[1].label=='contratado') {
                                    po.listaContratados.push($rootScope.postulates[i]);
                                    po.listaContratados[po.listaContratados.length-1].isDescartado=true;
                                } 
                            } else {
                                 if ($rootScope.postulates[i].status[0].label=='postulado') {
                                    po.listaInicial.push($rootScope.postulates[i]);
                                } else if ($rootScope.postulates[i].status[0].label=='preseleccionado') {
                                    po.listaPreseleccionados.push($rootScope.postulates[i]);
                                } else if ($rootScope.postulates[i].status[0].label=='entrevistado') {
                                    po.listaEntrevistados.push($rootScope.postulates[i]);
                                } else if ($rootScope.postulates[i].status[0].label=='contratado') {
                                    po.listaContratados.push($rootScope.postulates[i]);
                                }
                            }
                        }else{
                            if ($rootScope.postulates[i].status[0].label=='postulado') {
                                po.listaInicial.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='preseleccionado') {
                                po.listaPreseleccionados.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='entrevistado') {
                                po.listaEntrevistados.push($rootScope.postulates[i]);
                            } else if ($rootScope.postulates[i].status[0].label=='contratado') {
                                po.listaContratados.push($rootScope.postulates[i]);
                            }
                        }
                    }

                    po.prepararSecciones();
                }
            }
            
            po.prepararSecciones = function(){
                if (po.listaInicial.length) {
                    /*for (var i = 0; i < po.listaInicial.length; i++) {
                        if (!po.listaInicial[i].postulated_by) {
                            po.listaInicial[i].postulated_by=="person";
                        }
                    }*/
                    po.listaAdmin = $filter('filter')(po.listaInicial,{postulated_by:'admin'},true);
                    po.lastItem = po.listaAdmin.length ? false:true;
                    po.listaPerson = $filter('filter')(po.listaInicial,{postulated_by:"person"},true);
                    po.tempo2(po.listaAdmin,po.listaPerson);
                }
                if (po.listaPreseleccionados.length) {
                    po.totalItemsPre = po.listaPreseleccionados.length;
                    po.entryLimitPre = 5; // items per page
                    po.noOfPagesPre = Math.ceil(po.totalItemsPre / po.entryLimitPre);
                }
                if (po.listaEntrevistados.length) {
                    po.totalItemsEnt = po.listaEntrevistados.length;
                    po.entryLimitEnt = 5; // items per page
                    po.noOfPagesEnt = Math.ceil(po.totalItemsEnt / po.entryLimitEnt);
                }
                if (po.listaContratados.length) {
                    po.totalItemsCon = po.listaContratados.length;
                    po.entryLimitCon = 5; // items per page
                    po.noOfPagesCon = Math.ceil(po.totalItemsCon / po.entryLimitCon);
                }
            }

            po.cambiarEstatus = function(id,estado,id2){
                var objEnvio = { "status":  
                    [ 
                        {
                            "label": estado,
                            "date": po.convertDate(new Date())
                        }
                    ]
                }

                $http.post(ServerDestino + 'job/' + id + '/updatePostulation',objEnvio).then(function(response){
                    if(response.data.message=='OK'){
                        alertify.success(' <div class="text-center font-size-16 text-white"> El perfil ha sido '+estado+' </div>');
                        po.filtrarCambio(id,estado);
                    }else{
                        alertify.error(' <div class="text-center font-size-16 text-white"> Error al enviar datos de postulación </div>');
                    }
                });
            }

            po.filtrarCambio = function(id,estado){
                var found = $filter('filter')($rootScope.postulates,{_id:id},true)[0];
                if(found){
                    switch(estado){
                        case "preseleccionado":
                            po.cambiarSeccion(po.listaInicial,found,po.listaPreseleccionados,"inicial");
                            po.styles.preseleccionado = "parpadea";
                        break;

                        case "entrevistado":
                            po.cambiarSeccion(po.listaPreseleccionados,found,po.listaEntrevistados);
                            po.styles.entrevistado = "parpadea";
                        break;

                        case "contratado":
                            po.cambiarSeccion(po.listaEntrevistados,found,po.listaContratados);
                            po.styles.contratado = "parpadea";
                        break;

                        case "descartado":
                            /*po.descartador(found);*/
                            found.isDescartado = true;
                        break;
                    }
                }
            }

            po.desHojaVida = function (id) {
                var win = window.open(ServerDestino + "pdf/profile/" + id, '_blank');
                win.focus();
            }

            po.cambiarSeccion = function(objOrigen,objSelect,objDestino,typo){
                var indx = objOrigen.indexOf(objSelect);
                objDestino.push(objSelect);
                if (typo=="inicial") {
                    var found1 = $filter('filter')(po.listaAdmin,{_id:objSelect._id},true)[0];
                    if(found1){
                        po.listaAdmin.splice(po.listaAdmin.indexOf(found1),1);
                    }else{
                        var found2  = $filter('filter')(po.listaPerson,{_id:objSelect._id},true)[0];
                        if (found2) {
                            po.listaPerson.splice(po.listaPerson.indexOf(found1),1);
                        }
                    }
                }
                objOrigen.splice(indx,1);

            }

            po.noParpadear = function(pos){
                po.styles[pos] = "";
            }

            po.convertDate = function(inputFormat) {
                function pad(s) { return (s < 10) ? '0' + s : s; }
                    var d = new Date(inputFormat);
                return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
            }

            po.getLast = function(id){
                if (id) {
                    var found = $filter('filter')(po.listaAdmin,{_id:id},true)[0];
                    
                    if (found) {
                        var indx = po.listaAdmin.indexOf(found);
                        if (indx==po.listaAdmin.length-1) {
                            po.lastItem = true;
                            return true;
                        } else {
                            po.lastItem = false;
                            return false;
                        }
                    } else {
                        po.lastItem = false;
                        return false;
                    }
                }
                po.lastItem = false;
                return false;
            }

            po.separarEstados();
            /*po.tempo($rootScope.postulates);*/

            /*po.getPerson();*/
        } else {
            po.goToUrl('/#/dashboard-company');
        }
	}

	PostulatesController
        .$inject = ["localStorageService", "$rootScope", "$http", "$location", "$mdDialog", "$mdMedia",
            "$scope", "$window", "$q", "$routeParams", "$filter","upload"
    ];
    angular
        .module('spa-SE')
        .controller('postulatesController', PostulatesController);
})();