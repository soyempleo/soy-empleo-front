/**
 * Created by WilderAlejandro on 23/11/2015.
 */


/**
 *Este factory es usado para consultar, guardar y eliminar una persona
 **/
angular
  	.module('spa-SE')
  	.factory("persons", ['$resource', function($resource){
	    return $resource(ServerDestino+"persons/:id", {id: "@id"});
  	}]);

/**
 *Este factory es usado para consultar, guardar y eliminar idiomas de personas
 **/
angular
  	.module('spa-SE')
  	.factory("personsIdioma", ['$resource', function($resource){
	    return $resource(ServerDestino+"languagesperson/:id", {id: "@id"});
	}]);

/**
 *Este factory es usado para consultar, guardar y eliminar Software de personas
 **/
angular
  	.module('spa-SE')
  	.factory("personsSoftware", ['$resource', function($resource){
	    return $resource(ServerDestino+"softwareperson/:id", {id: "@id"});
	}]);
/**
 *Este es un factory que retorna el $resourse necesario para actualizar persona
 **/

angular
  	.module('spa-SE')
  	.factory("putPersons", ['$resource', function($resource){
	    return $resource(ServerDestino+"persons/:id", {id: "@id"}, {update: {method: "PUT"}});
	}]);

/*angular
  .module('spa-SE3')
  .service('urlValidator',["$http", "$q", function ($http, $q){
    this.url = function (url){
      return $http.get(url).catch(function (err){
      // recover here if err is 404
      if(err.status === 404) return null; //returning recovery
      // otherwise return a $q.reject
      return $q.reject(err);
      })
    }
    
}]);*/

angular
  .module('spa-SE')
  .service('upload', ["$http", "$q", function ($http, $q) {
      this.uploadFile = function (envio) {
          var deferred = $q.defer();
          /*var formData = new FormData($(envio.formId)[0]);*/
          var formData = new FormData();
          formData.append(envio.nombreCampo, envio.file);
          /*if(envio.formId == "form#empreAditional"){
            formData.append("position", envio.position);
          }*/          
          return $http.post(envio.url, formData, {
              headers: {
                  'Content-type': undefined,
                  'Authorization': 'Bearer ' + envio._token                
              },
              transformRequest: angular.identity
          })
              .success(function (res) {

                  deferred.resolve(res);
              })
              .error(function (msg, code) {
                  deferred.reject(msg);
              })
          return deferred.promise;
      }
}]);