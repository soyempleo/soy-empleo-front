angular
    .module('spa-SE')
    .filter('phonenumber', function () {
    return function (tel) {
        if (!tel) { return ''; }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 7: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(2);
                break;

            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
});

angular
    .module('spa-SE')
    .filter('range', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
          input.push(i);
        }

        return input;
    };
});

angular
    .module('spa-SE')
    .filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start;
            return input.slice(start);
        }
        return [];
    };
});

angular
    .module('spa-SE')
    .filter('capitalize', function() {
    return function(input, all) {
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
    }
});

angular
    .module('spa-SE')
    .filter('multipleFiltro', ['filterFilter', function (filterFilter) {
        return function (items, query) {
            if (!query) return items;
                var terms = query.split(/\s+/);
                var result = items;
                terms.forEach(function (term) {
                result = filterFilter(result,term); 
                });
            return result;
        }
}]);

angular
    .module('spa-SE')
    .filter('priceFilter', function() {
    return function( items, rangeInfo ) {
        var filtered = [];
        var min = parseInt(rangeInfo.minPrice);
        var max = parseInt(rangeInfo.maxPrice);
        angular.forEach(items, function(item) {
            if( parseInt(item.base_salary) >= min && parseInt(item.base_salary) <= max ) {
                filtered.push(item);
            }
        });
        return filtered;
    };
});

function parseDate (date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate()); 
}

function parseDate2(input) {
  var parts = input.split('-');
  return new Date(parts[2], parts[1], parts[0]); 
}


angular
    .module('spa-SE')
    .filter('fechaFilter', function() {
    return function(items, from, to) {
        var df = parseDate(from);
        var dt = parseDate(to);
        var result = [];        
        for (var i=0; i<items.length; i++){
            var tf = new Date(items[i].fecha);
            tf.setDate(tf.getDate() + 1);
            if (tf > df && tf < dt)  {
                result.push(items[i]);
            }
        }            
        return result;
    };
});

angular
    .module('spa-SE')
    .filter('orderObjectBy', function() {
        return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
          filtered.push(item);
        });
        filtered.sort(function (a, b) {
          return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
  };
});

angular
    .module('spa-SE')
    .filter('orderObjectBy2', function() {
        return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
          filtered.push(item);
        });
        filtered.sort(function (a, b) {
          return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
  };
});

angular
    .module('spa-SE')
    .filter('bothFilter',function($filter){
        return function(items,priceItems,multipleItems){
            var filtered = [];
            items = $filter('multipleFiltro')(items, multipleItems);
            filtered = $filter('priceFilter')(items,priceItems);
            return filtered;
        }
});

angular
    .module('spa-SE')
    .filter("nivel", function() {
        return function(text) {
            if (text == "1") {
                return "Básico";
            } else if (text == "2") {
                return "Intermedio";
            } {
                return "Avanzado";
            }
        };
    });

angular
    .module('spa-SE')
    .filter("nivel2", function() {
        return function(text) {
            if (text >= "1" && text <= "2") {
                return "Básico";
            } else if (text >= "3" && text <= "4") {
                return "Normal";
            }else if (text >= "5" && text <= "6") {
                return "Intermedio";
            } else if (text >= "7" && text <= "8") {
                return "Avanzado";
            }else{
                return "Experto";
            }
        };
    });


angular
    .module('spa-SE')
    .filter("sino", function() {
        return function(text) {
            if (text == "S") {
                return "Si";
            } else {
                return "No";
            }
        };
    });
