/**
 * Created by Wilder on 05-06-2015.
 */
var openMenu = true;
function hideMenu() {
    var menuWill = $("#menuWill");
    var ngViews = $("#ngViews");
    var derecha = $("#ladoDerecho");
    var fcontainer = $("#fcontainer");
    if (this.openMenu == true) {
        menuWill.removeClass("will-open");
        menuWill.addClass("will-left");
        fcontainer.removeClass("align-body");
        fcontainer.addClass("align-body-full");
        menuWill.addClass("push");
        menuWill.removeClass("content-open")
        menuWill.addClass("content-left");
        derecha.removeClass("col-xs-10-will");
        derecha.addClass("col-xs-12-will");
        this.openMenu = false;
    }
    else {

        menuWill.removeClass("will-left");
        menuWill.addClass("will-open");
        fcontainer.removeClass("align-body-full");
        fcontainer.addClass("align-body");
        menuWill.addClass("push");
        menuWill.addClass("push");
        menuWill.removeClass("content-left")
        menuWill.addClass("content-open");
        derecha.removeClass("col-xs-12-will");
        derecha.addClass("col-xs-10-will");

        this.openMenu = true;

    }
}

function getResolution() {

    var valor = $(window).width();
    var ladoIzquierdo = $("#ladoIzquierdo");
    var derecha = $("#ladoDerecho");

    console.log(valor);
    if (valor < 940) {

        var menu = $("#pushyMenu");
        var ngView = $("#ngView");
        menu.removeClass("pushy-open");
        menu.addClass("pushy-left");
        ladoIzquierdo.hide();
        derecha.removeClass("col-xs-10-will");
        derecha.addClass("col-xs-12-will");
    }
}

var primera = false;
$(document).ready(function () {
    setInterval(function () {
        var dimension = $(window).width();
        if (dimension < 940) {
            if (this.primera == false) {
                this.openMenu = true;
                hideMenu();
                primera = true;
            }
        }
    }, 400);
});