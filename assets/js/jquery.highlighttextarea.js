!function (t) {
    "use strict";
    var i = function (s, n) {
        this.settings = t.extend({}, i.DEFAULTS), this.scrollbarWidth = e.getScrollbarWidth(), this.isInput = "input" == s[0].tagName.toLowerCase(), this.active = !1, this.$el = s, this.$el.wrap('<div class="highlightTextarea"></div>'), this.$main = this.$el.parent(), this.$main.prepend('<div class="highlightTextarea-container"><div class="highlightTextarea-highlighter"></div></div>'), this.$container = this.$main.children().first(), this.$highlighter = this.$container.children(), this.setOptions(n), this.settings.id && (this.$main[0].id = this.settings.id), this.settings.resizable && this.applyResizable(), this.updateCss(), this.bindEvents(), this.highlight()
    };
    i.DEFAULTS = {
        words: {},
        ranges: {},
        color: "transparent",
        caseSensitive: !0,
        wordsOnly: !1,
        resizable: !1,
        id: "",
        debug: !1
    }, i.prototype.highlight = function () {
        var i = e.htmlEntities(this.$el.val()), s = this;
        s.spacer = "", this.settings.wordsOnly && (s.spacer = "\\b"), t.each(this.settings.words, function (t, e) {
            i = i.replace(new RegExp(s.spacer + "(" + e.join("|") + ")" + s.spacer, s.regParam), '<span style="background-color:' + t + ';">$1</span>')
        }), t.each(this.settings.ranges, function (t, s) {
            if (s.start < i.length) {
                i = e.strInsert(i, s.end, "</span>");
                var n = '<span style="background-color:' + s.color + ';"';
                null != s["class"] && (n += 'class="' + s["class"] + '"'), n += ">", i = e.strInsert(i, s.start, n)
            }
        }), this.$highlighter.html(i), this.updateSizePosition()
    }, i.prototype.setWords = function (t) {
        this.setOptions({words: t, ranges: {}})
    }, i.prototype.setRanges = function (t) {
        this.setOptions({words: {}, ranges: t})
    }, i.prototype.enable = function () {
        this.bindEvents(), this.highlight()
    }, i.prototype.disable = function () {
        this.unbindEvents(), this.$highlighter.empty()
    }, i.prototype.destroy = function () {
        this.disable(), e.cloneCss(this.$container, this.$el, ["background-image", "background-color", "background-position", "background-repeat", "background-origin", "background-clip", "background-size", "background-attachment"]), this.$main.replaceWith(this.$el), this.$el.removeData("highlighter")
    }, i.prototype.setOptions = function (i) {
        "object" != typeof i || t.isEmptyObject(i) || (t.extend(this.settings, i), this.regParam = this.settings.caseSensitive ? "gm" : "gim", t.isEmptyObject(this.settings.words) ? t.isEmptyObject(this.settings.ranges) || (this.settings.words = {}, this.settings.ranges = e.cleanRanges(this.settings.ranges, this.settings.color)) : (this.settings.words = e.cleanWords(this.settings.words, this.settings.color), this.settings.ranges = {}), this.settings.debug ? this.$main.addClass("debug") : this.$main.removeClass("debug"), this.active && this.highlight())
    }, i.prototype.bindEvents = function () {

        setInterval(function () {
            t.proxy(i.updateSizePosition, i);
            this.value = this.value, this.scrollLeft = 0, i.updateSizePosition.call(i);
        }, 600)

        if (!this.active) {
            this.active = !0;
            var i = this;
            this.$highlighter.on({
                "this.highlighter": function () {
                    i.$el.focus()
                }
            }), this.$el.on({
                "input.highlightTextarea": e.throttle(function () {
                    this.highlight()
                }, 100, this), "resize.highlightTextarea": e.throttle(function () {
                    this.updateSizePosition(!0)
                }, 50, this), "scroll.highlightTextarea select.highlightTextarea": e.throttle(function () {
                    this.updateSizePosition()
                }, 50, this)
            }), this.isInput && this.$el.on({
                "keydown.highlightTextarea keypress.highlightTextarea keyup.highlightTextarea": function () {
                    setTimeout(t.proxy(i.updateSizePosition, i), 1);
                }, "blur.highlightTextarea": function () {
                    this.value = this.value, this.scrollLeft = 0, i.updateSizePosition.call(i)
                }
            })
        }
    }, i.prototype.unbindEvents = function () {
        this.active && (this.active = !1, this.$highlighter.off(".highlightTextarea"), this.$el.off(".highlightTextarea"))
    }, i.prototype.updateCss = function () {
        e.cloneCss(this.$el, this.$main, ["float", "vertical-align"]), this.$main.css({
            width: this.$el.outerWidth(!0),
            height: this.$el.outerHeight(!0)
        }), e.cloneCss(this.$el, this.$container, ["background-image", "background-color", "background-position", "background-repeat", "background-origin", "background-clip", "background-size", "background-attachment", "padding-top", "padding-right", "padding-bottom", "padding-left"]), this.$container.css({
            top: e.toPx(this.$el.css("margin-top")) + e.toPx(this.$el.css("border-top-width")),
            left: e.toPx(this.$el.css("margin-left")) + e.toPx(this.$el.css("border-left-width")),
            width: this.$el.width(),
            height: this.$el.height()
        }), e.cloneCss(this.$el, this.$highlighter, ["font-size", "font-family", "font-style", "font-weight", "font-variant", "font-stretch", "line-height", "vertical-align", "word-spacing", "text-align", "letter-spacing", "text-rendering"]), this.$el.css({background: "none"})
    }, i.prototype.applyResizable = function () {
        jQuery.ui && this.$el.resizable({
            handles: "se", resize: e.throttle(function () {
                this.updateSizePosition(!0)
            }, 50, this)
        })
    }, i.prototype.updateSizePosition = function (t) {
        t && (this.$main.css({
            width: this.$el.outerWidth(!0),
            height: this.$el.outerHeight(!0)
        }), this.$container.css({width: this.$el.width(), height: this.$el.height()}));
        var i, e = 0;
        this.isInput ? i = 99999 : (("scroll" == this.$el.css("overflow") || "scroll" == this.$el.css("overflow-y") || "hidden" != this.$el.css("overflow") && "hidden" != this.$el.css("overflow-y") && this.$el[0].clientHeight < this.$el[0].scrollHeight) && (e = this.scrollbarWidth), i = this.$el.width() - e), this.$highlighter.css({
            width: i,
            height: this.$el.height() + this.$el.scrollTop(),
            top: -this.$el.scrollTop(),
            left: -this.$el.scrollLeft()
        })
    };
    var ajuste = function (t) {
        console.log("Paso");
        t && (this.$main.css({
            width: this.$el.outerWidth(!0),
            height: this.$el.outerHeight(!0)
        }), this.$container.css({width: this.$el.width(), height: this.$el.height()}));
        var i, e = 0;
        this.isInput ? i = 99999 : (("scroll" == this.$el.css("overflow") || "scroll" == this.$el.css("overflow-y") || "hidden" != this.$el.css("overflow") && "hidden" != this.$el.css("overflow-y") && this.$el[0].clientHeight < this.$el[0].scrollHeight) && (e = this.scrollbarWidth), i = this.$el.width() - e), this.$highlighter.css({
            width: i,
            height: this.$el.height() + this.$el.scrollTop(),
            top: -this.$el.scrollTop(),
            left: -this.$el.scrollLeft()
        })
    };

    setInterval(function () {
        t.proxy(i.updateSizePosition, i);
    }, 800);


    var e = function () {
    };
    e.getScrollbarWidth = function () {
        var i = t('<div style="width:50px;height:50px;overflow:auto"><div>&nbsp;</div></div>').appendTo("body"), e = i.children(), s = e.innerWidth() - e.height(100).innerWidth();
        return i.remove(), s
    }, e.cloneCss = function (t, i, e) {
        for (var s = 0, n = e.length; n > s; s++)i.css(e[s], t.css(e[s]))
    }, e.toPx = function (i) {
        if (i != i.replace("em", "")) {
            var e = t('<div style="font-size:1em;margin:0;padding:0;height:auto;line-height:1;border:0;">&nbsp;</div>').appendTo("body");
            return i = Math.round(parseFloat(i.replace("em", "")) * e.height()), e.remove(), i
        }
        return i != i.replace("px", "") ? parseInt(i.replace("px", "")) : parseInt(i)
    }, e.htmlEntities = function (i) {
        return i ? t("<div></div>").text(i).html() : ""
    }, e.strInsert = function (t, i, e) {
        return t.slice(0, i) + e + t.slice(i)
    }, e.throttle = function (t, i, e) {
        var s = {pid: null, last: 0};
        return function () {
            function n() {
                return s.last = (new Date).getTime(), e ? t.apply(e, Array.prototype.slice.call(h)) : t.apply(o, Array.prototype.slice.call(h))
            }

            var r = (new Date).getTime() - s.last, h = arguments, o = this;
            return r > i ? n() : (clearTimeout(s.pid), void(s.pid = setTimeout(n, i - r)))
        }
    }, e.cleanWords = function (i, s) {
        var n = {};
        t.isArray(i) || (i = [i]);
        for (var r = 0, h = i.length; h > r; r++) {
            var o = i[r];
            if (t.isPlainObject(o)) {
                n[o.color] || (n[o.color] = []), t.isArray(o.words) || (o.words = [o.words]);
                for (var a = 0, l = o.words.length; l > a; a++)n[o.color].push(e.htmlEntities(o.words[a]))
            } else n[s] || (n[s] = []), n[s].push(e.htmlEntities(o))
        }
        return n
    }, e.cleanRanges = function (i, e) {
        var s = [];
        (t.isPlainObject(i) || t.isNumeric(i[0])) && (i = [i]);
        for (var n = 0, r = i.length; r > n; n++) {
            var h = i[n];
            if (t.isArray(h))s.push({color: e, start: h[0], end: h[1]}); else if (h.ranges) {
                (t.isPlainObject(h.ranges) || t.isNumeric(h.ranges[0])) && (h.ranges = [h.ranges]);
                for (var o = 0, a = h.ranges.length; a > o; o++)t.isArray(h.ranges[o]) ? s.push({
                    color: h.color,
                    "class": h["class"],
                    start: h.ranges[o][0],
                    end: h.ranges[o][1]
                }) : (h.ranges[o].length && (h.ranges[o].end = h.ranges[o].start + h.ranges[o].length), s.push(h.ranges[o]))
            } else h.length && (h.end = h.start + h.length), s.push(h)
        }
        s.sort(function (t, i) {
            return t.start == i.start ? t.end - i.end : t.start - i.start
        });
        var l = -1;
        return t.each(s, function (i, e) {
            e.start >= e.end && t.error("Invalid range end/start"), e.start < l && t.error("Ranges overlap"), l = e.end
        }), s.reverse(), s
    }, t.fn.highlightTextarea = function (e) {
        var s = arguments;
        return this.each(function () {
            var n = t(this), r = n.data("highlighter"), h = "object" == typeof e && e;
            (r || "destroy" != e) && (r || (r = new i(n, h), n.data("highlighter", r)), "string" == typeof e && r[e].apply(r, Array.prototype.slice.call(s, 1)))
        })
    }
}(window.jQuery || window.Zepto);