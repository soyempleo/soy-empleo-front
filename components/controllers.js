var app = angular.module('spa-SE', []);

app.controller('homeCtrl', function ($scope) {
    $scope.session = Login.inactivo;
    $scope.showModal = false;
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
});
app.controller('personaCtrl', function ($scope) {
    $scope.session = Login.inactivo;
});
app.controller('contactoCtrl', function ($scope) {
    $scope.session = Login.inactivo;

    $scope.goUrl = function() {
        $location.url('persona');
    };
});
app.controller('empresaCtrl', function ($scope) {
    $scope.session = Login.inactivo;
});
app.controller('sessionCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('loginCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('hojadevidaCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('aplicacionCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('actualizaCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('buscarCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('configCtrl', function ($scope) {
    $scope.session = Login.activo;
});
app.controller('detalleCtrl', function ($scope) {
    $scope.session = Login.activo;
});

app.controller('formCtrl', function ($scope) {
    $scope.formData = {};
    $scope.formBasico = {};
    $scope.formNivel = {};
    $scope.formCurso = {};
    $scope.formEmpresa = {};
    $scope.formReferido = {};
    $scope.formPreferido = {};
    $scope.formAptitud = {};

    $scope.dynamicPopover = {
	    content: 'Hola Prueba',
	    title: 'Titulo'
	};

    $scope.patternTexto=/^[a-zA-Z]*$/;
    $scope.patternAlpha=/^[a-zA-Z0-9]*$/;
    $scope.patternFecha=/^(0[1-9]|[12][0-9]|3[01])[-](0[1-9]|1[012])[-](19|20)\d\d$/;
    $scope.patternTelefono=/^\+?\d{3}?[-]?\(?(?:\d{3})\)?[\s]?\d\d\d\d$/;

    $scope.mostrarNombre = false;
    $scope.mostrarDatos = false;
    $scope.mostrarEstudios = false;
    $scope.mostrarOtros = false;
    $scope.mostrarLaboral = false;
    $scope.mostrarSkills = false;
    $scope.mostrarPreferencias = false;

    $scope.submitForm = function (formData) {
	    $scope.mostrarNombre = true;
	    $scope.mostrarDatos = true;
	};

    $scope.submitFormBasico = function(formBasico){
        $scope.mostrarEstudios = true;
        $scope.mostrarOtros = true;
        $scope.mostrarLaboral = true;
        $scope.mostrarSkills = true;
        $scope.mostrarPreferencias = true;
    }
});