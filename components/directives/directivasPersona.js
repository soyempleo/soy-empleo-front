/**
 * Created by WilderAlejandro on 23/11/2015.
 */

/**
 * Directiva que se encarga de consumir un json listado y lo cambia por un input que tiene una funcion de autocompletado
 * en la vista se usa de la siguiente manera:
 *
 * <np-autocomplete name="nombreParaValidacion" model="nombreModelo" list="ListaQueConsume" idx="idDelElemento"
 placeholder="Texto de ayuda placeholder" tovalidate="ParaValidarElinputQueSeGenera">
 </np-autocomplete>
 *
 */
angular
    .module('spa-SE')
    .directive("npAutocomplete", function () {
    return {
        restrict: 'E',
        scope: {
            list: '=',
            ngModel: '=',
            tovalidate: '=',
            validateext: '=',
            disabled: '=',
            placeholder: '@',
            class: '@',
            idx: '@',
            name: '@'
        },
        template: '<input type="text" name="{{name}}" ng-model="ngModel" id="{{idx}}"  class="{{class}}" ng-disabled="disabled" ' +
        'ng-class="{error: tovalidate.$invalid && tovalidate.$dirty,' +
        'success: tovalidate.$valid && tovalidate.$dirty && validateext}" placeholder="{{placeholder}}" required>',
        link: function (scope, element, attrs) {
            $(document).ready(function () {
                var loop = setInterval(function () {
                    var lista = [];
                    if (scope.list) {
                        jQuery.each(scope.list, function (i, val) {
                            lista.push(stringClean(val));
                        });
                        scope.$apply(function () {
                            $("#" + attrs.idx).autocomplete({
                                source: lista,
                                select: function (ev, ui) {
                                    scope.ngModel = ui.item.label;
                                }
                            });
                        });
                        clearInterval(loop);
                    }
                }, 200);
            });
        }
    }
});


angular
    .module('spa-SE')
    .directive('npRedirect', function ($location) {
    return {
        link: function (scope, element, attrs) {
            element.on('click', function () {
                scope.$apply(function () {
                    $location.path(attrs.pRedirect);
                })
            });
        }
    }
});

angular
    .module('spa-SE')
    .directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});

angular
    .module('spa-SE')
    .directive('autoComplete', function($timeout) {
        return function(scope, iElement, iAttrs) {
                iElement.autocomplete({
                    source: scope[iAttrs.uiItems],
                    select: function() {
                        $timeout(function() {
                          iElement.trigger('input');
                        }, 0);
                    }
                });
        };
});

angular
    .module('spa-SE')
    .directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});

/*angular
.module('spa-SE')
.directive('clickAndDisable', function() {
  return {
    scope: {
      clickAndDisable: '&'
    },
    link: function(scope, iElement, iAttrs) {
      iElement.bind('click', function() {
        iElement.prop('disabled',true);
        scope.clickAndDisable().finally(function() {
          iElement.prop('disabled',false);
        })
      });
    }
  };
});*/