/*var gulp            = require('gulp'),
    cleanCSS        = require('gulp-clean-css'),
    concatCSS       = require('gulp-concat-css'),
    runSequence     = require('run-sequence'),
    connect         = require('gulp-connect'),
    cache           = require('gulp-cache'),
    gulpNgConfig    = require('gulp-ng-config');

gulp.task('concatPublic', function () {
  return  gulp.src('assets/css/styles/*.css')
  .pipe(concatCSS("main1.min.css"))
  .pipe(gulp.dest('assets/css/'));
});

gulp.task('minifyPublic', function() {
  return gulp.src('assets/css/main1.min.css')
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(gulp.dest('assets/css/'));
});

gulp.task('buildConstants', function() {
    gulp.src('soyempleoenv.json')
        .pipe(gulpNgConfig('soyempleo.env', {
            environment: 'env.local'
        }))
        .pipe(gulp.dest('public/components/constants'))
});

gulp.task('buildServices', function() {
    gulp.src('soyempleoservices.json')
        .pipe(gulpNgConfig('soyempleoservices.env'))
        .pipe(gulp.dest('public/components/constants'))
});


gulp.task('clear', function (done) {
  return cache.clearAll(done);
});

gulp.task('serve', function () {
  connect.server({
    port: 8001,
    livereload: true
  });
});

gulp.task('run', function(){
  runSequence('concatPublic','minifyPublic','clear')
});

gulp.task('change', function(){
  gulp.watch('assets/css/styles/main.css', ['run'])
});

gulp.task('default', ['run','serve','change']);
*/
var gulp = require('gulp'),
cleanCSS = require('gulp-clean-css'),
concatCSS = require('gulp-concat-css'),
runSequence = require('run-sequence'),
connect = require('gulp-connect'),
cache = require('gulp-cache');
gulp.task('concatPublic', function () {
  return  gulp.src('assets/css/styles/*.css')
  .pipe(concatCSS("main1.min.css"))
  .pipe(gulp.dest('assets/css/'));
});
gulp.task('minifyPublic', function() {
  return gulp.src('assets/css/main1.min.css')
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(gulp.dest('assets/css/'));
});
gulp.task('clear', function (done) {
  return cache.clearAll(done);
});
gulp.task('serve', function () {
  connect.server({
    port: 8001,
    livereload: true
  });
});
gulp.task('run', function(){
  runSequence('concatPublic','minifyPublic','clear')
});
gulp.task('change', function(){
  gulp.watch('assets/css/styles/main.css', ['run'])
});
gulp.task('default', ['run','serve','change']);